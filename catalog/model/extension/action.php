<?php
/**
 * Created by PhpStorm.
 * User: AtrDevue - action.php
 * Date: 01.11.15
 * Time: 04:33
 * Project: dip
 */

class ModelExtensionAction extends Model {
    public function getAction($action_id) {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "action n LEFT JOIN " . DB_PREFIX . "action_description nd ON n.action_id = nd.action_id WHERE n.action_id = '" . (int)$action_id . "' AND nd.language_id = '" . (int)$this->config->get('config_language_id') . "'");

        return $query->row;
    }

    public function getAllAction($data) {
        $sql = "SELECT * FROM " . DB_PREFIX . "action n LEFT JOIN " . DB_PREFIX . "action_description nd ON n.action_id = nd.action_id WHERE nd.language_id = '" . (int)$this->config->get('config_language_id') . "' AND n.status = '1' ORDER BY date_added DESC";

        if (isset($data['start']) && isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 10;
            }

            $sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
        }

        $query = $this->db->query($sql);

        return $query->rows;
    }

    public function getTotalAction() {
        $query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "action");

        return $query->row['total'];
    }

    public function getMenu() {
        $query = $this->db->query("SELECT n.alias, n.action_id, n.discount, nd.title FROM " . DB_PREFIX . "action n LEFT JOIN " . DB_PREFIX . "action_description nd ON n.action_id = nd.action_id WHERE nd.language_id = '" . (int)$this->config->get('config_language_id') . "' AND n.status = '1' ORDER BY date_added DESC LIMIT 3");

        return $query->rows;
    }
}
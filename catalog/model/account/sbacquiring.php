<?php
class ModelAccountsbacquiring extends Model {
	private $key;
	private $iv;

	public function getOrderStatus($order_status_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_status WHERE order_status_id = '" . (int)$order_status_id . "' AND language_id = '" . (int)$this->config->get('config_language_id') . "'");
		
		return $query->row;
	}

	public function getPaymentStatus($order_id) {
		$query = $this->db->query("SELECT `status` FROM " . DB_PREFIX . "sbacquiring WHERE num_order = '" . (int)$order_id . "' ");
		
		return $query->row;
	}

	public function yanencrypt($value, $key) {
		$key = hash('sha256', $key, true);
		$iv = mcrypt_create_iv(32, MCRYPT_RAND);
		return strtr(base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, $key, $value, MCRYPT_MODE_ECB, $iv)), '+/=', '-_,');
	}
	
	public function yandecrypt($value, $key) {
		$key = hash('sha256', $key, true);
		$iv = mcrypt_create_iv(32, MCRYPT_RAND);
		return trim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, $key, base64_decode(strtr($value, '-_,', '+/=')), MCRYPT_MODE_ECB, $iv));
	}

	public function getPayMethods() {
		
		$yu = array('sbacquiring');
    	$yu_codes = array();  
    	foreach ($yu as $yucode){if ($this->config->get($yucode.'_status')){$yu_codes[] = $yucode;}}

		return $yu_codes;
	}

	public function getPaymentType($paymentType) {
		if ($paymentType == 'PC'){
			$pt = 'sbacquiring';
		}		

		return $pt;
	}
}
?>
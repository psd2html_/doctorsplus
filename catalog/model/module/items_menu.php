<?php
/**
 * Created by PhpStorm.
 * User: AtrDevue - items_menu.php
 * Date: 16.04.15
 * Time: 10:08
 * Project: thai-style
 */
class ModelModuleItemsMenu extends Model
{
    public $table_name = 'information';

    public function getLeftMenu()
    {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . $this->table_name . " i LEFT JOIN " . DB_PREFIX . "information_description id ON (i.information_id = id.information_id) LEFT JOIN " . DB_PREFIX . "information_to_store i2s ON (i.information_id = i2s.information_id) WHERE id.language_id = '" . (int)$this->config->get('config_language_id') . "' AND i2s.store_id = '" . (int)$this->config->get('config_store_id') . "' AND i.status = '1' AND i.menu = '1' ORDER BY i.sort_order, LCASE(id.title) ASC");

        return $query->rows;
    }
}
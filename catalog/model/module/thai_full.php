<?php

/**
 * Created by PhpStorm.
 * User: AtrDevue - common.php
 * Date: 02.04.15
 * Time: 21:42
 * Project: thai-style
 */
class ModelModuleThaiFull extends Model
{
    public $table_name = 'thai_full';

    public function getListData()
    {
        $query = ' SELECT * FROM ' . DB_PREFIX . $this->table_name;

        $query = $this->db->query($query);
        $row = $query->rows;

        $out = array();
        if (!empty($row))
        {
            foreach ($row as $r)
            {
                $out[$r['alias']] = $r['value'];
            }
        }

        return $out;
    }
}
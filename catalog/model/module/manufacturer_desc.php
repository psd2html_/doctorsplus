<?php
/**
 * Created by PhpStorm.
 * User: AtrDevue - manufacturer_desc.php
 * Date: 05.04.15
 * Time: 16:00
 * Project: thai-style
 */
class ModelModuleManufacturerDesc extends Model
{
    public $table_name = 'manufacturer';

    public function getListData()
    {
        $query = ' SELECT * FROM ' . DB_PREFIX . $this->table_name;

        $query = $this->db->query($query);
        $row = $query->rows;

        $out = array();
        if (!empty($row))
        {
            foreach ($row as $r)
            {
                //$out[$r['alias']] = $r['value'];
            }
        }

        return $out;
    }
}
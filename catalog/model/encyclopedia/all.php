<?php
/**
 * Created by PhpStorm.
 * User: AtrDevue - all.php
 * Date: 15.09.15
 * Time: 22:44
 * Project: dip
 */
class ModelEncyclopediaAll extends Model {

    public function getTotalEncyclopedia() {
        $query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "news");

        return $query->row['total'];
    }

    public function getAllEncyclopedia($data, $category = false) {
        $sql = "SELECT * FROM " . DB_PREFIX . "news n LEFT JOIN " . DB_PREFIX . "news_description nd ON n.news_id = nd.news_id WHERE nd.language_id = '" . (int)$this->config->get('config_language_id') . "' AND n.status = '1'";

        if ($category && $category > 0) {
            $sql .= " AND n.nc_id = " . (int)$category . " ";
        }

        $sql .= " ORDER BY date_added DESC";

        if (isset($data['start']) && isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 10;
            }

            $sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
        }

        $query = $this->db->query($sql);

        return $query->rows;
    }

    public function getTagEncyclopedia($data, $tag) {

        $items = $this->db->query("SELECT news_id FROM " . DB_PREFIX . "news_tags_id WHERE tags_id = '" . (int)$tag . "'");

        $itar = array();
        foreach ($items->rows as $i) {
            $itar[] = $i['news_id'];
        }

        $sql = "SELECT * FROM " . DB_PREFIX . "news n LEFT JOIN " . DB_PREFIX . "news_description nd ON n.news_id = nd.news_id WHERE n.news_id IN(" . implode(",", $itar) . ") AND nd.language_id = '" . (int)$this->config->get('config_language_id') . "' AND n.status = '1'";

        $sql .= " ORDER BY date_added DESC";

        if (isset($data['start']) && isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 10;
            }

            $sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
        }

        $query = $this->db->query($sql);

        return $query->rows;
    }

    public function getTagsItem($news_id) {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "news_tags_id n LEFT JOIN " . DB_PREFIX . "news_tags nt ON n.tags_id = nt.tags_id WHERE n.news_id = '" . (int)$news_id . "' ORDER BY nt.name DESC");

        $tg = array();
        if (!empty($query->rows)) {
            foreach ($query->rows as $row) {
                //$out[$row['tags_id']] = $row;
                if (!empty($row['name'])) {
                    $tg[$row['tags_id']] = $row['name'];
                }
            }
        }

        return $tg;
    }

    public function tagInfo($tags_id) {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "news_tags WHERE tags_id = '" . (int)$tags_id . "'");

        return $query->row;
    }

    public function getAllTags() {
        $query = $this->db->query("SELECT nt.alias, nt.name FROM " . DB_PREFIX . "news_tags_id n LEFT JOIN " . DB_PREFIX . "news_tags nt ON n.tags_id = nt.tags_id GROUP BY n.tags_id ORDER BY nt.name ASC");
        return $query->rows;
    }

    public function getNews($news_id) {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "news n LEFT JOIN " . DB_PREFIX . "news_description nd ON n.news_id = nd.news_id WHERE n.news_id = '" . (int)$news_id . "' AND nd.language_id = '" . (int)$this->config->get('config_language_id') . "'");

        return $query->row;
    }

    public function getCategory() {

        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "news_category n LEFT JOIN " . DB_PREFIX . "news_category_description nd ON n.news_cat_id = nd.news_cat_id WHERE nd.language_id = '" . (int)$this->config->get('config_language_id') . "' ORDER BY sort_order DESC");
        return $query->rows;
    }

    public function getCategoryItem ($news_cat_id) {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "news_category n LEFT JOIN " . DB_PREFIX . "news_category_description nd ON n.news_cat_id = nd.news_cat_id WHERE n.news_cat_id = '" . (int)$news_cat_id . "' AND nd.language_id = '" . (int)$this->config->get('config_language_id') . "'");

        return $query->row;
    }

    public function getLatest() {
        $query = $this->db->query("SELECT n.news_id, nd.title, n.alias  FROM " . DB_PREFIX . "news n LEFT JOIN " . DB_PREFIX . "news_description nd ON n.news_id = nd.news_id WHERE nd.language_id = '" . (int)$this->config->get('config_language_id') . "' AND n.status = '1' ORDER BY date_added DESC  LIMIT 0,3");

        return $query->rows;
    }

}
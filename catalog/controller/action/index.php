<?php
/**
 * Created by PhpStorm.
 * User: AtrDevue - index.php
 * Date: 01.11.15
 * Time: 04:25
 * Project: dip
 */

class ControllerActionIndex extends Controller {
    public function index() {

        if (!empty($this->request->get['action_id'])) {

            return $this->item();

        } else {

            $this->language->load('information/action');

            $this->load->model('extension/action');
            $this->document->setTitle($this->language->get('heading_title'));

            $data['breadcrumbs'] = array();

            $data['breadcrumbs'][] = array(
                'text' 		=> 'Главная',
                'href' 		=> $this->url->link('common/home')
            );
            $data['breadcrumbs'][] = array(
                'text' 		=> 'Акции и спецпредложения',
                'href' 		=> $this->url->link('action/action')
            );

            $url = '';

            if (isset($this->request->get['page'])) {
                $url .= '&page=' . $this->request->get['page'];
            }

            if (isset($this->request->get['page'])) {
                $page = $this->request->get['page'];
            } else {
                $page = 1;
            }

            $filter_data = array(
                'page' 	=> $page,
                'limit' => 10,
                'start' => 10 * ($page - 1),
            );

            $total = $this->model_extension_action->getTotalAction();

            $pagination = new Pagination();
            $pagination->total = $total;
            $pagination->page = $page;
            $pagination->limit = 10;
            $pagination->url = $this->url->link('action/index', 'page={page}');

            $data['pagination'] = $pagination->render();

            $data['results'] = sprintf($this->language->get('text_pagination'), ($total) ? (($page - 1) * 10) + 1 : 0, ((($page - 1) * 10) > ($total - 10)) ? $total : ((($page - 1) * 10) + 10), $total, ceil($total / 10));

            $data['heading_title'] = 'Акции';
            $data['text_title'] = $this->language->get('text_title');
            $data['text_description'] = $this->language->get('text_description');
            $data['text_date'] = $this->language->get('text_date');
            $data['text_view'] = $this->language->get('text_view');

            $all_action = $this->model_extension_action->getAllAction($filter_data);

            $data['all_action'] = array();

            $this->load->model('tool/image');

            foreach ($all_action as $action) {
                $description = explode ("\n",html_entity_decode($action['short_description']));
                $data['all_action'][] = array (
                    'title' 		=> $action['title'],
                    'image_title' 	=> html_entity_decode($action['image_title']),
                    'text_mere'     => !empty($action['prod']) ? 'Перейти к товарам акции' : 'Подробнее',
                    'image'			=> $this->model_tool_image->resize($action['image'], 870, 336),
                    'discount'      => $action['discount'],
                    'description' 	=> !empty($description) ? '<p class="info">' . implode('<br>',$description) . '</p>' : '',
                    'period' 		=> strip_tags(html_entity_decode($action['period'])),
                    //'view' 			=> $this->url->link('action/index, 'action_id=' . $action['action_id']),
                    'view' 			=> 'action/' . $action['alias'],
                    'date_added' 	=> date($this->language->get('date_format_short'), strtotime($action['date_added']))
                );
            }

            $data['column_left'] = $this->load->controller('common/column_left');
            $data['column_right'] = $this->load->controller('common/column_right');
            $data['content_top'] = $this->load->controller('common/content_top');
            $data['content_bottom'] = $this->load->controller('common/content_bottom');
            $data['footer'] = $this->load->controller('common/footer');
            $data['header'] = $this->load->controller('common/header');

            if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/action/action.tpl')) {
                $this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/action/action.tpl', $data));
            } else {
                $this->response->setOutput($this->load->view('default/template/information/action_list.tpl', $data));
            }
        }
    }

    public function item() {
        //die('action');
        $this->load->model('extension/action');

        $this->language->load('information/action');

        if (!empty($this->request->get['action_id'])) {
            $action_id = $this->request->get['action_id'];
        } else {
            $action_id = 0;
        }

        $action = $this->model_extension_action->getAction($action_id);

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' 			=> 'Главная',
            'href' 			=> $this->url->link('common/home')
        );

        $data['breadcrumbs'][] = array(
            'text' => 'Акции и спецпредложения',
            'href' => $this->url->link('action/index')
        );

        if ($action) {
            $data['breadcrumbs'][] = array(
                'text' 		=> $action['title'],
                'href' 		=> 'action/' . $action['alias']  //$this->url->link('information/action/action', 'action_id=' . $action_id)
            );

            $this->document->setTitle($action['title']);
            $this->document->setDescription($action['meta_description']);

            $data['menu'] = $this->model_extension_action->getMenu();

            $data['heading_title'] = $action['title'];
            $data['action_id'] = $action['action_id'];
            $data['action_url'] = $action['url'];
            $data['description'] = html_entity_decode($action['description']);
            $data['discount'] = html_entity_decode($action['discount']);
            $data['image_title'] = $action['image_title'];
            $data['link_action'] = $this->url->link('action/index');

            $this->load->model('tool/image');
            $data['image'] = $this->model_tool_image->resize($action['image'], 870, 336);

            $this->load->model('catalog/product');
            $data['products'] = array();
            $results = $this->model_catalog_product->getProductAction($action['action_id']);

            if (!empty($results)) {

                if (!isset($this->session->data['wishlist'])) {
                    $this->session->data['wishlist'] = array();
                }
                $data['wishlist_array'] = $this->session->data['wishlist'];

                $data['item_cart'] = $this->cart->isArray();

                foreach($results as $result) {

                    if ($result['image']) {
                        $image = $this->model_tool_image->resize($result['image'], $this->config->get('config_image_product_width'), $this->config->get('config_image_product_height'));
                    } else {
                        $image = $this->model_tool_image->resize('placeholder.png', $this->config->get('config_image_product_width'), $this->config->get('config_image_product_height'));
                    }

                    if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
                        $price = number_format($result['price'], 2, '.', ' '); //$this->currency->format($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax')));
                    } else {
                        $price = false;
                    }

                    if ((float)$result['special']) {
                        $special = $this->currency->format($this->tax->calculate($result['special'], $result['tax_class_id'], $this->config->get('config_tax')));
                    } else {
                        $special = false;
                    }

                    if ($this->config->get('config_tax')) {
                        $tax = $this->currency->format((float)$result['special'] ? $result['special'] : $result['price']);
                    } else {
                        $tax = false;
                    }

                    if ($this->config->get('config_review_status')) {
                        $rating = (int)$result['rating'];
                    } else {
                        $rating = false;
                    }

                    $data['products'][] = array(
                        'product_id'  => $result['product_id'],
                        'item_cart'   => in_array($result['product_id'],$data['item_cart']) ? 1 : false,
                        'item_wishlist' => in_array($result['product_id'],$data['wishlist_array']) ? 1 : false,
                        'thumb'       => $image,
                        'name'        => $result['name'],
                        'description' => utf8_substr(strip_tags(html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8')), 0, $this->config->get('config_product_description_length')) . '..',
                        'price'       => $price,
                        'hit'         => $result['hit'],
                        'new'         => $result['new'],
                        'discounts'   => $result['discounts'],
                        'width'       => (int)$result['width'],
                        'manufacturer'=> $result['manufacturer'],
                        'special'     => $special,
                        'tax'         => $tax,
                        'minimum'     => $result['minimum'] > 0 ? $result['minimum'] : 1,
                        'rating'      => $rating,
                        'href'        => $this->url->link('product/product', 'product_id=' . $result['product_id'])
                    );
                }
            }

            $data['column_left'] = $this->load->controller('common/column_left');
            $data['column_right'] = $this->load->controller('common/column_right');
            $data['content_top'] = $this->load->controller('common/content_top');
            $data['content_bottom'] = $this->load->controller('common/content_bottom');
            $data['footer'] = $this->load->controller('common/footer');
            $data['header'] = $this->load->controller('common/header');

            if (!empty($data['products'])) {
                if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/action/product.tpl')) {
                    $this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/action/product.tpl', $data));
                } else {
                    $this->response->setOutput($this->load->view('default/template/information/action.tpl', $data));
                }
            } else {
                if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/action/item.tpl')) {
                    $this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/action/item.tpl', $data));
                } else {
                    $this->response->setOutput($this->load->view('default/template/information/action.tpl', $data));
                }
            }
        } else {
            $data['breadcrumbs'][] = array(
                'text' 		=> $this->language->get('text_error'),
                'href' 		=> $this->url->link('information/action', 'action_id=' . $action_id)
            );

            $this->document->setTitle($this->language->get('text_error'));

            $data['heading_title'] = 'Ошибка 404';
            $data['text_error'] = '';
            $data['button_continue'] = $this->language->get('button_continue');
            $data['continue'] = $this->url->link('common/home');

            $data['column_left'] = $this->load->controller('common/column_left');
            $data['column_right'] = $this->load->controller('common/column_right');
            $data['content_top'] = $this->load->controller('common/content_top');
            $data['content_bottom'] = $this->load->controller('common/content_bottom');
            $data['footer'] = $this->load->controller('common/footer');
            $data['header'] = $this->load->controller('common/header');

            if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/error/not_found.tpl')) {
                $this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/error/not_found.tpl', $data));
            } else {
                $this->response->setOutput($this->load->view('default/template/error/not_found.tpl', $data));
            }
        }

    }
}
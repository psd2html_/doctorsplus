<?php
/**
 * Created by PhpStorm.
 * User: AtrDevue - all.php
 * Date: 15.09.15
 * Time: 22:36
 * Project: dip
 */
class ControllerEncyclopediaAll extends Controller {

    public function index() {

        if (!empty($this->request->get['news_id'])) {

            return $this->item();

        }

        $this->language->load('encyclopedia/all');
        $this->load->model('encyclopedia/all');

        $this->document->setTitle($this->language->get('heading_title'));

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' 		=> 'Главная',
            'href' 		=> $this->url->link('common/home')
        );
        $data['breadcrumbs'][] = array(
            'text' 		=> $this->language->get('heading_title'),
            'href' 		=> $this->url->link('encyclopedia/all')
        );

        $category = false;

        if (!empty($this->request->get['news_cat_id'])) {

            $cat = $this->model_encyclopedia_all->getCategoryItem((int)$this->request->get['news_cat_id']);

            if (!empty($cat)) {
                $data['breadcrumbs'][] = array(
                    'text' 		=> $cat['title'],
                    'href' 		=> 'encyclopedia/' . $cat['alias']
                );
                $category = $cat['news_cat_id'];
            } else {
                return $this->errorp();
            }
        }

        $url = '';

        if (isset($this->request->get['page'])) {
            $url .= '&page=' . $this->request->get['page'];
        }

        if (isset($this->request->get['page'])) {
            $page = $this->request->get['page'];
        } else {
            $page = 1;
        }

        $filter_data = array(
            'page' 	=> $page,
            'limit' => 10,
            'start' => 10 * ($page - 1),
        );

        $total = $this->model_encyclopedia_all->getTotalEncyclopedia();
        $pagination = new Pagination();
        $pagination->total = $total;
        $pagination->page = $page;
        $pagination->limit = 10;
        $pagination->url = $this->url->link('encyclopedia/all', 'page={page}');
        $data['pagination'] = $pagination->render();

        $data['results'] = sprintf($this->language->get('text_pagination'), ($total) ? (($page - 1) * 10) + 1 : 0, ((($page - 1) * 10) > ($total - 10)) ? $total : ((($page - 1) * 10) + 10), $total, ceil($total / 10));

        $data['text_view'] = $this->language->get('text_view');

        $data['menu'] = $this->model_encyclopedia_all->getCategory();
        $data['latest'] = $this->model_encyclopedia_all->getLatest();
        $data['tags'] = $this->model_encyclopedia_all->getAllTags();

        $all_en = $this->model_encyclopedia_all->getAllEncyclopedia($filter_data, $category);

        $data['all_en'] = array();
        $this->load->model('tool/image');

        foreach ($all_en as $news) {
            $description = explode ("\n",html_entity_decode($news['meta_description']));
            $data['all_news'][] = array (
                'title' 		=> $news['title'],
                'image_title' 		=> html_entity_decode($news['image_title']),
                'image'			=> $this->model_tool_image->resize($news['image'], 350, 246),
                'description' 	=> !empty($description) ? '<p>' . implode('</p><p>',$description) . '</p>' : '',
                'tags' 			=> $this->model_encyclopedia_all->getTagsItem($news['news_id']),
                'view' 			=> 'encyclopedia/' . $news['alias'] . '.html',
                'date_added' 	=> date($this->language->get('date_format_short'), strtotime($news['date_added']))
            );
        }

        $data['column_left'] = $this->load->controller('common/column_left');
        $data['column_right'] = $this->load->controller('common/column_right');
        $data['content_top'] = $this->load->controller('common/content_top');
        $data['content_bottom'] = $this->load->controller('common/content_bottom');
        $data['footer'] = $this->load->controller('common/footer');
        $data['header'] = $this->load->controller('common/header');

        if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/encyclopedia/all.tpl')) {
            $this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/encyclopedia/all.tpl', $data));
        } else {
            $this->response->setOutput($this->load->view('default/template/information/news_list.tpl', $data));
        }
    }

    public function item() {

        $this->language->load('encyclopedia/all');
        $this->load->model('encyclopedia/all');

        if (!empty($this->request->get['news_id'])) {
            $news_id = $this->request->get['news_id'];
        } else {
            $news_id = 0;
        }

        $news = $this->model_encyclopedia_all->getNews($news_id);

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' 			=> 'Главная',
            'href' 			=> $this->url->link('common/home')
        );

        $data['breadcrumbs'][] = array(
            'text' 		=> $this->language->get('heading_title'),
            'href' 		=> $this->url->link('encyclopedia/all')
        );

        if ($news) {
            $data['breadcrumbs'][] = array(
                'text' 		=> $news['title'],
                'href' 		=> 'encyclopedia/' . $news['alias']  //$this->url->link('information/news/news', 'news_id=' . $news_id)
            );

            $this->document->setTitle($news['title']);
            $this->document->setDescription($news['meta_description']);

            $data['menu'] = $this->model_encyclopedia_all->getCategory();
            $data['latest'] = $this->model_encyclopedia_all->getLatest();
            $data['tags'] = $this->model_encyclopedia_all->getAllTags();

            $this->load->model('tool/image');
            $data['image'] = $this->model_tool_image->resize($news['image'], 350, 246);

            $data['heading_title'] = $news['title'];
            $data['news_id'] = $news['news_id'];
            $data['description'] = html_entity_decode($news['description']);
            $data['link_action'] = $this->url->link('news/action');

            $data['column_left'] = $this->load->controller('common/column_left');
            $data['column_right'] = $this->load->controller('common/column_right');
            $data['content_top'] = $this->load->controller('common/content_top');
            $data['content_bottom'] = $this->load->controller('common/content_bottom');
            $data['footer'] = $this->load->controller('common/footer');
            $data['header'] = $this->load->controller('common/header');

            if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/encyclopedia/item.tpl')) {
                $this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/encyclopedia/item.tpl', $data));
            } else {
                $this->response->setOutput($this->load->view('default/template/information/news.tpl', $data));
            }
        } else {
            return $this->errorp();
        }
    }

    public function errorp() {
        $data['breadcrumbs'][] = array(
            'text' 		=> $this->language->get('text_error'),
            'href' 		=> $this->url->link('information/news', 'news_id=' . $news_id)
        );

        $this->document->setTitle($this->language->get('text_error'));

        $data['heading_title'] = 'Ошибка 404';
        $data['text_error'] = '';
        $data['button_continue'] = $this->language->get('button_continue');
        $data['continue'] = $this->url->link('common/home');

        $data['column_left'] = $this->load->controller('common/column_left');
        $data['column_right'] = $this->load->controller('common/column_right');
        $data['content_top'] = $this->load->controller('common/content_top');
        $data['content_bottom'] = $this->load->controller('common/content_bottom');
        $data['footer'] = $this->load->controller('common/footer');
        $data['header'] = $this->load->controller('common/header');

        if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/error/not_found.tpl')) {
            $this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/error/not_found.tpl', $data));
        } else {
            $this->response->setOutput($this->load->view('default/template/error/not_found.tpl', $data));
        }
    }

    public function tag() {
        die('function tag');
    }
}
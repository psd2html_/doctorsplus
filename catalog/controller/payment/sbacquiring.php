<?php
class ControllerPaymentsbacquiring extends Controller {
	public function index() {
		$this->language->load('payment/sbacquiring');
		$this->load->model('account/sbacquiring');
		$data['instructionat'] = $this->config->get('sbacquiring_instruction_attach');
		$data['btnlater'] = $this->config->get('sbacquiring_button_later');
		$order_info = $this->model_checkout_order->getOrder($this->session->data['order_id']);
		$action = $this->url->link('account/sbacquiring/pay');
		$paymentredir = $action .
				'&paymentType=' . 'sbacquiring' .
				'&order_id='	. $order_info['order_id'] . 
				'&code='        . substr($this->model_account_sbacquiring->yanencrypt($order_info['order_id'], $this->config->get('config_encryption')), 0, 8) .
				'&first=1';

		$online_url = $this->url->link('account/sbacquiring') .
					'&order_id='	. $order_info['order_id'] . '&code=' . substr($this->model_account_sbacquiring->yanencrypt($order_info['order_id'], $this->config->get('config_encryption')), 0, 8);

	  	$data['continue'] = $this->url->link('checkout/success');

		$this->load->language('account/sbacquiring');

		if ($this->config->get('sbacquiring_fixen')) {
			if ($this->config->get('sbacquiring_fixen') == 'fix'){
			    $out_summ = $this->config->get('sbacquiring_fixen_amount');
			}
			else{
			    $out_summ = $order_info['total'] * $this->config->get('sbacquiring_fixen_amount') / 100;
			}
		}
		else{
			$out_summ = $order_info['total'];
		}

		
		if ($this->config->get('sbacquiring_createorder_or_notcreate')){
			$data['notcreate'] = 'notcreate';
		}

		$data['pay_url'] = $paymentredir;
		$data['button_confirm'] = $this->language->get('button_confirm');
		$data['payment_url'] = $this->url->link('checkout/success');
		$data['button_later'] = $this->language->get('button_pay_later');

		if ($this->config->get('sbacquiring_instruction_attach')){
			$data['text_instruction'] = $this->language->get('text_instruction');

			$instros = explode('$', ($this->config->get('sbacquiring_instruction_' . $this->config->get('config_language_id'))));
			$instroz = "";
			foreach ($instros as $instro) {
				if ($instro == 'href' || $instro == 'orderid' ||  $instro == 'itogo' || $instro == 'komis' || $instro == 'total-komis' || $instro == 'plus-komis'){
				    if ($instro == 'href'){
				        $instro_other = $online_url;
				    }
				    if ($instro == 'orderid'){
				        $instro_other = $order_info['order_id'];
					}
					if ($instro == 'itogo'){
					    $instro_other = $this->currency->format($order_info['total'], $order_info['currency_code'], $order_info['currency_value'], true);
					}
					if ($instro == 'komis'){
						if($this->config->get('sbacquiring_komis')){
					    	$instro_other = $this->config->get('sbacquiring_komis') . '%';
						}
						else{$instro_other = '';}
					}
					if ($instro == 'total-komis'){
						if($this->config->get('sbacquiring_komis')){
					    	$instro_other = $this->currency->format($order_info['total'] * $this->config->get('sbacquiring_komis')/100, $order_info['currency_code'], $order_info['currency_value'], true);
						}
						else{$instro_other = '';}
					}
					if ($instro == 'plus-komis'){
						if($this->config->get('sbacquiring_komis')){
					    	$instro_other = $this->currency->format($order_info['total'] + ($order_info['total'] * $this->config->get('sbacquiring_komis')/100), $order_info['currency_code'], $order_info['currency_value'], true);
						}
						else{$instro_other = '';}
					}
				}
				else {
				    $instro_other = nl2br(htmlspecialchars_decode($instro));
				}
				    $instroz .=  $instro_other;
			}
				$data['sbacquiringi'] = $instroz;
		}
		
		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/payment/sbacquiring.tpl')) {
            return $this->load->view($this->config->get('config_template') . '/template/payment/sbacquiring.tpl', $data);
		} else {
            return $this->load->view('default/template/payment/sbacquiring.tpl', $data);
        }
	}
	
	public function confirm() {
  		$this->language->load('payment/sbacquiring');
		$this->load->model('checkout/order');
		$this->load->model('account/sbacquiring');
			if ($this->config->get('sbacquiring_mail_instruction_attach')){
				$order_info = $this->model_checkout_order->getOrder($this->session->data['order_id']);
				$out_summ = $this->currency->format($order_info['total'], $order_info['currency_code'], $order_info['currency_value'], FALSE);
				$inv_id = $this->session->data['order_id'];
				$action= $order_info['store_url'] . 'index.php?route=account/sbacquiring';
				$online_url = $action .

				'&order_id='	. $order_info['order_id'] . '&code='        . substr($this->model_account_sbacquiring->yanencrypt($order_info['order_id'], $this->config->get('config_encryption')), 0, 8);

		    	$comment  = $this->language->get('text_instruction') . "\n\n";
		    	$instros = explode('$', ($this->config->get('sbacquiring_mail_instruction_' . $this->config->get('config_language_id'))));
				      $instroz = "";
				      foreach ($instros as $instro) {
				      	if ($instro == 'href' || $instro == 'orderid' ||  $instro == 'itogo' || $instro == 'komis' || $instro == 'total-komis' || $instro == 'plus-komis'){
				      		if ($instro == 'href'){
				            	$instro_other = $online_url;
				        	}
				            if ($instro == 'orderid'){
				            	$instro_other = $inv_id;
					       	}
					       	if ($instro == 'itogo'){
					            $instro_other = $this->currency->format($out_summ, $order_info['currency_code'], $order_info['currency_value'], true);
					       	}
					       	if ($instro == 'komis'){
								if($this->config->get('sbacquiring_komis')){
							    	$instro_other = $this->config->get('sbacquiring_komis') . '%';
								}
								else{$instro_other = '';}
							}
							if ($instro == 'total-komis'){
								if($this->config->get('sbacquiring_komis')){
							    	$instro_other = $this->currency->format($order_info['total'] * $this->config->get('sbacquiring_komis')/100, $order_info['currency_code'], $order_info['currency_value'], true);
								}
								else{$instro_other = '';}
							}
							if ($instro == 'plus-komis'){
								if($this->config->get('sbacquiring_komis')){
							    	$instro_other = $this->currency->format($order_info['total'] + ($order_info['total'] * $this->config->get('sbacquiring_komis')/100), $order_info['currency_code'], $order_info['currency_value'], true);
								}
								else{$instro_other = '';}
							}
				       	}
				       	else {
				       		$instro_other = nl2br($instro);
				       	}
				       	$instroz .=  $instro_other;
				      }
				$comment .= $instroz;
		    	$comment = htmlspecialchars_decode($comment);
		    	$this->model_checkout_order->addOrderHistory($this->session->data['order_id'], $this->config->get('sbacquiring_on_status_id'), $comment, true);
	    	}
	    	else{
				$this->model_checkout_order->addOrderHistory($this->session->data['order_id'], $this->config->get('sbacquiring_on_status_id'), '', true);
			}
	}
	
	public function confirmLater() {
		$this->load->model('checkout/order');
		$this->model_checkout_order->addOrderHistory($this->session->data['order_id'], $this->config->get('sbacquiring_on_status_id'), '', true, true);

	}
}
?>
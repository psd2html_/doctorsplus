<?php
/**
 * Created by PhpStorm.
 * User: AtrDevue - items_menu.php
 * Date: 16.04.15
 * Time: 10:00
 * Project: thai-style
 */
class ControllerModuleItemsMenu extends Controller
{
    public function index()
    {
        $this->load->language('module/items_menu');
        
        $data['faq'] = $this->url->link('information/faq');
        $data['news'] = $this->url->link('encyclopedia/all');
        $data['contact'] = $this->url->link('information/contact');
        
        $data['heading_title'] = $this->language->get('heading_title');
        $data['select_section'] = $this->language->get('select_section');
        $data['text_contact'] = $this->language->get('text_contact');
	   $data['text_faq'] = $this->language->get('text_faq');
	   

        $this->load->model('module/items_menu');

//         $data['informations'] = array();
// 
//         $this_url = 'http://' . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
// 
//         foreach ($this->model_module_items_menu->getLeftMenu() as $result) {
//             $href = $this->url->link('information/information', 'information_id=' . $result['information_id']);
//             $data['informations'][] = array(
//                 'title' => $result['title'],
//                 'active'=> strcasecmp($this_url, $href),
//                 'href'  => $href
//             );
//         }

        $this->load->model('catalog/information');

		$data['categories'] = array();
        
        $informations = $this->model_catalog_information->getInformationsMenu();

        foreach ($informations as $key=>$information) {
            $data['categories'][] = array(
                'name'     => $information['title'],
                'children' => null,
                'column'   => 1,
                'blank'    => 0,
                'href'     => $this->url->link('information/information', 'information_id=' . $information['information_id'])
            );
        }
        
        
        
//         $data['contact'] = $this->url->link('information/contact');
// 
//         $data['informations'][] = array(
//             'title' => 'Контакты',
//             'active'=> strcasecmp($this_url, $data['contact']),
//             'href'  => $data['contact']
//         );
// 
//         $data['faq'] = $this->url->link('information/faq');
// 
//         $data['informations'][] = array(
//             'title' => 'Вопрос-ответ',
//             'active'=> strcasecmp($this_url, $data['faq']),
//             'href'  => $data['faq']
//         );

        /*$data['informations'][] = array(
            'title' => $data['text_contact'],
            'active'=> strcasecmp($this_url, $data['contact']),
            'href'  => $data['contact']
        );*/

        if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/items_menu.tpl')) {
            return $this->load->view($this->config->get('config_template') . '/template/module/items_menu.tpl', $data);
        } else {
            return $this->load->view('default/template/module/items_menu.tpl', $data);
        }

    }
}
<?php
class ControllerModuleBanner extends Controller {
	public function index($setting) {
		static $module = 0;

		$this->load->model('design/banner');
		$this->load->model('tool/image');

		$this->document->addStyle('catalog/view/theme/thai-style/stylesheet/owl.carousel.css');
		$this->document->addScript('catalog/view/theme/thai-style/js/owl.carousel.min.js');

		$data['banners'] = array();
		$data['base'] = $this->config->get('config_url');

		$results = $this->model_design_banner->getBanner($setting['banner_id']);

		foreach ($results as $result) {
				$data['banners'][] = array(
					'title' => html_entity_decode($result['title']),
					'description'  => $result['description'],
					'link'  => $result['link'],
					'image' => !empty($result['image']) ? $this->model_tool_image->resize($result['image'], 870, 340) : ''
				);

		}

		$data['module'] = $module++;

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/banner.tpl')) {
			return $this->load->view($this->config->get('config_template') . '/template/module/banner.tpl', $data);
		} else {
			return $this->load->view('default/template/module/banner.tpl', $data);
		}
	}
}
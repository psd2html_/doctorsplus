<?php
class ControllerModuleManufacturerHome extends Controller {
	public function index() {

		$this->load->model('catalog/manufacturer');
		$this->load->model('tool/image');

		$manufacturer = $this->model_catalog_manufacturer->getManufacturersHome();

		$data = array();

		foreach ($manufacturer as $m) {
			$data[] = array(
					'id' => $m['manufacturer_id'],
					'name' => $m['name'],
					'image' => !empty($m['image']) ? 'image/' . $m['image'] : '',
					'href' => $this->url->link('product/manufacturer/info', 'manufacturer_id=' . $m['manufacturer_id'], 'canonical')
			);
		}

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/manufacturer_home.tpl')) {
			return $this->load->view($this->config->get('config_template') . '/template/module/manufacturer_home.tpl', $data);
		} else {
			return $this->load->view('default/template/module/category.tpl', $data);
		}
	}
}
<?php
/**
 * Created by PhpStorm.
 * User: AtrDevue - loginid.php
 * Date: 12.10.15
 * Time: 02:56
 * Project: dip
 */
class ControllerCheckoutLoginid extends Controller
{
    public function index()
    {
        // Validate cart has products and has stock.
        if ((!$this->cart->hasProducts() && empty($this->session->data['vouchers'])) || (!$this->cart->hasStock() && !$this->config->get('config_stock_checkout'))) {
            $this->response->redirect($this->url->link('checkout/cart'));
        }

        if ($this->customer->isLogged())
        {
            $this->response->redirect($this->url->link('checkout/checkout'));
        }
		
		$data['oneclick'] = $this->config->get('config_checkout_fast');
		
        $this->load->language('checkout/checkoutlog');
        $this->load->language('default');
        $this->document->setTitle($this->language->get('heading_title'));

        $data['heading_title'] = $this->language->get('heading_title');

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/home')
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_cart'),
            'href' => $this->url->link('checkout/cart')
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('checkout/loginid', '', 'SSL')
        );

        $data['fast'] = $this->url->link('checkout/fast');

        $data['order'] = $this->url->link('checkout/order');

        $data['column_left'] = $this->load->controller('common/column_left');
        $data['column_right'] = $this->load->controller('common/column_right');
        $data['content_top'] = $this->load->controller('common/content_top');
        $data['content_bottom'] = $this->load->controller('common/content_bottom');
        $data['footer'] = $this->load->controller('common/footer');
        $data['header'] = $this->load->controller('common/header');

        if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/checkout/loginid.tpl')) {
            $this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/checkout/loginid.tpl', $data));
        } else {
            $this->response->setOutput($this->load->view('default/template/checkout/login.tpl', $data));
        }
    }
}
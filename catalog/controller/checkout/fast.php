<?php

/**
 * Created by PhpStorm.
 * User: AtrDevue - fast.php
 * Date: 12.10.15
 * Time: 10:19
 * Project: dip
 */
class ControllerCheckoutFast extends Controller
{
    private $error = array();

    public function index()
    {
        // Validate cart has products and has stock.
        if ((!$this->cart->hasProducts() && empty($this->session->data['vouchers'])) || (!$this->cart->hasStock() && !$this->config->get('config_stock_checkout')))
        {
            $this->response->redirect($this->url->link('checkout/cart'));
        }

        if ($this->customer->isLogged())
        {
            $this->response->redirect($this->url->link('checkout/checkout'));
        }

        $this->load->language('checkout/fast');
        $this->load->language('default');

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate())
        {
            $order_data = $this->request->post;
            $order_data['totals'] = array();
            $total = 0;

            $taxes = $this->cart->getTaxes();

            $this->load->model('extension/extension');

            $sort_order = array();

            $results = $this->model_extension_extension->getExtensions('total');

            foreach ($results as $key => $value) {
                $sort_order[$key] = $this->config->get($value['code'] . '_sort_order');
            }

            array_multisort($sort_order, SORT_ASC, $results);

            foreach ($results as $result) {
                if ($this->config->get($result['code'] . '_status')) {
                    $this->load->model('total/' . $result['code']);

                    $this->{'model_total_' . $result['code']}->getTotal($order_data['totals'], $total, $taxes);
                }
            }

            $sort_order = array();

            foreach ($order_data['totals'] as $key => $value) {
                $sort_order[$key] = $value['sort_order'];
            }

            array_multisort($sort_order, SORT_ASC, $order_data['totals']);

            $order_data['invoice_prefix'] = $this->config->get('config_invoice_prefix');
            $order_data['store_id'] = $this->config->get('config_store_id');
            $order_data['store_name'] = $this->config->get('config_name');

            if ($order_data['store_id']) {
                $order_data['store_url'] = $this->config->get('config_url');
            } else {
                $order_data['store_url'] = HTTP_SERVER;
            }

            $this->load->model('checkout/order');

            $order_data['products'] = array();

            foreach ($this->cart->getProducts() as $product) {
                $option_data = array();

                foreach ($product['option'] as $option) {
                    $option_data[] = array(
                        'product_option_id'       => $option['product_option_id'],
                        'product_option_value_id' => $option['product_option_value_id'],
                        'option_id'               => $option['option_id'],
                        'option_value_id'         => $option['option_value_id'],
                        'name'                    => $option['name'],
                        'value'                   => $option['value'],
                        'type'                    => $option['type']
                    );
                }

                $order_data['products'][] = array(
                    'product_id' => $product['product_id'],
                    'name'       => $product['name'],
                    'model'      => $product['model'],
                    'option'     => $option_data,
                    'download'   => $product['download'],
                    'quantity'   => $product['quantity'],
                    'subtract'   => $product['subtract'],
                    'price'      => $product['price'],
                    'total'      => $product['total'],
                    'tax'        => $this->tax->getTax($product['price'], $product['tax_class_id']),
                    'reward'     => $product['reward']
                );
            }

            $order_data['total'] = $total;

            $order_data['language_id'] = $this->config->get('config_language_id');
            $order_data['currency_id'] = $this->currency->getId();
            $order_data['currency_code'] = $this->currency->getCode();
            $order_data['currency_value'] = $this->currency->getValue($this->currency->getCode());
            $order_data['ip'] = $this->request->server['REMOTE_ADDR'];

            if (!empty($this->request->server['HTTP_X_FORWARDED_FOR'])) {
                $order_data['forwarded_ip'] = $this->request->server['HTTP_X_FORWARDED_FOR'];
            } elseif (!empty($this->request->server['HTTP_CLIENT_IP'])) {
                $order_data['forwarded_ip'] = $this->request->server['HTTP_CLIENT_IP'];
            } else {
                $order_data['forwarded_ip'] = '';
            }

            if (isset($this->request->server['HTTP_USER_AGENT'])) {
                $order_data['user_agent'] = $this->request->server['HTTP_USER_AGENT'];
            } else {
                $order_data['user_agent'] = '';
            }

            if (isset($this->request->server['HTTP_ACCEPT_LANGUAGE'])) {
                $order_data['accept_language'] = $this->request->server['HTTP_ACCEPT_LANGUAGE'];
            } else {
                $order_data['accept_language'] = '';
            }

            $order_data['shipping_country'] = 'Russian Federation';
            $order_data['shipping_country_id'] = 176;
            $order_data['shipping_zone'] = 'Moscow';
            $order_data['shipping_zone_id'] = 2761;

            $order_data['shipping_method'] = 'быстрый заказ';
            $order_data['shipping_code'] = 'pickup.pickup';
            $order_data['payment_method'] = 'Оплата наличными';
            $order_data['payment_code'] = 'cheque';
			
			//var_dump($this->model_checkout_order);
			$this->session->data['order_id'] = $this->model_checkout_order->addOrder($order_data);
			$this->model_checkout_order->addOrderHistory($this->session->data['order_id'], $this->config->get('cheque_order_status_id'));//, $this->request->post['mess'], false, false);
            //$this->model_checkout_order->addOrderGuest(1,$order_data);
            //$this->model_checkout_order->addOrder($order_data);
            //$this->response->setOutput(var_dump($order_data));
            $this->response->redirect($this->url->link('checkout/success'));


        }

        //$this->load->controller('checkout/confirm/index');

        $this->document->setTitle($this->language->get('heading_title'));
        $data['heading_title'] = $this->language->get('heading_title');

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/home')
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_cart'),
            'href' => $this->url->link('checkout/cart')
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('checkout/loginid', '', 'SSL')
        );

        $data['cart_utl'] = $this->url->link('checkout/cart');

        if (isset($this->error['firstname']))
        {
            $data['error_firstname'] = $this->error['firstname'];
        }
        else
        {
            $data['error_firstname'] = '';
        }

        if (isset($this->error['lastname']))
        {
            $data['error_lastname'] = $this->error['lastname'];
        }
        else
        {
            $data['error_lastname'] = '';
        }

        if (isset($this->error['email']))
        {
            $data['error_email'] = $this->error['email'];
        }
        else
        {
            $data['error_email'] = '';
        }

        if (isset($this->error['telephone']))
        {
            $data['error_telephone'] = $this->error['telephone'];
        }
        else
        {
            $data['error_telephone'] = '';
        }

        if (isset($this->error['comment']))
        {
            $data['error_comment'] = $this->error['comment'];
        }
        else
        {
            $data['error_comment'] = '';
        }

        // data
        if (isset($this->request->post['firstname']))
        {
            $data['firstname'] = $this->request->post['firstname'];
        }
        else
        {
            $data['firstname'] = '';
        }

        if (isset($this->request->post['lastname']))
        {
            $data['lastname'] = $this->request->post['lastname'];
        }
        else
        {
            $data['lastname'] = '';
        }

        if (isset($this->request->post['email']))
        {
            $data['email'] = $this->request->post['email'];
        }
        else
        {
            $data['email'] = '';
        }

        if (isset($this->request->post['telephone']))
        {
            $data['telephone'] = $this->request->post['telephone'];
        }
        else
        {
            $data['telephone'] = '';
        }

        if (isset($this->request->post['comment']))
        {
            $data['comment'] = $this->request->post['comment'];
        }
        else
        {
            $data['comment'] = '';
        }

        if (isset($this->request->post['subscribe']))
        {
            $data['subscribe'] = 1;
        }
        else
        {
            $data['subscribe'] = 0;
        }

        $this->load->model('tool/image');
        $this->load->model('tool/upload');

        $data['products'] = array();

        $products = $this->cart->getProducts();

        foreach ($products as $product)
        {
            $product_total = 0;

            foreach ($products as $product_2)
            {
                if ($product_2['product_id'] == $product['product_id'])
                {
                    $product_total += $product_2['quantity'];
                }
            }

            if ($product['minimum'] > $product_total)
            {
                $data['error_warning'] = sprintf($this->language->get('error_minimum'), $product['name'], $product['minimum']);
            }

            if ($product['image'])
            {
                $image = $this->model_tool_image->resize($product['image'], 227, 218);
            }
            else
            {
                $image = '';
            }

            $option_data = array();

            foreach ($product['option'] as $option)
            {
                if ($option['type'] != 'file')
                {
                    $value = $option['value'];
                }
                else
                {
                    $upload_info = $this->model_tool_upload->getUploadByCode($option['value']);

                    if ($upload_info)
                    {
                        $value = $upload_info['name'];
                    }
                    else
                    {
                        $value = '';
                    }
                }

                $option_data[] = array(
                    'name' => $option['name'],
                    'value' => (utf8_strlen($value) > 20 ? utf8_substr($value, 0, 20) . '..' : $value)
                );
            }

            // Display prices
            if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price'))
            {
                $price = number_format($this->tax->calculate($product['price'], $product['tax_class_id'], $this->config->get('config_tax')), 2, '.', ' '); //$this->currency->format($this->tax->calculate($product['price'], $product['tax_class_id'], $this->config->get('config_tax')));
            }
            else
            {
                $price = false;
            }

            // Display prices
            if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price'))
            {
                $total = number_format($this->tax->calculate($product['price'], $product['tax_class_id'], $this->config->get('config_tax')) * $product['quantity'], 2, '.', ' '); //$this->currency->format($this->tax->calculate($product['price'], $product['tax_class_id'], $this->config->get('config_tax')) * $product['quantity']);
            }
            else
            {
                $total = false;
            }

            $recurring = '';

            if ($product['recurring'])
            {
                $frequencies = array(
                    'day' => $this->language->get('text_day'),
                    'week' => $this->language->get('text_week'),
                    'semi_month' => $this->language->get('text_semi_month'),
                    'month' => $this->language->get('text_month'),
                    'year' => $this->language->get('text_year'),
                );

                if ($product['recurring']['trial'])
                {
                    $recurring = sprintf($this->language->get('text_trial_description'), $this->currency->format($this->tax->calculate($product['recurring']['trial_price'] * $product['quantity'], $product['tax_class_id'], $this->config->get('config_tax'))), $product['recurring']['trial_cycle'], $frequencies[$product['recurring']['trial_frequency']], $product['recurring']['trial_duration']) . ' ';
                }

                if ($product['recurring']['duration'])
                {
                    $recurring .= sprintf($this->language->get('text_payment_description'), $this->currency->format($this->tax->calculate($product['recurring']['price'] * $product['quantity'], $product['tax_class_id'], $this->config->get('config_tax'))), $product['recurring']['cycle'], $frequencies[$product['recurring']['frequency']], $product['recurring']['duration']);
                }
                else
                {
                    $recurring .= sprintf($this->language->get('text_payment_cancel'), $this->currency->format($this->tax->calculate($product['recurring']['price'] * $product['quantity'], $product['tax_class_id'], $this->config->get('config_tax'))), $product['recurring']['cycle'], $frequencies[$product['recurring']['frequency']], $product['recurring']['duration']);
                }
            }

            $data['products'][] = array(
                'key' => $product['key'],
                'thumb' => $image,
                'name' => $product['name'],
                'model' => $product['model'],
                'option' => $option_data,
                'recurring' => $recurring,
                'quantity' => $product['quantity'],
                'stock' => $product['stock'] ? true : !(!$this->config->get('config_stock_checkout') || $this->config->get('config_stock_warning')),
                'reward' => ($product['reward'] ? sprintf($this->language->get('text_points'), $product['reward']) : ''),
                'price' => $price,
                'total' => $total,
                'href' => $this->url->link('product/product', 'product_id=' . $product['product_id'])
            );
        }


        // Totals
        $this->load->model('extension/extension');

        $total_data = array();
        $data['total_item'] = 0;
        $total = 0;
        $taxes = $this->cart->getTaxes();

        // Display prices
        if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price'))
        {
            $sort_order = array();

            $results = $this->model_extension_extension->getExtensions('total');

            foreach ($results as $key => $value)
            {
                $sort_order[$key] = $this->config->get($value['code'] . '_sort_order');
            }

            array_multisort($sort_order, SORT_ASC, $results);

            foreach ($results as $result)
            {
                if ($this->config->get($result['code'] . '_status'))
                {
                    $this->load->model('total/' . $result['code']);

                    $this->{'model_total_' . $result['code']}->getTotal($total_data, $total, $taxes);
                }
            }

            $sort_order = array();

            foreach ($total_data as $key => $value)
            {
                if ($key == 'sub_total')
                {
                    $data['total_item'] = number_format($value['value'], 2, '.', ' ');
                }
                $sort_order[$key] = $value['sort_order'];
            }

            array_multisort($sort_order, SORT_ASC, $total_data);
        }


        $data['column_left'] = $this->load->controller('common/column_left');
        $data['column_right'] = $this->load->controller('common/column_right');
        $data['content_top'] = $this->load->controller('common/content_top');
        $data['content_bottom'] = $this->load->controller('common/content_bottom');
        $data['footer'] = $this->load->controller('common/footer');
        $data['header'] = $this->load->controller('common/header');

        if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/checkout/fast.tpl'))
        {
            $this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/checkout/fast.tpl', $data));
        }
        else
        {
            $this->response->setOutput($this->load->view('default/template/checkout/checkout.tpl', $data));
        }
    }

    public function validate()
    {
        $this->load->language('checkout/checkout');

        if ((utf8_strlen(trim($this->request->post['firstname'])) < 1) || (utf8_strlen(trim($this->request->post['firstname'])) > 32))
        {
            $this->error['firstname'] = $this->language->get('error_firstname');
        }

        if ((utf8_strlen(trim($this->request->post['lastname'])) < 1) || (utf8_strlen(trim($this->request->post['lastname'])) > 32))
        {
            $this->error['lastname'] = $this->language->get('error_lastname');
        }

        if ((utf8_strlen($this->request->post['email']) > 96) || !preg_match('/^[^\@]+@.*.[a-z]{2,15}$/i', $this->request->post['email']))
        {
            $this->error['email'] = $this->language->get('error_email');
        }

        $sar = array('+', '(', ')', ' ', '-');
        $phone_temp = str_replace($sar, '', $this->request->post['telephone']);

        if (utf8_strlen($phone_temp) != 11 || !is_numeric($phone_temp))
        {
            $this->error['telephone'] = $this->language->get('error_telephone');
        }
/*
        if ((utf8_strlen(trim($this->request->post['comment'])) < 3) || (utf8_strlen(trim($this->request->post['comment'])) > 500))
        {
            $this->error['comment'] = $this->language->get('error_comment');
        }
*/
        return !$this->error;

    }
}
<?php
class ControllerCheckoutLittlecart extends Controller {
	private $error = array();

	public function index() {
		// Validate cart has products and has stock.
		if ((!$this->cart->hasProducts() && empty($this->session->data['vouchers'])) || (!$this->cart->hasStock() && !$this->config->get('config_stock_checkout'))) {
			$this->response->redirect($this->url->link('checkout/cart'));
		}

		// Validate minimum quantity requirements.
		$products = $this->cart->getProducts();

		foreach ($products as $product) {
			$product_total = 0;

			foreach ($products as $product_2) {
				if ($product_2['product_id'] == $product['product_id']) {
					$product_total += $product_2['quantity'];
				}
			}

			if ($product['minimum'] > $product_total) {
				$this->response->redirect($this->url->link('checkout/cart'));
			}
		}

		$this->load->language('checkout/checkout');
        $this->load->language('default');

		$data['post_url'] = $this->url->link('checkout/checkout', '', 'SSL');

		if (isset($this->session->data['error'])) {
			$data['error_warning'] = $this->session->data['error'];
			unset($this->session->data['error']);
		} else {
			$data['error_warning'] = '';
		}

		$data['logged'] = $this->customer->isLogged();

		if (isset($this->session->data['account'])) {
			$data['account'] = $this->session->data['account'];
		} else {
			$data['account'] = '';
		}

		$data['shipping_required'] = $this->cart->hasShipping();

		$data['cart_utl'] = $this->url->link('checkout/cart');

		$this->load->model('tool/image');
		$this->load->model('tool/upload');

		$data['products'] = array();
		$products = $this->cart->getProducts();

		foreach ($products as $product)
		{
			$product_total = 0;

			foreach ($products as $product_2)
			{
				if ($product_2['product_id'] == $product['product_id'])
				{
					$product_total += $product_2['quantity'];
				}
			}

			if ($product['minimum'] > $product_total)
			{
				$data['error_warning'] = sprintf($this->language->get('error_minimum'), $product['name'], $product['minimum']);
			}

			if ($product['image'])
			{
				$image = $this->model_tool_image->resize($product['image'], 227, 218);
			}
			else
			{
				$image = '';
			}

			$option_data = array();

			foreach ($product['option'] as $option)
			{
				if ($option['type'] != 'file')
				{
					$value = $option['value'];
				}
				else
				{
					$upload_info = $this->model_tool_upload->getUploadByCode($option['value']);

					if ($upload_info)
					{
						$value = $upload_info['name'];
					}
					else
					{
						$value = '';
					}
				}

				$option_data[] = array(
					'name' => $option['name'],
					'value' => (utf8_strlen($value) > 20 ? utf8_substr($value, 0, 20) . '..' : $value)
				);
			}

			// Display prices
			if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price'))
			{
				$price = number_format($this->tax->calculate($product['price'], $product['tax_class_id'], $this->config->get('config_tax')), 2, '.', ' '); //$this->currency->format($this->tax->calculate($product['price'], $product['tax_class_id'], $this->config->get('config_tax')));
			}
			else
			{
				$price = false;
			}

			// Display prices
			if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price'))
			{
				$total = number_format($this->tax->calculate($product['price'], $product['tax_class_id'], $this->config->get('config_tax')) * $product['quantity'], 2, '.', ' '); //$this->currency->format($this->tax->calculate($product['price'], $product['tax_class_id'], $this->config->get('config_tax')) * $product['quantity']);
			}
			else
			{
				$total = false;
			}

			$recurring = '';

			if ($product['recurring'])
			{
				$frequencies = array(
					'day' => $this->language->get('text_day'),
					'week' => $this->language->get('text_week'),
					'semi_month' => $this->language->get('text_semi_month'),
					'month' => $this->language->get('text_month'),
					'year' => $this->language->get('text_year'),
				);

				if ($product['recurring']['trial'])
				{
					$recurring = sprintf($this->language->get('text_trial_description'), $this->currency->format($this->tax->calculate($product['recurring']['trial_price'] * $product['quantity'], $product['tax_class_id'], $this->config->get('config_tax'))), $product['recurring']['trial_cycle'], $frequencies[$product['recurring']['trial_frequency']], $product['recurring']['trial_duration']) . ' ';
				}

				if ($product['recurring']['duration'])
				{
					$recurring .= sprintf($this->language->get('text_payment_description'), $this->currency->format($this->tax->calculate($product['recurring']['price'] * $product['quantity'], $product['tax_class_id'], $this->config->get('config_tax'))), $product['recurring']['cycle'], $frequencies[$product['recurring']['frequency']], $product['recurring']['duration']);
				}
				else
				{
					$recurring .= sprintf($this->language->get('text_payment_cancel'), $this->currency->format($this->tax->calculate($product['recurring']['price'] * $product['quantity'], $product['tax_class_id'], $this->config->get('config_tax'))), $product['recurring']['cycle'], $frequencies[$product['recurring']['frequency']], $product['recurring']['duration']);
				}
			}

			$data['products'][] = array(
				'key' => $product['key'],
				'thumb' => $image,
				'name' => $product['name'],
				'model' => $product['model'],
				'option' => $option_data,
				'recurring' => $recurring,
				'quantity' => $product['quantity'],
				'stock' => $product['stock'] ? true : !(!$this->config->get('config_stock_checkout') || $this->config->get('config_stock_warning')),
				'reward' => ($product['reward'] ? sprintf($this->language->get('text_points'), $product['reward']) : ''),
				'price' => $price,
				'total' => $total,
				'href' => $this->url->link('product/product', 'product_id=' . $product['product_id'])
			);
		}


		// Totals
		$this->load->model('extension/extension');

		$total_data = array();
		$data['total_item'] = 0;
		$total = 0;
		$taxes = $this->cart->getTaxes();

		// Display prices
		if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price'))
		{
			$sort_order = array();

			$results = $this->model_extension_extension->getExtensions('total');

			foreach ($results as $key => $value)
			{
				$sort_order[$key] = $this->config->get($value['code'] . '_sort_order');
			}

			array_multisort($sort_order, SORT_ASC, $results);

			foreach ($results as $result)
			{
				if ($this->config->get($result['code'] . '_status'))
				{
					$this->load->model('total/' . $result['code']);

					$this->{'model_total_' . $result['code']}->getTotal($total_data, $total, $taxes);
				}
			}

			$sort_order = array();

			foreach ($total_data as $key => $value)
			{
				if ($key == 'sub_total')
				{
					$data['total_item'] = number_format($value['value'], 2, '.', ' ');
				}
				$sort_order[$key] = $value['sort_order'];
			}

			array_multisort($sort_order, SORT_ASC, $total_data);
		}
		$data['totals'] = $total_data;

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/checkout/littlecart.tpl')) {
			$this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/checkout/littlecart.tpl', $data));
		} else {
			$this->response->setOutput($this->load->view('default/template/checkout/littlecart.tpl', $data));
		}
	}
}
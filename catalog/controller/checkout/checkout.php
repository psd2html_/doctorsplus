<?php
class ControllerCheckoutCheckout extends Controller {
	private $error = array();

	public function index() {
		// Validate cart has products and has stock.
		if ((!$this->cart->hasProducts() && empty($this->session->data['vouchers'])) || (!$this->cart->hasStock() && !$this->config->get('config_stock_checkout'))) {
			$this->response->redirect($this->url->link('checkout/cart'));
		}

		if ($this->customer->isLogged() == false)
		{
			$this->response->redirect($this->url->link('checkout/loginid'));
		}

		// Validate minimum quantity requirements.
		$products = $this->cart->getProducts();

		foreach ($products as $product) {
			$product_total = 0;

			foreach ($products as $product_2) {
				if ($product_2['product_id'] == $product['product_id']) {
					$product_total += $product_2['quantity'];
				}
			}

			if ($product['minimum'] > $product_total) {
				$this->response->redirect($this->url->link('checkout/cart'));
			}
		}

		$this->load->language('checkout/checkout');
        $this->load->language('default');

		$this->document->setTitle($this->language->get('heading_title'));

        //$this->document->addStyle('catalog/view/javascript/bootstrap/css/bootstrap.css');
		$this->document->addScript('catalog/view/javascript/jquery/datetimepicker/moment.js');
		$this->document->addScript('catalog/view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.js');
		//$this->document->addStyle('catalog/view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.css');

		// Required by klarna
		if ($this->config->get('klarna_account') || $this->config->get('klarna_invoice')) {
			$this->document->addScript('http://cdn.klarna.com/public/kitt/toc/v1.0/js/klarna.terms.min.js');
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_cart'),
			'href' => $this->url->link('checkout/cart')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('checkout/checkout', '', 'SSL')
		);

		$data['post_url'] = $this->url->link('checkout/checkout', '', 'SSL');

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_checkout_option'] = $this->language->get('text_checkout_option');
		$data['text_checkout_account'] = $this->language->get('text_checkout_account');
		$data['text_checkout_payment_address'] = $this->language->get('text_checkout_payment_address');
		$data['text_checkout_shipping_address'] = $this->language->get('text_checkout_shipping_address');
		$data['text_checkout_shipping_method'] = $this->language->get('text_checkout_shipping_method');
		$data['text_checkout_payment_method'] = $this->language->get('text_checkout_payment_method');
		$data['text_checkout_confirm'] = $this->language->get('text_checkout_confirm');

		if (isset($this->session->data['error'])) {
			$data['error_warning'] = $this->session->data['error'];
			unset($this->session->data['error']);
		} else {
			$data['error_warning'] = '';
		}

		$data['logged'] = $this->customer->isLogged();

		if (isset($this->session->data['account'])) {
			$data['account'] = $this->session->data['account'];
		} else {
			$data['account'] = '';
		}

		$data['shipping_required'] = $this->cart->hasShipping();

		$data['cart_utl'] = $this->url->link('checkout/cart');

		$this->load->model('tool/image');
		$this->load->model('tool/upload');

		$data['products'] = array();
		$products = $this->cart->getProducts();

		foreach ($products as $product)
		{
			$product_total = 0;

			foreach ($products as $product_2)
			{
				if ($product_2['product_id'] == $product['product_id'])
				{
					$product_total += $product_2['quantity'];
				}
			}

			if ($product['minimum'] > $product_total)
			{
				$data['error_warning'] = sprintf($this->language->get('error_minimum'), $product['name'], $product['minimum']);
			}

			if ($product['image'])
			{
				$image = $this->model_tool_image->resize($product['image'], 227, 218);
			}
			else
			{
				$image = '';
			}

			$option_data = array();

			foreach ($product['option'] as $option)
			{
				if ($option['type'] != 'file')
				{
					$value = $option['value'];
				}
				else
				{
					$upload_info = $this->model_tool_upload->getUploadByCode($option['value']);

					if ($upload_info)
					{
						$value = $upload_info['name'];
					}
					else
					{
						$value = '';
					}
				}

				$option_data[] = array(
					'name' => $option['name'],
					'value' => (utf8_strlen($value) > 20 ? utf8_substr($value, 0, 20) . '..' : $value)
				);
			}

			// Display prices
			if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price'))
			{
				$price = number_format($this->tax->calculate($product['price'], $product['tax_class_id'], $this->config->get('config_tax')), 2, '.', ' '); //$this->currency->format($this->tax->calculate($product['price'], $product['tax_class_id'], $this->config->get('config_tax')));
			}
			else
			{
				$price = false;
			}

			// Display prices
			if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price'))
			{
				$total = number_format($this->tax->calculate($product['price'], $product['tax_class_id'], $this->config->get('config_tax')) * $product['quantity'], 2, '.', ' '); //$this->currency->format($this->tax->calculate($product['price'], $product['tax_class_id'], $this->config->get('config_tax')) * $product['quantity']);
			}
			else
			{
				$total = false;
			}

			$recurring = '';

			if ($product['recurring'])
			{
				$frequencies = array(
					'day' => $this->language->get('text_day'),
					'week' => $this->language->get('text_week'),
					'semi_month' => $this->language->get('text_semi_month'),
					'month' => $this->language->get('text_month'),
					'year' => $this->language->get('text_year'),
				);

				if ($product['recurring']['trial'])
				{
					$recurring = sprintf($this->language->get('text_trial_description'), $this->currency->format($this->tax->calculate($product['recurring']['trial_price'] * $product['quantity'], $product['tax_class_id'], $this->config->get('config_tax'))), $product['recurring']['trial_cycle'], $frequencies[$product['recurring']['trial_frequency']], $product['recurring']['trial_duration']) . ' ';
				}

				if ($product['recurring']['duration'])
				{
					$recurring .= sprintf($this->language->get('text_payment_description'), $this->currency->format($this->tax->calculate($product['recurring']['price'] * $product['quantity'], $product['tax_class_id'], $this->config->get('config_tax'))), $product['recurring']['cycle'], $frequencies[$product['recurring']['frequency']], $product['recurring']['duration']);
				}
				else
				{
					$recurring .= sprintf($this->language->get('text_payment_cancel'), $this->currency->format($this->tax->calculate($product['recurring']['price'] * $product['quantity'], $product['tax_class_id'], $this->config->get('config_tax'))), $product['recurring']['cycle'], $frequencies[$product['recurring']['frequency']], $product['recurring']['duration']);
				}
			}

			$data['products'][] = array(
				'key' => $product['key'],
				'thumb' => $image,
				'name' => $product['name'],
				'model' => $product['model'],
				'option' => $option_data,
				'recurring' => $recurring,
				'quantity' => $product['quantity'],
				'stock' => $product['stock'] ? true : !(!$this->config->get('config_stock_checkout') || $this->config->get('config_stock_warning')),
				'reward' => ($product['reward'] ? sprintf($this->language->get('text_points'), $product['reward']) : ''),
				'price' => $price,
				'total' => $total,
				'href' => $this->url->link('product/product', 'product_id=' . $product['product_id'])
			);
		}


		// Totals
		$this->load->model('extension/extension');

		$total_data = array();
		$data['total_item'] = 0;
		$total = 0;
		$taxes = $this->cart->getTaxes();

		// Display prices
		if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price'))
		{
			$sort_order = array();

			$results = $this->model_extension_extension->getExtensions('total');

			foreach ($results as $key => $value)
			{
				$sort_order[$key] = $this->config->get($value['code'] . '_sort_order');
			}

			array_multisort($sort_order, SORT_ASC, $results);

			foreach ($results as $result)
			{
				if ($this->config->get($result['code'] . '_status'))
				{
					$this->load->model('total/' . $result['code']);

					$this->{'model_total_' . $result['code']}->getTotal($total_data, $total, $taxes);
				}
			}

			$sort_order = array();

			foreach ($total_data as $key => $value)
			{
				if ($key == 'sub_total')
				{
					$data['total_item'] = number_format($value['value'], 2, '.', ' ');
				}
				$sort_order[$key] = $value['sort_order'];
			}

			array_multisort($sort_order, SORT_ASC, $total_data);
		}
		$data['totals'] = $total_data;

		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/checkout/checkout.tpl')) {
			$this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/checkout/checkout.tpl', $data));
		} else {
			$this->response->setOutput($this->load->view('default/template/checkout/checkout.tpl', $data));
		}
	}

	public function country() {
		$json = array();

		$this->load->model('localisation/country');

		$country_info = $this->model_localisation_country->getCountry($this->request->get['country_id']);

		if ($country_info) {
			$this->load->model('localisation/zone');

			$json = array(
				'country_id'        => $country_info['country_id'],
				'name'              => $country_info['name'],
				'iso_code_2'        => $country_info['iso_code_2'],
				'iso_code_3'        => $country_info['iso_code_3'],
				'address_format'    => $country_info['address_format'],
				'postcode_required' => $country_info['postcode_required'],
				'zone'              => $this->model_localisation_zone->getZonesByCountryId($this->request->get['country_id']),
				'status'            => $country_info['status']
			);
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function customfield() {
		$json = array();

		$this->load->model('account/custom_field');

		// Customer Group
		if (isset($this->request->get['customer_group_id']) && is_array($this->config->get('config_customer_group_display')) && in_array($this->request->get['customer_group_id'], $this->config->get('config_customer_group_display'))) {
			$customer_group_id = $this->request->get['customer_group_id'];
		} else {
			$customer_group_id = $this->config->get('config_customer_group_id');
		}

		$custom_fields = $this->model_account_custom_field->getCustomFields($customer_group_id);

		foreach ($custom_fields as $custom_field) {
			$json[] = array(
				'custom_field_id' => $custom_field['custom_field_id'],
				'required'        => $custom_field['required']
			);
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function save() {

		$out = array();
		if ((!$this->cart->hasProducts() && empty($this->session->data['vouchers'])) || (!$this->cart->hasStock() && !$this->config->get('config_stock_checkout')))
		{
			$out['error']['all'] = 'Корзина пустая';
		}

		$this->load->language('default');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			/*$order_data = $this->request->post;
			$order_data['totals'] = array();
			$total = 0;

			$taxes = $this->cart->getTaxes();

			$this->load->model('extension/extension');

			$sort_order = array();

			$results = $this->model_extension_extension->getExtensions('total');

			foreach ($results as $key => $value) {
				$sort_order[$key] = $this->config->get($value['code'] . '_sort_order');
			}

			array_multisort($sort_order, SORT_ASC, $results);

			foreach ($results as $result) {
				if ($this->config->get($result['code'] . '_status')) {
					$this->load->model('total/' . $result['code']);

					$this->{'model_total_' . $result['code']}->getTotal($order_data['totals'], $total, $taxes);
				}
			}

			$sort_order = array();

			foreach ($order_data['totals'] as $key => $value) {
				$sort_order[$key] = $value['sort_order'];
			}

			array_multisort($sort_order, SORT_ASC, $order_data['totals']);

			$order_data['invoice_prefix'] = $this->config->get('config_invoice_prefix');
			$order_data['store_id'] = $this->config->get('config_store_id');
			$order_data['store_name'] = $this->config->get('config_name');

			if ($order_data['store_id']) {
				$order_data['store_url'] = $this->config->get('config_url');
			} else {
				$order_data['store_url'] = HTTP_SERVER;
			}

			$this->load->model('checkout/order');

			$order_data['products'] = array();

			foreach ($this->cart->getProducts() as $product) {
				$option_data = array();

				foreach ($product['option'] as $option) {
					$option_data[] = array(
						'product_option_id'       => $option['product_option_id'],
						'product_option_value_id' => $option['product_option_value_id'],
						'option_id'               => $option['option_id'],
						'option_value_id'         => $option['option_value_id'],
						'name'                    => $option['name'],
						'value'                   => $option['value'],
						'type'                    => $option['type']
					);
				}

				$order_data['products'][] = array(
					'product_id' => $product['product_id'],
					'name'       => $product['name'],
					'model'      => $product['model'],
					'option'     => $option_data,
					'download'   => $product['download'],
					'quantity'   => $product['quantity'],
					'subtract'   => $product['subtract'],
					'price'      => $product['price'],
					'total'      => $product['total'],
					'tax'        => $this->tax->getTax($product['price'], $product['tax_class_id']),
					'reward'     => $product['reward']
				);
			}

			$order_data['total'] = $total;

			$order_data['language_id'] = $this->config->get('config_language_id');
			$order_data['currency_id'] = $this->currency->getId();
			$order_data['currency_code'] = $this->currency->getCode();
			$order_data['currency_value'] = $this->currency->getValue($this->currency->getCode());
			$order_data['ip'] = $this->request->server['REMOTE_ADDR'];

			if (!empty($this->request->server['HTTP_X_FORWARDED_FOR'])) {
				$order_data['forwarded_ip'] = $this->request->server['HTTP_X_FORWARDED_FOR'];
			} elseif (!empty($this->request->server['HTTP_CLIENT_IP'])) {
				$order_data['forwarded_ip'] = $this->request->server['HTTP_CLIENT_IP'];
			} else {
				$order_data['forwarded_ip'] = '';
			}

			if (isset($this->request->server['HTTP_USER_AGENT'])) {
				$order_data['user_agent'] = $this->request->server['HTTP_USER_AGENT'];
			} else {
				$order_data['user_agent'] = '';
			}

			if (isset($this->request->server['HTTP_ACCEPT_LANGUAGE'])) {
				$order_data['accept_language'] = $this->request->server['HTTP_ACCEPT_LANGUAGE'];
			} else {
				$order_data['accept_language'] = '';
			}

			$order_data['shipping_country'] = 'Russian Federation';
			$order_data['shipping_country_id'] = 176;
			$order_data['shipping_zone'] = 'Moscow';
			$order_data['shipping_zone_id'] = 2761;*/

			$this->load->model('checkout/order');


			$this->model_checkout_order->addOrderHistory($this->session->data['order_id'], $this->config->get('cod_order_status_id'),$this->request->post['mess']);

			$out['redirect'] = $this->url->link('checkout/success');

		} else {
			$out['error'] = $this->error;
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($out));
	}

	public function address() {
		$json = array();

		if ($this->request->post['shipping_address'] == 'existing'){
			$this->load->model('account/address');

			if (empty($this->request->post['address_id'])) {
				$json['error']['warning'] = 'Выберите ваш адрес';
			} elseif (!in_array($this->request->post['address_id'], array_keys($this->model_account_address->getAddresses()))) {
				$json['error']['warning'] = 'Выберите ваш адрес';
			}

			if (!$json) {
				// Default Shipping Address
				$this->load->model('account/address');

				$this->session->data['shipping_address'] = $this->model_account_address->getAddress($this->request->post['address_id']);
				$json['address'] = 'ok';
			}

		}elseif ($this->request->post['shipping_address'] == 'new') {
			if ((utf8_strlen(trim($this->request->post['city'])) < 2) || (utf8_strlen(trim($this->request->post['city'])) > 100))
			{
				$json['error']['city'] = 'Населённый пункт должен содержать от 2 до 100 символов!';
			}

			if ((utf8_strlen(trim($this->request->post['address_1'])) < 2) || (utf8_strlen(trim($this->request->post['address_1'])) > 255))
			{
				$json['error']['address_1'] = 'Название улицы должено содержать от 2 до 255 символов!';
			}

			if ((utf8_strlen(trim($this->request->post['haus'])) < 1) || (utf8_strlen(trim($this->request->post['haus'])) > 10))
			{
				$json['error']['haus'] = 'Номер дома должен быть от 1 до 10 символов!';
			}

			if (empty($json)) {
				$this->load->model('account/address');
				$data = array();

				$data['firstname'] = $this->customer->getFirstName();
				$data['lastname'] = $this->customer->getLastName();
				$data['company'] = '';
				$data['address_1'] = $this->request->post['address_1'];
				$data['address_2'] = '';
				$data['house'] = $this->request->post['haus'];
				$data['housing'] = $this->request->post['korp'];
				$data['apartment'] = $this->request->post['apart'];
				$data['city'] = $this->request->post['city'];
				$data['postcode'] = $this->request->post['postcode'];
				$data['country_id'] = 176;
				$data['zone_id'] = 2761;

				$address_id = $this->model_account_address->addAddress($data);

				$this->session->data['shipping_address'] = $this->model_account_address->getAddress($address_id);
				$json['address'] = 'ok';
			}
		} else {
			$json['address'] = 'ok';
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function validate() {
		$this->load->language('checkout/checkout');

		if (utf8_strlen(trim($this->request->post['mess'])) > 500)
		{
			$this->error['mess'] = 'Коментарий не должен содержать более 500 символов!';
		}

		return !$this->error;
	}
}
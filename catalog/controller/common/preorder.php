<?php
/**
 * Created by PhpStorm.
 * User: AtrDevue - callback.php
 * Date: 26.10.15
 * Time: 07:35
 * Project: dip
 */
class ControllerCommonPreorder extends Controller {
    private $error = array();

    public function index() {
        $json = array();

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {

            $html =  "От " . $this->request->post['name'] . "<br>";
            $html .= "Сообщение отправленное с сайта " . $this->config->get('config_name') . ". " . "<br><br>";
            $html .= "Телефон: " . $this->request->post['phone'] . "<br><br>" . " Сообщение: " . "<br><br>" . $this->request->post['mess'] . " <br><br>";
            $html .= "Товар: <a href=\"" . $this->request->post['href'] . "\">" . $this->request->post['product_name'] . "</a> <br><br>";
            $text = '';

            $mail = new Mail();
            $mail->protocol = $this->config->get('config_mail_protocol');
            $mail->parameter = $this->config->get('config_mail_parameter');
            $mail->smtp_hostname = $this->config->get('config_mail_smtp_host');
            $mail->smtp_username = $this->config->get('config_mail_smtp_username');
            $mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
            $mail->smtp_port = $this->config->get('config_mail_smtp_port');
            $mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout');

            $mail->setTo($this->config->get('config_email'));
            $mail->setFrom($this->config->get('config_email'));
            $mail->setSender($this->config->get('config_name'));
            $mail->setSubject('Предзаказ товара');
            $mail->setHtml($html);
            $mail->send();

            $json['success'] = 'ok';
        } else {
            $json['errors'] = 'Ошибка передачи данных!';
            $json['error'] = $this->error;
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    public function validate() {

        $sar = array('+', '(', ')', ' ', '-');
        $phone_temp = str_replace($sar, '', $this->request->post['phone']);

        if (strlen($phone_temp) != 11 || !is_numeric($phone_temp)) {
            $this->error['phone'] = 'Номер телефона должен содержать 11 символов!';
        }

        return !$this->error;
    }
}
<?php
class ControllerAccountsbacquiring extends Controller {
    private $error = array();
    public function index() {

      $this->load->model('account/sbacquiring');
      $yu_codes = $this->model_account_sbacquiring->getPayMethods();

      if (isset($this->request->get['order_id'])){

        $inv_id = $this->request->get['order_id'];
        $this->load->model('checkout/order');
        $order_info = $this->model_checkout_order->getOrder($inv_id);
      	$platp = substr($this->model_account_sbacquiring->yanencrypt($order_info['order_id'], $this->config->get('config_encryption')), 0, 8);
        
        if ($order_info['order_id'] == 0){$this->response->redirect($this->url->link('error/not_found'));}
        if (!$this->customer->isLogged()) {
          $data['back'] = $this->url->link('common/home');
        }
        else{
          $data['back'] = $this->url->link('account/order');
        }
	      $action = $this->url->link('account/sbacquiring/pay');
    
    		$data['merchant_url'] = $action .
			'&order_id=' 		. $inv_id .
            '&paymentType=' . $order_info['payment_code'] . '&code=' . substr($this->model_account_sbacquiring->yanencrypt($order_info['order_id'], $this->config->get('config_encryption')), 0, 8);

        $this->load->model('account/sbacquiring');
        $paystat = $this->model_account_sbacquiring->getPaymentStatus($inv_id);
        if (!isset($paystat['status'])){$paystat['status'] = 0;}

        $data['paystat'] = $paystat['status'];
        $this->load->language('account/'.$order_info['payment_code']);
        $data['button_pay'] = $this->language->get('button_pay');
		$data['button_back'] = $this->language->get('button_back');
		$data['heading_title'] = $this->language->get('heading_title');

        if ($paystat['status'] != 1){

        	if (!in_array($order_info['payment_code'], $yu_codes)) {
		    	$this->response->redirect($this->url->link('error/not_found'));
		    }

	        	if ($this->config->get($order_info['payment_code'].'_fixen')) {
					if ($this->config->get($order_info['payment_code'].'_fixen') == 'fix'){
					    $out_summ = $this->config->get($order_info['payment_code'].'_fixen_amount');
					}
					else{
					    $out_summ = $order_info['total'] * $this->config->get($order_info['payment_code'].'_fixen_amount') / 100;
					}
				}
				else{
					$out_summ = $order_info['total'];
				}
		        if ($this->config->get($order_info['payment_code'].'_hrefpage_text_attach')) {
		          $data['hrefpage_text'] = '';

		          $instros = explode('$', ($this->config->get($order_info['payment_code'].'_hrefpage_text_' . $this->config->get('config_language_id'))));
		                  $instroz = "";
		                  foreach ($instros as $instro) {
		                    if ($instro == 'orderid' ||  $instro == 'itogo' || $instro == 'komis' || $instro == 'total-komis' || $instro == 'plus-komis'){
		                      
		                        if ($instro == 'orderid'){
		                            $instro_other = $order_info['order_id'];
		                      }
		                      if ($instro == 'itogo'){
		                          $instro_other = $this->currency->format($out_summ, $order_info['currency_code'], $order_info['currency_value'], true);
		                      }
		                      if ($instro == 'komis'){
		                        if($this->config->get($order_info['payment_code'].'_komis')){
		                            $instro_other = $this->config->get($order_info['payment_code'].'_komis') . '%';
		                        }
		                        else{$instro_other = '';}
		                      }
		                      if ($instro == 'total-komis'){
		                        if($this->config->get($order_info['payment_code'].'_komis')){
		                            $instro_other = $this->currency->format($out_summ * $this->config->get($order_info['payment_code'].'_komis')/100, $order_info['currency_code'], $order_info['currency_value'], true);
		                        }
		                        else{$instro_other = '';}
		                      }
		                      if ($instro == 'plus-komis'){
		                        if($this->config->get($order_info['payment_code'].'_komis')){
		                            $instro_other = $this->currency->format($out_summ + ($out_summ * $this->config->get($order_info['payment_code'].'_komis')/100), $order_info['currency_code'], $order_info['currency_value'], true);
		                        }
		                        else{$instro_other = '';}
		                      }
		                    }
		                    else {
		                      $instro_other = nl2br(htmlspecialchars_decode($instro));
		                    }
		                    $instroz .=  $instro_other;
		                  }

		          $data['hrefpage_text'] .= $instroz;
		        }
		        else{        
		        $data['send_text'] = $this->language->get('send_text');
		        $data['send_text2'] = $this->language->get('send_text2');
		        $data['inv_id'] = $inv_id;
		        $data['out_summ'] = $this->currency->format($out_summ, $order_info['currency_code'], $order_info['currency_value'], true);
		        }
		}
		else{
			$data['hrefpage_text'] = $this->language->get('oplachen');
		}

        $data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');

        if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/account/sbacquiring.tpl')) {
			$this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/account/sbacquiring.tpl', $data));
		} else {
			$this->response->setOutput($this->load->view('default/template/account/sbacquiring.tpl', $data));
		}
    
      }
      else{
        echo "No data";
      }
  }

  public function pay() {
  	if (isset($this->request->get['code']) & isset($this->request->get['order_id'])){

      $this->load->model('checkout/order');
  	  $this->load->model('account/sbacquiring');
      $order_info = $this->model_checkout_order->getOrder($this->request->get['order_id']);
  	  $platp = substr($this->model_account_sbacquiring->yanencrypt($order_info['order_id'], $this->config->get('config_encryption')), 0, 8);
	  if ($this->request->get['code'] != $platp) {
	        $this->response->redirect($this->url->link('error/not_found'));
	  }	
      $yu_codes = $this->model_account_sbacquiring->getPayMethods();
      
      
      if (in_array($this->request->get['paymentType'], $yu_codes)) {

      	$codeforpay = $this->request->get['paymentType'];

        
        $paymentType = $this->config->get($codeforpay.'_methodcode');
        $userName = $this->config->get($codeforpay.'_userName');
        $password = $this->model_account_sbacquiring->yandecrypt($this->config->get($codeforpay.'_password'), $this->config->get('config_encryption'));
        if ($this->config->get($codeforpay.'_met')) {

        	// LIVE
        	$server = 'https://securepayments.sberbank.ru/payment/rest/registerPreAuth.do';

            // TEST
            //$server = 'https://3dsec.sberbank.ru/payment/rest/registerPreAuth.do';
        }
        else {

        	// LIVE
        	$server = 'https://securepayments.sberbank.ru/payment/rest/register.do';

            // TEST
            //$server = 'https://3dsec.sberbank.ru/payment/rest/register.do';

        }
        $returnUrl = $this->url->link('account/sbacquiring/success');
        $failUrl = $this->url->link('account/sbacquiring/fail');
        $this->language->load('account/'.$codeforpay);
        if ($this->config->get('config_store_id') == 0){
        	$store_name = $this->config->get('config_name');
        }
        else{
        	$this->load->model('setting/store');
			$stores = $this->model_setting_store->getStores();
			foreach ($stores as $store) {
				if ($this->config->get('config_store_id') == $store['store_id']){
					$store_name = $store['name'];
				}
			}
        }

        $description = $store_name . ': ' . $this->language->get('pay_order_text_target') . ' ' . $order_info['order_id'];

        if ($this->config->get($codeforpay.'_komis')) {
        	$proc=$this->config->get($codeforpay.'_komis');
        }
        if ($this->config->get($codeforpay.'_fixen')) {
			if ($this->config->get($codeforpay.'_fixen') == 'fix'){
				$out_summ = $this->config->get($codeforpay.'_fixen_amount');
			}
			else{
				$out_summ = $order_info['total'] * $this->config->get($codeforpay.'_fixen_amount') / 100;
			}
		}
		else {
			$out_summ = $order_info['total'];
		}

		if(!$this->config->get($codeforpay.'_createorder_or_notcreate')){
	          if (isset($this->session->data['order_id'])){
		        if ($this->request->get['order_id'] == $this->session->data['order_id']){
		            $this->cart->clear();
		            
		            unset($this->session->data['shipping_method']);
		            unset($this->session->data['shipping_methods']);
		            unset($this->session->data['payment_method']);
		            unset($this->session->data['payment_methods']);
		            unset($this->session->data['guest']);
		            unset($this->session->data['comment']);
		            unset($this->session->data['order_id']);  
		            unset($this->session->data['coupon']);
		            unset($this->session->data['reward']);
		            unset($this->session->data['voucher']);
		            unset($this->session->data['vouchers']);
		        }
		      }
		  }
      }
      else{
        echo 'error: no payment method';
        exit();
      }
      
      if (is_numeric($out_summ)){
        if ($this->currency->has('RUB')){
          $totalrub = $out_summ;
          if (isset($proc)){$amount = $this->currency->format(($totalrub*$proc/100)+$totalrub, 'RUB', $this->currency->getValue('RUB'), false);}
          else{$amount = $this->currency->format($totalrub, 'RUB', $this->currency->getValue('RUB'), false);}
        }
        else{echo 'No currency RUB'; exit();}
      }
      else{
        echo 'error: no total sum';
        exit();
      }
		    $prices = preg_replace('/\(.*\)/', '', $amount);        
		    $delimetr = substr($prices,-3,1);
		    $price = preg_replace('/[^\d]/', '', $prices);    
		    $amount = ($delimetr == ',' || $delimetr == '.') ? $price : $price.'00';
		

      if (is_numeric($this->request->get['order_id'])){
        $orderNumber = $this->request->get['order_id'];
      }
      else{
        echo 'error: no order id';
        exit();
      }
      $language = 'RU';
      $currency ='';

      if (isset($this->request->get['first'])){
      	$first = 'first-';
  	  }
  	  else{
  	  	$first = '';
  	  }

  	  $ordercompromis = $order_info['order_id'] . '-'.$first. time();
/*
      $postdata = http_build_query(
		    array(
		        'userName' => $userName,
		        'password' => $password,
		        'orderNumber' => $ordercompromis,
		        'amount' => $amount,
		        'returnUrl' => $returnUrl,
		        'failUrl' => $failUrl,
		        'description' => $description,
		        'currency' => '643',
		        'language' => $language
		    )
		);
		 
		$opts = array('http' =>
		    array(
		        'method'  => 'POST',
		        'header'  => 'Content-type: application/x-www-form-urlencoded',
		        'content' => $postdata
		    )
		);

	  $context  = stream_context_create($opts);
	  $result = file_get_contents($server, false, $context);
*/

	  $forcurlpost = 'userName='.$userName.'&password='.$password.'&orderNumber='.$ordercompromis.'&amount='.$amount.'&returnUrl='.$returnUrl.'&failUrl='.$failUrl.'&description='.$description.'&currency=643&language='.$language;  
	  if ( $curl = curl_init() ) {
	    curl_setopt($curl, CURLOPT_URL, $server);
	    curl_setopt($curl, CURLOPT_RETURNTRANSFER,true);
	    curl_setopt($curl, CURLOPT_POST, true);
	    curl_setopt($curl, CURLOPT_POSTFIELDS, $forcurlpost);
	    $result = curl_exec($curl);
	    curl_close($curl);
	  }
	  else{
	  	$this->log->write('SberBank error: No curl library in host');
	  	echo 'Server Error';
	  	exit();
	  }

	  $result = json_decode($result);
	  if (isset($result->formUrl)){
	  	$this->session->data['sbacquiring'] = $result->orderId . '~' . $ordercompromis;
	  	$this->response->redirect($result->formUrl);
	  }
	  else{
	  	$result = (array) $result;
	  	$this->log->write('SberBank error: code=' . implode(' - ', $result));
	  	echo 'No data';
	  }
	}
	else{
		$this->response->redirect($this->url->link('error/not_found'));
	}
  }

  public function callback() {
		if (isset($this->request->get['orderNumber']) && isset($this->request->get['operation'])) {

			$this->load->model('account/sbacquiring');
			$this->load->model('checkout/order'); 
   			$sbacquiring_order = explode('-', $this->request->get['orderNumber']);
			$ordernum = $sbacquiring_order[0];
			$order_info = $this->model_checkout_order->getOrder($ordernum);
			$paymentcode = $order_info['payment_code'];
	        $paystat = $this->model_account_sbacquiring->getPaymentStatus($ordernum);
	        if (!isset($paystat['status'])){$paystat['status'] = 0;}
	        if ($paystat['status'] != 1){
	        	if ($this->request->get['operation'] == 'deposited' && $this->request->get['status'] == 1 && !$this->config->get($paymentcode.'_met') || $this->request->get['operation'] == 'approved' && $this->request->get['status'] == 1){
		        	$userName = $this->config->get($paymentcode.'_userName');
	        		$password = $this->model_account_sbacquiring->yandecrypt($this->config->get($paymentcode.'_password'), $this->config->get('config_encryption'));
		        	$language = 'RU';
		        	$forcurlpost = 'userName='.$userName.'&password='.$password.'&orderId='.$this->request->get['mdOrder'].'&language='.$language;  
					if ( $curl = curl_init() ) {

                        // LIVE
                        curl_setopt($curl, CURLOPT_URL, 'https://securepayments.sberbank.ru/payment/rest/getOrderStatus.do');

                        // TEST
                        //curl_setopt($curl, CURLOPT_URL, 'https://3dsec.sberbank.ru/payment/rest/getOrderStatus.do');


                        curl_setopt($curl, CURLOPT_RETURNTRANSFER,true);
					    curl_setopt($curl, CURLOPT_POST, true);
					    curl_setopt($curl, CURLOPT_POSTFIELDS, $forcurlpost);
					    $result = curl_exec($curl);
					    curl_close($curl);
					}
					else{
					  	$this->log->write('SberBank error: No curl');
					  	exit();
					}

					$result = json_decode($result);
					if (isset($result->OrderStatus)){
					  	if ($result->OrderStatus == 2 || $result->OrderStatus == 1){
					  		$payOK = 1;
					  	}
					}
					else{
					  	$result = (array) $result;
					  	$this->log->write('SberBank error: code=' . implode(' - ', $result));
					  	echo 'No data';
					  	exit();
					}
				}
				else{
					$this->model_checkout_order->addOrderHistory($ordernum, $this->config->get('sbacquiring_on_status_id'), '', true, true);
					exit();
				}

				if (isset($payOK)) {

			  		if ($this->config->get($paymentcode.'_fixen')) {
						if ($this->config->get($paymentcode.'_fixen') == 'fix'){
						    $totalrub = $this->config->get($paymentcode.'_fixen_amount');
						}
						else{
						    $totalrub = $order_info['total'] * $this->config->get($paymentcode.'_fixen_amount') / 100;
						}
					}
					else{
						$totalrub = $order_info['total'];
					}

					
					if ($this->config->get($paymentcode.'_komis')){
						$youpayment = $this->currency->format(($totalrub * $this->config->get($paymentcode.'_komis')/100) + $totalrub, 'RUB', $this->currency->getValue('RUB'), false);
					}
					else {
						$youpayment = $this->currency->format($totalrub, 'RUB', $this->currency->getValue('RUB'), false);
					}

					$payments = $result->Amount;
					$amount = $youpayment;
					$prices = preg_replace('/\(.*\)/', '', $amount);        
		    		$delimetr = substr($prices,-3,1);
		    		$price = preg_replace('/[^\d]/', '', $prices);    
		    		$amount = ($delimetr == ',' || $delimetr == '.') ? $price : $price.'00';
					if($amount == $payments){
								
						if($payments) {
							if ($sbacquiring_order[1] == 'first'){
								$sbacquiring_order[1] = $sbacquiring_order[2];
							}
							$query = $this->db->query ("INSERT INTO `" . DB_PREFIX . "sbacquiring` SET `num_order` = '".$order_info['order_id']."' , `sum` = '".$youpayment."' , `date_enroled` = '".$sbacquiring_order[1]."', `date_created` = '".$order_info['date_added']."', `user` = '".$order_info['payment_firstname']." ".$order_info['payment_lastname']."', `email` = '".$order_info['email']."', `status` = '1', `sender` = '".$this->request->get['mdOrder']."' ");

									if($this->config->get($paymentcode.'_createorder_or_notcreate') && $order_info['order_status_id'] != $this->config->get($paymentcode.'_on_status_id')){
										$this->language->load('payment/'.$paymentcode);
											if ($this->config->get($paymentcode.'_mail_instruction_attach')){
												$inv_id = $order_info['order_id'];
												if ($this->config->get($paymentcode.'_fixen')) {
													if ($this->config->get($paymentcode.'_fixen') == 'fix'){
													    $out_summ = $this->config->get($paymentcode.'_fixen_amount');
													}
													else{
													    $out_summ = $order_info['total'] * $this->config->get($paymentcode.'_fixen_amount') / 100;
													}
												}
												else{
													$out_summ = $order_info['total'];
												}
												$action= $order_info['store_url'] . 'index.php?route=account/sbacquiring';
												$online_url = $action .

												'&order_id='	. $order_info['order_id'] . '&code=' . substr($this->model_account_sbacquiring->yanencrypt($order_info['order_id'], $this->config->get('config_encryption')), 0, 8);

										    	$comment  = $this->language->get('text_instruction') . "\n\n";
										    	$instros = explode('$', ($this->config->get($paymentcode.'_mail_instruction_' . $order_info['language_id'])));
												      $instroz = "";
												      foreach ($instros as $instro) {
												      	if ($instro == 'href' || $instro == 'orderid' ||  $instro == 'itogo' || $instro == 'komis' || $instro == 'total-komis' || $instro == 'plus-komis'){
												      		if ($instro == 'href'){
												            	$instro_other = $online_url;
												        	}
												            if ($instro == 'orderid'){
												            	$instro_other = $inv_id;
													       	}
													       	if ($instro == 'itogo'){
													            $instro_other = $this->currency->format($out_summ, $order_info['currency_code'], $order_info['currency_value'], true);
													       	}
													       	if ($instro == 'komis'){
																if($this->config->get($paymentcode.'_komis')){
															    	$instro_other = $this->config->get($paymentcode.'_komis') . '%';
																}
																else{$instro_other = '';}
															}
															if ($instro == 'total-komis'){
																if($this->config->get($paymentcode.'_komis')){
															    	$instro_other = $this->currency->format($out_summ * $this->config->get($paymentcode.'_komis')/100, $order_info['currency_code'], $order_info['currency_value'], true);
																}
																else{$instro_other = '';}
															}
															if ($instro == 'plus-komis'){
																if($this->config->get($paymentcode.'_komis')){
															    	$instro_other = $this->currency->format($out_summ + ($out_summ * $this->config->get($paymentcode.'_komis')/100), $order_info['currency_code'], $order_info['currency_value'], true);
																}
																else {$instro_other = '';}
															}
												       	}
												       	else {
												       		$instro_other = nl2br($instro);
												       	}
												       	$instroz .=  $instro_other;
												      }
												$comment .= $instroz;
										    	$comment = htmlspecialchars_decode($comment);
										    	$this->model_checkout_order->addOrderHistory($order_info['order_id'], $this->config->get($paymentcode.'_order_status_id'), $comment, true);
									    	}
									    	else {
												$this->model_checkout_order->addOrderHistory($order_info['order_id'], $this->config->get($paymentcode.'_order_status_id'), true);
											}

											if ($this->config->get($paymentcode.'_success_alert_customer')){
									        	if ($this->config->get($paymentcode.'_success_comment_attach')) {
									          		$instros = explode('$', ($this->config->get($paymentcode.'_success_comment_' . $order_info['language_id'])));
									              	$instroz = "";
									              	foreach ($instros as $instro) {
									                	if ($instro == 'orderid' ||  $instro == 'itogo'){
									                    	if ($instro == 'orderid'){
									                    		$instro_other = $order_info['order_id'];
									                  		}
									                  		if ($instro == 'itogo'){
									                      		$instro_other = $this->currency->format($totalrub, $order_info['currency_code'], $order_info['currency_value'], true);
									                  		}
									                	}
									                	else {
									                  		$instro_other = nl2br(htmlspecialchars_decode($instro));
									                	}
									                	$instroz .=  $instro_other;
									              	}
									          		$message = $instroz;
									          		$this->model_checkout_order->addOrderHistory($order_info['order_id'], $this->config->get($paymentcode.'_order_status_id'), $message, true);
									        	}
									        	else{
									          		$message = '';
									          		$this->model_checkout_order->addOrderHistory($order_info['order_id'], $this->config->get($paymentcode.'_order_status_id'), $message, true);
									        	}
								    		}
									}

									else {

								    	if ($this->config->get($paymentcode.'_success_alert_customer')){
								        	if ($this->config->get($paymentcode.'_success_comment_attach')) {
								          		$instros = explode('$', ($this->config->get($paymentcode.'_success_comment_' . $order_info['language_id'])));
								              	$instroz = "";
								              	foreach ($instros as $instro) {
								                	if ($instro == 'orderid' ||  $instro == 'itogo'){
								                    	if ($instro == 'orderid'){
								                    		$instro_other = $order_info['order_id'];
								                  		}
								                  		if ($instro == 'itogo'){
								                      		$instro_other = $this->currency->format($totalrub, $order_info['currency_code'], $order_info['currency_value'], true);
								                  		}
								                	}
								                	else {
								                  		$instro_other = nl2br(htmlspecialchars_decode($instro));
								                	}
								                	$instroz .=  $instro_other;
								              	}
								          		$message = $instroz;
								          		$this->model_checkout_order->addOrderHistory($order_info['order_id'], $this->config->get($paymentcode.'_order_status_id'), $message, true);
								        	}
								        	else{
								          		$message = '';
								          		$this->model_checkout_order->addOrderHistory($order_info['order_id'], $this->config->get($paymentcode.'_order_status_id'), $message, true);
								        	}
								    	}
								    	else{
								      		$this->model_checkout_order->addOrderHistory($order_info['order_id'], $this->config->get($paymentcode.'_order_status_id'), false);
								    	}

									}

						    		if ($this->config->get($paymentcode.'_success_alert_admin')) {
						      
						        		$subject = sprintf(html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8'), $order_info['order_id']);
						        
						        		// Text 
								        $this->load->language('account/'.$paymentcode);
								        $text = sprintf($this->language->get('success_admin_alert'), $order_info['order_id']) . "\n";
								        
								        
								      
								        $mail = new Mail(); 
								        $mail->protocol = $this->config->get('config_mail_protocol');
								        $mail->parameter = $this->config->get('config_mail_parameter');
								        $mail->hostname = $this->config->get('config_smtp_host');
								        $mail->username = $this->config->get('config_smtp_username');
								        $mail->password = $this->config->get('config_smtp_password');
								        $mail->port = $this->config->get('config_smtp_port');
								        $mail->timeout = $this->config->get('config_smtp_timeout');
								        $mail->setTo($this->config->get('config_email'));
								        $mail->setFrom($this->config->get('config_email'));
								        $mail->setSender($order_info['store_name']);
								        $mail->setSubject(html_entity_decode($subject, ENT_QUOTES, 'UTF-8'));
								        $mail->setText(html_entity_decode($text, ENT_QUOTES, 'UTF-8'));
								        $mail->send();
								        
								        // Send to additional alert emails
								        $emails = explode(',', $this->config->get('config_alert_emails'));
						        
						        		foreach ($emails as $email) {
						          			if ($email && preg_match('/^[^\@]+@.*\.[a-z]{2,6}$/i', $email)) {
						            			$mail->setTo($email);
						            			$mail->send();
						          			}
							    		}
							    	}
						   	}
						else{
							$this->log->write('SberBank error: ups!');
						}
					}
					else{
						$this->log->write('SberBank error: Amount of payment not equal');
					}

			  	}
			  	else{
				  	$this->model_checkout_order->addOrderHistory($ordernum, $this->config->get('sbacquiring_on_status_id'), '', true, true);
			  		echo "No Data";
			  	}
			}

		}
  }

  public function success() {
  	if (isset($this->request->get['orderId'])){
   		$this->load->model('checkout/order');
   		$this->load->model('account/sbacquiring');
   		if (isset($this->session->data['sbacquiring'])){
   			$sborder = explode('~', $this->session->data['sbacquiring']);
   		}
   		else{
   			$sborder = 0;
   		}
   		$sbacquiring_order = explode('-', $sborder[1]);
   		foreach ($sbacquiring_order as $sb_first) {
   			if ($sb_first == 'first'){
	            $first = 1;
	        }
   		}
		$order_info = $this->model_checkout_order->getOrder($sbacquiring_order[0]);
  	}
  	else{
   		echo 'No order';
   		exit();
  	}

  	if ($sbacquiring_order[0] == $order_info['order_id']){
      $inv_id = $order_info['order_id'];
      $data['inv_id'] = $order_info['order_id'];

      $action = $this->url->link('account/sbacquiring');
				$online_url = $action .
				'&order_id=' . $order_info['order_id'] . '&code=' . substr($this->model_account_sbacquiring->yanencrypt($order_info['order_id'], $this->config->get('config_encryption')), 0, 8);

      $data['success_text'] = '';


      $paymentcode = $order_info['payment_code'];

      	if ($this->config->get($paymentcode.'_fixen')) {
			if ($this->config->get($paymentcode.'_fixen') == 'fix'){
			    $out_summ = $this->config->get($paymentcode.'_fixen_amount');
			}
			else{
			    $out_summ = $order_info['total'] * $this->config->get($paymentcode.'_fixen_amount') / 100;
			}
		}
		else{
			$out_summ = $order_info['total'];
		}

      
      	$this->load->language('account/'.$paymentcode);
      	$data['heading_title'] = $this->language->get('heading_title');
      	$this->document->setTitle($this->language->get('heading_title'));
      	$data['button_ok'] = $this->language->get('button_ok');

      	//if ($order_info['order_status_id'] == $this->config->get($paymentcode.'_order_status_id')) {
      	if (true) {

      	if (isset($first)) {
	        $data['success_text'] .=  $this->language->get('success_text_first');
	    }

      	  	if($this->config->get($paymentcode.'_createorder_or_notcreate')  && isset($first)){
		        
			            $this->cart->clear();
			            
			            unset($this->session->data['shipping_method']);
			            unset($this->session->data['shipping_methods']);
			            unset($this->session->data['payment_method']);
			            unset($this->session->data['payment_methods']);
			            unset($this->session->data['guest']);
			            unset($this->session->data['comment']);
			            unset($this->session->data['order_id']);  
			            unset($this->session->data['coupon']);
			            unset($this->session->data['reward']);
			            unset($this->session->data['voucher']);
			            unset($this->session->data['vouchers']);
		    }

	      if ($this->config->get($paymentcode.'_success_page_text_attach')) {

	      $instros = explode('$', ($this->config->get($paymentcode.'_success_page_text_' . $this->config->get('config_language_id'))));
	              $instroz = "";
	              foreach ($instros as $instro) {
	                if ($instro == 'orderid' ||  $instro == 'itogo' || $instro== 'href' || $instro == 'komis' || $instro == 'total-komis' || $instro == 'plus-komis'){
	                  if ($instro == 'orderid'){
	                    $instro_other = $inv_id;
	                  }
	                  if ($instro == 'itogo'){
	                      $instro_other = $this->currency->format($out_summ, $order_info['currency_code'], $order_info['currency_value'], true);
	                  }
	                  if ($instro == 'href'){
	                      $instro_other = $online_url;
	                  }
	                  if ($instro == 'komis'){
	                        if($this->config->get($paymentcode.'_komis')){
	                            $instro_other = $this->config->get($paymentcode.'_komis') . '%';
	                        }
	                        else{$instro_other = '';}
	                      }
	                      if ($instro == 'total-komis'){
	                        if($this->config->get($paymentcode.'_komis')){
	                            $instro_other = $this->currency->format($out_summ * $this->config->get($paymentcode.'_komis')/100, $order_info['currency_code'], $order_info['currency_value'], true);
	                        }
	                        else{$instro_other = '';}
	                      }
	                      if ($instro == 'plus-komis'){
	                        if($this->config->get($paymentcode.'_komis')){
	                            $instro_other = $this->currency->format($out_summ + ($out_summ * $this->config->get($paymentcode.'_komis')/100), $order_info['currency_code'], $order_info['currency_value'], true);
	                        }
	                        else{$instro_other = '';}
	                      }
	                }
	                else {
	                  $instro_other = nl2br(htmlspecialchars_decode($instro));
	                }
	                $instroz .=  $instro_other;
	              }

	      $data['success_text'] .= $instroz;
	      }
	      else{
	          $data['success_text'] .=  sprintf($this->language->get('success_text'), $inv_id);
	      }
	    }
	    else{

	      	if (isset($first) && $order_info['order_status_id'] == $this->config->get($paymentcode.'_on_status_id')) {
		        $data['success_text'] .=  $this->language->get('success_text_first');
		    }
	    	if ($this->config->get($paymentcode.'_waiting_page_text_attach')) {

	      $instros = explode('$', ($this->config->get($paymentcode.'_waiting_page_text_' . $this->config->get('config_language_id'))));
	              $instroz = "";
	              foreach ($instros as $instro) {
	                if ($instro == 'orderid' ||  $instro == 'itogo' || $instro== 'href' || $instro == 'komis' || $instro == 'total-komis' || $instro == 'plus-komis'){
	                  if ($instro == 'orderid'){
	                    $instro_other = $inv_id;
	                  }
	                  if ($instro == 'itogo'){
	                      $instro_other = $this->currency->format($out_summ, $order_info['currency_code'], $order_info['currency_value'], true);
	                  }
	                  if ($instro == 'href'){
	                      $instro_other = $online_url;
	                  }
	                  if ($instro == 'komis'){
	                        if($this->config->get($paymentcode.'_komis')){
	                            $instro_other = $this->config->get($paymentcode.'_komis') . '%';
	                        }
	                        else{$instro_other = '';}
	                      }
	                      if ($instro == 'total-komis'){
	                        if($this->config->get($paymentcode.'_komis')){
	                            $instro_other = $this->currency->format($out_summ * $this->config->get($paymentcode.'_komis')/100, $order_info['currency_code'], $order_info['currency_value'], true);
	                        }
	                        else{$instro_other = '';}
	                      }
	                      if ($instro == 'plus-komis'){
	                        if($this->config->get($paymentcode.'_komis')){
	                            $instro_other = $this->currency->format($out_summ + ($out_summ * $this->config->get($paymentcode.'_komis')/100), $order_info['currency_code'], $order_info['currency_value'], true);
	                        }
	                        else{$instro_other = '';}
	                      }
	                }
	                else {
	                  $instro_other = nl2br(htmlspecialchars_decode($instro));
	                }
	                $instroz .=  $instro_other;
	              }

	      $data['success_text'] .= $instroz;
	      }
	      else{
	      		if($order_info['order_status_id'] == $this->config->get($paymentcode.'_on_status_id')){
	         		$data['success_text'] .=  sprintf($this->language->get('success_text_wait'), $inv_id, $online_url);
	         	}
	         	else{
	          		$data['success_text'] .=  sprintf($this->language->get('success_text_wait_noorder'), $online_url);
	          	}
	      }
	    }
	  
      if ($this->customer->isLogged()) {
        
        			
        			if(!$this->config->get($paymentcode.'_createorder_or_notcreate')){ 
        				$data['success_text'] .=  sprintf($this->language->get('success_text_loged'), $this->url->link('account/order', '', 'SSL'), $this->url->link('account/order/info&order_id=' . $inv_id, '', 'SSL'));
        			}
        			else{
        				if ($order_info['order_status_id'] == $this->config->get($paymentcode.'_order_status_id')) {
        					$data['success_text'] .=  sprintf($this->language->get('success_text_loged'), $this->url->link('account/order', '', 'SSL'), $this->url->link('account/order/info&order_id=' . $inv_id, '', 'SSL'));
        				}
        			}
	        		if ($order_info['order_status_id'] != $this->config->get($paymentcode.'_order_status_id')) {
	        			if($order_info['order_status_id'] == $this->config->get($paymentcode.'_on_status_id')){
	        				$data['success_text'] .=  sprintf($this->language->get('waiting_text_loged'), $this->url->link('account/order', '', 'SSL'));
	        			}
	        		}

      }
      
      $data['breadcrumbs'] = array();

      $data['breadcrumbs'][] = array(
        'text'      => $this->language->get('text_home'),
        'href'      => $this->url->link('common/home')
      );
      
      if (isset($first)) {
        $this->language->load('checkout/success');
        $data['breadcrumbs'][] = array(
          'href'      => $this->url->link('checkout/cart'),
          'text'      => $this->language->get('text_basket')
        );
        
        $data['breadcrumbs'][] = array(
          'href'      => $this->url->link('checkout/checkout', '', 'SSL'),
          'text'      => $this->language->get('text_checkout')
        );
        $data['button_ok_url'] = $this->url->link('common/home');
      }
      else{
        if ($this->customer->isLogged()) {
          $data['breadcrumbs'][] = array(
            'text'      => $this->language->get('lich'),
            'href'      => $this->url->link('account/account', '', 'SSL')
          );

          $data['breadcrumbs'][] = array(
            'text'      => $this->language->get('history'),
            'href'      => $this->url->link('account/order', '', 'SSL')
          );
          $data['button_ok_url'] = $this->url->link('account/order', '', 'SSL');
        }
        else{
          $data['button_ok_url'] = $this->url->link('common/home');
        }
      }

      $data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');
      
      if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/account/sbacquiring_success.tpl')) {
			$this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/account/sbacquiring_success.tpl', $data));
		} else {
			$this->response->setOutput($this->load->view('default/template/account/sbacquiring_success.tpl', $data));
		}
    }
    else{
      echo "No data";
    }
  }

  public function fail(){
  	if (isset($this->request->get['orderId'])){
   		$this->load->model('checkout/order'); 
   		$this->load->model('account/sbacquiring');
   		$sborder = explode('~', $this->session->data['sbacquiring']);
   		$sbacquiring_order = explode('-', $sborder[1]);
   		foreach ($sbacquiring_order as $sb_first) {
   			if ($sb_first == 'first'){
	            $first = 1;
	        }
   		}
		$order_info = $this->model_checkout_order->getOrder($sbacquiring_order[0]);
  	}
  	else{
   		echo 'No order';
   		exit();
  	}

  	if ($sbacquiring_order[0] == $order_info['order_id']){
  	  $paymentcode = $order_info['payment_code'];
      $inv_id = $order_info['order_id'];
      $this->load->language('account/'.$paymentcode);
      $data['heading_title'] = $this->language->get('heading_title_fail');
      $this->document->setTitle($this->language->get('heading_title'));
      $data['button_ok'] = $this->language->get('button_ok');
      $data['inv_id'] = $order_info['order_id'];

      $action= $this->url->link('account/'.$paymentcode);
				$online_url = $action .
				'&order_id=' . $order_info['order_id'] . '&code=' . substr($this->model_account_sbacquiring->yanencrypt($order_info['order_id'], $this->config->get('config_encryption')), 0, 8);

      $data['fail_text'] = '';

      	if (isset($first) && $order_info['order_status_id'] == $this->config->get($paymentcode.'_on_status_id')) {
	      	$data['fail_text'] .=  $this->language->get('fail_text_first');
	    }
      	if ($this->config->get($paymentcode.'_fixen')) {
			if ($this->config->get($paymentcode.'_fixen') == 'fix'){
			    $out_summ = $this->config->get($paymentcode.'_fixen_amount');
			}
			else{
			    $out_summ = $order_info['total'] * $this->config->get($paymentcode.'_fixen_amount') / 100;
			}
		}
		else{
			$out_summ = $order_info['total'];
		}

	    	if ($this->config->get($paymentcode.'_fail_page_text_attach')) {

	      $instros = explode('$', ($this->config->get($paymentcode.'_fail_page_text_' . $this->config->get('config_language_id'))));
	              $instroz = "";
	              foreach ($instros as $instro) {
	                if ($instro == 'orderid' ||  $instro == 'itogo' || $instro== 'href' || $instro == 'komis' || $instro == 'total-komis' || $instro == 'plus-komis'){
	                  if ($instro == 'orderid'){
	                    $instro_other = $inv_id;
	                  }
	                  if ($instro == 'itogo'){
	                      $instro_other = $this->currency->format($out_summ, $order_info['currency_code'], $order_info['currency_value'], true);
	                  }
	                  if ($instro == 'href'){
	                      $instro_other = $online_url;
	                  }
	                  if ($instro == 'komis'){
	                        if($this->config->get($paymentcode.'_komis')){
	                            $instro_other = $this->config->get($paymentcode.'_komis') . '%';
	                        }
	                        else{$instro_other = '';}
	                      }
	                      if ($instro == 'total-komis'){
	                        if($this->config->get($paymentcode.'_komis')){
	                            $instro_other = $this->currency->format($out_summ * $this->config->get($paymentcode.'_komis')/100, $order_info['currency_code'], $order_info['currency_value'], true);
	                        }
	                        else{$instro_other = '';}
	                      }
	                      if ($instro == 'plus-komis'){
	                        if($this->config->get($paymentcode.'_komis')){
	                            $instro_other = $this->currency->format($out_summ + ($out_summ * $this->config->get($paymentcode.'_komis')/100), $order_info['currency_code'], $order_info['currency_value'], true);
	                        }
	                        else{$instro_other = '';}
	                      }
	                }
	                else {
	                  $instro_other = nl2br(htmlspecialchars_decode($instro));
	                }
	                $instroz .=  $instro_other;
	              }

	      $data['fail_text'] .= $instroz;
	      }
	      else{
	      		if($order_info['order_status_id'] == $this->config->get($paymentcode.'_on_status_id')){
	         		$data['fail_text'] .=  sprintf($this->language->get('fail_text'), $inv_id, $online_url, $online_url);
	         	}
	         	else{
	          		$data['fail_text'] .=  sprintf($this->language->get('fail_text_noorder'), $online_url);
	          	}
	      }

	  
      if ($this->customer->isLogged()) {
        
        			
        			if($order_info['order_status_id'] == $this->config->get($paymentcode.'_on_status_id')){ 
        				$data['fail_text'] .=  sprintf($this->language->get('fail_text_loged'), $this->url->link('account/order', '', 'SSL'), $this->url->link('account/order/info&order_id=' . $inv_id, '', 'SSL'), $this->url->link('account/order', '', 'SSL'));
        			}

      }
      
      $data['breadcrumbs'] = array();

      $data['breadcrumbs'][] = array(
        'text'      => $this->language->get('text_home'),
        'href'      => $this->url->link('common/home'),
        'separator' => false
      );
      
      if (isset($first)) {
        $this->language->load('checkout/success');
        $data['breadcrumbs'][] = array(
          'href'      => $this->url->link('checkout/cart'),
          'text'      => $this->language->get('text_basket'),
          'separator' => $this->language->get('text_separator')
        );
        
        $data['breadcrumbs'][] = array(
          'href'      => $this->url->link('checkout/checkout', '', 'SSL'),
          'text'      => $this->language->get('text_checkout'),
          'separator' => $this->language->get('text_separator')
        );
        $data['button_ok_url'] = $this->url->link('common/home');
      }
      else{
        if ($this->customer->isLogged()) {
          $data['breadcrumbs'][] = array(
            'text'      => $this->language->get('lich'),
            'href'      => $this->url->link('account/account', '', 'SSL'),
            'separator' => $this->language->get('text_separator')
          );

          $data['breadcrumbs'][] = array(
            'text'      => $this->language->get('history'),
            'href'      => $this->url->link('account/order', '', 'SSL'),
            'separator' => $this->language->get('text_separator')
          );
          $data['button_ok_url'] = $this->url->link('account/order', '', 'SSL');
        }
        else{
          $data['button_ok_url'] = $this->url->link('common/home');
        }
      }

      $data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');
      
      if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/account/sbacquiring_fail.tpl')) {
			$this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/account/sbacquiring_fail.tpl', $data));
		} else {
			$this->response->setOutput($this->load->view('default/template/account/sbacquiring_fail.tpl', $data));
		}
    }
    else{
      echo "No data";
    }
  }


}
?>
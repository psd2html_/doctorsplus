<?php

// Heading

$_['heading_title']    = 'Режим обслуживания';
$_['text_home'] = 'Главная';


// Text

$_['text_maintenance'] = 'Магазин временно закрыт';

$_['text_message']     = '<h1 style="text-align:center;">Магазин временно закрыт. Мы выполняем профилактические работы.<br />Вскоре магазин будет доступен. Пожалуйста, зайдите позже.</h1>';


<?php
// Text
$_['text_title']				= 'Оплата наличными';
$_['text_instruction']			= 'Оплата наличными. Инструкция';
$_['text_payable']				= 'Получатель платежа: ';
$_['text_address']				= 'Прием платежей по адресу: ';
$_['text_payment']				= 'Ваш заказ не будет отправлен, пока не будет произведена оплата.';
$_['text_descroption']          = '';
$_['logo']                      = '/catalog/view/theme/thai-style/images/cash.jpg';


<?php
// Heading
$_['heading_title'] 	= 'Вопрос-ответ';
 
// Text
$_['text_question'] 	= 'Вопрос';
$_['text_answer'] 	    = 'Ответ';
$_['text_error'] 		= 'Страница, которую вы ищете не может быть найден.';

// Entry
$_['entry_name']     = 'Ваше имя';
$_['entry_email']    = 'Ваш E-Mail';
$_['entry_phone']    = 'Ваш телефон';
$_['entry_enquiry']  = 'Ваш вопрос или сообщение';
$_['entry_captcha']  = 'Введите код, указанный на картинке';

// Email
$_['email_subject']  = 'Сообщение от %s';

// Errors
$_['error_name']     = 'Имя должно быть от 3 до 32 символов!';
$_['error_email']    = 'E-mail адрес введен неверно!';
$_['error_phone']    = 'Телефон не верный!';
$_['error_enquiry']  = 'Длина текста должна быть от 10 до 3000 символов!';
$_['error_captcha']  = 'Проверочный код не совпадает с изображением!';
<?php
/**
 * Created by PhpStorm.
 * User: AtrDevue - action.php
 * Date: 01.11.15
 * Time: 04:55
 * Project: dip
 */
$_['heading_title'] 	= 'Акции';

// Text
$_['text_title'] 		= 'Название';
$_['text_description'] 	= 'Описание';
$_['text_date'] 		= 'Дата добавления';
$_['text_view'] 		= 'Просмотреть';
$_['text_error'] 		= 'Нет акций';
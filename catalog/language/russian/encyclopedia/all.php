<?php
/**
 * Created by PhpStorm.
 * User: AtrDevue - all.php
 * Date: 15.09.15
 * Time: 22:39
 * Project: dip
 */
// Heading
$_['heading_title'] 	= 'Энциклопедия';

// Text
$_['text_latest'] 		= 'Последние добавленные';
$_['text_alfa'] 	    = 'Алфавитный указатель';
$_['text_vategory'] 	= 'Категории';
$_['text_view'] 		= 'Подробнее';
$_['text_error'] 		= 'Нет записей';

$_['text_pagination']      = 'Страница (%s) из (%s) и (%s) и (%s)';
<?php
// Heading 
$_['heading_title'] 	 = 'оставайся на связи';

//Fields
$_['entry_email'] 		 = 'Email';
$_['entry_name'] 		 = 'Имя';

//Buttons
$_['entry_button'] 		 = 'Подписываться';
$_['entry_unbutton'] 	 = 'Отписаться';

//text
$_['text_subscribe'] 	 = 'Подписка';
$_['text_email'] 	 = 'Введите свой адрес электронной почты';

$_['mail_subject']   	 = 'News Letter Subscribe';

//Error
$_['error_invalid'] 	 = 'Неверный адрес электронной почты';

$_['subscribe']	    	 = 'Подписка успешно оформленна';
$_['unsubscribe'] 	     = 'Вы успешно отписались';
$_['alreadyexist'] 	     = 'Уже существуете';
$_['notexist'] 	    	 = 'Email адрес не существует';

?>
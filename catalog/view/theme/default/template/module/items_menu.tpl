<nav class="leftmenu">
    <ul class="leftmenu__list">
        <?php foreach ($informations as $information) { ?>
        <li<?php if($information['active'] == 0) { echo ' class="current"'; } ?>><a href="<?php echo $information['href']; ?>"><?php echo $information['title']; ?></a></li>
        <?php } ?>
    </ul>
</nav>
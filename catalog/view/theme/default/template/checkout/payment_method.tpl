<?php if ($error_warning) { ?>
<div class="alert alert-warning"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?></div>
<?php } ?>
<?php if ($payment_methods) { ?>
<?php $dis = (!$logged && $account != 'guest') ? ' disabled ' : ''  ?>
<h4>3. Способ оплаты</h4>
<div class="order-step__form">
  <div class="row">
    <?php foreach ($payment_methods as $payment_method) { ?>
    <div class="col-b6 col-m12">
      <?php if ($payment_method['code'] == $code || !$code) { ?>
      <?php $code = $payment_method['code']; ?>
      <input type="radio" name="payment_method" id="<?php echo $payment_method['code']; ?>" value="<?php echo $payment_method['code']; ?>"  checked="checked" />
      <?php } else { ?>
      <input type="radio" name="payment_method" id="<?php echo $payment_method['code']; ?>" value="<?php echo $payment_method['code']; ?>"  />
      <?php } ?>
      <label class="l-float" for="<?php echo $payment_method['code']; ?>">
        <?php if(!empty($payment_method['logo'])) { ?>
      <!--  <img src="<?php echo $payment_method['logo']; ?>" alt="<?php echo $payment_method['title']; ?>">-->
        <?php } ?>
		 <?php if($payment_method['code'] == 'sbacquiring') { ?>
      <!--  <img src="/catalog/view/theme/thai-style/images/d-5.jpg" alt="<?php echo $payment_method['title']; ?>"> -->
        <?php } ?>
        <p>
          <span><?php echo $payment_method['title']; ?></span><br>
          <?php echo $payment_method['descroption']; ?>
        </p>
        <?php if ($payment_method['terms']) { ?>
        (<?php echo $payment_method['terms']; ?>)
        <?php } ?>
      </label>
    </div>
    <?php } ?>
  </div>
</div>
<?php } ?>
<?php if ($text_agree) { ?>
<div class="buttons" style="display: none">
  <div class="pull-right"><?php echo $text_agree; ?>
    <?php if ($agree) { ?>
    <input type="checkbox" name="agree" value="1" checked="checked" />
    <?php } else { ?>
    <input type="checkbox" name="agree" value="1" />
    <?php } ?>
    &nbsp;
    <input type="button" value="<?php echo $button_continue; ?>" id="button-payment-method" class="btn btn-primary" />
  </div>
</div>
<?php } else { ?>
<div class="buttons" style="display: none">
  <div class="pull-right">
    <input type="button" value="<?php echo $button_continue; ?>" id="button-payment-method" class="btn btn-primary" />
  </div>
</div>
<?php } ?>


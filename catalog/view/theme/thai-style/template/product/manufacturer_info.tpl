<?php echo $header; ?>
<?php echo $content_top; ?>
<!-- page -->
<div id="page">
    <div class="wrap">
        <!-- breadcrumbs -->
        <div class="breadcrumbs">
            <?php $end_breadcrumb = array_pop($breadcrumbs); ?>
            <?php foreach ($breadcrumbs as $breadcrumb) { ?>
            <a href="<?php echo $breadcrumb['href']; ?>" class="pathway"><?php echo $breadcrumb['text']; ?></a>
            <?php } ?>
            <span><?php echo $end_breadcrumb['text']; ?></span>
        </div>
        <!-- end.breadcrumbs -->
        <div class="page-content catalog">
            <div class="catalog-wrap">
                <?php echo $column_left; ?>
                <!-- filter-->
                <aside id="shop-filter">
                <?php echo $column_filter; ?>
                </aside>
                  <!-- //filter -->
                <!-- products -->
                <div id="shop-products">
                    <div class="catalog-header">
                        <h1><?php echo $heading_title; ?></h1>
                        <!-- pagination top -->
                        <div class="pagination-top">
                            <?php if(!empty($_GET['limit']) && $_GET['limit'] == 1000) { ?>
                            <span class="pagination-list__title">Посмотреть все</span>
                            <?php } ?>
                            <?php if(!empty($pagination)) { ?>
                            <a href="<?php echo $limits_all ?>" class="show-all-product">Посмотреть все</a>
                            <ul class="pagination-list">
                                <span class="pagination-list__title">По страницам</span>
                                <?php echo $pagination; ?>
                            </ul>
                            <?php } elseif (!empty($_GET['limit']) && $_GET['limit'] == 1000) { ?>
                            <a href="<?php echo $limits_page ?>" class="show-all-product">По страницам</a>
                            <?php } ?>
                        </div>
                    </div>
                        <?php if (!empty($products)) { ?>
                    <div class="row">
                        <?php foreach ($products as $product) { ?>
                        <div class="col-b4 col-m6 col-xs12">
                            <div class="product-item prod-link-<?php echo $product['product_id']; ?>">
                                <?php if ($product['new']) { ?>
                                <span class="product-label new">НОВИНКА</span>
                                <?php } ?>
                                <?php if ($product['hit']) { ?>
                                <span class="product-label hit">ЛИДЕР ПРОДАЖ</span>
                                <?php } ?>
                                <?php if ($product['discounts']) { ?>
                                <span class="product-label">СКИДКА -<?php echo $product['discounts']; ?>%</span>
                                <?php } ?>
                                <div class="product-img"><a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>"></a></div>
                                <a href="<?php echo $product['href']; ?>" class="product-name">
                                    <?php echo $product['name']; ?>
                                </a>
                                <?php if ($product['price']) { ?>
                                <div class="product-price"><span><?php echo $product['price']; ?></span> рублей</div>
                                <?php } ?>
                                <?php if($product['item_wishlist']) { ?>
                                <a href="javascript:void(0);" class="popup fav-link popup-fav in-fav select" onclick="wishlist.remove('<?php echo $product['product_id']; ?>');"><span class="fav-tooltip">В избранном</span></a>
                                <?php } else { ?>
                                <a href="javascript:void(0);" class="popup fav-link popup-fav in-fav" onclick="wishlist.add('<?php echo $product['product_id']; ?>');"><span class="fav-tooltip">В избранное</span></a>
                                <?php } ?>
                                <!-- hover link -->
								<div class="to-cart">
                                    <?php if($product['item_cart']) { ?>
                                    <a href="javascript:void(0);" class="in-cart btn select" onclick="cart.add('<?php echo $product['product_id']; ?>', '<?php echo $product['minimum']; ?>');">Товар в корзине</a>
                                    <?php } else { ?>
                                    <a href="javascript:void(0);" class="in-cart btn" onclick="cart.add('<?php echo $product['product_id']; ?>', '<?php echo $product['minimum']; ?>');">Добавить в корзину</a>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                        <?php } ?>
                    </div>
                        <?php } else { ?>
                        <p>Товаров нет</p>
                        <?php } ?>
                    <?php if (!empty($pagination)) { ?>
                    <div class="pagination-bottom clearfix">
                        <?php if(!empty($_GET['limit']) && $_GET['limit'] == 1000) { ?>
                        <span class="pagination-list__title">Посмотреть все</span>
                        <?php } ?>
                        <?php if(!empty($pagination)) { ?>
                        <a href="<?php echo $limits_all ?>" class="show-all-product">Посмотреть все</a>
                        <ul class="pagination-list">
                            <span class="pagination-list__title">По страницам</span>
                            <?php echo $pagination; ?>
                        </ul>
                        <?php } elseif (!empty($_GET['limit']) && $_GET['limit'] == 1000) { ?>
                        <a href="<?php echo $limits_page ?>" class="show-all-product">По страницам</a>
                        <?php } ?>
                    </div>
                    <?php } ?>
                            <?php if ($description) { ?>
						<div class="col-sm-10"><?php echo $description; ?></div>
						<?php } ?>
                    <?php echo $content_bottom; ?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php echo $footer; ?>
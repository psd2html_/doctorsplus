<?php echo $header; ?>
<?php echo $content_top; ?>
<!------------------------ page -->
<div id="page">
    <div class="wrap">
        <!-- breadcrumbs -->
        <div class="breadcrumbs">
            <?php $end_breadcrumb = array_pop($breadcrumbs); ?>
            <?php foreach ($breadcrumbs as $breadcrumb) { ?>
            <a href="<?php echo $breadcrumb['href']; ?>" class="pathway"><?php echo $breadcrumb['text']; ?></a>
            <?php } ?>
            <span><?php echo $end_breadcrumb['text']; ?></span>
        </div>

        <!-- content -->
        <div class="page-content">
            <div class="main-content">
                <div class="productfull">
                    <!-- mob title -->
                    <div class="productfull__header-mob">
                        <h1><?php echo $heading_title; ?></h1>
                        <span class="short-info">Артикул: <?php echo $model; ?> </span>
                        <span class="short-info">Бренд: <a href="<?php echo $manufacturer_filter_id; ?>"><?php echo $manufacturer; ?></a></span>
                        <div class="prodlogo">
                            <img src="<?php echo $manufacturer_img; ?>" alt="<?php echo $manufacturer; ?>">
                        </div>
                    </div>

                    <div class="row">
                        <!-- images -->
                        <div class="productfull-l col-s12">
                            <div class="productfull__img-wrap">
                                <div class="productfull__img">
                                    <?php if ($p_new) { ?>
                                    <span class="product-label new">НОВИНКА</span>
                                    <?php } ?>
                                    <?php if ($p_hit) { ?>
                                    <span class="product-label hit">ЛИДЕР ПРОДАЖ</span>
                                    <?php } ?>
                                    <?php if (!empty($p_discounts)) { ?>
                                    <span class="product-label">СКИДКА -<?php echo $p_discounts; ?>%</span>
                                    <?php } ?>
                                    <i class="icon-zoom"></i>
                                    <?php foreach ($images as $key=>$image) { ?>
                                    <a href="<?php echo $image['popup']; ?>" class="img-zoom<?php echo ($key == 0 ? ' select' : ''); ?>" id="img<?php echo $key; ?>">
                                        <img oncontextmenu="return false;" class="locked" src="<?php echo $image['big']; ?>" alt="<?php echo $heading_title; ?>">
                                    </a>
                                    <?php } ?>
                                </div>
                                <div class="productfull__thumbs-wrap">
                                    <div class="productfull__thumbs">
                                        <?php foreach ($images as $key=>$image) { ?>
                                        <span class="thumbs-link" data-image="img<?php echo $key; ?>"><img src="<?php echo $image['thumb']; ?>" alt="<?php echo $heading_title; ?>"></span>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- info -->
                        <div class="productfull-r col-s12">
                            <div class="productfull__header">
                                <h1><?php echo $heading_title; ?></h1>
                                <div class="row">
                                    <div class="col-b5">
                                        <span class="short-info">Артикул: <?php echo $model; ?> </span>
                                        <span class="short-info">Бренд: <a href="<?php echo $manufacturer_filter_id; ?>"><?php echo $manufacturer; ?></a></span>
                                    </div>
                                    <div class="col-b7">
                                        <div class="prodlogo">
                                            <img src="<?php echo $manufacturer_img; ?>" alt="<?php echo $manufacturer; ?>">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!-- short desc mob -->
                            <div class="productfull__teaser-mob">
                                <p><?php echo $short_description; ?></p>
                            </div>

                            <!-- price -->
                            <div class="productfull__price">
                                <div class="row">
                                    <?php if ($price) { ?>
                                    <div class="col-b5 col-xs12 prodfull-price">
                                        <span class="price-num" id="current-price"><?php echo $price; ?></span>
                                    </div>
                                    <?php } ?>
                                    <div class="col-b7 col-xs12">
                                        <?php if ($discounts) { ?>
                                            <span class="prodfull-set">
                                            <?php foreach ($discounts as $discount) { ?>
                                                <b id="sale-price" class="data-quantity" data-quantity="<?php echo $discount['quantity']; ?>"><?php echo $discount['price']; ?></b> руб. при покупки <b><?php echo $discount['quantity']; ?></b> шт.
                                            <?php } ?>
                                            </span>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                            <!-- qty -->
                            <div class="productfull__qty">
                                <div class="row">
                                    <div class="col-b4 col-xs6">
                                        <label>Количество:</label>
                                        <div class="prodfull-qty">
                                            <span class="qty-minus count">−</span>
                                            <input type="text" name="quantity" id="quantity" class="inputbox current-qty" value="1">
                                            <input type="hidden" name="product_id" value="<?php echo $product_id; ?>" />
                                            <span class="qty-plus count">+</span>
                                        </div>
                                    </div>
                                    <div class="col-b4 col-xs6 size">
                                    <?php if ($options) { ?>
                                        <?php foreach ($options as $option) { ?>
                                            <?php if ($option['type'] == 'select') { ?>

                                                    <label for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?>:</label>
                                                    <select name="option[<?php echo $option['product_option_id']; ?>]" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control prod-size prodopt">
                                                        <option value="" class="skrit"><?php echo 'Выбрать';?></option>
														<?php foreach ($option['product_option_value'] as $option_value) { ?>
                                                        <option value="<?php echo $option_value['product_option_value_id']; ?>" data-points="<?php echo (isset($option_value['points_value']) ? $option_value['points_value'] : 0); ?>" data-prefix="<?php echo $option_value['price_prefix']; ?>" data-price="<?php echo $option_value['price_value']; ?>"><?php echo $option_value['name']; ?>
                                                        </option>
                                                        <?php } ?>
                                                    </select>

                                            <?php } ?>
                                        <?php } ?>
                                    <?php } ?>
                                    </div>
                                    <div class="col-b4 brand">
                                        <?php if ($p_quantity > 0) { ?>
                                        <span class="in-shop">Товар в наличии</span>
                                        <?php } ?>             
                                    </div>
                                </div>
                            </div>
                            <!-- short desc -->
                            <div class="productfull__teaser">
                                <p><?php echo $short_description; ?></p>
                            </div>
                            <!-- btn -->
                            <div class="productfull__btn clearfix prodpage">
	                            <?php if ($p_quantity > 0) { ?>
                                <button type="button" id="button-cart" onclick="cart.add('<?php echo $product_id; ?>', '1');" data-loading-text="Добавить в корзину" class="btn btn-cart btn-purple to-cart"><span>Добавить в корзину</span></button>
                                <?php } else { ?>
                                 <a href="#preorder" onclick="cart.preorder('<?php echo $product_href; ?>', '<?php echo $heading_title; ?>');" data-loading-text="Сделать предзаказ" class="btn btn-cart btn-flat-green to-cart popup"><span>Сделать предзаказ</span></a>
                                <?php } ?>
                                <?php if(!empty($if_wishlist)) { ?>
                                <button type="button" class="wishlist_<?php echo $product_id; ?> btn btn-fav btn-light in-fav" onclick="wishlist.remove('<?php echo $product_id; ?>');"><span>Добавлено в избранное</span></button>
                                <?php } else { ?>
                                <button type="button" class="wishlist_<?php echo $product_id; ?> btn btn-fav btn-light" onclick="wishlist.add('<?php echo $product_id; ?>');"><span>Добавить в избранное</span></button>
                                <?php } ?>
                            </div>
                        </div>
                    </div>

                    <!-- full info -->
                    <div class="row">
                        <?php if (!empty($description)) { ?>
                        <div class="productfull-l col-s12">
                            <h3 class="b-title">Описание</h3>
                            <div class="productfull__desc">
                                <?php echo $description; ?>
                            </div>
                        </div>
                        <?php } ?>
                        <div class="productfull-r col-s12">
                            <?php if ($attribute_groups) { ?>
                                <?php foreach ($attribute_groups as $attribute_group) { ?>
                                    <?php if($attribute_group['name'] == 'video') { ?>
                                        <div class="productfull__video">
                                            <?php foreach ($attribute_group['attribute'] as $attribute) { ?>
                                            <iframe width="100%" height="315" src="<?php echo $attribute['text']; ?>" frameborder="0" allowfullscreen></iframe>
                                            <?php } ?>
                                        </div>
                                    <?php } else { ?>
                                        <h3 class="b-title"><?php echo $attribute_group['name']; ?></h3>
                                        <div class="b-productfull__spec">
                                            <?php foreach ($attribute_group['attribute'] as $attribute) { ?>
                                            <div class="spec-item">
                                                <!--<b><?php echo $attribute['name']; ?>:</b><br>-->
                                                <?php echo $attribute['text']; ?>
                                            </div>
                                            <?php } ?>
                                        </div>
                                    <?php } ?>
                                <?php } ?>
                            <?php } ?>

                            <?php if ($contraind) { ?>
                                <div class="contraindications">
                                    <p><?php echo $text_contraind; ?></p>
                                </div>
                            <?php } ?>

                        </div>
                    </div>
                    <?php if ($review_status) { ?>
                    <!-- reviews -->
                    <div class="row">
                        <div class="productfull-l col-s12">
                            <h3 class="b-title">Отзывы покупателей (<?php echo $reviews_orig; ?>)</h3>
                            <div id="review" class="productfull__reviews">
                            </div>
                        </div>
                        <div class="productfull-r col-s12">
                            <div class="productfull__add-reviews">
                                <h4>Добавить отзыв</h4>
                                <form action="/">
                                    <?php if ($review_guest) { ?>
                                    <p>
                                        <input type="text" class="inputbox" name="name" id="input-name" placeholder="Имя">
                                        <div id="review_name"></div>
                                    </p>
                                    <p>
                                        <textarea id="input-review" class="inputbox" name="text" placeholder="Текст отзыва"></textarea>
                                        <div id="review_text"></div>
                                    </p>

                                    <div class="add-reviews__btn clearfix">
                                        <div class="rate">
                                            <label>Рейтинг</label>
                                            <div class="rate__stars">
                                                <div id="rate__blank"></div>
                                                <div id="rate__hover"></div>
                                                <div id="rate__votes"></div>
                                            </div>

                                            <input type="radio"  class="rate-star" name="rating" id="star1" value="1" style="display: none;"/>
                                            <input type="radio"  class="rate-star" name="rating" id="star2" value="2" style="display: none;" />
                                            <input type="radio"  class="rate-star" name="rating" id="star3" value="3" style="display: none;" />
                                            <input type="radio"  class="rate-star" name="rating" id="star4" value="4" style="display: none;" />
                                            <input type="radio"  class="rate-star" name="rating" id="star5" value="5" style="display: none;" />
                                        </div>
                                        <button type="button" id="button-review"  data-loading-text="Идёт загрузка комментария" class="btn btn-flat-green">Отправить отзыв</button>
                                    </div>
                                    <?php if ($site_key) { ?>
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <div class="g-recaptcha" data-sitekey="<?php echo $site_key; ?>"></div>
                                        </div>
                                    </div>
                                    <?php } ?>
                                    <?php } else { ?>
                                    <?php echo $text_login; ?>
                                    <?php } ?>
                                </form>
                            </div>

                        </div>
                    </div>
                    <?php } ?>
                </div><!-- //productfull -->
                <?php if ($products) { ?>
                <!-- brand-products -->
                <div class="brand-products">
                    <?php if ($manufacturer) { ?>
                    <h3 class="b-title">Товары бренда <?php echo $manufacturer; ?> </h3>
                    <?php } else { ?>
                    <h3><?php echo $text_related; ?></h3>
                    <?php } ?>
                    <!-- products -->
                    <div id="rel-products">
                        <div class="row">
                            <?php foreach ($products as $product) { ?>
                            <div class="col-b3 col-m4 col-s6 col-xs12">
                                <div class="product-item prod-link-<?php echo $product['product_id']; ?>">
                                    <?php if ($product['new']) { ?>
                                    <span class="product-label new">НОВИНКА</span>
                                    <?php } ?>
                                    <?php if ($product['hit']) { ?>
                                    <span class="product-label hit">ЛИДЕР ПРОДАЖ</span>
                                    <?php } ?>
                                    <?php if ($product['discounts']) { ?>
                                    <span class="product-label">СКИДКА -<?php echo $product['discounts']; ?>%</span>
                                    <?php } ?>
                                    <div class="product-img"><a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>"></a></div>
                                    <a href="<?php echo $product['href']; ?>" class="product-name">
                                        <?php echo $product['name']; ?>
                                    </a>
                                    <div class="product-price"><span><?php echo $product['price']; ?></span> рублей</div>
                                    <?php if($product['item_wishlist']) { ?>
                                    <a href="javascript:void(0);" class="popup fav-link popup-fav in-fav select" onclick="wishlist.remove('<?php echo $product['product_id']; ?>');"><span class="fav-tooltip">В избранном</span></a>
                                    <?php } else { ?>
                                    <a href="javascript:void(0);" class="popup fav-link popup-fav in-fav" onclick="wishlist.add('<?php echo $product['product_id']; ?>');"><span class="fav-tooltip">В избранное</span></a>
                                    <?php } ?>
                                    <!-- hover link -->
                                    <div class="to-cart">
                                        <?php if($product['item_cart']) { ?>
                                        <a href="javascript:void(0);" class="btn in-cart select" onclick="cart.add('<?php echo $product['product_id']; ?>', '<?php echo $product['minimum']; ?>');">Добавлено в в корзину</a>
                                        <?php } else { ?>
                                        <a href="javascript:void(0);" class="btn in-cart" onclick="cart.add('<?php echo $product['product_id']; ?>', '<?php echo $product['minimum']; ?>');">Добавить в корзину</a>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                            <?php } ?>
                        </div>
                    </div><!-- //products -->
                    <div class="all-brand-prod"><a href="<?php echo $manufacturer_filter_id; ?>" class="btn btn-flat-green">Показать все</a></div>
                </div>
                <?php } ?>
            </div>
        </div>
    </div><!-- //wrap -->
</div><!-- //page -->
<script type="text/javascript"><!--
$('select[name=\'recurring_id\'], input[name="quantity"]').change(function(){
	$.ajax({
		url: 'index.php?route=product/product/getRecurringDescription',
		type: 'post',
		data: $('input[name=\'product_id\'], input[name=\'quantity\'], select[name=\'recurring_id\']'),
		dataType: 'json',
		beforeSend: function() {
			$('#recurring-description').html('');
		},
		success: function(json) {
			$('.alert, .text-danger').remove();

			if (json['success']) {
				$('#recurring-description').html(json['success']);
			}
		}
	});
});
//--></script>
<script type="text/javascript"><!--
    function toggleCart(){
        var miniCart = $('.b-topmenu__cart');
        miniCart.toggleClass('hover');
        miniCart.find('.topmenu-dd').fadeToggle(200);
        miniCart.find('.shadow-help').fadeToggle(200);
        miniCart.next('li').toggleClass('next-li');
    }

$('#button-cart').on('click', function() {
	$.ajax({
		url: 'index.php?route=checkout/cart/add',
		type: 'post',
		data: $('#product input[type=\'text\'], #product input[type=\'hidden\'], #product input[type=\'radio\']:checked, #product input[type=\'checkbox\']:checked, #product select, #product textarea'),
		dataType: 'json',
		beforeSend: function() {
			//$('#button-cart').button('loading');
		},
		complete: function() {
			//$('#button-cart').button('reset');
		},
		success: function(json) {
			$('.alert, .text-danger').remove();
			$('.form-group').removeClass('has-error');

			if (json['error']) {
				if (json['error']['option']) {
					for (i in json['error']['option']) {
						var element = $('#input-option' + i.replace('_', '-'));

						if (element.parent().hasClass('input-group')) {
							element.parent().after('<div class="text-danger">' + json['error']['option'][i] + '</div>');
						} else {
							element.after('<div class="text-danger">' + json['error']['option'][i] + '</div>');
						}
					}
				}

				if (json['error']['recurring']) {
					$('select[name=\'recurring_id\']').after('<div class="text-danger">' + json['error']['recurring'] + '</div>');
				}

				// Highlight any found errors
				$('.text-danger').parent().addClass('has-error');
			}

			if (json['success']) {
				//$('.breadcrumb').after('<div class="alert alert-success">' + json['success'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');

				//$('#cart > button').html('<i class="fa fa-shopping-cart"></i> ' + json['total']);
                $('#cart').html(json['html']);
                $('#cart').find('.topmenu-dd').css('opacity','1').hide();

				$('html, body').animate({ scrollTop: 0 }, 'slow');

				$('#cart > ul').load('index.php?route=common/cart/info ul li');

                toggleCart();
                setTimeout(function(){
                    if(!$(".b-topmenu__cart").find('.topmenu-dd').is(":hover")){
                        toggleCart();
                    }
                }, 1500);
			}
		}
	});
});
//--></script>
<script type="text/javascript"><!--
$('.date').datetimepicker({
	pickTime: false
});

$('.datetime').datetimepicker({
	pickDate: true,
	pickTime: true
});

$('.time').datetimepicker({
	pickDate: false
});

$('button[id^=\'button-upload\']').on('click', function() {
	var node = this;

	$('#form-upload').remove();

	$('body').prepend('<form enctype="multipart/form-data" id="form-upload" style="display: none;"><input type="file" name="file" /></form>');

	$('#form-upload input[name=\'file\']').trigger('click');

	if (typeof timer != 'undefined') {
    	clearInterval(timer);
	}

	timer = setInterval(function() {
		if ($('#form-upload input[name=\'file\']').val() != '') {
			clearInterval(timer);

			$.ajax({
				url: 'index.php?route=tool/upload',
				type: 'post',
				dataType: 'json',
				data: new FormData($('#form-upload')[0]),
				cache: false,
				contentType: false,
				processData: false,
				beforeSend: function() {
					$(node).button('loading');
				},
				complete: function() {
					$(node).button('reset');
				},
				success: function(json) {
					$('.text-danger').remove();

					if (json['error']) {
						$(node).parent().find('input').after('<div class="text-danger">' + json['error'] + '</div>');
					}

					if (json['success']) {
						alert(json['success']);

						$(node).parent().find('input').attr('value', json['code']);
					}
				},
				error: function(xhr, ajaxOptions, thrownError) {
					alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
				}
			});
		}
	}, 500);
});
//--></script>
<script type="text/javascript"><!--
$('#review').delegate('.pagination a', 'click', function(e) {
  e.preventDefault();

    //$('#review').fadeOut('slow');

    //$('#review').load(this.href);

    //$('#review').fadeIn('slow');
    $.ajax({
        url: this.href,
        type: 'get',
        dataType: 'html',
        beforeSend: function() {

        },
        complete: function() {
            $('.page_review_' + $(this).data('pagenext')).fadeIn('slow');
        },
        success: function(html) {
            $('.reviews__btn').remove();
            $('#review').after(html);
        }
    });

});

$('#review').load('index.php?route=product/product/review&product_id=<?php echo $product_id; ?>');

$('#button-review').on('click', function() {
	$.ajax({
		url: 'index.php?route=product/product/write&product_id=<?php echo $product_id; ?>',
		type: 'post',
		dataType: 'json',
		data: 'name=' + encodeURIComponent($('input[name=\'name\']').val()) + '&text=' + encodeURIComponent($('textarea[name=\'text\']').val()) + '&rating=' + encodeURIComponent($('input[name=\'rating\']:checked').val() ? $('input[name=\'rating\']:checked').val() : ''),
		beforeSend: function() {
			//$('#button-review').button('loading');
            //$('#button-review').hide();
		},
		complete: function() {
			//$('#button-review').button('reset');
		},
		success: function(json) {
			$('.alert-success, .alert-danger').remove();

			if (json['error']) {
                //$('#button-review').show('slow');
                $.each(json['error'], function( index, element ) {
                    $('#review_' + index).html('<div class="error-panel">' + element + '</div>');
                });
			}

			if (json['success']) {
				$('.productfull__add-reviews form').html('<div class="success-panel">' + json['success'] + '</div>');

				$('input[name=\'name\']').val('');
				$('textarea[name=\'text\']').val('');
				$('input[name=\'rating\']:checked').prop('checked', false);
			}
		}
	});
});

$(document).ready(function() {
    $(document).ready(function () {
        width = Math.max($(window).width(), window.innerWidth);

        //logo width
        $('.prodlogo').width($('.prodfull-set').width());

        //rate
        //var margin_doc = $(".rate__stars").offset();

        function rateStars(){
            var margin_doc = $(".rate__stars").offset();
            $('.rate__stars').hover(function() {
                        $('#rate__hover').show();
                        $('#rate__votes').hide();
                    },
                    function() {
                        $('#rate__votes').show();
                        $('#rate__hover').hide();
                    });
            $(".rate__stars").mousemove(function(e){
                var widht_votes = e.pageX - margin_doc.left;
                if (widht_votes == 0) widht_votes =1 ;
                user_votes = Math.ceil(widht_votes/15);
                $('#rate__hover').width(user_votes*15);
            });

            //rate fixing
            $('.rate__stars').click(function(){
                if(user_votes)
                    $('#star'+user_votes).prop( "checked", true );
                $('#rate__votes').width(user_votes*15).show();
                $('#rate__hover').hide();

            });
        }
        rateStars();

        //thumb
        $('.thumbs-link').on('click', function(){
            var img = $(this).data('image');
            $('.img-zoom').hide();
            $('#'+img).css('display', 'block');
            console.log(img);
        });

        //select
//         $(".prod-size").selectmenu();

        //set change
        var setQty = 3;
        var salePrice = $('#sale-price').text();
        salePrice = salePrice.replace(/\s+/g,'');
        salePrice = parseInt(salePrice);
        var currentPrice = $('#current-price').text();
        currentPrice = currentPrice.replace(/\s+/g,'');
        currentPrice = parseInt(currentPrice);

        $('.prodfull-set').on('click', function(e){
            $('#current-price').text(salePrice);
            var inputbox = $('.current-qty');
            var qty = inputbox.val();
            if((qty%setQty) == 0){
                inputbox.val(parseInt(qty) + setQty);
            }else{
                inputbox.val(setQty);
            }
        });

        //qty change
        jQuery(".prodfull-qty").find(".qty-minus").click(function(){
            var inputbox = jQuery(this).parent().children(".inputbox");
            var value = inputbox.val();
            if(value > 1){
                var res = parseInt(value) - 1;
            }else{
                var res = 1;
            }
            if(value == setQty && salePrice){
                $('#current-price').text(currentPrice);
            }
            inputbox.val(res);
            $('#button-cart').attr('onclick',"cart.add('<?php echo $product_id; ?>', '" + res + "');");
        });
        jQuery(".prodfull-qty").find(".qty-plus").click(function(){
            var inputbox = jQuery(this).parent().children(".inputbox");
            var value = inputbox.val();
            var res = parseInt(value) + 1;
            if(value == setQty-1 && salePrice){
                $('#current-price').text(salePrice);
            }
            inputbox.val(res);
            $('#button-cart').attr('onclick',"cart.add('<?php echo $product_id; ?>', '" + res + "');");
        });

        //resize
        $(window).resize(function(e) {
            rateStars();
        });
    });
});
//--></script>
<script type="text/javascript"><!--
function price_format(n)
{ 
    c = <?php echo (empty($currency['decimals']) ? "0" : $currency['decimals'] ); ?>;
    d = '<?php echo $currency['decimal_point']; ?>'; // decimal separator
    t = ' '; // thousands separator
    s_left = '<?php echo $currency['symbol_left']; ?>';
    s_right = '<?php echo $currency['symbol_right']; ?>';
      
    n = n * <?php echo $currency['value']; ?>;

    //sign = (n < 0) ? '-' : '';

    //extracting the absolute value of the integer part of the number and converting to string
    i = parseInt(n = Math.abs(n).toFixed(c)) + ''; 

    j = ((j = i.length) > 3) ? j % 3 : 0; 
    return s_left + (j ? i.substr(0, j) + t : '') + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : '') + s_right; 
}

function calculate_tax(price)
{
    <?php // Process Tax Rates
      if (isset($tax_rates) && $tax) {
         foreach ($tax_rates as $tax_rate) {
           if ($tax_rate['type'] == 'F') {
             echo 'price += '.$tax_rate['rate'].';';
           } elseif ($tax_rate['type'] == 'P') {
             echo 'price += (price * '.$tax_rate['rate'].') / 100.0;';
           }
         }
      }
    ?>
    return price;
}

function process_discounts(price, quantity)
{
    <?php
      foreach ($dicounts_unf as $discount) {
        echo 'if ((quantity >= '.$discount['quantity'].') && ('.$discount['price'].' < price)) price = '.$discount['price'].';'."\n";
      }
    ?>
    return price;
}


animate_delay = 20;

main_price_final = calculate_tax(<?php echo $price_value; ?>);
main_price_start = calculate_tax(<?php echo $price_value; ?>);
main_step = 0;
main_timeout_id = 0;

function animateMainPrice_callback() {
    main_price_start += main_step;
    
    if ((main_step > 0) && (main_price_start > main_price_final)){
        main_price_start = main_price_final;
    } else if ((main_step < 0) && (main_price_start < main_price_final)) {
        main_price_start = main_price_final;
    } else if (main_step == 0) {
        main_price_start = main_price_final;
    }
    
    $('.autocalc-product-price').html( price_format(main_price_start) );
    
    if (main_price_start != main_price_final) {
        main_timeout_id = setTimeout(animateMainPrice_callback, animate_delay);
    }
}

function animateMainPrice(price) {
    main_price_start = main_price_final;
    main_price_final = price;
    main_step = (main_price_final - main_price_start) / 10;
    
    clearTimeout(main_timeout_id);
    main_timeout_id = setTimeout(animateMainPrice_callback, animate_delay);
}


<?php if ($special) { ?>
special_price_final = calculate_tax(<?php echo $special_value; ?>);
special_price_start = calculate_tax(<?php echo $special_value; ?>);
special_step = 0;
special_timeout_id = 0;

function animateSpecialPrice_callback() {
    special_price_start += special_step;
    
    if ((special_step > 0) && (special_price_start > special_price_final)){
        special_price_start = special_price_final;
    } else if ((special_step < 0) && (special_price_start < special_price_final)) {
        special_price_start = special_price_final;
    } else if (special_step == 0) {
        special_price_start = special_price_final;
    }
    
    $('.autocalc-product-special').html( price_format(special_price_start) );
    
    if (special_price_start != special_price_final) {
        special_timeout_id = setTimeout(animateSpecialPrice_callback, animate_delay);
    }
}

function animateSpecialPrice(price) {
    special_price_start = special_price_final;
    special_price_final = price;
    special_step = (special_price_final - special_price_start) / 10;
    
    clearTimeout(special_timeout_id);
    special_timeout_id = setTimeout(animateSpecialPrice_callback, animate_delay);
}
<?php } ?>


function recalculateprice()
{
    var main_price = <?php echo (float)$price_value; ?>;
    var input_quantity = Number($('input[name="quantity"]').val());
    var special = <?php echo (float)$special_value; ?>;
    var tax = 0;
    
    if (isNaN(input_quantity)) input_quantity = 0;
    
    <?php if ($special) { ?>
        special_coefficient = <?php echo ((float)$price_value/(float)$special_value); ?>;
    <?php } ?>
    main_price = process_discounts(main_price, input_quantity);
    tax = process_discounts(tax, input_quantity);
    
    
    var option_price = 0;
    
    <?php if ($points) { ?>
      var points = <?php echo (float)$points_value; ?>;
      $('input:checked,option:selected').each(function() {
          if ($(this).data('points')) points += Number($(this).data('points'));
      });
      $('.autocalc-product-points').html(points);
    <?php } ?>
    
    $('input:checked,option:selected').each(function() {
      if ($(this).data('prefix') == '=') {
        option_price += Number($(this).data('price'));
        main_price = 0;
        special = 0;
      }
    });
    
    $('input:checked,option:selected').each(function() {
      if ($(this).data('prefix') == '+') {
        option_price += Number($(this).data('price'));
      }
      if ($(this).data('prefix') == '-') {
        option_price -= Number($(this).data('price'));
      }
      if ($(this).data('prefix') == 'u') {
        pcnt = 1.0 + (Number($(this).data('price')) / 100.0);
        option_price *= pcnt;
        main_price *= pcnt;
        special *= pcnt;
      }
      if ($(this).data('prefix') == 'd') {
        pcnt = 1.0 - (Number($(this).data('price')) / 100.0);
        option_price *= pcnt;
        main_price *= pcnt;
        special *= pcnt;
      }
      if ($(this).data('prefix') == '*') {
        option_price *= Number($(this).data('price'));
        main_price *= Number($(this).data('price'));
        special *= Number($(this).data('price'));
      }
    });
    
    special += option_price;
    main_price += option_price;

    <?php if ($special) { ?>
      main_price = special * special_coefficient;
      tax = special;
    <?php } else { ?>
      tax = main_price;
    <?php } ?>
    
    // Process TAX.
    main_price = calculate_tax(main_price);
    special = calculate_tax(special);
    
    // Раскомментировать, если нужен вывод цены с умножением на количество
    //main_price *= input_quantity;
    //special *= input_quantity;
    //tax *= input_quantity;

    // Display Main Price
    animateMainPrice(main_price);
      
    <?php if ($special) { ?>
      animateSpecialPrice(special);
    <?php } ?>
}

$(document).ready(function() {
    $('input[type="checkbox"]').bind('change', function() { recalculateprice(); });
    $('input[type="radio"]').bind('change', function() { recalculateprice(); });
    $('select').bind('change', function() { recalculateprice(); });
    
    $quantity = $('input[name="quantity"]');
    $quantity.data('val', $quantity.val());
    (function() {
        if ($quantity.val() != $quantity.data('val')){
            $quantity.data('val',$quantity.val());
            recalculateprice();
        }
        setTimeout(arguments.callee, 250);
    })();    
    
    recalculateprice();
});

//--></script>
<?php echo $footer; ?>
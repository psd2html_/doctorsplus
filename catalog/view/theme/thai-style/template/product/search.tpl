<?php echo $header; ?>
<?php echo $content_top; ?>
<!-- page -->
<div id="page">
    <div class="wrap">
        <!-- breadcrumbs -->
        <div class="breadcrumbs">
            <?php $end_breadcrumb = array_pop($breadcrumbs); ?>
            <?php foreach ($breadcrumbs as $breadcrumb) { ?>
            <a href="<?php echo $breadcrumb['href']; ?>" class="pathway"><?php echo $breadcrumb['text']; ?></a>
            <?php } ?>
            <span><?php echo $end_breadcrumb['text']; ?></span>
        </div>
        <!-- end.breadcrumbs -->
        <!-- content -->
        <div class="page-content">
            <div id="content" class="main-content">
                <div class="search-header">
                    <h1>Поиск</h1>
                    <form action="/search" class="main-searchform">
                        <input type="text" name="search" id="input-search" value="<?php echo $search; ?>" class="inputbox" placeholder="Поиск">
                        <input type="hidden" name="sub_category" value="1" />
                        <input type="hidden" name="description" value="1" />
                        <input type="button" value="Найти" id="button-search" class="btn-search">
                    </form>
                </div>
                <!-- products -->
                <div id="fav-products">
                    <?php if (!empty($products)) { ?>
                    <div class="row">
                        <?php foreach ($products as $product) { ?>
                        <div class="col-b3 col-m4 col-s6 col-xs12">
                            <div class="product-item prod-link-<?php echo $product['product_id']; ?>">
                                <?php if ($product['new']) { ?>
                                <span class="product-label new">NEW</span>
                                <?php } ?>
                                <?php if ($product['hit']) { ?>
                                <span class="product-label hit">ХИТ</span>
                                <?php } ?>
                                <?php if ($product['discounts']) { ?>
                                <span class="product-label sale">-<?php echo $product['discounts']; ?>%</span>
                                <?php } ?>
                                <div class="product-img"><a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>"></a></div>
                                <a href="<?php echo $product['href']; ?>" class="product-name">
                                    <?php echo $product['name']; ?>
                                </a>
                                <?php if ($product['price']) { ?>
                                <div class="product-price"><span><?php echo $product['price']; ?></span> рублей</div>
                                <?php } ?>
                                <?php if($product['item_wishlist']) { ?>
                                <a href="javascript:void(0);" class="popup-fav in-fav fav-link select" onclick="wishlist.remove('<?php echo $product['product_id']; ?>');"><span class="fav-tooltip">В избранном</span></a>
                                <?php } else { ?>
                                <a href="javascript:void(0);" class="popup-fav in-fav fav-link" onclick="wishlist.add('<?php echo $product['product_id']; ?>');"><span class="fav-tooltip">В избранное</span></a>
                                <?php } ?>
                                <div class="to-cart">
                                    <?php if($product['item_cart']) { ?>
                                    <a href="javascript:void(0);" class="in-cart btn select" onclick="cart.add('<?php echo $product['product_id']; ?>', '<?php echo $product['minimum']; ?>');">Товар в корзине</a>
                                    <?php } else { ?>
                                    <a href="javascript:void(0);" class="in-cart btn" onclick="cart.add('<?php echo $product['product_id']; ?>', '<?php echo $product['minimum']; ?>');">Добавить в корзину</a>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                        <?php } ?>
                    </div>
                    <?php } else { ?>
                    <p class="none-res">По вашему запросу ничего не найдено</p>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript"><!--
$('#button-search').bind('click', function() {
	url = '/search';

	var search = $('#content input[name=\'search\']').prop('value');

	if (search) {

		url += (url == '/search' ? '?' : '&') + 'search=' + encodeURIComponent(search);
	}

	var category_id = $('#content select[name=\'category_id\']').prop('value');

	if (category_id > 0) {
		url += (url == '/search' ? '?' : '&') + 'category_id=' + encodeURIComponent(category_id);
	}

	var sub_category = $('#content input[name=\'sub_category\']:checked').prop('value');

	if (sub_category) {
		url += (url == '/search' ? '?' : '&') + 'sub_category=true';
	}

	var filter_description = $('#content input[name=\'description\']:checked').prop('value');

	if (filter_description) {
		url += (url == '/search' ? '?' : '&') + 'description=true';
	}
    console.log(url);
	location = url;
});

$('#content input[name=\'search\']').bind('keydown', function(e) {
	if (e.keyCode == 13) {
		$('#button-search').trigger('click');
	}
});

$('select[name=\'category_id\']').on('change', function() {
	if (this.value == '0') {
		$('input[name=\'sub_category\']').prop('disabled', true);
	} else {
		$('input[name=\'sub_category\']').prop('disabled', false);
	}
});

$('select[name=\'category_id\']').trigger('change');
--></script>
<?php echo $footer; ?>
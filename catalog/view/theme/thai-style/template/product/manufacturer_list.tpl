<?php echo $header; ?>
<div class="<?php echo $class; ?>"><?php echo $content_top; ?></div>
<div id="page">
    <div class="wrap">
        <div class="breadcrumbs">
            <?php $end_breadcrumb = array_pop($breadcrumbs); ?>
            <?php foreach ($breadcrumbs as $breadcrumb) { ?>
            <a href="<?php echo $breadcrumb['href']; ?>" class="pathway"><?php echo $breadcrumb['text']; ?></a>
            <?php } ?>
            <span><?php echo $end_breadcrumb['text']; ?></span>
        </div>
        <?php echo $column_left; ?>
        <?php if ($column_left && $column_right) { ?>
        <?php $class = 'col-sm-6'; ?>
        <?php } elseif ($column_left || $column_right) { ?>
        <?php $class = 'col-sm-9'; ?>
        <?php } else { ?>
        <?php $class = 'col-sm-12'; ?>
        <?php } ?>
        <div class="page-content">
            <h1><?php echo $heading_title; ?></h1>
            <div class="row brands">
                <?php foreach ($categories as $category) { ?>
                <div class="col-brand">
                    <div class="brand-item">
                        <?php if (!empty($category['image'])) { ?>
                        <img src="<?php echo $category['image']; ?>" alt="<?php echo $category['name']; ?>">
                        <?php } ?>
                        <!-- hover link -->
                        <div class="brand-hover">
                            <a href="<?php echo $category['href']; ?>">
                                <?php echo $category['meta_description']; ?><br /><br />
                                <span class="more">Перейти к товарам</span>
                            </a>
                        </div>
                    </div>
                </div>
                <?php } ?>
            </div>
            <?php echo $content_bottom; ?>
        </div>
        <?php echo $column_right; ?>
    </div>
</div>
<?php echo $footer; ?>
<?php echo $header; ?>
<?php echo $content_top; ?>
<!-- page -->
<div id="page">
    <div class="wrap">
        <!-- breadcrumbs -->
        <div class="breadcrumbs">
            <?php $end_breadcrumb = array_pop($breadcrumbs); ?>
            <?php foreach ($breadcrumbs as $breadcrumb) { ?>
            <a href="<?php echo $breadcrumb['href']; ?>" class="pathway"><?php echo $breadcrumb['text']; ?></a>
            <?php } ?>
            <span><?php echo $end_breadcrumb['text']; ?></span>
        </div>
        <!-- end.breadcrumbs -->
        <div class="page-content sale-products">
            <h1><?php echo $heading_title; ?></h1>
            <div class="row">
                <!-- rightcol -->
                <aside class="col-b3 col-m5 col-s12 sales-menu">
                    <ul class="sales-menu__list">
                        <?php if(!empty($menu)) { ?>
                        <?php foreach($menu as $m) { ?>
                        <li<?php echo ($m['action_id'] == $action_id ? ' class="current"' : ''); ?>><a href="/action/<?php echo $m['alias']; ?>"><?php echo $m['discount']; ?><span><?php echo $m['title']; ?></span></a></li>
                        <?php } ?>
                        <?php } ?>
                    </ul>
                </aside>
                <!-- main content -->
                <div class="col-b9 col-m7 col-s12 b-sale">
                    <div class="main-content">
                        <div class="salefull">
                            <?php echo $description; ?>
                            <?php if(!empty($action_url)) { ?>
                            <p><a href="<?php echo $action_url; ?>" class="btn btn-flat-green">Перейти к товарам акции</a></p>
                            <?php } ?>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php echo $footer; ?>
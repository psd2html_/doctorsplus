<?php echo $header; ?>
<?php echo $content_top; ?>
<!-- page -->
<div id="page">
    <div class="wrap">
        <!-- breadcrumbs -->
        <div class="breadcrumbs">
            <?php $end_breadcrumb = array_pop($breadcrumbs); ?>
            <?php foreach ($breadcrumbs as $breadcrumb) { ?>
            <a href="<?php echo $breadcrumb['href']; ?>" class="pathway"><?php echo $breadcrumb['text']; ?></a>
            <?php } ?>
            <span><?php echo $end_breadcrumb['text']; ?></span>
        </div>
        <!-- end.breadcrumbs -->
        <!-- content -->
        <div class="page-content">
            <h1><?php echo $heading_title; ?></h1>
            <?php if(!empty($all_action)) { ?>
                <?php foreach($all_action as $an) { ?>
                    <div class="row sale-item">
                        <div class="col-b9 col-m8 col-s12">
                            <a href="<?php echo $an['view']; ?>" class="sale-item__img">
                                <img src="<?php echo $an['image']; ?>" alt="<?php echo $an['title']; ?>">
                                <div class="sale-item__title">
                                    <?php if(!empty($an['discount'])) { ?>
                                    <p class="big"><?php echo $an['discount']; ?></p>
                                    <?php } ?>
                                    <?php if(!empty($an['image_title'])) { ?>
                                    <p class="mid"><?php echo $an['image_title']; ?></p>
                                    <?php } ?>
                                    <span class="more">Подробнее →</span>
                                </div>
                            </a>
                        </div>
                        <div class="col-b3 col-m4 col-s12">
                            <div class="sale-item__info">
                                <?php if(!empty($an['period'])) { ?>
                                <div class="sale-item__date">
                                    <h4>Период</h4>
                                    <p class="time"><?php echo $an['period']; ?></p>
                                </div>
                                <?php } ?>
                                <?php if(!empty($an['description'])) { ?>
                                <div class="sale-item__text">
                                    <h4>Условия проведения</h4>
                                    <?php echo $an['description']; ?>
                                </div>
                                <?php } ?>
                                <a href="<?php echo $an['view']; ?>" class="btn btn-flat-green"><?php echo $an['text_mere']; ?></a>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            <?php } else { ?>
            <p>На данный момент нет активных акций. Следите за новостями что бы не пропустить новые акции. </p>
            <?php } ?>
        </div>
    </div>
</div>
<?php echo $footer; ?>
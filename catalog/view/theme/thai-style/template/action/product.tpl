<?php echo $header; ?>
<?php echo $content_top; ?>
<!-- page -->
<div id="page">
    <div class="wrap">
        <!-- breadcrumbs -->
        <div class="breadcrumbs">
            <?php $end_breadcrumb = array_pop($breadcrumbs); ?>
            <?php foreach ($breadcrumbs as $breadcrumb) { ?>
            <a href="<?php echo $breadcrumb['href']; ?>" class="pathway"><?php echo $breadcrumb['text']; ?></a>
            <?php } ?>
            <span><?php echo $end_breadcrumb['text']; ?></span>
        </div>
        <!-- end.breadcrumbs -->
        <div class="page-content sale-products">
            <h1><?php echo $heading_title; ?></h1>
            <div class="row">
                <!-- rightcol -->
                <aside class="col-b3 col-m5 col-s12 sales-menu">
                    <ul class="sales-menu__list">
                        <?php if(!empty($menu)) { ?>
                        <?php foreach($menu as $m) { ?>
                        <li<?php echo ($m['action_id'] == $action_id ? ' class="current"' : ''); ?>><a href="/action/<?php echo $m['alias']; ?>"><?php echo $m['discount']; ?><span><?php echo $m['title']; ?></span></a></li>
                        <?php } ?>
                        <?php } ?>
                    </ul>
                </aside>
                <!-- main content -->
                <div class="col-b9 col-m7 col-s12 b-sale">
                    <div class="main-content">
                        <div class="sale-products">
                            <div class="sale-item__img">
                                <img src="<?php echo $image; ?>" alt="<?php echo $heading_title; ?>">
                                <div class="sale-item__title">
                                    <?php if(!empty($discount)) { ?>
                                    <p class="big"><?php echo $discount; ?></p>
                                    <?php } ?>
                                    <p class="mid"><?php echo $image_title; ?></p>
                                    <span class="more">Подробнее →</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- products -->
            <div id="fav-products">
                <h2>Товары по акции</h2>
                <div class="row">
                    <?php foreach($products as $product) { ?>
                    <div class="col-b3 col-m4 col-s6 col-xs12">
                        <div class="product-item prod-link-<?php echo $product['product_id']; ?>">
                            <?php if ($product['new']) { ?>
                            <span class="product-label new">НОВИНКА</span>
                            <?php } ?>
                            <?php if ($product['hit']) { ?>
                            <span class="product-label hit">ЛИДЕР ПРОДАЖ</span>
                            <?php } ?>
                            <?php if ($product['discounts']) { ?>
                            <span class="product-label">СКИДКА -<?php echo $product['discounts']; ?>%</span>
                            <?php } ?>
                            <div class="product-img"><a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>"></a></div>
                            <a href="<?php echo $product['href']; ?>" class="product-name">
                                <?php echo $product['name']; ?>
                            </a>
                            <?php if ($product['price']) { ?>
                            <div class="product-price"><span><?php echo $product['price']; ?></span> рублей</div>
                            <?php } ?>
                            <?php if($product['item_wishlist']) { ?>
                            <a href="javascript:void(0);" class="popup fav-link popup-fav in-fav select" onclick="wishlist.remove('<?php echo $product['product_id']; ?>');"><span class="fav-tooltip">В избранном</span></a>
                            <?php } else { ?>
                            <a href="javascript:void(0);" class="popup fav-link popup-fav in-fav" onclick="wishlist.add('<?php echo $product['product_id']; ?>');"><span class="fav-tooltip">В избранное</span></a>
                            <?php } ?>
                            <!-- hover link -->
                            <div class="to-cart">
                                <?php if($product['item_cart']) { ?>
                                <a href="javascript:void(0);" class="btn in-cart select" onclick="cart.add('<?php echo $product['product_id']; ?>', '<?php echo $product['minimum']; ?>');">Добавлено в в корзину</a>
                                <?php } else { ?>
                                <a href="javascript:void(0);" class="btn in-cart" onclick="cart.add('<?php echo $product['product_id']; ?>', '<?php echo $product['minimum']; ?>');">Добавить в корзину</a>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                    <?php } ?>
                </div>
            </div>
            <!-- sale desc -->
            <div id="fav-products">
                <h2>Подробности проведения акции</h2>
                <?php echo $description; ?>
            </div>
        </div>
    </div>
</div>
<?php echo $footer; ?>
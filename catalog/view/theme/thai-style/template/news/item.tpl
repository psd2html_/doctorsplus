<?php echo $header; ?>
<!-- page -->
<div id="page">
    <div class="wrap">
        <!-- breadcrumbs -->
        <div class="breadcrumbs breadcrumb">
            <?php $end_breadcrumb = array_pop($breadcrumbs); ?>
            <?php foreach ($breadcrumbs as $breadcrumb) { ?>
            <a href="<?php echo $breadcrumb['href']; ?>" class="pathway"><?php echo $breadcrumb['text']; ?></a>
            <?php } ?>
            <span><?php echo $end_breadcrumb['text']; ?></span>
        </div>
        <!-- end.breadcrumbs -->
        <div class="page-content sales">
            <div class="row">
                <div class="col-m3 col-b3 col-s12 pull-right">
                    <ul class="sales-menu">
                        <?php if(!empty($menu)) { ?>
                            <?php foreach($menu as $m) { ?>
                                <li<?php echo ($m['news_id'] == $news_id ? ' class="current"' : ''); ?>><a href="/action/<?php echo $m['alias']; ?>"><?php echo $m['title']; ?></a></li>
                            <?php } ?>
                        <?php } ?>
                    </ul>
                </div>
                <div class="col-m9 col-b9 col-s12 pull-left">
                    <?php if(!empty($image)) { ?>
                        <div class="current-sale__img">
                            <img src="<?php echo $image; ?>" alt="<?php echo $heading_title; ?>">
                        </div>
                    <?php } else { ?>
                        <h1 class="page-title"><?php echo $heading_title; ?></h1>
                        <?php echo $description; ?>
                    <?php } ?>
                </div>
            </div>
            <?php if(!empty($image)) { ?>
            <div class="current-sale__product">
                <h1 class="page-title">Товары по акции</h1>
                <div class="row">
                    <?php foreach($products as $product) { ?>
                    <div class="col-b3 col-m4 col-s6 col-xs12">
                        <div class="product-item">
                            <?php if ($product['new']) { ?>
                            <span class="product-label new">NEW</span>
                            <?php } ?>
                            <?php if ($product['hit']) { ?>
                            <span class="product-label hit">ХИТ</span>
                            <?php } ?>
                            <?php if ($product['discounts']) { ?>
                            <span class="product-label sale">-<?php echo $product['discounts']; ?>%</span>
                            <?php } ?>
                            <div class="product-name">
                                <?php echo (!empty($product['manufacturer']) ? $product['manufacturer'] . ' - ' : '') . $product['name'] . ($product['width'] > 0 ? ' ' . $product['width'] . ' мл'  : ''); ?>
                            </div>
                            <div class="product-img"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>"></div>
                            <?php if ($product['price']) { ?>
                            <div class="product-price"><?php echo $product['price']; ?> <span class="rub">д</span></div>
                            <?php } ?>
                            <!-- hover link -->
                            <div class="product-link prod-link-<?php echo $product['product_id']; ?>">
                                <?php if($product['item_wishlist']) { ?>
                                <a href="javascript:void(0);" onclick="wishlist.remove('<?php echo $product['product_id']; ?>');" class="in-fav select"><span>Удалить</span></a>
                                <?php } else { ?>
                                <a href="javascript:void(0);" onclick="wishlist.add('<?php echo $product['product_id']; ?>');" class="in-fav"><span>В избранное</span></a>
                                <?php } ?>
                                <a href="javascript:void(0);" onclick="cart.add('<?php echo $product['product_id']; ?>', '<?php echo $product['minimum']; ?>');" class="in-cart<?php if($product['item_cart']) { echo ' select'; } ?>"><span>Добавить в корзину</span></a>
                                <a href="<?php echo $product['href']; ?>" class="more"><span>Подробнее о продукте</span></a>
                            </div>
                        </div>
                    </div>
                    <?php } ?>
                </div>
            </div>
            <div class="current-sale__desc">
                <?php if(!empty($description)) { ?>
                <h2>Подробности проведения акции</h2>
                <?php echo $description; ?>
                <?php } ?>
            </div>
            <?php } ?>
            <p><a href="<?php echo $link_action; ?>" class="btn">Перейти на страницу акции</a></p>
        </div>
    </div>
</div>
<?php echo $footer; ?>
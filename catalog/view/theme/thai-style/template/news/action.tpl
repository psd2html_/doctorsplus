<?php echo $header; ?>
<!-- page -->
<div id="page">
    <div class="wrap">
        <!-- breadcrumbs -->
        <div class="breadcrumbs breadcrumb">
            <?php $end_breadcrumb = array_pop($breadcrumbs); ?>
            <?php foreach ($breadcrumbs as $breadcrumb) { ?>
            <a href="<?php echo $breadcrumb['href']; ?>" class="pathway"><?php echo $breadcrumb['text']; ?></a>
            <?php } ?>
            <span><?php echo $end_breadcrumb['text']; ?></span>
        </div>
        <!-- end.breadcrumbs -->
        <h1 class="page-title"><?php echo $heading_title; ?></h1>
        <div class="page-content sales">
            <?php if(!empty($all_news)) { ?>
                <?php foreach($all_news as $an) { ?>
                    <div class="row sale-item">
                        <div class="col-m9 col-s12">
                            <?php if(!empty($an['image'])) { ?>
                            <div class="sale-item__img">
                                <a href="<?php echo $an['view']; ?>"><img src="<?php echo $an['image']; ?>" alt="<?php echo $an['title']; ?>"></a>
                                <div class="sale-item__title"><?php echo $an['image_title']; ?></div>
                            </div>
                            <?php } ?>
                        </div>
                        <div class="col-m3 col-s12">
                            <div class="sale-item__info">
                                <?php if(!empty($an['period'])) { ?>
                                <div class="sale-item__date">
                                    <h4>Период</h4>
                                    <p><?php echo $an['period']; ?></p>
                                </div>
                                <?php } ?>
                                <?php if(!empty($an['description'])) { ?>
                                <div class="sale-item__text">
                                    <h4>Условия проведения</h4>
                                    <?php echo $an['description']; ?>
                                </div>
                                <?php } ?>
                                <a href="<?php echo $an['view']; ?>" class="btn">Перейти к акции</a>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            <?php } else { ?>
            <p>На данный момент нет активных акций. Следите за новостями что бы не пропустить новые акции. </p>
            <?php } ?>
        </div>
    </div>
</div>
<?php echo $footer; ?>
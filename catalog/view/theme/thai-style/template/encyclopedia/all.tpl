<?php echo $header; ?>
<?php echo $content_top; ?>
<!-- page -->
<div id="page">
    <div class="wrap">
        <!-- breadcrumbs -->
        <div class="breadcrumbs">
            <?php $end_breadcrumb = array_pop($breadcrumbs); ?>
            <?php foreach ($breadcrumbs as $breadcrumb) { ?>
            <a href="<?php echo $breadcrumb['href']; ?>" class="pathway"><?php echo $breadcrumb['text']; ?></a>
            <?php } ?>
            <span><?php echo $end_breadcrumb['text']; ?></span>
        </div>
        <!-- end.breadcrumbs -->
        <!-- content -->
        <div class="page-content">
            <h2 class="blog-title">Энциклопедия</h2>
            <div class="row">
                <!-- main content -->
                <div class="col-b9 col-m12">
                    <div class="main-content blog">
                        <?php if(!empty($all_news)) { ?>
                            <?php foreach($all_news as $an) { ?>
                            <a href="<?php echo $an['view']; ?>" class="blog-item">
                                <?php if(!empty($an['image'])) { ?>
                                <div class="blog-item__img">
                                    <img src="<?php echo $an['image']; ?>" alt="<?php echo $an['title']; ?>">
                                </div>
                                <?php } ?>
                                <div class="blog-item__teaser">
                                    <h3 class="title"><?php echo $an['title']; ?></h3>
                                    <p><?php echo $an['description']; ?></p>
                                    <?php if(!empty($an['tags'])) { ?>
                                        <div class="tags"><span>
                                        <?php echo implode('</span>,<span>', $an['tags']) ?>
                                        <span></div>
                                    <?php } ?>
                                    <span class="more">Подробнее →</span>
                                </div>
                            </a>
                            <?php } ?>
                        <?php } else { ?>
                        <p>На данный момент нет активных акций. Следите за новостями что бы не пропустить новые акции. </p>
                        <?php } ?>
                    </div>
                </div>
                <!-- rightcol -->
                <aside class="col-b3 col-m4 rightcol">
                    <?php if(!empty($tags)) { ?>
                    <div class="rightside-box">
                        <h4 class="rightside-box__title">Алфавитный указатель</h4>
                        <?php
                            function mb_strcasecmp($str1, $str2, $encoding = null) {
                                if (null === $encoding) { $encoding = mb_internal_encoding(); }
                                return strcmp(mb_strtoupper($str1, $encoding), mb_strtoupper($str2, $encoding));
                            }

                            $hide = true;
                            foreach($tags as $t) {
                                if ((mb_strcasecmp('л', ltrim($t['name'])) < 0) && $hide) {
                                    echo '<div id="view_all" style="display:none">';
                                    $hide = false; 
                                }
                                echo '<a href="/encyclopedia/tag/' . $t['alias'] . '" class="abc-link fleter">' . $t['name'] . '</a><br />'; 
                            }
                            if (!$hide){
                                echo '</div>';
                                echo '<p><a href="#" class="open-all" id="show-all">Показать все</a></p>';
                            }
                        ?>
                        <script type="text/javascript">
                            jQuery('#show-all').on('click', function(){
                                if (jQuery(this).text() == 'Показать все') {
                                    jQuery('#view_all').fadeIn();
                                    jQuery(this).text('Свернуть');
                                }else{
                                    jQuery('#view_all').fadeOut();
                                    jQuery(this).text('Показать все');
                                }
                                return false;
                            })
                        </script>
                    </div>
                    <?php } ?>

                    <?php if(!empty($menu)) { ?>
                    <div class="rightside-box">
                        <h4 class="rightside-box__title">Категории</h4>
                        <?php foreach($menu as $m) { ?>
                        <a href="/encyclopedia/<?php echo $m['alias']; ?>" class="cat-link"><?php echo $m['title']; ?></a>
                        <?php } ?>
                    </div>
                    <?php } ?>

                    <?php if(!empty($latest)) { ?>
                    <div class="rightside-box last-items">
                        <h4 class="rightside-box__title">Последние добавленные</h4>
                        <?php foreach($latest as $l) { ?>
                        <a href="/encyclopedia/<?php echo $l['alias']; ?>.html" class="last-link"><?php echo $l['title']; ?></a>
                        <?php } ?>
                    </div>
                    <?php } ?>
                </aside>
            </div>
        </div>
    </div>
</div>
<?php echo $footer; ?>
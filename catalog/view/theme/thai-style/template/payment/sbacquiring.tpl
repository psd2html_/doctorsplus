
<div class="buttons">
  <div class="pull-right">
    <input type="button" value="<?php echo $button_confirm; ?>" id="button-confirm" class="btn btn-flat-green btn-wide" />
  </div>
</div>
<?php if ($btnlater){ ?><div class="pull-left"><input class="paylater btn btn-secondary" type="button" value="<?php echo $button_later; ?>" id="button-pay"  style="float:right;"/></div> <?php } ?>
<script type="text/javascript"><!--
$('#button-confirm').bind('click', function() {
	$.ajax({ 
		type: 'get',
		<?php if (!isset($notcreate)) { ?>
		url: 'index.php?route=payment/sbacquiring/confirm',
		<?php } ?>
		success: function() {
			location = '<?php echo $pay_url; ?>';
		}		
	});
});
<?php if ($btnlater){ ?>
$('#button-pay').bind('click', function() {
	$.ajax({ 
		type: 'get',
		url: 'index.php?route=payment/sbacquiring/confirmLater',
		success: function() {
			location = '<?php echo $payment_url; ?>';
		}		
	});
});
<?php } ?>
//--></script>
<?php echo $header; ?>
<?php echo $content_top; ?>
<!-- page -->
<div id="page">
    <div class="wrap">
        <!-- breadcrumbs -->
        <div class="breadcrumbs">
            <?php $end_breadcrumb = array_pop($breadcrumbs); ?>
            <?php foreach ($breadcrumbs as $breadcrumb) { ?>
            <a href="<?php echo $breadcrumb['href']; ?>" class="pathway"><?php echo $breadcrumb['text']; ?></a>
            <?php } ?>
            <span><?php echo $end_breadcrumb['text']; ?></span>
        </div>
        <!-- end.breadcrumbs -->
        <!-- content -->
        <div class="page-content">
            <!-- main content -->
            <div class="main-content page404">
                <h1>ОШИБКА 404 СТРАНИЦА НЕ НАЙДЕНА</h1>
                <p><a href="<?php echo $producthref; ?>" class="btn green-wide">перейти в каталог</a></p>
            </div>
        </div>
    </div>
</div>
<?php echo $footer; ?>
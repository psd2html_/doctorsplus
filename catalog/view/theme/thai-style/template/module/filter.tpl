<div class="leftside-filter">
    <form action="/">
        <div class="b-filter price-select">
            <p class="b-filter__title">Цена в рублях</p>
            <div class="b-filter__price">
                от
                <input type="text" name="fprice_from" id="fprice_from" value="<?php echo $value_min_max['min']; ?>"  />
                до
                <input type="text" name="fprice_to" id="fprice_to" value="<?php echo $value_min_max['max']; ?>"  />
                <div class="price-slider__num">
                    <span class="price-from"><?php echo $value_min_max['min']; ?></span>
                    <span class="price-to"><?php echo $value_min_max['max']; ?></span>
                </div>
                <div id="price-slider"></div>
            </div>
        </div>

        <div class="b-filter sort-select">
            <p class="b-filter__title b-filter__toggle">Сортировка</p>
            <div class="b-filter__value">
                <input type="radio" name="sort" id="fs1-1" value="p.new&order=DESC"<?php if ($url_sort == 'p.new' && $url_order == 'DESC') { echo ' checked ';} ?>/> <label for="fs1-1">Новые сверху</label><br/>
                <input type="radio" name="sort" id="fs1-2" value="p.price&order=DESC"<?php if ($url_sort == 'p.price' && $url_order == 'DESC') { echo ' checked ';} ?>/> <label for="fs1-2">Цены по убыванию</label><br/>
                <input type="radio" name="sort" id="fs1-3" value="p.price&order=ASC"<?php if ($url_sort == 'p.price' && $url_order == 'ASC') { echo ' checked ';} ?>/> <label for="fs1-3">Цены по возрастанию</label><br/>
            </div>
        </div>

        <?php foreach ($filter_groups as $filter_group) { ?>
        <?php if($filter_group['name'] == 'Категории') {
                $s_name = 'cat-';
            } elseif ($filter_group['name'] == 'Бренды') {
                $s_name = 'brand-';
            } else {
                $s_name = '';
            }
            ?>
        <div class="b-filter <?php echo $s_name; ?>select">
            <p class="b-filter__title  b-filter__toggle"><?php echo $filter_group['name']; ?></p>
            <div class="b-filter__value" style="">
                <?php foreach ($filter_group['filter'] as $filter) { ?>
                <div class="value-wrap">
                <input type="checkbox" name="filter[]" id="f<?php echo $filter_group['filter_group_id']; ?>-<?php echo $filter['filter_id']; ?>" value="<?php echo $filter['filter_id']; ?>" <?php if(in_array($filter['filter_id'], $filter_category)) { echo ' checked '; } ?> />
                <label for="f<?php echo $filter_group['filter_group_id']; ?>-<?php echo $filter['filter_id']; ?>"><?php echo $filter['name']; ?></label><br/></div>
                <?php } ?>
                
                <?php if($filter_group['name'] == 'Бренды') { ?>
                  <span class="show-all">Показать все бренды</span>
                <?php } ?>
            </div>
        </div>
        <?php } ?>
        
        <div class="b-filter__btn">
            <input type="button" class="btn" id="button-filter" value="Применить">
            <a href="/products" class="reset-btn fltr">Сбросить фильтр</a>
        </div>
    </form>
</div>
<script>
    $('#button-filter').on('click', function() {
        filter = [];

        $('input[name^=\'filter\']:checked').each(function(element) {
            filter.push(this.value);
        });

        sort = '';
        val_sort = $('input:radio[name=sort]:checked').val();

        if (val_sort) {
            sort = '&sort=' + val_sort;
        }

        priceFrom = '';
        priceFormValue = $('input:text[name=fprice_from]').val();
        priceFrom = '&fprice_from=' + priceFormValue;

        priceTo = '';
        priceToValue = $('input:text[name=fprice_to]').val();
        priceTo = '&fprice_to=' + priceToValue;

        location = '<?php echo $action; ?>' + ( filter.length > 0 ? '&filter=' + filter.join(',') : '') + sort + priceFrom + priceTo;
    });

    $(window).load(function() {
        //slider
        var left1 = $('.ui-slider-handle').eq(0).position().left;
        $('.price-from').css('left', left1 - 10);

        var left2 = $('.ui-slider-handle').eq(1).position().left;
        $('.price-to').css('left', left2 - 27);
    });
    $(document).ready(function () {
        //slider in filter
        $( "#price-slider" ).slider({
            range: true,
            min: 100,
            max: <?php echo $min_max['max']; ?>,
        values: [ <?php echo $value_min_max['min']; ?>, <?php echo $value_min_max['max']; ?> ],
            slide: function( event, ui ) {
                jQuery( "#fprice_from" ).val( ui.values[ 0 ]);
                var left1 = $('.ui-slider-handle').eq(0).position().left;
                $('.price-from').text(ui.values[ 0 ]).css('left', left1 - 10);

                jQuery( "#fprice_to" ).val( ui.values[ 1 ]);
                var left2 = $('.ui-slider-handle').eq(1).position().left;
                $('.price-to').text(ui.values[ 1 ]).css('left', left2 - 27);
            }
        });

        $(window).resize(function(e) {
            var left1 = $('.ui-slider-handle').eq(0).position().left;
            $('.price-from').css('left', left1 - 10);

            var left2 = $('.ui-slider-handle').eq(1).position().left;
            $('.price-to').css('left', left2 - 27);
        });
    });
</script>

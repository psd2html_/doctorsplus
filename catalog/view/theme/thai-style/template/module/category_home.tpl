<!------------------------ catalog -->
<div class="mainpage-cat">
    <div class="wrap">
        <div class="row">
            <?php foreach ($categories as $key=>$category) { ?>
            <div class="mainpage-cat__item">
                <a href="<?php echo $category['href']; ?>" class="mainpage-cat__link cat<?php echo $category['category_id']; ?>">
                    <i class="icon icon<?php echo $category['category_id']; ?>"></i>
                    <span class="mainpage-cat__title"><?php echo $category['name']; ?></span>
                </a>
            </div>
            <?php } ?>
        </div>
    </div>
</div><!-- //catalog -->
<nav class="leftmenu">
    <ul class="leftmenu__list">
        <?php $url = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH); ?>
        <?php if (!$logged) { ?>
        <li><a href="<?php echo $login; ?>"><?php echo $text_login; ?></a></li>
        <li><a href="<?php echo $register; ?>"><?php echo $text_register; ?></a></li>
        <li><a href="<?php echo $forgotten; ?>"><?php echo $text_forgotten; ?></a></li>
        <?php } ?>
        <?php if ($logged) { ?>
        <li<?php if($url == '/edit-account') { echo ' class="current"';} ?>><a href="<?php echo $edit; ?>"><?php echo $text_edit; ?></a></li>
        <?php } ?>
        <li<?php if($url == '/address-book') { echo ' class="current"';} ?>><a href="<?php echo $address; ?>"><?php echo $text_address; ?></a></li>
        <li<?php if($url == '/order-history') { echo ' class="current"';} ?>><a href="<?php echo $order; ?>"><?php echo $text_order; ?></a></li>
        <li<?php if($url == '/wishlist') { echo ' class="current"';} ?>><a href="<?php echo $wishlist; ?>"><?php echo $text_wishlist; ?></a></li>
        <?php if ($logged) { ?>
        <!--<li><a href="<?php echo $logout; ?>" class="list-group-item"><?php echo $text_logout; ?></a></li>-->
        <?php } ?>
    </ul>
</nav>

<nav class="leftmenu">
    <ul class="leftmenu__list">
                <?php foreach ($categories as $cat) { ?>
                <li><a<?php echo ($cat['blank'] == 1 ? ' target="_blank" ' : ''); ?> href="<?php echo $cat['href']; ?>"><?php echo $cat['name']; ?></a></li>
                <?php } ?>
                <li><a href="<?php echo $faq; ?>">Вопрос-ответ</a></li>
                <li><a href="<?php echo $contact; ?>">Контакты</a></li>
				
    </ul>
</nav>
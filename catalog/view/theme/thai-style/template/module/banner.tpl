<!-- main slider -->
<div class="mainslider-wrap">
    <div id="mainslider" class="mainslider-items">
        <?php foreach ($banners as $banner) { ?>
        <div class="mainslider__item">
            <div class="mainslider__parent">
                <a href="<?php echo $banner['link']; ?>" class="mainslider__text">
                    <p class="big"><?php echo $banner['title']; ?></p>
                    <p class="mid"><?php echo $banner['description']; ?></p>
                    <span class="more">Подробнее →</span>
                </a>
            </div>
            <img class="mainslider__img img_responsive" src="<?php echo $banner['image']; ?>" alt="<?php echo $banner['title']; ?>">
        </div>
        <?php } ?>
    </div>
    <div style="clear: both; float:none"></div>
    <!-- nav -->
    <div class="mainslider__nav">
        <span class="mainslider-prev"></span>
        <span class="mainslider-next"></span>
    </div>
</div>
<script type="text/javascript"><!--
    $(document).ready(function () {
        //slider
        $('#mainslider').carouFredSel({
            responsive: true,
            items: {
                visible: 1
            },
            scroll: {
                fx: "crossfade",
                duration: 1200,
                items:1
            },
            swipe: {
                onTouch: true
            },
            auto: true,
            prev: '.mainslider-prev',
            next: '.mainslider-next',
            height: 340,
            circular: true,
            onCreate: onCreate
        });

        function onCreate() {
            $(window).on('resize', onResize).trigger('resize');
        };
        function onResize() {
            var height = $('.mainslider__img').height();
            $('.mainslider-wrap, caroufredsel_wrapper, #mainslider, .mainslider__item').css('height', height + 'px');
        };

        $('.mainslider__img').on('load', function(){
            onResize();
        });
        //owl partners
        $('.partner-slider').owlCarousel({
            loop:true,
            responsive:{
                0:{
                    items:1,
                    margin:0
                },
                500:{
                    items:2
                },
                600:{
                    items:3
                },
                750:{
                    items:4
                }
            }
        });
    });
--></script>

<!-- cat menu for mob -->
<div class="b-catmenu">
    <!-- category list -->
    <div class="mob-catmenu-toggle">Выберите пункт меню<i class="toggle-icon"></i></div>
    <nav class="catmenu-wrap">
        <ul class="catmenu__links">
            <?php foreach ($categories as $category) { ?>
            <li><a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a></li>
            <?php } ?>
        </ul>
    </nav>
</div>
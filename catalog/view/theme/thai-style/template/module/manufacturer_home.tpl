<!------------------------ partners -->
<div class="b-partners">
    <div class="wrap">
        <div class="partners-wrap">
            <h3>Наши партнеры</h3>
            <div class="partner-slider">
                <?php foreach($data as $d) { ?>
                <div class="partner-item">
                    <a href="http://doctorsplus.ru/brands"><img src="<?php echo $d['image']; ?>" alt="<?php echo $d['name']; ?>"></a>
                </div>
                <?php } ?>
            </div>
        </div>
    </div>
</div>
<!-- //partners -->
    <nav class="catmenu-wrap">
        <ul class="catmenu__links">
            <?php foreach ($categories as $category) { ?>
            <li><a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a></li>
            <?php } ?>
        </ul>
    </nav>
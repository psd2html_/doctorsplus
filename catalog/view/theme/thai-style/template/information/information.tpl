<?php echo $header; ?>
<?php echo $content_top; ?>
<!-- page -->
<div id="page">
    <div class="wrap">
        <!-- breadcrumbs -->
        <div class="breadcrumbs">
            <?php $end_breadcrumb = array_pop($breadcrumbs); ?>
            <?php foreach ($breadcrumbs as $breadcrumb) { ?>
            <a href="<?php echo $breadcrumb['href']; ?>" class="pathway"><?php echo $breadcrumb['text']; ?></a>
            <?php } ?>
            <span><?php echo $end_breadcrumb['text']; ?></span>
        </div>
        <!-- end.breadcrumbs -->
        <div class="page-content">
            <div class="row">
                <?php echo $column_left; ?>
                <?php if ($column_left && $column_right) { ?>
                <?php $class = 'col-m4 col-b6 col-xs12'; ?>
                <?php } elseif ($column_left || $column_right) { ?>
                <?php $class = 'col-m8 col-b9 col-xs12'; ?>
                <?php } else { ?>
                <?php $class = 'col-m12 col-b12 col-xs12'; ?>
                <?php } ?>
                <div id="content" class="<?php echo $class; ?>">
                    <div class="main-content about">
                        <h1><?php echo $heading_title; ?></h1>
                        <?php echo $description; ?>
                    </div>
                    <?php echo $content_bottom; ?>
                </div>
                <?php echo $column_right; ?>

            </div>
        </div>
    </div>
</div>
<?php echo $footer; ?>
<?php echo $header; ?>
<?php echo $content_top; ?>
<!-- page -->
<div id="page">
    <div class="wrap">
        <!-- breadcrumbs -->
        <div class="breadcrumbs">
            <?php $end_breadcrumb = array_pop($breadcrumbs); ?>
            <?php foreach ($breadcrumbs as $breadcrumb) { ?>
            <a href="<?php echo $breadcrumb['href']; ?>" class="pathway"><?php echo $breadcrumb['text']; ?></a>
            <?php } ?>
            <span><?php echo $end_breadcrumb['text']; ?></span>
        </div>
        <!-- end.breadcrumbs -->
        <div class="page-content">
            <div class="row">
                <?php echo $column_left; ?>
                <?php if ($column_left && $column_right) { ?>
                <?php $class = 'col-m4 col-b6 col-xs12'; ?>
                <?php } elseif ($column_left || $column_right) { ?>
                <?php $class = 'col-m8 col-b9 col-xs12'; ?>
                <?php } else { ?>
                <?php $class = 'col-m12 col-b12 col-xs12'; ?>
                <?php } ?>
                <div id="content" class="<?php echo $class; ?>">
                    <div class="main-content contact">
                        <h1><?php echo $heading_title; ?></h1>
                        <div class="row">
                            <div class="c-col-r c-col">
                                <div class="content-border">
                                    <?php echo $content_bottom; ?>
                                </div>
                            </div>
                            <div class="c-col-l c-col">
                                <h5>Обратная связь</h5>
                                <div class="contact-form">
                                    <form action="<?php echo $action; ?>" method="post" class="clearfix">
                                    <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" class="contact-form clearfix">
                                        <p><label for="name">Имя</label>
                                            <input type="text" name="name" value="<?php echo $name; ?>" id="input-name" class="form-control inputbox" />
                                            <?php if ($error_name) { ?>
                                            <div class="error-panel"><?php echo $error_name; ?></div>
                                            <?php } ?>
                                        </p>
                                        <p><label for="name">Эл. почта</label>
                                            <input type="text" name="email" value="<?php echo $email; ?>" id="input-email" class="form-control inputbox" />
                                            <?php if ($error_email) { ?>
                                            <div class="error-panel"><?php echo $error_email; ?></div>
                                            <?php } ?>
                                        </p>
                                        <p><label for="name">Текст сообщения</label>
                                            <textarea name="enquiry" rows="10" id="input-enquiry" class="form-control inputbox"><?php echo $enquiry; ?></textarea>
                                            <?php if ($error_enquiry) { ?>
                                            <div class="error-panel"><?php echo $error_enquiry; ?></div>
                                            <?php } ?>
                                        </p>
                                        <input type="submit" value="Отправить" class="btn btn-flat-green">
                                    </form>
                                </div>
                            </div>
                        </div>
                        <?php if ($geocode) { ?>
                        <!-- map -->
                        <h5>Где мы находимся </h5>
                        <div class="contact-map">
                            <div id="map">
                                <script type="text/javascript" charset="utf-8" async src="https://api-maps.yandex.ru/services/constructor/1.0/js/?sid=PtIeFLgjxqH5lr2Tr6v-HpGtG14zEzME&width=100%&height=400&lang=ru_RU&sourceType=constructor&scroll=true"></script>
                            </div>
                        </div>
                        <?php } ?>
                    </div>
                </div>
                <?php echo $column_right; ?>

            </div>
        </div>
    </div>
</div>
<?php echo $footer; ?>

<?php echo $header; ?>
<?php echo $content_top; ?>
<!-- page -->
<div id="page">
  <div class="wrap">
    <!-- breadcrumbs -->
    <div class="breadcrumbs">
      <?php $end_breadcrumb = array_pop($breadcrumbs); ?>
      <?php foreach ($breadcrumbs as $breadcrumb) { ?>
      <a href="<?php echo $breadcrumb['href']; ?>" class="pathway"><?php echo $breadcrumb['text']; ?></a>
      <?php } ?>
      <span><?php echo $end_breadcrumb['text']; ?></span>
    </div>
    <!-- end.breadcrumbs -->
    <div class="page-content">
      <div class="row">
        <?php echo $column_left; ?>
        <div class="col-b9 col-m8 col-s12">
          <div class="main-content">
            <h1><?php echo $heading_title; ?></h1>
            <div class="content-border">
              <p>В этом разделе вы можете найти ответы на многие вопросы, касающиеся работы нашего сайта. Если вы не нашли интересующей вас информации, то можете отправить нам запрос с помощью формы обратной связи.</p>
              <div class="b-faq">
                <?php foreach ($all_faq as $faq) { ?>
                <div class="faq-item">
                  <div class="faq-header"><?php echo $faq['question']; ?></div>
                  <div class="faq-text">
                    <?php echo $faq['answer']; ?>
                  </div>
                </div>
                <?php } ?>
              </div>
            </div>
            <!-- add question form -->
            <div class="add-form">
              <?php if(!empty($send)) { ?>
              <h4>Вопрос отправлен</h4>
              <p class="success-panel">Спасибо за Ваш вопрос! Мы постараемся связаться с Вами и ответить на Ваш вопрос в ближайшее время.</p>
              <?php } else { ?>
              <h4>Добавить вопрос</h4>
              <p class="ps">* Все поля обязательны для заполнения</p>
              <form action="<?php echo $action; ?>" method="post" class="faq-form">
                <div class="row">
                  <div class="faq-col col-b6 col-m12">
                    <label for="name"><span>*</span>Имя</label>
                    <?php if ($error_name) { ?>
                    <div class="error-panel"><?php echo $error_name; ?></div>
                    <?php } ?>
                    <input type="text" name="name" id="name" class="inputbox" value="<?php echo $name; ?>">
                    <div class="form-row">
                      <div class="col-b6 col-xs12">
                        <label for="email"><span>*</span>Эл. почта</label>
                        <?php if ($error_email) { ?>
                        <div class="error-panel"><?php echo $error_email; ?></div>
                        <?php } ?>
                        <input type="text" name="email" id="email" class="inputbox" value="<?php echo $email; ?>">
                      </div>
                      <div class="col-b6 col-xs12">
                        <label for="phone"><span>*</span>Телефон</label>
                        <?php if ($error_phone) { ?>
                        <div class="error-panel"><?php echo $error_phone; ?></div>
                        <?php } ?>
                        <input type="text" name="phone" id="phone" class="inputbox" value="<?php echo $phone; ?>">
                      </div>
                    </div>
                  </div>
                  <div class="faq-col col-b6 col-m12">
                    <label for="enquiry"><span>*</span>Текст сообщения</label>
                    <?php if ($error_enquiry) { ?>
                    <div class="error-panel"><?php echo $error_enquiry; ?></div>
                    <?php } ?>
                    <textarea id="enquiry" name="enquiry" class="inputbox"><?php echo $enquiry; ?></textarea>
                  </div>
                </div>
                <div class="row">
                  <div class="faq-col faq-btn col-b12">
                    <input type="submit" value="Отправить" class="btn btn-flat">
                  </div>
                </div>
              </form>
              <?php } ?>
            </div>
          </div>
        </div>
        <?php echo $column_right; ?>

      </div>
    </div>
  </div>
</div>
<?php echo $footer; ?>
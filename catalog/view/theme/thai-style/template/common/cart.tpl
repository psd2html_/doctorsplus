<div class="dd-toggle header-info__minicart btn">
    <?php if ((int)$count_total > 0) { ?>
    <span class="minicart-qty"><?php echo $count_total; ?></span>
    <?php } ?>
    <span class="minicart-label">Корзина</span>

    <!-- minicart -->
    <div class="dd-box">
        <div class="dd-box-inside">
            <?php if ($products || $vouchers) { ?>
            <div class="minicart__scroll">
                <?php foreach ($products as $product) { ?>
                <div class="minicart-item">
                    <?php if ($product['thumb']) { ?>
                    <div class="minicart-img">
                        <a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>"></a>
                    </div>
                    <?php } ?>
                    <div class="minicart-info">
                        <p class="minicart-prod"><?php echo $product['name']; ?> </p>
                        <span class="minicart-price"><?php echo $product['quantity']; ?> х <?php echo $product['price']; ?>руб.</span>
                        <span class="minicart-price">= <?php echo $product['total']; ?> руб.</span>
                    </div>
                </div>
                <?php } ?>
            </div>
            <div class="minicart__check">ИТОГО: <span><?php echo $price_total; ?> руб.</span></div>
            <a href="<?php echo $cart; ?>" class="btn btn-flat minicart-link">Просмотреть корзину</a>
            <?php } else { ?>
            <div class="minicart__check"><?php echo $text_empty; ?></div>
            <?php } ?>
        </div>
    </div>
</div>

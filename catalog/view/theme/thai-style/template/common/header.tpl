<!DOCTYPE html>
<!--[if lt IE 9]> <html class="no-js ie8"> <![endif]-->
<!--[if IE 9]><html class="no-js ie9"><![endif]-->
<head>
    <meta charset="utf-8">
    <title><?php echo $title; ?></title>
    <base href="<?php echo $base; ?>" />
    <?php if ($description) { ?>
    <meta name="description" content="<?php echo $description; ?>" />
    <?php } ?>
    <?php if ($keywords) { ?>
    <meta name="keywords" content= "<?php echo $keywords; ?>" />
    <?php } ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="author" content="back-end Valentin Rasulov, artdevue.com && front-end developer Alena Tretiakova, lemweb.ru">

    <!-- favicon.ico -->
    <link rel="shortcut icon" href="catalog/view/theme/thai-style/favicon.ico">
    <?php if ($icon) { ?>
    <link href="<?php echo $icon; ?>" rel="icon" />
    <?php } ?>

    <!-- soc meta -->
    <meta property="og:site_name" content="Докторам и пациентам">
    <meta property="og:type" content="website">
    <meta property="og:keywords" content="<?php echo !empty($keywords) ? $keywords : ''; ?>">
    <link rel="image_src" href="#">
    <meta property="og:image" content="#">

    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->

    <!-- fonts -->
    <link href='http://fonts.googleapis.com/css?family=PT+Sans:400,700' rel='stylesheet' type='text/css'>

    <?php foreach ($links as $link) { ?>
    <link href="<?php echo $link['href']; ?>" rel="<?php echo $link['rel']; ?>" />
    <?php } ?>
    <!-- СSS here -->
    <link rel="stylesheet" href="catalog/view/theme/thai-style/css/jquery-ui.min.css">
    <link rel="stylesheet" href="catalog/view/theme/thai-style/css/jquery.fancybox.css">
    <link rel="stylesheet" href="catalog/view/theme/thai-style/css/owl.carousel.css">
    <link rel="stylesheet" href="catalog/view/theme/thai-style/css/main.css">
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <?php foreach ($styles as $style) { ?>
    <link href="<?php echo $style['href']; ?>" type="text/css" rel="<?php echo $style['rel']; ?>" media="<?php echo $style['media']; ?>" />
    <?php } ?>
    <script src="catalog/view/theme/thai-style/js/jquery-1.11.0.min.js"></script>
    <script src="catalog/view/theme/thai-style/js/jquery-ui.min.js"></script>
    <script src="catalog/view/javascript/common.js" type="text/javascript"></script>
    <?php foreach ($scripts as $script) { ?>
    <script src="<?php echo $script; ?>" type="text/javascript"></script>
    <?php } ?>
    <script src="catalog/view/theme/thai-style/js/jquery.ui.touch-punch.min.js"></script>
    <?php echo $google_analytics; ?>
    <script>
	    $(document).ready(function () {
		   		$(document).on('click', '.productfull__img a', function() {
			   		setTimeout(function(){
				   		$('.fancybox-image').noContext();
				   		$('.fancybox-image').noDrag();
	    			}, 700);
	    		});
	    		$('.mainslider-wrap').noContext();
	    		$('.mainslider-wrap').noDrag();
	    		
	    		$('.productfull__teaser').noCopy();
	    		$('.productfull__desc').noCopy();
	    		$('.b-productfull__spec').noCopy();
	    		$('#shop-products .col-sm-10').noCopy();
	    		$('.fullpost').noCopy();
	    		$('.fullpost').noContext();
	    		
	    		$('.product-img').noContext();
	    		$('.product-img').noDrag();
	    	
	  
	    		$('.productfull__img').noContext();
	    		$('.productfull__img').noDrag();
	    	
	    	
	    		$('.thumbs-link').noContext();
	   			$('.thumbs-link').noDrag();
			
		});
    </script>
</head>
<body>
<!------------------------ toppanel -->
<div class="toppanel">
    <div class="wrap">
        <!-- topmenu -->
        <nav class="topmenu">
            <div class="topmenu-mob-toggle"><span>Дополнительное меню</span><i class="arrow arrow-blue"></i></div>
            <ul class="topmenu__list">
                <?php foreach ($categories as $cat) { ?>
                <li><a<?php echo ($cat['blank'] == 1 ? ' target="_blank" ' : ''); ?> href="<?php echo $cat['href']; ?>"><?php echo $cat['name']; ?></a></li>
                <?php } ?>
                <li><a href="<?php echo $faq; ?>"><?php echo $text_faq; ?></a></li>
                <li><a href="<?php echo $contact; ?>">Контакты</a></li>
                <li><a href="<?php echo $news; ?>">Энциклопедия</a></li>
            </ul>
        </nav>

        <!-- search -->
        <div class="mob-search-toggle"><span></span></div>
        <div class="b-search">
            <form id="search" class="search-form" action="/search">
                <!--<input type="hidden" name="route" value="product/search">-->
                <h4>Поиск по каталогу</h4>
                <?php echo $search; ?>
            </form>
        </div>
    </div>
</div>
<script Language="JavaScript">
<!--
function show_hide(value)
{
    document.getElementById('element').style.display = value;
}
-->
</script>

<!------------------------ header -->
<header>
    <div class="wrap">
        <div class="b-headergroup clearfix">
            <!-- logo -->
            <div class="b-logo">
                <a href="<?php echo $home; ?>" class="logo" title="<?php echo $name; ?>"></a>
                <div class="logo-text"><?php echo $logotext; ?></div>
            </div>

            <!-- info -->
            <div class="b-header-info">
                <div class="header-info__contact clearfix">
                    <?php if(!empty($telephone)) { ?>
                    <div class="contact-col tel">
                        <span>Телефон для справок</span>
                        <a href="tel:<?php echo $telephone; ?>"><?php echo $telephone; ?></a>
                    </div>
                    <?php } ?>
                    <div class="contact-col skype">
                        <span>Мы в скайпе</span>
                        <a href="skype:contact-dp?call">contact-dp</a>
                    </div>
                </div>

                <!-- header dropdown -->
                <div class="header-info__dd clearfix">
                    <!-------- topmenu user -->
                    <div class="dd-col user">
                        <div class="dd-toggle header-info__user">
                            <span>Личный кабинет</span><i class="arrow"></i>

                            <div class="dd-box">
                                <div class="dd-box-inside">
                                    <?php if ($logged) { ?>
                                    <p class="user-name"><?php echo $fullname; ?></p>
                                    <ul class="dd-menu" role="menu">
                                        <li><a href="<?php echo $account; ?>">Персональные данные</a></li>
                                        <li><a href="<?php echo $address; ?>">Адреса доставки</a></li>
                                        <li><a href="<?php echo $order; ?>">История заказов</a></li>
                                        <li><a href="<?php echo $wishlist; ?>">Избранное</a></li>
                                        <li><a href="<?php echo $logout; ?>" class="logout">Выйти</a></li>
                                    </ul>
                                    <?php } else { ?>
                                    <form action="/" class="login-form loginpop">
                                        <p>
                                            <input type="email" class="inputbox pop_email" id="email" placeholder="Логин / Электронная почта">
                                        </p>
                                        <p>
                                            <input type="password" class="inputbox pop_password" id="name" placeholder="Пароль">
                                        </p>
                                        <div class="perror"></div>
                                        <p>
                                            <input type="button" class="btn btn-flat-green" value="Войти">
                                            <a href="#passwords" class="lost-pass popup">Забыли пароль?</a>
                                        </p>
                                    </form>
                                    <div class="b-reg">
                                        <p>Еще не зарегистрированы?</p>
                                        <a href="#reg" class="reg-link popup">Зарегистрироваться</a>
                                    </div>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div><!-- //topmenu user -->

                    <!-------- topmenu minicart -->
                    <div id="cart" class="dd-col cart">
                        <?php echo $cart; ?>
                    </div><!-- //topmenu minicart -->

                </div>
            </div>
        </div>
    </div>
</header>
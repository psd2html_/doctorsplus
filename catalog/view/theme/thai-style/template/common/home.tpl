<?php echo $header; ?>
<div class="<?php echo $class; ?>"><?php echo $content_top; ?></div>
<?php if (!empty($slider_left) && !empty($slider_right)) { ?>
<!-- b-slider -->
<div class="b-slider">
    <div class="wrap">
        <div class="row">
            <?php echo $slider_left; ?>
            <?php echo $slider_right; ?>
        </div>
    </div><!-- //wrap -->
</div><!-- //b-slider -->
<?php } ?>
<?php echo $column_left; ?>
<?php if ($column_left && $column_right) { ?>
<?php $class = 'col-sm-6'; ?>
<?php } elseif ($column_left || $column_right) { ?>
<?php $class = 'col-sm-9'; ?>
<?php } else { ?>
<?php $class = 'col-sm-12'; ?>
<?php } ?>
<div id="content" class="<?php echo $class; ?>"><?php echo $content_bottom; ?></div>
<?php echo $column_right; ?>
<?php echo $footer; ?>
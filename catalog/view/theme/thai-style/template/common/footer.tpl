<!------------------------ footer -->
<footer>
    <div class="wrap">
        <div class="row">
            <div class="footermenu">
                <h4>Каталог</h4>
                <ul class="footermenu__list">
                    <?php foreach ($categories as $cat) { ?>
                    <li><a<?php echo ($cat['blank'] == 1 ? ' target="_blank" ' : ''); ?> href="<?php echo $cat['href']; ?>"><?php echo $cat['name']; ?></a></li>
                    <?php } ?>
                </ul>
            </div>
            <div class="footermenu">
                <h4>О компании</h4>
                <ul class="footermenu__list">
                    <?php foreach ($menus as $cat) { ?>
                    <li><a<?php echo ($cat['blank'] == 1 ? ' target="_blank" ' : ''); ?> href="<?php echo $cat['href']; ?>"><?php echo $cat['name']; ?></a></li>
                    <?php } ?>
                    <li><a href="<?php echo $contact; ?>">Контакты</a></li>
                    <li><a href="<?php echo $encyclopedia; ?>">Энциклопедия</a></li>
                    <li><a href="<?php echo $faq; ?>"><?php echo $text_faq; ?></a></li>
                </ul>

                <ul class="footer-contact" itemscope itemtype="http://schema.org/Organization">
                    <li class="tel"><a href="tel:<?php echo $telephone; ?>" itemprop="telephone"><?php echo $telephone; ?></a></li>
                    <li class="skype"><a href="skype:contact-dp?call">contact-dp</a></li>
                    <li class="email"><a href="mailto:<?php echo $email; ?>" itemprop="email"><?php echo $email; ?></a></li>
                </ul>
            </div>
            <div class="footer-info">
                <div class="b-payway">
                    <span class="label">Способы оплаты:</span>
                    <a href="http://doctorsplus.ru/shipping">
					<span class="payway__item cash"><i>cash</i></span>
					<span class="payway__item visa"><i>visa</i></span>
					<span class="payway__item mc"><i>mastercard</i></span>
				</a>
                </div>
                <div class="b-consult">
                    <h3>КОНСУЛЬТАЦИЯ СПЕЦИАЛИСТОВ</h3>
                    <div class="consutl__text">По любым вопросам о товарах интернет-магазина, о процессе регистрации на сайте или оформлении заказа.</div>
                    <a href="#callback" class="popup btn green-wide consutl__btn">задать вопрос</a>
                </div>
            </div>
        </div>
    </div>
</footer>

<!-- copyright -->
<div class="copyright">
    <div class="wrap">
        <div class="copyrule">Copyright © <?php echo date('Y'); ?></div>
        <ul class="social">
		<li class="facebook"><a href="<?php echo $facebook; ?>" target="_blank"><i class="fa fa-facebook-official" aria-hidden="true"></i></a></li>
		<li class="twitter"><a href="<?php echo $twitter; ?>" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
		<li class="vkontakte"><a href="<?php echo $vkontakte; ?>" target="_blank"><i class="fa fa-vk" aria-hidden="true"></i></a></li>
          <li class="instagram"><a href="<?php echo $instagram; ?>" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
        </ul>
        <div class="created-by">
            <a target="_blank" href="http://artdevue.com">Разработка сайта</a>
        </div>
    </div>
</div>

<div class="popup-help callback" id="callback">
    <div class="popup-header">Задать вопрос специалисту</div>
    <form id="callback_form">
        <div class="popup-content">
            <div class="perror"></div>
            <p class="ps">* Все поля обязательны для заполнения</p>
            <p><label for="c_name"><span>*</span>Имя</label>
                <input type="text" name="name" class="inputbox" id="c_name">
            </p>
            <div class="row half">
                <div class="col-b6 col-s12">
                    <p><label for="c_email">Эл. почта</label>
                        <input type="email" name="email" class="inputbox" id="c_email">
                    </p>
                </div>
                <div class="col-b6 col-s12">
                    <p><label for="c_phone"><span>*</span>Телефон</label>
                        <input type="text" class="inputbox" name="phone" id="c_phone">
                    </p>
                </div>
            </div>
            <label for="c_mess"><span>*</span>Текст сообщения</label>
            <textarea name="mess" id="c_mess" class="inputbox"></textarea>
            <div class="b-btn clearfix">
                <div class="popup-error"></div>
                <input type="button" class="btn btn-flat vprsp" value="Отправить">
            </div>
        </div>
    </form>
</div>
<script type="text/javascript"><!--
    $('#callback').on('click','input[type="button"]',function() {
        var cform = $('#callback_form');
        var formData = cform .serializeArray();
        $.ajax({
            url: '/callback',
            type: 'post',
            dataType: 'json',
            data: formData,
            cache: false,
            beforeSend: function() {
                $('.perror').html('');
                cform.find('.error').removeClass('error');
                cform.find('.error-tipss').remove();
            },
            success: function(json) {
                if (json['error']) {
                    $.each(json['error'], function( index, element ) {
                        cform.find('[name=\'' + index + '\']').addClass('error').before('<span class="error-tipss">' + element + '</span>');
                    });
                }
                if (json['errors']) {
                    cform.find('.perror').html('<div class="error-panel">' + json['errors'] + '</div>');
                }
                if (json['success']) {
                    cform.find('.popup-content').html('<p class="text-center">Ваше сообщение отправлено!</p>');
                }
            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    });
//--></script>

<div class="popup-help preorder" id="preorder">
    <div class="popup-header">Предзаказ</div>
    <form id="preorder_form">
        <div class="popup-content">
            <div class="perror"></div>
            <p class="ps">* Все поля обязательны для заполнения</p>
            <div class="row half">
                <div class="col-b6 col-s12">
                    <p><label for="p_name">Имя</label>
                        <input type="text" name="name" class="inputbox" id="p_name">
                    </p>
                </div>
                <div class="col-b6 col-s12">
                    <p><label for="p_phone"><span>*</span>Телефон</label>
                        <input type="text" class="inputbox" name="phone" id="p_phone">
                        <span class="under-input">Телефон нужен только для связи менеджера с вами</span>
                    </p>
                </div>
            </div>
            <label for="p_mess">Комментарий</label>
            <textarea name="mess" id="p_mess" class="inputbox"></textarea>
            <div class="b-btn clearfix">
                <div class="popup-error"></div>
                <input type="hidden" name="href" id="p_href" value="">
                <input type="hidden" name="product_name" id="p_product_name" value="">
                <input type="button" class="btn btn-flat vprsp" value="Отправить">
            </div>
        </div>
    </form>
</div>
<script type="text/javascript"><!--
    $('#preorder').on('click','input[type="button"]',function() {
        var pform = $('#preorder_form');
        var formData = pform .serializeArray();
        $.ajax({
            url: '/index.php?route=common/preorder',
            type: 'post',
            dataType: 'json',
            data: formData,
            cache: false,
            beforeSend: function() {
                $('.perror').html('');
                pform.find('.error').removeClass('error');
                pform.find('.error-tipss').remove();
            },
            success: function(json) {
                if (json['error']) {
                    $.each(json['error'], function( index, element ) {
                        pform.find('[name=\'' + index + '\']').addClass('error').before('<span class="error-tipss">' + element + '</span>');
                    });
                }
                if (json['errors']) {
                    pform.find('.perror').html('<div class="error-panel">' + json['errors'] + '</div>');
                }
                if (json['success']) {
                    pform.find('.popup-content').html('<p class="text-center">Ваше сообщение отправлено!</p>');
                }
            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    });
//--></script>
<?php if (!$logged) { ?>
<!-- popups -->
<div class="popup-help reg" id="reg">
    <div class="popup-header">Регистрация</div>
    <div class="popup-content">
        <div class="row wide">
            <div class="col-b6 col-s12">
                <p><label for="firstname">Имя:</label>
                    <input type="text" name="firstname" class="inputbox" id="firstname">
                    <span class="error-tipss error_firstname"></span>
                </p>
                <div class="row half">
                    <div class="col-b6 col-s12">
                        <p><label for="password">Пароль:</label>
                            <input type="password" name="password" class="inputbox" id="password">
                            <span class="error-tipss error_password"></span>
                        </p>
                    </div>
                    <div class="col-b6 col-s12">
                        <p><label for="confirm">Повторный пароль:</label>
                            <input type="password" name="confirm" class="inputbox" id="confirm">
                            <span class="error-tipss error_confirm"></span>
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-b6 col-s12">
              <span class="error-tipss error_warning"></span>
                <p><label for="email">Электронная почта:</label>
                    <input type="email" name="email" class="inputbox" id="email">
                    <span class="error-tipss error_email"></span>
                </p>
                <p><label for="telephone">Телефон:</label>
                    <input type="text" name="telephone" class="inputbox" id="telephone">
                    <span class="under-input">Нужен только для оповещения о статусах вашего заказа (СМС) или для связи с менеджером.</span>
                    <span class="error-tipss error_telephone"></span>
                </p>
            </div>
        </div>
        <div class="b-btn clearfix">
            <div class="popup-error"></div>
            <input type="button" class="btn btn-flat-green" value="Зарегистрироваться">
        </div>
    </div>
</div>

<div class="popup-help password" id="passwords">
    <div class="popup-header">Забыли пароль?</div>
    <div class="popup-content">
        <p>Введите свой адрес электронной почты ниже и мы отправим Вам новый пароль</p>
        <p><label for="pemail">Электронная почта</label>
            <input type="email" class="inputbox" id="pemail">
        </p>
        <div class="perror"></div>
        <input type="button" class="btn btn-flat-green btn-wide" value="Вышлите мне ссылку на смену пароля">
    </div>
</div>


<script type="text/javascript"><!--
    $('#reg').on('click','input[type="button"]',function() {
        var loginName = $('#reg').find('#firstname').val();
        var loginEmail = $('#reg').find('#email').val();
        var loginPhone = $('#reg').find('#telephone').val();
        var loginPassword = $('#reg').find('#password').val();
        var loginCPassword = $('#reg').find('#confirm').val();
        $.ajax({
            url: '<?php echo $register; ?>',
            type: 'post',
            dataType: 'json',
            data: {firstname: loginName, email: loginEmail, telephone: loginPhone, password: loginPassword, confirm: loginCPassword, ajax: 1},
            cache: false,
            beforeSend: function() {
                //$('#button-login').button('loading');
                $('#reg').find('input.error').removeClass('error');
                $('.error-tipss').html('');
            },
            success: function(json) {

                if (json['error']) {

                    $.each(['firstname','email','telephone','password','warning'], function( index, value ) {
                        $('#reg').find('.error_' + index).html('');
                    });

                    $.each(json['error'], function( index, value ) {
                        $('#reg').find('.error_' + index).html(value);
                        $('#reg').find('input#' + index).addClass('error');
                    });
                }

                if (json['success']) {
                    $('#reg').find('.popup-content').html('<p class="text-center">Добро пожаловать и благодарим Вас за регистрацию!</p>');
                    setTimeout(function() {
                                //location.reload();
                                window.location = "/edit-account"
                            }, 3000
                    );
                }
            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    });

    $('#passwords').on('click','input[type="button"]',function() {
        var loginEmail = $('#passwords').find('#pemail').val();
        $.ajax({
            url: '<?php echo $action_forgot; ?>',
            type: 'post',
            dataType: 'json',
            data: {email: loginEmail, ajax: 1},
            cache: false,
            beforeSend: function() {
                $('.error-tips').remove();
                $('#pemail').removeClass('error');
            },
            success: function(json) {

                if (json['error']) {
                    $('#passwords').find('#pemail').addClass('error').before('<div class="error-tips">' + json['error'] + '</div>');
                }

                if (json['success']) {
                    $('#passwords').find('.popup-content').html('<p class="text-center">На Ваш e-mail адрес выслано письмо для восстановления пароля.</p>');
                }
            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    });

    loginpop();

    function loginpop() {
        $('.loginpop').on('click','input[type="button"]',function() {
		  this.disabled = true;
		  setTimeout(function() {
            $('input[type="button"]').removeAttr("disabled");      
			}, 1000);
            var itlog = $(this).parents('.loginpop');
            var loginName = itlog.find('.pop_email').val();
            var loginPas = itlog.find('.pop_password').val();
            $.ajax({
                url: '<?php echo $action_login; ?>',
                type: 'post',
                dataType: 'json',
                data: {email: loginName, password: loginPas, ajax: 1},
                cache: false,
                success: function(json) {

                    if (json['error']) {
                        itlog.find('.perror').html('<div class="error-panel">' + json['error'] + '</div>');
                    }

                    if (json['success']) {
                        itlog.find('.popup-content').html('<p class="text-center">Вы удачно вошли в систему сайта!<br/>Удачных покупок!</p>');
                        setTimeout(function() {
                                    location.reload();
                                }, 500
                        );

                    }
                },
                error: function(xhr, ajaxOptions, thrownError) {
                    alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                }
            });
        });
    }
    //--></script>
<?php } ?>
<div class="popup-help fav" id="add_wishlist">
    <div class="popup-header">Товар добавлен в Избранное.</div>
    <div class="popup-content">
        <div class="row wide">
<?php if (!$logged) { ?>
            <div class="col-b7 col-s12 loginpop">
                <p style="margin-bottom: 40px;">Чтобы посмотреть добавленные в Избранное товары, Вам необходимо авторизоваться или зарегистрироваться.</p>
                <p><label for="l_name">Логин / e-mail:</label>
                    <input type="text" class="inputbox pop_email" id="l_name">
                </p>
                <p><label for="l_password">Пароль:</label>
                    <input type="password" class="inputbox pop_password" id="l_password">
                </p>
                <div class="perror"></div>
                <div class="row b-btn">
                    <div class="col-b4 col-xs6 f-right">
                        <a href="#reg" class="reg-link popup">Регистрация</a>
                    </div>
                    <div class="col-b4 col-xs6">
                        <input type="button" class="btn btn-flat-green" value="Войти">
                    </div>
                    <div class="col-b4 col-xs12">
                        <a href="#password" class="lost-pass popup">Забыли пароль?</a>
                    </div>
                </div>
            </div>
            <div class="col-b5 prod-card">
                <div class="product-item">
                    <div class="product-img fav-img"></div>
                    <div class="product-name wsl">
                        Кольпоскоп МК-200 с цифровой видеосистемой
                    </div>
                    <div class="product-price"><span>5490</span> рублей</div>
                </div>
            </div>
<?php } else { ?>
            <div class="col-b6 prod-card">
                <div class="product-item">
                    <div class="product-img fav-img"></div>
                    <div class="product-name wsl">
                        Кольпоскоп МК-200 с цифровой видеосистемой
                    </div>
                    <div class="product-price"><span>5490</span> рублей</div>
                </div>
            </div>
<?php } ?>
        </div>
    </div>
</div>
<div class="popup-help fav" id="fav">
    <div class="popup-header">Удаление из избранного</div>
    <div class="popup-content">
        <div class="row">
            <div class="product-delete"></div>
        </div>
    </div>
</div>
<!-- script here -->
<script src="catalog/view/theme/thai-style/js/jquery.placeholder.min.js"></script>
<script src="catalog/view/theme/thai-style/js/jquery.easing.1.3.min.js"></script>
<script src="catalog/view/theme/thai-style/js/owl.carousel.min.js"></script>
<script src="catalog/view/theme/thai-style/js/jquery.touchSwipe.min.js"></script>
<script src="catalog/view/theme/thai-style/js/jquery.fancybox.pack.js"></script>
<script src="catalog/view/theme/thai-style/js/jquery.carouFredSel-6.2.1-packed.js"></script>
<script src="catalog/view/theme/thai-style/js/main.js"></script>
</body>
</html>
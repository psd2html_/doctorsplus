<?php echo $header; ?>
<?php echo $content_top; ?>
<!-- page -->
<div id="page">
    <div class="wrap">
        <!-- breadcrumbs -->
        <div class="breadcrumbs breadcrumb">
            <?php $end_breadcrumb = array_pop($breadcrumbs); ?>
            <?php foreach ($breadcrumbs as $breadcrumb) { ?>
            <a href="<?php echo $breadcrumb['href']; ?>" class="pathway"><?php echo $breadcrumb['text']; ?></a>
            <?php } ?>
            <span><?php echo $end_breadcrumb['text']; ?></span>
        </div>
        <!-- end.breadcrumbs -->
        <?php if ($success) { ?>
        <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?></div>
        <?php } ?>
        <?php if ($error_warning) { ?>
        <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?></div>
        <?php } ?>
        <div class="page-content">
            <div class="checkout-confirm">
                <div class="row">
                    <div class="col-b12 col-m12 col-s12">
                        <h1><?php echo $heading_title; ?></h1>
                        <p><?php echo $text_message; ?></p>
                        <div class="buttons user-btn">
                            <div class="pull-right"><a href="<?php echo $continue; ?>" class="btn btn-primary">Перейти на Главную</a></div>
                        </div>
                    </div>
                    <div class="col-b1 col-m0"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php echo $footer; ?>
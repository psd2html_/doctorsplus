<?php echo $header; ?>
<?php echo $content_top; ?>
<!-- page -->
<div id="page">
    <div class="wrap">
        <!-- breadcrumbs -->
        <div class="breadcrumbs breadcrumb">
            <?php $end_breadcrumb = array_pop($breadcrumbs); ?>
            <?php foreach ($breadcrumbs as $breadcrumb) { ?>
            <a href="<?php echo $breadcrumb['href']; ?>" class="pathway"><?php echo $breadcrumb['text']; ?></a>
            <?php } ?>
            <span><?php echo $end_breadcrumb['text']; ?></span>
        </div>
        <!-- end.breadcrumbs -->
        <?php if ($success) { ?>
        <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?></div>
        <?php } ?>
        <?php if ($error_warning) { ?>
        <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?></div>
        <?php } ?>
        <div class="page-content">
            <div class="row">
                <?php echo $column_left; ?>
                <div class="col-m8 col-b9 col-s12">
                    <div class="main-content">
                        <h1>Избранное</h1>
                        <?php if ($products) { ?>
                        <div id="fav-products">
                            <div class="row">
                                <?php foreach ($products as $product) { ?>
                                <div class="col-b4 col-m6 col-xs12">
                                    <div class="product-item prod-link-<?php echo $product['product_id']; ?> wslspr">
                                        <?php if ($product['new']) { ?>
                                        <span class="product-label new">NEW</span>
                                        <?php } ?>
                                        <?php if ($product['hit']) { ?>
                                        <span class="product-label hit">ХИТ</span>
                                        <?php } ?>
                                        <?php if ($product['discounts']) { ?>
                                        <span class="product-label sale">-<?php echo $product['discounts']; ?>%</span>
                                        <?php } ?>
                                        <div class="product-img"><a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>"></a></div>
                                        <a href="#" class="product-name">
                                            <?php echo $product['name']; ?>
                                        </a>
                                        <?php if ($product['price']) { ?>
                                        <div class="product-price"><span><?php echo $product['price']; ?></span> рублей</div>
                                        <?php } ?>
                                        <a href="javascript:void(0);" class="popup-fav in-fav fav-link select" onclick="wishlist.remove('<?php echo $product['product_id']; ?>');"><span class="fav-tooltip">В избранном</span></a>

                                        <!-- hover link -->
                                        <div class="to-cart">
                                            <a href="javascript:void(0);" class="btn in-cart<?php if($product['item_cart']) { echo ' select'; } ?>"  onclick="cart.add('<?php echo $product['product_id']; ?>');" title="Добавить в корзину">Добавить в корзину</a>
                                        </div>
                                    </div>
                                </div>
                                <?php } ?>
                            </div>
                        </div>
                        <?php } else { ?>
                        <p>Нет избранных продуктов</p>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php echo $footer; ?>
<div class="row">
    <?php foreach ($products as $product) { ?>
    <div class="col-b3 col-m4 col-s6 col-xs12">
        <div class="product-item">
            <span class="product-label">СКИДКА -25%</span>
            <div class="product-img"><a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['image']; ?>" alt="<?php echo $product['name']; ?>"></a></div>
            <a href="#" class="product-name">
                <?php echo $product['name']; ?>
            </a>
            <div class="product-price"><span><?php echo $product['quantity']; ?> x <?php echo $product['price']; ?> </span> рублей</div>
        </div>
    </div>
    <?php } ?>
</div>
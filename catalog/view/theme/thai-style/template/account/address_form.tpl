<?php echo $header; ?>
<!-- page -->
<div id="page">
    <div class="wrap">
        <!-- breadcrumbs -->
        <div class="breadcrumbs breadcrumb">
            <?php $end_breadcrumb = array_pop($breadcrumbs); ?>
            <?php foreach ($breadcrumbs as $breadcrumb) { ?>
            <a href="<?php echo $breadcrumb['href']; ?>" class="pathway"><?php echo $breadcrumb['text']; ?></a>
            <?php } ?>
            <span><?php echo $end_breadcrumb['text']; ?></span>
        </div>
        <!-- end.breadcrumbs -->

        <h1 class="page-title"><?php echo $heading_title; ?></h1>
        <div class="page-content">
            <div class="row">
                <?php echo $column_left; ?>
                <div class="col-m8 col-b9 col-s12">
                    <nav class="usermenu-top">
                        <ul class="usermenu-top__list">
                            <li><a href="<?php echo $account; ?>">Персональные данные</a></li>
                            <li class="current"><a href="<?php echo $address; ?>">Адреса доставки</a></li>
                            <li><a href="<?php echo $order; ?>">История заказов</a></li>
                            <li><a href="<?php echo $wishlist; ?>">Избранное</a></li>
                        </ul>
                    </nav>
                    <div class="text-content">
                        <div class="row">
                            <div class=" col-b6 col-m12">
                                <h4 class="user-form-head">Добавить адрес</h4>
                                <p><label for="name">Город:</label>
                                    <input type="text" class="inputbox" id="name">
                                </p>
                                <p><label for="name">Индекс:</label>
                                    <input type="text" class="inputbox" id="name">
                                </p>
                                <p><label for="name">Улица:</label>
                                    <input type="text" class="inputbox" id="name">
                                </p>
                                <div class="b-input">
                                    <div class="col-b4 col-s4">
                                        <label for="name">Дом:</label>
                                        <input type="text" class="inputbox" id="name">
                                    </div>
                                    <div class="col-b4 col-s4">
                                        <label for="name">Корпус:</label>
                                        <input type="text" class="inputbox" id="name">
                                    </div>
                                    <div class="col-b4 col-s4">
                                        <label for="name">Квартира:</label>
                                        <input type="text" class="inputbox" id="name">
                                    </div>
                                </div>
                            </div>
                            <div class=" col-b6 col-m12">
                                <h4 class="user-form-head">Сохранённые адреса</h4>
                                <table class="user-adress">
                                    <tr>
                                        <td class="adress-check">
                                            <input type="checkbox" class="checkbox-big" name="fields[1]" id="a4" value="1" /> <label for="a4">г. Тула <br>ул. Лескова, д. 15, кв. 45</label>
                                        </td>
                                        <td class="del">
                                            <a href="#" class="del-link">Удалить</a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="adress-check">
                                            <input type="checkbox" class="checkbox-big" name="fields[1]" id="a3" value="1" /> <label for="a3">г. Тула <br>ул. Толстого, д. 1, кв. 5</label>
                                        </td>
                                        <td class="del">
                                            <a href="#" class="del-link">Удалить</a>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="user-btn">
                <a href="catalog.html" class="back-in-cat back-in-cat__top btn">Вернуться в каталог</a>
                <input type="submit" class="btn btn-purple btn-disabled" value="Сохранить" disabled>
            </div>

            <a href="catalog.html" class="back-in-cat back-in-cat__bottom btn">Вернуться в каталог</a>
        </div>
    </div>
</div>
<script type="text/javascript"><!--
// Sort the custom fields
$('.form-group[data-sort]').detach().each(function() {
	if ($(this).attr('data-sort') >= 0 && $(this).attr('data-sort') <= $('.form-group').length) {
		$('.form-group').eq($(this).attr('data-sort')).before(this);
	}

	if ($(this).attr('data-sort') > $('.form-group').length) {
		$('.form-group:last').after(this);
	}

	if ($(this).attr('data-sort') < -$('.form-group').length) {
		$('.form-group:first').before(this);
	}
});
//--></script>
<script type="text/javascript"><!--
$('button[id^=\'button-custom-field\']').on('click', function() {
	var node = this;

	$('#form-upload').remove();

	$('body').prepend('<form enctype="multipart/form-data" id="form-upload" style="display: none;"><input type="file" name="file" /></form>');

	$('#form-upload input[name=\'file\']').trigger('click');

	if (typeof timer != 'undefined') {
    	clearInterval(timer);
	}

	timer = setInterval(function() {
		if ($('#form-upload input[name=\'file\']').val() != '') {
			clearInterval(timer);

			$.ajax({
				url: 'index.php?route=tool/upload',
				type: 'post',
				dataType: 'json',
				data: new FormData($('#form-upload')[0]),
				cache: false,
				contentType: false,
				processData: false,
				beforeSend: function() {
					$(node).button('loading');
				},
				complete: function() {
					$(node).button('reset');
				},
				success: function(json) {
					$(node).parent().find('.text-danger').remove();

					if (json['error']) {
						$(node).parent().find('input').after('<div class="text-danger">' + json['error'] + '</div>');
					}

					if (json['success']) {
						alert(json['success']);

						$(node).parent().find('input').attr('value', json['code']);
					}
				},
				error: function(xhr, ajaxOptions, thrownError) {
					alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
				}
			});
		}
	}, 500);
});
//--></script>
<script type="text/javascript"><!--
$('.date').datetimepicker({
	pickTime: false
});

$('.datetime').datetimepicker({
	pickDate: true,
	pickTime: true
});

$('.time').datetimepicker({
	pickDate: false
});
//--></script>
<script type="text/javascript"><!--
$('select[name=\'country_id\']').on('change', function() {
	$.ajax({
		url: 'index.php?route=account/account/country&country_id=' + this.value,
		dataType: 'json',
		beforeSend: function() {
			$('select[name=\'country_id\']').after(' <i class="fa fa-circle-o-notch fa-spin"></i>');
		},
		complete: function() {
			$('.fa-spin').remove();
		},
		success: function(json) {
			if (json['postcode_required'] == '1') {
				$('input[name=\'postcode\']').parent().parent().addClass('required');
			} else {
				$('input[name=\'postcode\']').parent().parent().removeClass('required');
			}

			html = '<option value=""><?php echo $text_select; ?></option>';

			if (json['zone'] && json['zone'] != '') {
				for (i = 0; i < json['zone'].length; i++) {
					html += '<option value="' + json['zone'][i]['zone_id'] + '"';

					if (json['zone'][i]['zone_id'] == '<?php echo $zone_id; ?>') {
						html += ' selected="selected"';
			  		}

			  		html += '>' + json['zone'][i]['name'] + '</option>';
				}
			} else {
				html += '<option value="0" selected="selected"><?php echo $text_none; ?></option>';
			}

			$('select[name=\'zone_id\']').html(html);
		},
		error: function(xhr, ajaxOptions, thrownError) {
			alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
		}
	});
});

$('select[name=\'country_id\']').trigger('change');
//--></script>
<?php echo $footer; ?>

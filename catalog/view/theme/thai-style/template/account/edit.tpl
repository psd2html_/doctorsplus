<?php echo $header; ?>
<?php echo $content_top; ?>
<!-- page -->
<div id="page">
    <div class="wrap">
        <!-- breadcrumbs -->
        <div class="breadcrumbs">
            <?php $end_breadcrumb = array_pop($breadcrumbs); ?>
            <?php foreach ($breadcrumbs as $breadcrumb) { ?>
            <a href="<?php echo $breadcrumb['href']; ?>" class="pathway"><?php echo $breadcrumb['text']; ?></a>
            <?php } ?>
            <span><?php echo $end_breadcrumb['text']; ?></span>
        </div>
        <!-- content -->
        <div class="page-content">
            <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
                <div class="row">
                    <?php echo $column_left; ?>
                    <!-- main content -->
                    <div class="col-b9 col-m8 col-s12">
                        <div class="main-content">
                            <h1>Персональные данные</h1>
                            <div class="user-content">
                                <!-- end.breadcrumbs -->
                                <?php if ($success) { ?>
                                <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?></div>
                                <?php } ?>
                                <?php if ($error_warning) { ?>
                                <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?></div>
                                <?php } ?>
                                <div class="row">
                                    <div class="col-b6 col-m6 col-xs12">
                                        <p><label for="input-firstname">Имя:</label>
                                            <?php if ($error_firstname) { ?>
                                            <span class="error-tips"><?php echo $error_firstname; ?></span>
                                            <?php } ?>
                                            <input type="text" name="firstname" value="<?php echo $firstname; ?>" placeholder="<?php echo $entry_firstname; ?>" id="input-firstname" class="inputbox<?php if ($error_firstname) { echo ' error';} ?>" />
                                            <span class="input-tips">Имя и фамилия необходимы для доставки почтой</span>
                                        </p>
                                        <p><label for="input-email">Электронная почта:</label>
                                            <?php if ($error_email) { ?>
                                            <span class="error-tips"><?php echo $error_email; ?></span>
                                            <?php } ?>
                                            <input type="email" name="email" value="<?php echo $email; ?>" placeholder="<?php echo $entry_email; ?>" id="input-email" class="inputbox<?php if ($error_email) { echo ' error';} ?>" />
                                            <span class="input-tips">Для получения уведомлений о состоянии заказа</span>
                                        </p>
                                        <p><label for="input-telephone">Телефон:</label>
                                            <?php if ($error_telephone) { ?>
                                            <span class="error-tips"><?php echo $error_telephone; ?></span>
                                            <?php } ?>
                                            <input type="tel" name="telephone" value="<?php echo $telephone; ?>" placeholder="<?php echo $entry_telephone; ?>" id="input-telephone" class="inputbox<?php if ($error_telephone) { echo ' error';} ?>" />
                                            <span class="input-tips">Без тире, пробелов и скобок, например: +71112223456 </span>
                                        </p>
                                    </div>
                                    <div class="col-b6 col-m6 col-xs12">
                                        <p><label for="password_old">Текущий пароль:</label>
                                            <?php if ($error_password_old) { ?>
                                            <span class="error-tips"><?php echo $error_password_old; ?></span>
                                            <?php } ?>
                                            <input type="password" name="password_old" class="inputbox" id="password_old">
                                            <span class="input-tips"></span>
                                        </p>
                                        <p><label for="name">Введите новый пароль:</label>
                                            <?php if ($error_password_new) { ?>
                                            <span class="error-tips"><?php echo $error_password_new; ?></span>
                                            <?php } ?>
                                            <input type="password" name="password_new" class="inputbox" id="password_new">
                                            <span class="input-tips"></span>
                                        </p>
                                        <p><label for="name">Повторите новый пароль:</label>
                                            <?php if ($error_password_confirm) { ?>
                                            <span class="error-tips"><?php echo $error_password_confirm; ?></span>
                                            <?php } ?>
                                            <input type="password" name="password_confirm" class="inputbox" id="password_confirm">
                                            <span class="input-tips"></span>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="user-btn">
                                <div class="row">
                                    <div class="col-b6 col-m8 col-xs12 col-right">
                                        <div class="b-check">
                                            <p>
                                                <?php if ($newsletter == 1) { ?>
                                                <input type="checkbox" checked class="checkbox" name="newsletter" id="p4" value="1">
                                                <?php } else { ?>
                                                <input type="checkbox" class="checkbox" name="newsletter" id="p4" value="1">
                                                <?php } ?>
                                                <label class="l-float grey" for="p4">Подписаться на скидки и акции
                                            </p>
                                        </div>
                                        <div class="b-submit">
                                            <input type="submit" class="btn btn-flat-green" value="Сохранить">
                                        </div>
                                    </div>
                                    <div class="col-b6 col-m4 col-xs12 col-left">
                                        <a href="<a href="<?php echo $back_catalog; ?>" class="back-in-cat btn-flat btn">Вернуться в каталог</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<script type="text/javascript"><!--
// Sort the custom fields
$('.form-group[data-sort]').detach().each(function() {
	if ($(this).attr('data-sort') >= 0 && $(this).attr('data-sort') <= $('.form-group').length) {
		$('.form-group').eq($(this).attr('data-sort')).before(this);
	}

	if ($(this).attr('data-sort') > $('.form-group').length) {
		$('.form-group:last').after(this);
	}

	if ($(this).attr('data-sort') < -$('.form-group').length) {
		$('.form-group:first').before(this);
	}
});
//--></script>
<script type="text/javascript"><!--
$('button[id^=\'button-custom-field\']').on('click', function() {
	var node = this;

	$('#form-upload').remove();

	$('body').prepend('<form enctype="multipart/form-data" id="form-upload" style="display: none;"><input type="file" name="file" /></form>');

	$('#form-upload input[name=\'file\']').trigger('click');

	if (typeof timer != 'undefined') {
    	clearInterval(timer);
	}

	timer = setInterval(function() {
		if ($('#form-upload input[name=\'file\']').val() != '') {
			clearInterval(timer);

			$.ajax({
				url: 'index.php?route=tool/upload',
				type: 'post',
				dataType: 'json',
				data: new FormData($('#form-upload')[0]),
				cache: false,
				contentType: false,
				processData: false,
				beforeSend: function() {
					$(node).button('loading');
				},
				complete: function() {
					$(node).button('reset');
				},
				success: function(json) {
					$(node).parent().find('.text-danger').remove();

					if (json['error']) {
						$(node).parent().find('input').after('<div class="text-danger">' + json['error'] + '</div>');
					}

					if (json['success']) {
						alert(json['success']);

						$(node).parent().find('input').attr('value', json['code']);
					}
				},
				error: function(xhr, ajaxOptions, thrownError) {
					alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
				}
			});
		}
	}, 500);
});
//--></script>
<script type="text/javascript"><!--
$('.date').datetimepicker({
	pickTime: false
});

$('.datetime').datetimepicker({
	pickDate: true,
	pickTime: true
});

$('.time').datetimepicker({
	pickDate: false
});
//--></script>
<?php echo $footer; ?>
<?php echo $header; ?>
<?php echo $content_top; ?>
<!-- page -->
<div id="page">
    <div class="wrap">
        <!-- breadcrumbs -->
        <div class="breadcrumbs">
            <?php $end_breadcrumb = array_pop($breadcrumbs); ?>
            <?php foreach ($breadcrumbs as $breadcrumb) { ?>
            <a href="<?php echo $breadcrumb['href']; ?>" class="pathway"><?php echo $breadcrumb['text']; ?></a>
            <?php } ?>
            <span><?php echo $end_breadcrumb['text']; ?></span>
        </div>
        <!-- end.breadcrumbs -->
        <div class="page-content">
            <div class="row">
                <?php echo $column_left; ?>
                <div class="col-b9 col-m8 col-s12">
                    <div class="main-content">
                        <h1>История заказов</h1>
                        <?php if ($success) { ?>
                        <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?></div>
                        <?php } ?>
                        <?php if ($error_warning) { ?>
                        <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?></div>
                        <?php } ?>
						
                        <?php if ($orders) { ?>
                            <?php foreach ($orders as $order) { ?>
                        <!-- order-item -->
                        <div class="order-item">
                            <div class="order-title">Заказ № <?php echo $order['order_id']; ?> от <?php echo $order['date_added']; ?></div>
                            <div class="row order-info">
                                <div class="col-b6 col-s12">
                                    <p><b>Сумма:</b> <?php echo $order['total']; ?> рублей</p>
                                    <p><b>Способ оплаты:</b> <?php echo $order['payment_method']; ?></p>
                                </div>
                                <div class="col-b6 col-s12">
                                    <p><b>Способ доставки:</b> <?php echo $order['shipping_method']; ?></p>
                                    <p><b>Статус:</b> <?php echo $order['status']; ?></p>
                                </div>
                            </div>
                            <!--<div class="order-toggle"  data-href="<?php echo $order['href']; ?>"><span>Состав заказа</span><i class="arrow"></i></div> -->
                            <div class="pull-right order-view"><a href="<?php echo $order['href']; ?>" data-toggle="tooltip" title="<?php echo $button_view; ?>" class="btn btn-info"><i class="fa fa-eye"></i></a></td></div>                            
						   <!-- products -->
						  
                            <div class="order-products order-info"">
                            </div>
                        </div>
                            <?php } ?>
                            <div class="text-right"><?php echo $pagination; ?></div>
                        <?php } else { ?>
                        <p>Вы ещё не совершали покупок.</p>
                        <?php } ?>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript"><!--
    $(document).ready(function() {
        $( ".order-toggle" ).click(function() {
            if (!$(this).hasClass('open')) {
                var objOrserInfo = $(this).parent().find('.order-products');
                $.ajax({
                    url: $(this).data('href'),
                    type: 'post',
                    dataType: 'html',
                    cache: false,
                    success: function(html) {
                        objOrserInfo.html(html);
                        equalheight('.product-item');
                    },
                    error: function(xhr, ajaxOptions, thrownError) {
                        alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                    }
                });
            }
        });
    });
//--></script>
<?php echo $footer; ?>
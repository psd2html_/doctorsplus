<?php echo $header; ?>
<!-- page -->
<div id="page">
    <div class="wrap">
        <!-- breadcrumbs -->
        <div class="breadcrumbs breadcrumb">
            <?php $end_breadcrumb = array_pop($breadcrumbs); ?>
            <?php foreach ($breadcrumbs as $breadcrumb) { ?>
            <a href="<?php echo $breadcrumb['href']; ?>" class="pathway"><?php echo $breadcrumb['text']; ?></a>
            <?php } ?>
            <span><?php echo $end_breadcrumb['text']; ?></span>
        </div>
        <!-- end.breadcrumbs -->
        <?php if (!empty($success)) { ?>
        <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?></div>
        <?php } ?>
        <?php if ($error_warning) { ?>
        <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?></div>
        <?php } ?>
        <h1 class="page-title"><?php echo $heading_title; ?></h1>
        <div class="page-content">
            <div class="row">
                <?php echo $column_left; ?>
                <div class="col-m8 col-b9 col-s12">
                    <p><?php echo $text_email; ?></p>
                    <div class="text-content">
                        <div class="well">
                            <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" class="form-horizontal">
                                <div class="form-group required">
                                    <label class="col-sm-2 control-label" for="input-email"><?php echo $entry_email; ?></label>
                                    <div class="col-sm-10">
                                        <input type="text" name="email" value="" placeholder="<?php echo $entry_email; ?>" id="input-email" class="form-control inputbox" />
                                    </div>
                                </div>
                                <div class="buttons clearfix user-btn">
                                    <div class="pull-left"><a href="<?php echo $back; ?>" class="btn btn-default">Вернуться</a></div>
                                    <div class="pull-right">
                                        <input type="submit" value="Напомнить" class="btn btn-purple" />
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php echo $footer; ?>
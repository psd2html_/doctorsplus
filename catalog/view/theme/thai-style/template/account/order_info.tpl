<?php echo $header; ?>
<!-- page -->
<div id="page">
    <div class="wrap">
        <!-- breadcrumbs -->
        <div class="breadcrumbs breadcrumb">
            <?php $end_breadcrumb = array_pop($breadcrumbs); ?>
            <?php foreach ($breadcrumbs as $breadcrumb) { ?>
            <a href="<?php echo $breadcrumb['href']; ?>" class="pathway"><?php echo $breadcrumb['text']; ?></a>
            <?php } ?>
            <span><?php echo $end_breadcrumb['text']; ?></span>
        </div>
        <!-- end.breadcrumbs -->
        <?php if ($success) { ?>
        <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?></div>
        <?php } ?>
        <?php if ($error_warning) { ?>
        <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?></div>
        <?php } ?>
        <div class="page-content user user-favourite-page">
            <div class="row">
                <?php echo $column_left; ?>
                <div class="col-m8 col-b9 col-s12">
                    <div class="text-content">
                        <table class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <td class="text-left" colspan="2"><?php echo $text_order_detail; ?></td>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td class="text-left" style="width: 50%;"><?php if ($invoice_no) { ?>
                                    <b><?php echo $text_invoice_no; ?></b> <?php echo $invoice_no; ?><br />
                                    <?php } ?>
                                    <b><?php echo $text_order_id; ?></b> #<?php echo $order_id; ?><br />
                                    <b><?php echo $text_date_added; ?></b> <?php echo $date_added; ?></td>
                                <td class="text-left"><?php if ($payment_method) { ?>
                                    <b><?php echo $text_payment_method; ?>:</b> <?php echo $payment_method; ?><br />
                                    <?php } ?>
                                    <?php if ($shipping_method) { ?>
                                    <b><?php echo $text_shipping_method; ?>:</b> <?php echo $shipping_method; ?><br />
                                    <?php } ?>
                                    <b>Статус заказа:</b> <?php echo $order_status; ?>                                 
                                </td>
                            </tr>
                            </tbody>
                        </table>
                        <table class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <td class="text-left" style="width: 50%;"><?php echo $text_payment_address; ?></td>
                                <?php if ($shipping_address) { ?>
                                <td class="text-left"><?php echo $text_shipping_address; ?></td>
                                <?php } ?>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td class="text-left"><?php echo $payment_address; ?></td>
                                <?php if ($shipping_address) { ?>
                                <td class="text-left"><?php echo $shipping_address; ?></td>
                                <?php } ?>
                            </tr>
                            </tbody>
                        </table>
                        <div class="table-responsive">
                            <table class="table table-bordered table-hover">
                                <thead>
                                <tr>
                                    <td class="text-left"><?php echo $column_name; ?></td>
                                    <td class="text-left"><?php echo $column_model; ?></td>
                                    <td class="text-right"><?php echo $column_quantity; ?></td>
                                    <td class="text-right"><?php echo $column_price; ?></td>
                                    <td class="text-right"><?php echo $column_total; ?></td>
                                    <?php if ($products) { ?>
                                    <td style="width: 20px;"></td>
                                    <?php } ?>
                                </tr>
                                </thead>
                                <tbody>
                                <?php foreach ($products as $product) { ?>
                                <tr>
                                    <td class="text-left"><?php echo $product['name']; ?>
                                        <?php foreach ($product['option'] as $option) { ?>
                                        <br />
                                        &nbsp;<small> - <?php echo $option['name']; ?>: <?php echo $option['value']; ?></small>
                                        <?php } ?></td>
                                    <td class="text-left"><?php echo $product['model']; ?></td>
                                    <td class="text-right"><?php echo $product['quantity']; ?></td>
                                    <td class="text-right"><?php echo $product['price']; ?></td>
                                    <td class="text-right"><?php echo $product['total']; ?></td>
                                    <td class="text-right" style="white-space: nowrap;"><?php if ($product['reorder']) { ?>
                                        <a href="<?php echo $product['reorder']; ?>" data-toggle="tooltip" title="<?php echo $button_reorder; ?>" class="btn btn-primary"><i class="fa fa-shopping-cart"></i></a>
                                        <?php } ?>
                                    </td>
                                </tr>
                                <?php } ?>
                                <?php foreach ($vouchers as $voucher) { ?>
                                <tr>
                                    <td class="text-left"><?php echo $voucher['description']; ?></td>
                                    <td class="text-left"></td>
                                    <td class="text-right">1</td>
                                    <td class="text-right"><?php echo $voucher['amount']; ?></td>
                                    <td class="text-right"><?php echo $voucher['amount']; ?></td>
                                    <?php if ($products) { ?>
                                    <td></td>
                                    <?php } ?>
                                </tr>
                                <?php } ?>
                                </tbody>
                                <tfoot>
                                <?php foreach ($totals as $total) { ?>
                                <tr>
                                    <td colspan="3"></td>
                                    <td class="text-right"><b><?php echo $total['title']; ?></b></td>
                                    <td class="text-right"><?php echo $total['text']; ?></td>
                                    <?php if ($products) { ?>
                                    <td></td>
                                    <?php } ?>
                                </tr>
                                <?php } ?>
                                </tfoot>
                            </table>
                        </div>
                        <?php if ($comment) { ?>
                        <table class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <td class="text-left"><?php echo $text_comment; ?></td>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td class="text-left"><?php echo $comment; ?></td>
                            </tr>
                            </tbody>
                        </table>
                        <?php } ?>
                        <?php if ($payment_method && $payment_method == 'Оплата картой ') { ?>
                        <table class="table">
                        	<tr class="warning">
                        		<td class="text-center text-error">Ваш заказ не будет отправлен, до получения платежа.</td>
                        	</tr>
                        </table>
                        
                        <?php } ?>
                        <?php if ($histories) { ?>
                        <h3><?php echo $text_history; ?></h3>
                        <table class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <td class="text-left"><?php echo $column_date_added; ?></td>
                                <td class="text-left"><?php echo $column_status; ?></td>
                                <td class="text-left"><?php echo $column_comment; ?></td>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($histories as $history) { ?>
                            <tr>
                                <td class="text-left"><?php echo $history['date_added']; ?></td>
                                <td class="text-left"><?php echo $history['status']; ?></td>
                                <td class="text-left"><?php echo $history['comment']; ?></td>
                            </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                        <?php } ?>
                        <div class="buttons clearfix user-btn">
	                       
                            <div class="pull-right"><?php if ($active_cansel) { ?> <a href="<?php echo $cansel; ?>" class="btn btn-primary">Отменить заказ</a> <?php } ?><a href="<?php echo $reorder; ?>" class="btn btn-primary">Повторить весь заказ полностью</a> <a href="<?php echo $continue; ?>" class="btn btn-primary"><?php echo $button_continue; ?></a></div>
                             <div class="pull-left">
					            <?php 
						  
					            foreach ($sb as $sbcode){
						        
					                if($sb_style) {
					                  $sb_style = 'btn btn-info';
					                } else {
					                  $sb_style = 'sb_button';
					                }
					               
					                if ($order_status == $sb_status['name'] && $payment_code == $sbcode){
					                print "<div class='sbpay'><a class='$sb_style' href='index.php?route=account/sbacquiring&order_id=$order_id'>". $pay_text_lichkab. "</a></div>";
					                
					                } 
					             
					            }
					            ?>
					        </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php echo $footer; ?>
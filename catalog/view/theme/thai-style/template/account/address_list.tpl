<?php echo $header; ?>
<?php echo $content_top; ?>
<!-- page -->
<div id="page">
    <div class="wrap">
        <!-- breadcrumbs -->
        <div class="breadcrumbs breadcrumb">
            <?php $end_breadcrumb = array_pop($breadcrumbs); ?>
            <?php foreach ($breadcrumbs as $breadcrumb) { ?>
            <a href="<?php echo $breadcrumb['href']; ?>" class="pathway"><?php echo $breadcrumb['text']; ?></a>
            <?php } ?>
            <span><?php echo $end_breadcrumb['text']; ?></span>
        </div>
        <div id="review_c"></div>
        <!-- end.breadcrumbs -->

        <div class="page-content">
            <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
                <div class="row">
                    <?php echo $column_left; ?>
                    <!-- main content -->
                    <div class="col-b9 col-m8 col-s12">
                        <div class="main-content">
                            <h1>Адреса доставки</h1>
                            <div class="user-content">
                                <div class="row">
                                    <div class="col-b6 col-m6 col-xs12 add-adress formkey">
                                        <h4 class="user-form-head">Добавить адрес</h4>
                                        <p><label for="city">Город:</label>
                                            <?php if ($error_city) { ?>
                                            <span class="error-tips"><?php echo $error_city; ?></span>
                                            <?php } ?>
                                            <input type="text" name="city" value="<?php echo $city; ?>" class="inputbox<?php if ($error_city) { echo ' error';} ?>" id="city">
                                        </p>
                                        <p><label for="postcode">Индекс:</label>
                                            <?php if ($error_postcode) { ?>
                                            <span class="error-tips"><?php echo $error_postcode; ?></span>
                                            <?php } ?>
                                            <input type="text" name="postcode" value="<?php echo $postcode; ?>" class="inputbox<?php if ($error_postcode) { echo ' error';} ?>" id="postcode">
                                        </p>
                                        <p><label for="address_1">Улица:</label>
                                            <?php if ($error_address_1) { ?>
                                            <span class="error-tips"><?php echo $error_address_1; ?></span>
                                            <?php } ?>
                                            <input type="text" name="address_1" value="<?php echo $address_1; ?>" class="inputbox<?php if ($error_address_1) { echo ' error';} ?>" id="address_1">
                                        </p>
                                        <div class="b-input">
                                            <div class="col-b4 col-s4">
                                                <label for="house">Дом:</label>
                                                <?php if ($error_house) { ?>
                                                <span class="error-tips"><?php echo $error_house; ?></span>
                                                <?php } ?>
                                                <input type="text" name="house" value="<?php echo $house; ?>" class="inputbox<?php if ($error_house) { echo ' error';} ?>" id="house">
                                            </div>
                                            <div class="col-b4 col-s4">
                                                <?php if ($error_housing) { ?>
                                                <span class="error-tips"><?php echo $error_housing; ?></span>
                                                <?php } ?>
                                                <label for="housing">Корпус:</label>
                                                <input type="text" name="housing" value="<?php echo $housing; ?>" class="inputbox<?php if ($error_housing) { echo ' error';} ?>" id="housing">
                                            </div>
                                            <div class="col-b4 col-s4">
                                                <label for="apartment">Квартира:</label>
                                                <?php if ($error_apartment) { ?>
                                                <span class="error-tips"><?php echo $error_apartment; ?></span>
                                                <?php } ?>
                                                <input type="text" name="apartment" value="<?php echo $apartment; ?>" class="inputbox<?php if ($error_apartment) { echo ' error';} ?>" id="apartment">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-b6 col-m6 col-xs12 save-adress">
                                        <?php if ($addresses) { ?>
                                        <h4 class="user-form-head">Сохранённые адреса</h4>
                                        <table class="user-adress">
                                            <?php foreach ($addresses as $result) { ?>
                                            <tr>
                                                <td class="adress-check instylch">
                                                    <input type="radio" name="defrad" class="checkbox-big" data-id="<?php echo $result['address_id']; ?>" id="a<?php echo $result['address_id']; ?>" value="1" <?php echo(!empty($result['default']) ? ' checked' : ''); ?> /> <label for="a<?php echo $result['address_id']; ?>"><?php echo $result['address']; ?></label>
                                                </td>
                                                <td class="del">
                                                    <!--<a href="<?php echo $result['update']; ?>" class="edit-link">Изменить </a><br/>-->
                                                    <a href="<?php echo $result['delete']; ?>" class="del-link">Удалить</a>
                                                </td>
                                            </tr>
                                            <?php } ?>
                                        </table>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>

                            <div class="user-btn">
                                <div class="row">
                                    <div class="col-b6 col-m8 col-xs12 col-right">
                                        <div class="b-check">
                                            <p>
                                                <?php if ($newsletter == 1) { ?>
                                                <input type="checkbox" checked class="checkbox" name="newsletter" id="p4" value="1">
                                                <?php } else { ?>
                                                <input type="checkbox" class="checkbox" name="newsletter" id="p4" value="1">
                                                <?php } ?>
                                                <label class="l-float grey" for="p4">Подписаться на скидки и акции
                                            </p>
                                        </div>
                                        <div class="b-submit">
                                            <input type="submit" class="btn btn-flat-green" value="Добавить">
                                        </div>
                                    </div>
                                    <div class="col-b6 col-m4 col-xs12 col-left">
                                        <a href="<?php $back_catalog; ?>" class="back-in-cat btn-flat btn">Вернуться в каталог</a>
                                    </div>
                                </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
</div>
<script type="text/javascript"><!--
    // Sort the custom fields
    $(document).ready(function() {
        disBut();

        $('.formkey input').keyup(function() {
            disBut();
        });

        $(".user-adress input:radio").click(function() {
            var dataId = $(this).data('id');
            $.ajax({
                url: '<?php echo $url_default; ?>',
                type: 'post',
                dataType: 'json',
                data: {iddefault: dataId},
                cache: false,
                success: function(json) {

                    if (json['error']) {
                        $('#review_c').html('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + '</div>');
                    }

                    if (json['success']) {
                        alert(json['success']);

                        //$('#review_c').html('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + '</div>');
                    }
                },
                error: function(xhr, ajaxOptions, thrownError) {
                    alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                }
            });
        });
    });

    function disBut() {
        if ($('.formkey input[name="city"]').val().length > 2 && $('.formkey input[name="address_1"]').val().length > 2 && $('.formkey input[name="house"]').val().length > 0) {
            $('.user-btn input[type="submit"]').removeClass('btn-disabled').prop('disabled', false);
        } else {
            $('.user-btn input[type="submit"]').addClass('btn-disabled').prop('disabled', true);
        }
    }
    //--></script>

<?php echo $footer; ?>
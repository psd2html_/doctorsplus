<?php echo $header; ?>
<?php echo $content_top; ?>
<!-- page -->
<div id="page">
    <div class="wrap">
        <!-- breadcrumbs -->
        <div class="breadcrumbs breadcrumb">
            <?php $end_breadcrumb = array_pop($breadcrumbs); ?>
            <?php foreach ($breadcrumbs as $breadcrumb) { ?>
            <a href="<?php echo $breadcrumb['href']; ?>" class="pathway"><?php echo $breadcrumb['text']; ?></a>
            <?php } ?>
            <span><?php echo $end_breadcrumb['text']; ?></span>
        </div>
        <!-- end.breadcrumbs -->
        <?php if ($success) { ?>
        <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?></div>
        <?php } ?>
        <?php if ($error_warning) { ?>
        <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?></div>
        <?php } ?>

        <div class="page-content">
            <div class="checkout-confirm">
                <div class="row">
                    <div class="col-b6 col-m7 col-s12">
                        <h1>Ваш заказ успешно оформлен</h1>
                        <p>Информация выслана на Ваш e-mail. В ближайшее время Вам перезвонит наш специалист и подтвердит заказ.</p>

                        <?php if($islogged) { ?>
                        <div class="order-info">
                            <a href="<?php echo $order; ?>">Смотреть историю заказов</a>
                        </div>
                        <?php } ?>
                        <p><a href="<?php echo $continue; ?>" class="btn btn-primary">Перейти на Главную</a></p>
                    </div>
                    <div class="col-b1 col-m0"></div>
                    <div class="col-b5 col-m5 col-s12">
                        <div class="checkout-confirm__img">
                            <img src="/image/confirm.jpg" alt="sale1">
                            <div class="checkout-confirm__title">
                                <p class="mid">Благодарим вас зазаказ в интернет магазине докторам и пациентам </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php echo $footer; ?>
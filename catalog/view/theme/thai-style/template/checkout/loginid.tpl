<?php echo $header; ?>
<?php echo $content_top; ?>
<!-- page -->
<div id="page">
    <div class="wrap">
        <!-- breadcrumbs -->
        <div class="breadcrumbs breadcrumb">
            <?php $end_breadcrumb = array_pop($breadcrumbs); ?>
            <?php foreach ($breadcrumbs as $breadcrumb) { ?>
            <a href="<?php echo $breadcrumb['href']; ?>" class="pathway"><?php echo $breadcrumb['text']; ?></a>
            <?php } ?>
            <span><?php echo $end_breadcrumb['text']; ?></span>
        </div>
        <?php if ($attention) { ?>
        <div class="alert alert-info"><i class="fa fa-info-circle"></i> <?php echo $attention; ?>
            <button type="button" class="close" data-dismiss="alert">&times;</button>
        </div>
        <?php } ?>
        <?php if ($success) { ?>
        <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
            <button type="button" class="close" data-dismiss="alert">&times;</button>
        </div>
        <?php } ?>
        <?php if ($error_warning) { ?>
        <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
            <button type="button" class="close" data-dismiss="alert">&times;</button>
        </div>
        <?php } ?>
        <!-- end.breadcrumbs -->
        <!-- content -->
        <div class="page-content">
            <div class="checkout">
                <div class="row">
                    <div class="col-b6 col-m6 col-s12 checkout-login">
                        <div id="collapse-checkout-option" class="b-checkout-select">
                            <h4>Оформление заказа</h4>
                            <p style="font-size: 16px;">Войти с учетной записью <span class="nowrap">Докторам и Пациентам</span></p>
                            <div class="checkout-login__form clearfix" >
                                <p><label for="login">Логин/Электронная почта</label>
                                    <input type="text" name="email" class="inputbox" id="input-email" value="">
                                </p>
                                <p id="label-input-password"><label for="pass">Пароль</label>
                                    <input type="password" name="password" class="inputbox" id="input-password" value="">
                                </p>

                                <div class="checkout-btn clearfix">
                                    <div class="b-check">
                                        <p><input type="checkbox" name="fields[1]" id="p4" value="1" /><label class="l-float" for="p4">Запомнить меня</p>
                                    </div>
                                    <a href="#" class="forget-pass">Забыли пароль?</a>
                                    <div class="b-submit">
                                        <input type="submit" id="button-login" class="btn btn-flat-green" value="Войти">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-b6 col-m6 col-s12 checkout-select">
                        <div class="b-checkout-select">
                            <h4>Если вы покупаете впервые</h4>
                            <div class="checkout-item">
                                <a href="<?php echo $order; ?>" class="btn btn-flat-green">Оформить заказ с регистрацией</a>
                                <p class="tip">Оформление заказа для незарегистрированных пользователей</p>
                            </div>
                            <?php if ($oneclick) { ?>
                            <h4>Быстрый заказ</h4>
                            <div class="checkout-item">
                                <a href="<?php echo $fast ?>" class="btn btn-flat-green">Купить в 1 клик</a>
                                <p class="tip">Действует только для заказов для Ростова-на-Дону</p>
                            </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript"><!--
    // Login
    $(document).delegate('#button-login', 'click', function() {
        $.ajax({
            url: 'index.php?route=checkout/login/save',
            type: 'post',
            data: $('#collapse-checkout-option :input'),
            dataType: 'json',
            beforeSend: function() {
                //$('#button-login').button('loading');
                remerror();
            },
            complete: function() {
                //$('#button-login').button('reset');
            },
            success: function(json) {
                $('.alert, .text-danger').remove();
                //$('.form-group').removeClass('has-error');

                if (json['redirect']) {
                    location = json['redirect'];
                } else if (json['error']) {
                    $('#label-input-password').prepend('<span class="error-tipss">' + json['error']['warning'] + '</span>').next('input').addClass('error');
                    $('input[name=\'password\']').addClass('error');

                    // Highlight any found errors
                    //$('input[name=\'email\']').parent().addClass('has-error');
                    //$('input[name=\'password\']').parent().addClass('has-error');
                }
            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });

        $('input[name=\'password\'], input[name=\'email\']').keypress(function(){
            remerror();
        });
    });

    function remerror() {
        $('.error-tipss').remove();
        $('input.error').removeClass('error');
    }
//--></script>
<?php echo $footer; ?>
<div class="col-b6 col-s12 promo">
    <p>Введите промокод<span class="mob-hidden"> или номер карты</span>, чтобы получить скидку.</p>
    <span class="inputbox-holder">
        <input type="text" name="coupon" value="<?php echo $coupon; ?>" class="inputbox" id="input-coupon">
        <button class="btn btn-promo" id="button-coupon"><span>Получить скидку</span></button>
    </span>
</div>
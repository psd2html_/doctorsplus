<?php echo $header; ?>
<!-- page -->
<div id="page">
    <div class="wrap">
        <!-- breadcrumbs -->
        <div class="breadcrumbs breadcrumb">
            <?php $end_breadcrumb = array_pop($breadcrumbs); ?>
            <?php foreach ($breadcrumbs as $breadcrumb) { ?>
            <a href="<?php echo $breadcrumb['href']; ?>" class="pathway"><?php echo $breadcrumb['text']; ?></a>
            <?php } ?>
            <span><?php echo $end_breadcrumb['text']; ?></span>
        </div>
        <div class="page-content cart">
            <h1 class="page-title"><?php echo $heading_title; ?></h1>
            <div class="cart-list">
                <p><?php echo $text_error; ?></p>
                <div class="user-btn">
                    <div class="pull-right"><a href="<?php echo $continue; ?>" class="btn">Перейти на Главную</a></div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php echo $footer; ?>
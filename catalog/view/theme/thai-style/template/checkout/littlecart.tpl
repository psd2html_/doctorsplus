<div class="order-cart">
    <a href="/shopping-cart" class="btn-flat-green btn btn-wide">Редактировать корзину</a>

    <!-- products -->
    <div class="cart-prod__wrap">
        <div class="cart-prod" id="cart-slider">
            <?php foreach($products as $product) { ?>
            <div class="cart-item">
                <?php if ($product['thumb']) { ?>
                <td class="img">
                    <div class="cart-item__img"><a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>"></a></div>
                </td>
                <?php } ?>
                <div class="cart-item__info">
                    <a href="<?php echo $product['href']; ?>" class="title"><?php echo $product['name']; ?></a>
                    <?php if ($product['option']) { ?>
                    <p>
                        <?php foreach ($product['option'] as $option) { ?>
                        <?php echo $option['name']; ?>: <span><?php echo $option['value']; ?></span><br />
                        <?php } ?>
                    </p>
                    <?php } ?>
                    <p>Количество:<?php echo $product['quantity']; ?></p>
                    <p class="price">
                        <b><?php echo $product['total']; ?></b> руб.
                    </p>
                </div>
            </div>
            <?php } ?>
        </div>

        <!-- nav -->
        <div class="cart-prod__nav">
            <span class="cartprod-prev"></span>
            <span class="cartprod-next"></span>
        </div>
    </div>

    <!-- total -->
    <div class="cart-r__check">
        <!-- <a href="<?php echo $cart_utl; ?>" class="btn btn-promo">Изменить корзину</a> -->
        <div class="cart-r__total">
            <?php foreach ($totals as $total) { ?>
            <dl>
                <dt><?php echo $total['title']; ?></dt>
                <dd><?php echo (isset($total['formatted_value'])) ? $total['formatted_value'] : $total['value']; ?> руб.</dd>
            </dl>
            <?php } ?>
        </div>
    </div>
</div>        
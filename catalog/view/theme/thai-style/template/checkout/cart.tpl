<?php echo $header; ?>
<?php echo $content_top; ?>
<!-- page -->
<div id="page">
    <div class="wrap">
        <!-- breadcrumbs -->
        <div class="breadcrumbs">
            <?php $end_breadcrumb = array_pop($breadcrumbs); ?>
            <?php foreach ($breadcrumbs as $breadcrumb) { ?>
            <a href="<?php echo $breadcrumb['href']; ?>" class="pathway"><?php echo $breadcrumb['text']; ?></a>
            <?php } ?>
            <span><?php echo $end_breadcrumb['text']; ?></span>
        </div>
        <!-- end.breadcrumbs -->
        <!-- end.breadcrumbs -->
        <div class="page-content">
            <div class="main-content">
                <h1>Корзина</h1>
                <!-- cart list -->
                        <form action="<?php echo $action; ?>" method="post">
                        <div class="cart-list">
                            <table class="cart-table">
                            <?php foreach ($products as $product) { ?>
                            <tr>
                                <?php if ($product['thumb']) { ?>
                                <td class="img">
                                    <a class="prod__img" href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>"></a>
                                </td>
                                <?php } ?>
                                <td class="name">
                                    <a class="prod__name" href="javascript:void(0);"><?php echo $product['name']; ?></a>
                                </td>
                                <td class="spec">
                                    <?php if ($product['option']) { ?>
                                    <?php foreach ($product['option'] as $option) { ?>
                                    <?php echo $option['name']; ?>: <span><?php echo $option['value']; ?></span><br />
                                    <?php } ?>
                                    <?php } ?>
                                </td>
                                <td class="qty">
                                    <span class="qty-header">Количество:</span>
                                    <div class="b-cart-qty">
                                        <input type="text" id="quantity" name="quantity[<?php echo $product['key']; ?>]" value="<?php echo $product['quantity']; ?>"  class="inputbox" />
                                        <span class="qty-plus count">+</span>
                                        <span class="qty-minus count">−</span>
                                    </div>
                                    <button type="submit" data-toggle="tooltip" title="Обновить" class="btn btn-grey">Пересчитать</button>
                                </td>
                                <td class="sum">
                                    <p class="sum__num"><span><?php echo $product['total']; ?></span> руб.</p>
                                    <p class="sum__price">
                                        <span class="label">Цена за штуку</span>
                                        <span class="price__num"><?php echo $product['price']; ?></span> руб.
                                    </p>
                                </td>
                                <td class="del">
                                    <a href="javascript:void(0);" onclick="cart.remove('<?php echo $product['key']; ?>');" class="del-link">Удалить</a>
                                </td>
                            </tr>
                            <?php } ?>
                            </table>
                        </div>
                        </form>
                    <!-- flex cart (for tablet & mob) -->
                    <form action="<?php echo $action; ?>" method="post">
                    <div class="flex-cart">
                        <?php foreach ($products as $product) { ?>
                        <div class="flex-cart__item clearfix">
                            <?php if ($product['thumb']) { ?>
                            <div class="flex-cart__img">
                                <a class="img" href="<?php echo $product['href']; ?>">
                                    <img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>">
                                </a>
                            </div>
                            <?php } ?>
                            <div class="flex-cart__info">
                                <a class="name-link" href="javascript:void(0);"><?php echo $product['name']; ?></a>

                                <div class="flex-cart__data clearfix">
                                    <div class="spec">
                                       <!-- Размер: <span>52</span>-->
                                    </div>

                                    <div class="qty">
                                        Количество:
                                        <button type="submit" data-toggle="tooltip" title="Обновить" class="btn btn-no"><i class="fa fa-refresh"></i></button>
                                        <div class="b-cart-qty">
                                            <input type="text" id="quantity" name="quantity[<?php echo $product['key']; ?>]" value="<?php echo $product['quantity']; ?>"  class="inputbox" />
                                            <span class="qty-plus count">+</span>
                                            <span class="qty-minus count">−</span>
                                        </div>
                                    </div>

                                    <div class="sum">
                                        <p class="sum__num"><span><?php echo $product['total']; ?></span> руб.</p>
                                        <p class="sum__price">
                                            <span class="label">Цена за штуку</span>
                                            <span class="price__num"><?php echo $product['price']; ?></span> руб.
                                        </p>
                                    </div>
                                    <a href="javascript:void(0);" onclick="cart.remove('<?php echo $product['key']; ?>');" class="del-link">Удалить</a>
                                </div>
                            </div>
                        </div>
                        <?php } ?>
                    </div>
                    </form>
                    <!-- total -->
                    <div class="cart-checkout">
                        <div class="row">
                            <?php if ($coupon) { ?>
                            <?php echo $coupon; ?>
                            <?php } ?>
                            <div class="col-b6 col-s12 b-total">
                                <div class="total-sum">
                                    <div class="total-check clearfix">
                                        <?php if (!empty($totals['coupon'])) { ?>
                                        <span class="sum">Сумма: <b><?php echo $totals['sub_total']['text']; ?></b> руб.</span>
                                        <div class="sale">Скидка: <b><?php echo $totals['coupon']['text']; ?></b> руб.</div>
                                        <?php } ?>
                                    </div>
                                    <span class="total-label">Общая сумма:</span> <span class="total-num"><?php echo $totals['total']['text']; ?></span> руб.
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="cart-btn">
                        <div class="row">
                            <div class="col-b6 col-s12">
                                <a href="<?php echo $continue; ?>" class="back-in-cat back-in-cat__top btn btn-back">Продолжить покупки</a>
                                <!--<a href="#" class="cart-clear">Очистить корзину</a>-->
                            </div>
                            <div class="col-b6 col-s12">
                                <a href="<?php echo $checkout; ?>" class="btn btn-submit" style="float: right">Оформить заказ</a>
                            </div>
                        </div>
                    </div>
            </div>
        </div>
    </div>
</div>
<?php if ($coupon) { ?>
<script type="text/javascript"><!--
    $(document).ready(function () {
        //qty change
        jQuery(".b-cart-qty").find(".qty-minus").click(function(){
            var inputbox = jQuery(this).parent().children(".inputbox");
            var value = inputbox.val();
            if(value > 1){
                var res = parseInt(value) - 1;
            }else{
                var res = 1;
            }
            inputbox.val(res);
        });
        jQuery(".b-cart-qty").find(".qty-plus").click(function(){
            var inputbox = jQuery(this).parent().children(".inputbox");
            var value = inputbox.val();
            var res = parseInt(value) + 1;
            inputbox.val(res);
        });
    });

    var btc = $('.btn-checkout');
    <?php if (!$logged) { ?>
        $('a.tablink').on('click', function() {
            var hrif = $(this).attr('href');
            if (hrif == '#order-login') {
                btc.hide();
            } else {
                btc.show();
            };
        });

        // Login
        $(document).delegate('#button-login', 'click', function() {
            $.ajax({
                url: 'index.php?route=checkout/login/save',
                type: 'post',
                data: $('#order-login :input'),
                dataType: 'json',
                beforeSend: function() {
                    //$('#button-login').button('loading');
                },
                complete: function() {
                    //$('#button-login').button('reset');
                },
                success: function(json) {
                    $('.alert, .text-danger').remove();
                    $('p').removeClass('has-error');

                    if (json['redirect']) {
                        location = json['redirect'];
                    } else if (json['error']) {
                        $('#order-login').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error']['warning'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');

                        // Highlight any found errors
                        $('#order-login input[name=\'email\']').parent().addClass('has-error');
                        $('#order-login input[name=\'password\']').parent().addClass('has-error');
                    }
                },
                error: function(xhr, ajaxOptions, thrownError) {
                    alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                }
            });
        });
    <?php } else { ?>
    <?php } ?>
    $('#button-coupon').on('click', function() {
        var namCup = $(this).parent().find('input[name=\'coupon\']').val();
        $.ajax({
            url: 'index.php?route=checkout/coupon/coupon',
            type: 'post',
            data: 'coupon=' + encodeURIComponent(namCup),
            dataType: 'json',
            beforeSend: function() {
               $('.alert').remove();
                $('.form-mess').remove();
            },
            complete: function() {
                //$('#button-coupon').button('reset');
            },
            success: function(json) {
                $('.alert').remove();

                if (json['error']) {
                    //$('.promo').append('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + '</div>');
                    $('.promo').find('.inputbox-holder').append('<span class="form-mess error"> Купон не действителен</span>');
                    $('.form-mess').delay(2500).fadeOut(400);
                    //$('html, body').animate({ scrollTop: 0 }, 'slow');
                }

                if (json['redirect']) {
                    location = json['redirect'];
                }
            }
        });
    });
    //--></script>
<?php } ?>
<?php echo $footer; ?>
<?php if ($error_warning) { ?>
<div class="alert alert-warning"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?></div>
<?php } ?>
<?php if ($shipping_methods) { ?>
<h4>2. Способ доставки</h4>
<div class="order-step__form">
    <div class="row">
        <?php foreach ($shipping_methods as $shipping_method) { ?>
            <?php if (!$shipping_method['error']) { ?>
                <?php foreach ($shipping_method['quote'] as $quote) { ?>
                    <div class="col-b4 col-m12">
                        <?php if ($quote['code'] == $code || !$code) { ?>
                        <?php $code = $quote['code']; ?>
                        <input id="<?php echo $quote['code']; ?>" type="radio" name="shipping_method" value="<?php echo $quote['code']; ?>" checked="checked" />
                        <?php } else { ?>
                        <input id="<?php echo $quote['code']; ?>" type="radio" name="shipping_method" value="<?php echo $quote['code']; ?>" />
                        <?php } ?>
                        <label class="l-float" for="<?php echo $quote['code']; ?>">
                            
							<?php if(!empty($quote['logo'])) { ?>
                            <!--<img src="<?php echo $quote['logo']; ?>" alt="<?php echo $shipping_method['title']; ?>"> -->
                            <?php } ?>
                            <p><span><?php echo $shipping_method['title']; ?><?php echo $quote['title']; ?></span><br>
                           
                            	<?php if ($quote['code'] == 'free.free') { ?>
                            		<?php echo $quote['delivery']; ?>
                            	<?php } else { ?>
                                Стоимость: <?php echo (!empty($quote['cost']) ? $quote['cost'] : 0); ?> руб.
                                <br>
                                Срок доставки (дней): <?php echo (!empty($quote['delivery']) ? $quote['delivery'] : 0); ?>
                                <?php } ?>
                             </p>
                        </label>
                    </div>
                <?php } ?>
            <?php } else { ?>
            <div class="alert alert-danger"><?php echo $shipping_method['error']; ?></div>
            <?php } ?>
        <?php } ?>
    </div>
</div>
<?php } ?>
<div class="buttons" style="display: none">
    <div class="pull-right">
        <input type="button" value="<?php echo $button_continue; ?>" id="button-shipping-method" class="btn btn-primary" />
    </div>
</div>
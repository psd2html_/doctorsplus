
    <h4>1. Адрес доставки</h4>
    <div class="order-step__form">
        <p style="font-size: 13px;">Укажите адрес для определения условий доставки.</p>
        <?php if ($addresses) { ?>
        <div class="checkout-table adress row">
            <div class="col-b12 col-m12 /*checkadr*/" >
                <input class="checkbox-big" id="existing" type="radio" name="shipping_address" value="existing" checked="checked" />
                <label class="l-float" for="existing">
                    <p><?php echo $text_address_existing; ?></p>
                </label>
			<div id="shipping-existing" class="row">
                <?php foreach ($addresses as $address) { ?>
            <div class="col-b12 col-m12">
                <input type="radio" id="ad_<?php echo $address['address_id']; ?>" name="address_id" value="<?php echo $address['address_id']; ?>" <?php if ($address['address_id'] == $address_id) { echo 'checked="checked"';} ?> />
                <label class="l-float" for="ad_<?php echo $address['address_id']; ?>"
                    <p><?php echo $address['firstname']; ?> <?php echo $address['lastname']; ?>, <?php echo 'ул. ' . $address['address_1'] . ', д. ' . $address['house'] . (!empty($address['housing']) ? ', корп. ' . $address['housing'] : '') . (!empty($address['apartment']) ? ', кв. ' . $address['apartment'] : ''); ?>, <?php echo $address['city']; ?>, <?php echo $address['postcode']; ?></p>
                </label>
            </div>
                <?php } ?>
        </div>	
            </div>
            <div class="col-b12 col-m12 /*nocheckadr*/">
                <input class="checkbox-big" id="new" type="radio" name="shipping_address" value="new" />
                <label class="l-float" for="new">
                    <p><?php echo $text_address_new; ?></p>
                </label>
            </div>
        </div>
        <?php } else { ?>
        <input type="hidden" name="shipping_address" value="new" />
        <?php } ?>
        <div id="shipping-new" style="display: <?php echo ($addresses ? 'none' : 'block'); ?>;" >
            <div class="row " id="collapse-shipping-address">
                <div class="col-b7 col-m12">
                    <div class="row">
                        <div class="col-b6 col-s12">
                            <label for="input-shipping-city">Населенный пункт (город, поселок)</label>
                            <input type="text" name="city" value="<?php echo $city; ?>" id="input-shipping-city" class="inputbox" autocomplete="on" />
                        </div>
                        <div class="col-b6 col-s12">
                            <label for="input-shipping-address-1">Улица</label>
                            <input type="text" name="address_1" value="<?php echo $address_1; ?>" id="input-shipping-address-1" class="inputbox" autocomplete="off" />
                        </div>
                    </div>
                </div>
                <div class="col-b5 col-m12">
                    <div class="adress-num-wrap">
                        <div class="adress-num">
                            <label for="input-shipping-haus">Дом</label>
                            <input type="text" name="haus" value="<?php echo $haus; ?>" id="input-shipping-haus" class="inputbox" autocomplete="off" />
                        </div>
                        <div class="adress-num">
                            <label for="input-shipping-korp">Корпус</label>
                            <input type="text" name="korp" value="<?php echo $korp; ?>" id="input-shipping-korp" class="inputbox" autocomplete="off" />
                        </div>
                        <div class="adress-num">
                            <label for="input-shipping-apart">Квартира</label>
                            <input type="text" name="apart" value="<?php echo $apart; ?>" id="input-shipping-apart" class="inputbox" autocomplete="off"/>
                        </div>
                        <div class="adress-num"><label for="input-shipping-postcode">Индекс:</label>
                            <input type="text" name="postcode" value="<?php echo $postcode; ?>" id="input-shipping-postcode" class="inputbox"  autocomplete="off"/>
                        </div>
                    </div>
                </div>
				<div class="buttons clearfix"  style="display:none;">
            <div class="pull-right">
                <input type="button" value="Сохранить" id="button-shipping-address" class="btn btn-primary" />
            </div>
        </div>
            </div>
        </div>
    </div>
	<script type="text/javascript"><!--
$('input[name=\'shipping_address\']').on('change', function() {
	if (this.value == 'new') {
		$('#shipping-existing').hide();
		$('#shipping-new').show();
	} else {
		$('#shipping-existing').show();
		$('#shipping-new').hide();
	}
});
//--></script>
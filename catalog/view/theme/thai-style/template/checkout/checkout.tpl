<?php echo $header; ?>
<!-- page -->
<div id="page">
    <div class="wrap">
        <!-- breadcrumbs -->
        <div class="breadcrumbs breadcrumb">
            <?php $end_breadcrumb = array_pop($breadcrumbs); ?>
            <?php foreach ($breadcrumbs as $breadcrumb) { ?>
            <a href="<?php echo $breadcrumb['href']; ?>" class="pathway"><?php echo $breadcrumb['text']; ?></a>
            <?php } ?>
            <span><?php echo $end_breadcrumb['text']; ?></span>
        </div>
        <?php if ($attention) { ?>
        <div class="alert alert-info"><i class="fa fa-info-circle"></i> <?php echo $attention; ?>
            <button type="button" class="close" data-dismiss="alert">&times;</button>
        </div>
        <?php } ?>
        <?php if ($success) { ?>
        <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
            <button type="button" class="close" data-dismiss="alert">&times;</button>
        </div>
        <?php } ?>
        <?php if ($error_warning) { ?>
        <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
            <button type="button" class="close" data-dismiss="alert">&times;</button>
        </div>
        <?php } ?>
        <!-- end.breadcrumbs -->
        <!-- content -->
        <div class="page-content">
            <h1 class="page-title"><?php echo $heading_title; ?></h1>
            <div class="order-wrap">
                <div class="b-order-cart">
                    <div class="order-cart">
                        <a href="/shopping-cart" class="btn-flat-green btn btn-wide">Редактировать корзину</a>

                        <!-- products -->
                        <div class="cart-prod__wrap">
                            <div class="cart-prod" id="cart-slider">
                                <?php foreach($products as $product) { ?>
                                <div class="cart-item">
                                    <?php if ($product['thumb']) { ?>
                                    <td class="img">
                                        <div class="cart-item__img"><a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>"></a></div>
                                    </td>
                                    <?php } ?>
                                    <div class="cart-item__info">
                                        <a href="<?php echo $product['href']; ?>" class="title"><?php echo $product['name']; ?></a>
                                        <?php if ($product['option']) { ?>
                                        <p>
                                            <?php foreach ($product['option'] as $option) { ?>
                                            <?php echo $option['name']; ?>: <span><?php echo $option['value']; ?></span><br />
                                            <?php } ?>
                                        </p>
                                        <?php } ?>
                                        <p>Количество:<?php echo $product['quantity']; ?></p>
                                        <p class="price">
                                            <b><?php echo $product['total']; ?></b> руб.
                                        </p>
                                    </div>
                                </div>
                                <?php } ?>
                            </div>

                            <!-- nav -->
                            <div class="cart-prod__nav">
                                <span class="cartprod-prev"></span>
                                <span class="cartprod-next"></span>
                            </div>
                        </div>

                        <!-- total -->
                        <div class="cart-r__check">
                            <!-- <a href="<?php echo $cart_utl; ?>" class="btn btn-promo">Изменить корзину</a> -->
                            <div class="cart-r__total">
	                            <?php foreach ($totals as $total) { ?>
                                <dl>
                                    <dt><?php echo $total['title']; ?></dt>
                                    <dd><?php echo (isset($total['formatted_value'])) ? $total['formatted_value'] : $total['value']; ?> руб.</dd>
                                </dl>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="b-order-form">
				
                    <form id="formactpost" action="" method="post">
                        <!-- adress -->
                        <div class="order-step order-adress visible" id="collapse-address"></div>
                        <!-- delivery -->
                        <div class="order-step order-delivery"></div>
                        <!-- pay -->
                        <div class="order-step order-pay"></div>
                        <!-- personal -->
                        <div class="order-step order-submit" id="collapse-submit">
                            <div class="order-step__form">
                                <div class="row">
                                    <div class="erd"></div>
                                    <div class="row b-btn">
                                        <div class="col-b9 col-m12 col-s9 col-xs12">
                                        </div>
                                        <div class="col-b3 col-m12 col-s3 col-xs12">
                                             <input id="button-submit" type="button" class="btn btn-flat-green btn-wide" value="Подтвердить заказ">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="order-step order-personal" id="collapse-checkout-confirm" style="display:none">
                            <h4>4. Подтвердить</h4>
                            <div class="order-step__form panel-body">
                                
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript"><!--

$(document).ready(function() {
    shippingAddress();
});

    function shimetSave() {
        var smval = $('input[name=\'shipping_method\']:checked').val();
        $.ajax({
            url: 'index.php?route=checkout/shipping_method/save',
            type: 'post',
            data: {shipping_method: smval},
            dataType: 'json',
            beforeSend: function() {
                //$('#button-shipping-method').button('loading');
            },
            complete: function() {
                //$('#button-shipping-method').button('reset');
            },
            success: function(json) {
                $('.alert, .text-danger').remove();

                if (json['redirect']) {
                    //location = json['redirect'];
                } else if (json['error']) {
                    if (json['error']['warning']) {
                        $('#collapse-shipping-method .panel-body').prepend('<div class="alert alert-warning">' + json['error']['warning'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');
                    }
                } else {
                   // confirmMethod();
                   littlecart();
                }
            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    }
    
    function shimetFirstSave() {
        var smval = $('input[name=\'shipping_method\']:checked').val();
        $.ajax({
            url: 'index.php?route=checkout/shipping_method/save',
            type: 'post',
            data: {shipping_method: smval},
            dataType: 'json',
            success: function(json) {
                $('.alert, .text-danger').remove();

                if (json['error']) {
                    if (json['error']['warning']) {
                        $('#collapse-shipping-method .panel-body').prepend('<div class="alert alert-warning">' + json['error']['warning'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');
                    }
                } else {
                  
                }
            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    }
    
     $(document).delegate('input[name=\'shipping_method\']', 'click', function() {
		shimetSave();
    });

function littlecart() {
    $.ajax({
        url: 'index.php?route=checkout/littlecart',
        dataType: 'html',
        success: function(html) {
            $('.b-order-cart').html(html);
            //saveAddress();
            shippingMethod();
        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
    });
};


function shippingAddress() {
    $.ajax({
        url: 'index.php?route=checkout/shipping_address',
        dataType: 'html',
        success: function(html) {
            $('.order-adress').html(html);
            //saveAddress();
            //shippingMethod();
            littlecart();
        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
    });
};


function shippingMethod () {
    $.ajax({
        url: 'index.php?route=checkout/shipping_method',
        dataType: 'html',
        success: function(html) {
            // Add the shipping address
            $('.order-delivery').html(html);
            //shimetSave();
            paymentMethod();
        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
    });
}

function paymentMethod() {
    $.ajax({
        url: 'index.php?route=checkout/payment_method',
        dataType: 'html',
        success: function(html) {
            $('.order-pay').html(html);
            $.ajax({
                url: 'index.php?route=checkout/payment_address',
                dataType: 'html',
                success: function(html) {
                    pmsave();
                }
            });
        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
    });
};

    function confirmMethod() {
        $.ajax({
            url: 'index.php?route=checkout/confirm',
            dataType: 'html',
            success: function(html) {
                $('#collapse-checkout-confirm .panel-body').html(html);
                $('#collapse-checkout-confirm').show();
                $('#collapse-submit').hide();
            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    };

    $(document).delegate('input[name=\'payment_method\']', 'click', function() {
		pmsave();
    });

    //
    function pmsave() {
        var vop = $('.order-pay input[type=\'radio\']:checked').val();
        $.ajax({
            url: 'index.php?route=checkout/payment_method/save',
            type: 'post',
            data: {payment_method: vop},
            dataType: 'json',
            beforeSend: function() {
               // $('#button-payment-method').button('loading');
            },
            complete: function() {
                //$('#button-payment-method').button('reset');
            },
            success: function(json) {
                $('.alert, .text-danger').remove();

                if (json['redirect']) {
                    location = json['redirect'];
                } else if (json['error']) {
                    if (json['error']['warning']) {
                        $('#collapse-payment-method .panel-body').prepend('<div class="alert alert-warning">' + json['error']['warning'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');
                    }
                } else {
	                shimetFirstSave();
                }
            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    }

// button-submit
$(document).delegate('#button-submit', 'click', function() {
	
    var dataf = $("#formactpost").serializeArray();
    var address_id = false;
    jQuery.each(dataf, function(i, field){
        if (field.name == 'address_id')
            address_id = true;
    });

    if (!address_id){
        dataf.push({'name' : 'address_id', 'value': $('input[name=\'address_id\']:checked').val()});
    }

    $.ajax({
        url: 'index.php?route=checkout/checkout/address',
        type: 'post',
        data: dataf,
        dataType: 'json',
        beforeSend: function() {
            $('.error-panel, .error-tipss').remove();
            $('.erd').html('');
        },
        success: function(json) {
            if (json['address']) {
                //shimetSave();
                confirmMethod();
            } else if (json['error']) {
	            	if (json['error']['warning']) {
                        $('.checkout-table.adress').after('<span class="error-tips">' + json['error']['warning'] + '</span>');
                    }
                $.each(json['error'], function( index, element ) {
					$('input[name=\'' + index + '\']').addClass('error').before('<span class="error-tipss">' + element + '</span>');
                });
            } else {
	            
            }
        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
    });
});

$(document).delegate('input[type=\'radio\']', 'change', function(){
    if (!$('#collapse-submit').is(':visible')){
        $('#collapse-submit').show();
        $('#collapse-checkout-confirm').hide();
    }
})
//--></script>
<?php echo $footer; ?>
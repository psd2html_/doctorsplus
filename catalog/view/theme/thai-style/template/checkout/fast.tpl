<?php echo $header; ?>
<?php echo $content_top; ?>
<!-- page -->
<div id="page">
    <div class="wrap">
        <!-- breadcrumbs -->
        <div class="breadcrumbs breadcrumb">
            <?php $end_breadcrumb = array_pop($breadcrumbs); ?>
            <?php foreach ($breadcrumbs as $breadcrumb) { ?>
            <a href="<?php echo $breadcrumb['href']; ?>" class="pathway"><?php echo $breadcrumb['text']; ?></a>
            <?php } ?>
            <span><?php echo $end_breadcrumb['text']; ?></span>
        </div>
        <?php if ($attention) { ?>
        <div class="alert alert-info"><i class="fa fa-info-circle"></i> <?php echo $attention; ?>
            <button type="button" class="close" data-dismiss="alert">&times;</button>
        </div>
        <?php } ?>
        <?php if ($success) { ?>
        <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
            <button type="button" class="close" data-dismiss="alert">&times;</button>
        </div>
        <?php } ?>
        <?php if ($error_warning) { ?>
        <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
            <button type="button" class="close" data-dismiss="alert">&times;</button>
        </div>
        <?php } ?>
        <!-- end.breadcrumbs -->
        <!-- content -->
        <div class="page-content">
            <h1>Быстрый заказ</h1>
            <p>Для Ростова-на-Дону действует услуга самовывоза заказа</p>
            <div class="order-wrap">
                <div class="b-order-cart">
                    <div class="order-cart">
                        <a href="/shopping-cart" class="btn-flat-green btn btn-wide">Редактировать корзину</a>

                        <!-- products -->
                        <div class="cart-prod__wrap">
                            <div class="cart-prod" id="cart-slider">
                                <?php foreach($products as $product) { ?>
                                <div class="cart-item">
                                    <?php if ($product['thumb']) { ?>
                                    <td class="img">
                                        <div class="cart-item__img"><a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>"></a></div>
                                    </td>
                                    <?php } ?>
                                    <div class="cart-item__info">
                                        <a href="<?php echo $product['href']; ?>" class="title"><?php echo $product['name']; ?></a>
                                        <?php if ($product['option']) { ?>
                                        <p>
                                        <?php foreach ($product['option'] as $option) { ?>
                                        <?php echo $option['name']; ?>: <span><?php echo $option['value']; ?></span><br />
                                        <?php } ?>
                                        </p>
                                        <?php } ?>
                                       <p>Количество:<?php echo $product['quantity']; ?></p>
                                        <p class="price">
                                            <b><?php echo $product['total']; ?></b> руб.
                                        </p>
                                    </div>
                                </div>
                                <?php } ?>
                            </div>

                            <!-- nav -->
                            <div class="cart-prod__nav">
                                <span class="cartprod-prev"></span>
                                <span class="cartprod-next"></span>
                            </div>
                        </div>

                        <!-- total -->
                        <div class="cart-r__check">
                            <!-- <a href="<?php echo $cart_utl; ?>" class="btn btn-promo">Изменить корзину</a> -->
                            <div class="cart-r__total">
                                <dl>
                                    <dt><b>Общая сумма</b></dt>
                                    <dd><strong><?php echo $total_item; ?></strong> руб.</dd>
                                </dl>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="b-order-form">
                    <div class="fast-order__form">
                        <form action="" method="post">
                            <div class="col-b6 col-m12">
                                <div class="row">
                                    <div class="col-b6">
                                        <label for="name">Имя:</label>
                                        <?php if(!empty($error_firstname)) { echo '<span class="error-tips">' . $error_firstname . '</span>';} ?>
                                        <input type="text" required name="firstname" class="inputbox <?php if(!empty($error_firstname)) { echo 'error';} ?>" id="name" value="<?php echo $firstname; ?>">
                                    </div>
                                    <div class="col-b6">
                                        <label for="surname">Фамилия:</label>
                                        <?php if(!empty($error_lastname)) { echo '<span class="error-tips">' . $error_lastname . '</span>';} ?>
                                        <input type="text" required name="lastname" class="inputbox <?php if(!empty($error_lastname)) { echo 'error';} ?>" id="surname" value="<?php echo $lastname; ?>">
                                    </div>
                                </div>
                                <p><span class="input-tips">Имя и фамилия необходимы для доставки почтой </span></p>
                                <p><label for="name">Телефон:</label>
                                    <?php if(!empty($error_telephone)) { echo '<span class="error-tips">' . $error_telephone . '</span>';} ?>
                                    <input type="text" required name="telephone" class="inputbox <?php if(!empty($error_telephone)) { echo 'error';} ?>" id="tel" value="<?php echo $telephone; ?>" placeholder="89204655211">
                                    <span class="input-tips">Без тире, пробелов и скобок, например: +71112223456 </span>
                                </p>
                                <p><label for="name">Электронная почта:</label>
                                    <?php if(!empty($error_email)) { echo '<span class="error-tips">' . $error_email . '</span>';} ?>
                                    <input type="email" required name="email" class="inputbox <?php if(!empty($error_email)) { echo 'error';} ?>" id="email" value="<?php echo $email; ?>" placeholder="marina@ya.ru">
                                    <span class="input-tips">Для получения уведомлений о состоянии заказа</span>
                                </p>
                            </div>
                            <div class="col-b6 col-m12">
                                <p><label for="mess">Комментарии к заказу:</label>
                                    <?php if(!empty($error_comment)) { echo '<span class="error-tips">' . $error_comment . '</span>';} ?>
                                    <textarea name="comment" id="mess" class="inputbox <?php if(!empty($error_comment)) { echo 'error';} ?>"><?php echo $comment; ?></textarea>
                                </p>
                                <div class="row fast-order__btn">
                                    <div class="col-b7 col-m7 col-xs12">
                                        <input type="checkbox" name="subscribe" <?php if(!empty($subscribe)) { echo 'checked'; } ?> id="p4" value="1" /><label class="l-float grey" for="p4">Подписаться на скидки и акции</label>
                                    </div>
                                    <div class="col-b5 col-m5 col-xs12">
                                        <input type="submit" class="btn btn-flat-green btn-wide" value="Подтвердить заказ">
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        //vert slider
        var qty = $('.cart-item').length;
        if(qty > 3){
            $('.cart-prod__nav span').css('display', 'block');
            $('#cart-slider').carouFredSel({
                direction: "up",
                items: {
                    visible: 3
                },
                scroll : {
                    items: 1
                },
                auto: false,
                prev: '.cartprod-prev',
                next: '.cartprod-next',
                height: 531,
                circular: false
            });
        }
        
        littlecart();
    });
    
    
    function littlecart() {
	    $.ajax({
	        url: 'index.php?route=checkout/littlecart',
	        dataType: 'html',
	        success: function(html) {
	            $('.b-order-cart').html(html);
	            //saveAddress();
	            shippingMethod();
	        },
	        error: function(xhr, ajaxOptions, thrownError) {
	            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
	        }
	    });
	};
</script>
<?php echo $footer; ?>
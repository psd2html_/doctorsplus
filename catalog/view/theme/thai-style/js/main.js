if(jQuery) (function(){
    $.extend($.fn, {      
        rightClick: function(handler) {
            $(this).each( function() {
                $(this).mousedown( function(e) {
                    var evt = e;
                    $(this).mouseup( function() {
                        $(this).unbind('mouseup');
                        if( evt.button == 2 ) {
                            handler.call( $(this), evt );
                            return false;
                        } else {
                            return true;
                        }
                    });
                });
                $(this)[0].oncontextmenu = function() {
                    return false;
                }
            });
            return $(this);
        },      
        rightMouseDown: function(handler) {
            $(this).each( function() {
                $(this).mousedown( function(e) {
                    if( e.button == 2 ) {
                        handler.call( $(this), e );
                        return false;
                    } else {
                        return true;
                    }
                });
                $(this)[0].oncontextmenu = function() {
                    return false;
                }
            });
            return $(this);
        },     
        rightMouseUp: function(handler) {
            $(this).each( function() {
                $(this).mouseup( function(e) {
                    if( e.button == 2 ) {
                        handler.call( $(this), e );
                        return false;
                    } else {
                        return true;
                    }
                });
                $(this)[0].oncontextmenu = function() {
                    return false;
                }
            });
            return $(this);
        },       
        noContext: function() {
            $(this).each( function() {
                $(this)[0].oncontextmenu = function() {
                    return false;
                }
            });
            return $(this);
        },       
        noDrag: function() {
            $(this).each( function() {
                $(this)[0].ondragstart = function() {
                    return false;
                }
            });
            return $(this);
        },       
        noCopy: function() {
            $(this).each( function() {
                $(this)[0].oncopy = function() {
                    return false;
                }
                $(this)[0].oncut = function() {
                    return false;
                }
                $(this)[0].onselectstart = function() {
                    return false;
                }
            });
            return $(this);
        }
        
    });
    
})(jQuery);


/* Thanks to CSS Tricks for pointing out this bit of jQuery
It's been modified into a function called at page load and then each time the page is resized. One large modification was to remove the set height before each new calculation. */

equalheight = function(container){

var currentTallest = 0,
     currentRowStart = 0,
     rowDivs = new Array(),
     $el,
     topPosition = 0;
 $(container).each(function() {

   $el = $(this);
   $($el).height('auto')
   topPostion = $el.position().top;

   if (currentRowStart != topPostion) {
     for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
       rowDivs[currentDiv].height(currentTallest);
     }
     rowDivs.length = 0; // empty the array
     currentRowStart = topPostion;
     currentTallest = $el.height();
     rowDivs.push($el);
   } else {
     rowDivs.push($el);
     currentTallest = (currentTallest < $el.height()) ? ($el.height()) : (currentTallest);
  }
   for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
     rowDivs[currentDiv].height(currentTallest);
   }
 });
}

$.fn.equivalent = function (){
    var $blocks = $(this),
        maxH    = $blocks.eq(0).height(); 

    $blocks.each(function(){
        maxH = ( $(this).height() > maxH ) ? $(this).height() : maxH;
    });
    return maxH;
}


$(window).load(function() {
  equalheight('.product-item'); 
  setTimeout(function(){
    $('#rel-products').height($('.product-item').height() +85);
  }, 250);
  
});
$(document).ready(function () {

  //slider
  if($('#mainslider').length) {
    $('#mainslider').carouFredSel({
      responsive: true,
      items: {
        visible: 1
      },
      scroll: {
        fx: "crossfade",
        duration: 1200
      },
      swipe: {
        onTouch: true
      },
      auto: true,
      prev: '.mainslider-prev',
      next: '.mainslider-next',
      height: 340,
      circular: true
    });
  }

  //owl partners
  if($('.partner-slider').length){
    $('.partner-slider').owlCarousel({
      loop:true,
      responsive:{
        0:{
          items:1,
          margin:0
        },
        500:{
          items:2
        },
        600:{
          items:3
        },
        750:{
          items:4
        }
      }
    });
  }
  
  //brand-filter
  var brandsArray = $('.brand-select').find('.value-wrap');
  if(brandsArray.length > 5){
    $('.brand-select .show-all').css('display', 'inline-block');
    brandsArray.each(function(indx, el){
      if(indx > 4) {
        $(el).addClass('hide-value');
        $(el).hide();
      }
    });
    
    $('.brand-select .show-all').on('click', function(){
      $('.hide-value').slideToggle(300);
      $(this).toggleClass('open-br-v');
      if($(this).hasClass('open-br-v')){
        $(this).text('Скрыть бренды');
      }else{
        $(this).text('Показать все бренды');
      }
    });
  }
  
  //add to cart
  $(document).scroll(function () {
    if ($(this).scrollTop() > 250) {
      $('.header-info__minicart .dd-box').addClass('fixed');
    } else {
      $('.header-info__minicart .dd-box').removeClass('fixed');
    }
  });
    
  /*$('.product-item').find('.to-cart').on('click', function(e){
    e.preventDefault();
    $(this).unbind();
    var miniCart = $('.header-info__minicart .dd-box');
    miniCart.fadeIn(300);

    setTimeout(function(){
      miniCart.fadeOut(300);
    }, 1000);
  });*/
  
  //faq
  $('.faq-header').on('click', function(){
    console.log('d');
    el = $(this);
    sibl = el.parent('.faq-item').siblings();
    el.toggleClass('open');
    el.next('.faq-text').slideToggle();
    sibl.find('.faq-header').removeClass('open');
    sibl.find('.faq-text').slideUp();
  });
  
  //seo more
  $('.text-open').on('click', function(e){
    e.preventDefault();
    $(this).parent('.seotext').addClass('full');
    $(this).hide().next('.hidden-text').slideDown(400);
  });
  
  //placeholder ie
  jQuery('[placeholder]').placeholder();
  
  //popup
  $(".popup").fancybox({
    padding : 0,
		width		: 'auto',
    height  : 'auto',
    autoSize: false,
    openSpeed : 500,
    closeSpeed : 500,
    overlay : {
      speedOut   : 250,   // duration of fadeOut animation
      showEarly  : false,  // indicates if should be opened immediately or wait until the content is ready
    },
    tpl: { 
        closeBtn: '<div title="Закрыть" class="fancybox-item fancybox-close"></div>' 
          }
	});
  
  //prodfull popup
  $(".img-zoom").fancybox({
    padding : 30,
    onStart: function () {
	    console.log(1);
	    $('.fancybox-image')[0].oncontextmenu = function() { return false; }
    },
    overlay : {
      speedOut   : 250,   // duration of fadeOut animation
      showEarly  : false,  // indicates if should be opened immediately or wait until the content is ready
    },
    tpl: { 
        closeBtn: '<div title="Закрыть" class="fancybox-item fancybox-close"></div>' 
          }
	});
  
  //order open
  $('.order-toggle').on('click', function(e){
    $(this).next('.order-products').slideToggle();
    $(this).toggleClass('open');
  });
  
  //minicart-open-items
  $('.minicart-more').on('click', function(e){
    $(this).hide();
    $(this).prev('.minicart-hidden').slideDown();
  });
  
  // cat-item hover
  $('.product-item').on({
    mouseenter: function () {
      $(this).find('.to-cart').fadeIn(300);
    },
    mouseleave: function () {
      $(this).find('.to-cart').fadeOut(300);
    }
  });
  
  //open top mob-menu
  $('.topmenu-mob-toggle').on('click', function(e){
    e.preventDefault();
    $(this).toggleClass('open-t-m');
    $('.topmenu__list').fadeToggle(200);
    if($('.mob-search-toggle').hasClass('open-s')){
      $($('.mob-search-toggle')).toggleClass('open-s');
      $('.b-search').fadeToggle(200);
    }
  });
  
  //open search
  $('.mob-search-toggle').on('click', function(e){
    e.preventDefault();
    $(this).toggleClass('open-s');
    $('.b-search').fadeToggle(200);
    if($('.topmenu-mob-toggle').hasClass('open-t-m')){
      $($('.topmenu-mob-toggle')).toggleClass('open-t-m');
      $('.topmenu__list').fadeToggle(200);
    }
  });
  
  //open main menu
  $('.mob-catmenu-toggle').on('click', function(e){
    e.preventDefault();
    $(this).toggleClass('open-m-m');
    $('.catmenu-wrap').fadeToggle(200);
  });
  
  
  //check load and resize
  function check_window_size() {
    var width = Math.max($(window).width(), window.innerWidth);
    
    //mobile
    if(width <= 750){
      $('body').addClass('mobile');
      var mobile = true;
    }else{
      $('body').removeClass('mobile')
    }
    
    //tablet
    if(width <= 1025){
      $('body').addClass('tablet');
      var tablet = true;
    }else{
      $('body').removeClass('tablet')
    }
    
    // info tips
    $('.info-tooltip-wrap').hide();
    if(tablet){
      $('.info-tip__toogle').off();
      if(width >= 900){
        $('.info-tip__toogle').on('click', function(e){
          $(this).siblings('li').find('.info-tooltip-wrap').fadeOut();
          $(this).find('.info-tooltip-wrap').fadeToggle(300);
        });
      }
    }else{
      $('.info-tip__toogle').off();
      $('.info-tip__toogle').on({
        mouseenter: function () {
          $(this).find('.info-tooltip-wrap').fadeIn(300);
        },
        mouseleave: function () {
          $(this).find('.info-tooltip-wrap').fadeOut(300);
        }
      });
    }
    
    //brands
    if(tablet){
      $('.brand-item').off();
      $('.brand-item').on('click', function(e){
        $(this).toggleClass('open-b');
        if($(this).hasClass('open-b')){
          $(this).siblings('.col-brand').removeClass('open-b').find('.brand-hover').hide();
          $(this).find('.brand-hover').css('display', 'block');
        }else{
          $(this).find('.brand-hover').hide();
        }
      });
    }else{
      $('.brand-item').on({
        mouseenter: function () {
          $(this).find('.brand-hover').fadeIn(300).css("display","block");;
        },
        mouseleave: function () {
          $(this).find('.brand-hover').fadeOut(300);
        }
      });
    }

    //top dd
    if(tablet){
     /* $('body').off('click', '.dd-toggle');
      $('body').on('click', '.dd-toggle', function(e){
        $(this).toggleClass('open-dd');
        if($(this).hasClass('open-dd')){
          $(this).parent('.dd-col').siblings('.dd-col').find('.dd-toggle').removeClass('open-dd').find('.dd-box').hide();
          $(this).find('.dd-box').fadeIn(200);
        }else{
          $(this).find('.dd-box').fadeOut(200);
        }
      });*/
        $('body').on('click' ,function(e){
            var container = $(".user .dd-toggle");
            if (!container.is(e.target) && container.has(e.target).length === 0) {
                $(this).find('.user .dd-box').fadeOut(200);
                $(this).find('.user .dd-toggle').removeClass('open-dd');
            }
        });
        $('.dd-toggle').on('click', function(e){
            if($(this).hasClass('open-dd')){
                if (!$(this).is(e.target) && $(this).has(e.target).length === 0) {
                    $(this).find('.dd-box').fadeOut(200);
                    $(this).removeClass('open-dd');
                }
            }else{
                $(this).find('.dd-box').fadeIn(200);
                $(this).addClass('open-dd');
            }
        });
    }else{
      $('body').on({
      mouseenter: function () {
        $(this).find('.dd-box').fadeIn(200);
        $(this).parent('.dd-col').siblings('.dd-col').find('.dd-toggle').removeClass('open-dd').find('.dd-box').hide();
      },
      mouseleave: function () {
        $(this).find('.dd-box').fadeOut(200);
      }}, '.dd-toggle'
      );
    }

    // resize toggle top-menu
    if(width > 900){
      $('.topmenu__list').show();
      $('.topmenu-mob-toggle').removeClass('open-t-m');
    }else{
      $('.topmenu__list').hide();
      $('.topmenu-mob-toggle').removeClass('open-t-m');
    }
    
    // resize toggle search
    if(width > 550){
      $('.b-search').show();
      $('.mob-search-toggle').removeClass('open-s');
    }else{
      $('.b-search').hide();
      $('.mob-search-toggle').removeClass('open-s');
    }
    
    // resize toggle mainmenu
    if(width > 750){
      $('.catmenu-wrap').show();
      $('.mob-catmenu-toggle').removeClass('open-m-m');
    }else{
      $('.catmenu-wrap').hide();
      $('.mob-catmenu-toggle').removeClass('open-m-m');
    }
    
    //leftmenu
    /*if(mobile){
      if(!$('.leftmenu').hasClass('loaded')){
        var currItem = $('.leftmenu__list').find('.current a');
        var currHtml = currItem.html();
        $('.leftmenu').prepend('<div class="leftmenu-toggle">' + currHtml + '</div>');
        currItem.detach();
      
        setTimeout(function(){
          $('.leftmenu__list').slideUp();
          $('.leftmenu').addClass('loaded'); 
        }, 2000); 
      }
      
      $('.leftmenu-toggle').off();
      $('.leftmenu-toggle').on('click', function(e){
        e.preventDefault();
        $('.leftmenu').toggleClass('open-l-m');
        $('.leftmenu__list').slideToggle();
      });
    }*/

    //filter
    if(mobile){
      $('.b-filter__toggle').off('click');
      $('.b-filter__toggle').on('click', function(){
        el = $(this);
        sibl = el.parent('.b-filter').siblings();
        el.parent('.b-filter').toggleClass('open');
        el.next('.b-filter__value').slideToggle();
        sibl.removeClass('open');
        sibl.find('.b-filter__value').slideUp();
      });
    }else{
      $('.b-filter__value').show();
    }
  }// end check load and resize
  check_window_size();
  
  
  //resize
  $(window).resize(function(e) {
    check_window_size();
    equalheight('.product-item'); 
    setTimeout(function(){
      $('#rel-products').height($('.product-item').height() +85);
    }, 150);
  });
});


document.getElementsByTagName('img').ondragstart = function() { return false; };

/**
 * '.locked' polyfill
 */
(function () {
    "use strict";

    var el = document.createElement('div');
    el.style.cssText = 'pointer-events:auto';

    if (el.style.pointerEvents !== 'auto') {
        el = null;

        var _lock = function (evt) {
            evt = evt || window.event;
            var el = evt.target || evt.srcElement;
            if (el && /\slocked\s/.test(' ' + el.className + ' ')) {
                if (evt.stopPropagation) {
                    evt.preventDefault();
                    evt.stopPropagation();
                } else {
                    evt.returnValue = true;
                    evt.cancelBubble = true;
                }
            }
        };

        if (document.addEventListener) {
            document.addEventListener('mousedown', _lock, false);
            document.addEventListener('contextmenu', _lock, false);
        } else {
            document.attachEvent('onmousedown', _lock);
            document.attachEvent('oncontextmenu', _lock);
        }
    }
})();
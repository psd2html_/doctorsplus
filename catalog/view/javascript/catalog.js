$(window).load(function() {
    equalheight('.product-item');
    equalheight('.product-name');

    //slider
    var left1 = $('.ui-slider-handle').eq(0).position().left;
    $('.price-from').css('left', left1 - 17);

    var left2 = $('.ui-slider-handle').eq(1).position().left;
    $('.price-to').css('left', left2 - 17);

    var width = Math.max($(window).width(), window.innerWidth);
    if(width > 950) {
        $('#shop-filter').css('display', 'block');
    }else{
        $('#shop-filter').hide().css('opacity', 1);
    }
});

$(document).ready(function () {
    //slider in filter
    $( "#price-slider" ).slider({
        range: true,
        min: 0,
        max: 15000,
        values: [ 300, 3500 ],
        slide: function( event, ui ) {
            jQuery( "#fprice_from" ).val( ui.values[ 0 ]);
            var left1 = $('.ui-slider-handle').eq(0).position().left;
            $('.price-from').text(ui.values[ 0 ] + "р.").css('left', left1 - 17);

            jQuery( "#fprice_to" ).val( ui.values[ 1 ]);
            var left2 = $('.ui-slider-handle').eq(1).position().left;
            $('.price-to').text(ui.values[ 1 ] + "р.").css('left', left2 - 17);
        }
    });

    //open mob-filter
    $('.mob-filter').on('click', function(e){
        $('#shop-filter').fadeIn(200);
    });
    $('.filter-hide').on('click', function(e){
        $('#shop-filter').fadeOut(200);
    });

    $(window).resize(function(e) {
        var width = Math.max($(window).width(), window.innerWidth);

        equalheight('.product-item');
        equalheight('.product-name');

        //slider
        var width = Math.max($(window).width(), window.innerWidth);
        if(width > 950) {
            $('#shop-filter').css('display', 'block');
        }else{
            $('#shop-filter').css('display', 'block').css('opacity', 0);
        }
        var left1 = $('.ui-slider-handle').eq(0).position().left;
        $('.price-from').css('left', left1 - 17);

        var left2 = $('.ui-slider-handle').eq(1).position().left;
        $('.price-to').css('left', left2 - 17);

        if(width < 950) {
            $('#shop-filter').hide().css('opacity', 1);
        }
    });
});
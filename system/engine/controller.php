<?php
abstract class Controller {
	protected $registry;

	public function __construct($registry) {
		$this->registry = $registry;
	}

	public function __get($key) {
		return $this->registry->get($key);
	}

	public function __set($key, $value) {
		$this->registry->set($key, $value);
	}
	
	protected function oc_smsc_init() {

			// Load language
			$this->load->language('module/oc_smsc');

			// Load OC SMSC library
			require_once(DIR_SYSTEM . 'library/oc_smsc/gateway.php');

			// Add to global registry
			$this->registry->set('oc_smsc_gateway', new OCSMSCGateway($this->registry));

			return true;
	}
}
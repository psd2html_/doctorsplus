<?php
// HTTP
define('HTTP_SERVER', 'http://doctorsplus.ru/admin/');
define('HTTP_CATALOG', 'http://doctorsplus.ru/');

// HTTPS
define('HTTPS_SERVER', 'http://doctorsplus.ru/admin/');
define('HTTPS_CATALOG', 'http://doctorsplus.ru/');

// DIR
define('DIR_APPLICATION', '/home/c/cl150884/doctorsplus/public_html/doctorsplus/admin/');
define('DIR_SYSTEM', '/home/c/cl150884/doctorsplus/public_html/doctorsplus/system/');
define('DIR_LANGUAGE', '/home/c/cl150884/doctorsplus/public_html/doctorsplus/admin/language/');
define('DIR_TEMPLATE', '/home/c/cl150884/doctorsplus/public_html/doctorsplus/admin/view/template/');
define('DIR_CONFIG', '/home/c/cl150884/doctorsplus/public_html/doctorsplus/system/config/');
define('DIR_IMAGE', '/home/c/cl150884/doctorsplus/public_html/doctorsplus/image/');
define('DIR_CACHE', '/home/c/cl150884/doctorsplus/public_html/doctorsplus/system/cache/');
define('DIR_DOWNLOAD', '/home/c/cl150884/doctorsplus/public_html/doctorsplus/system/download/');
define('DIR_UPLOAD', '/home/c/cl150884/doctorsplus/public_html/doctorsplus/system/upload/');
define('DIR_LOGS', '/home/c/cl150884/doctorsplus/public_html/doctorsplus/system/logs/');
define('DIR_MODIFICATION', '/home/c/cl150884/doctorsplus/public_html/doctorsplus/system/modification/');
define('DIR_CATALOG', '/home/c/cl150884/doctorsplus/public_html/doctorsplus/catalog/');

// DB
define('DB_DRIVER', 'mpdo');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'cl150884_doctors');
define('DB_PASSWORD', 'weg76pAS');
define('DB_DATABASE', 'cl150884_doctors');
define('DB_PREFIX', 'ts_');

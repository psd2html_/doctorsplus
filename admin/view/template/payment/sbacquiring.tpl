<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
        <button type="submit" form="form-cod" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
        <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a>
      </div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button> 
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-pencil"></i></h3>
      </div>
      <div class="panel-body">
        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-cod" class="form-horizontal">
          <table class="table">
      	<tr>
        <td width="25%"><span class="required">*</span> <?php echo $entry_license; ?></td>
        <td><input type="text" name="sbacquiring_license" value="<?php if (isset($sbacquiring_license)){ echo $sbacquiring_license; }?>" />
          <br />
          <?php if ($error_license) { ?>
          <span class="error"><?php echo $error_license; ?></span>
          <?php } ?></td>
        </tr>
      	<tr>
        <td width="25%"><span class="required">*</span> <?php echo $entry_userName; ?></td>
        <td><input type="text" name="sbacquiring_userName" value="<?php if (isset($sbacquiring_userName)){ echo $sbacquiring_userName; }?>" />
          <br />
          <?php if ($error_userName) { ?>
          <span class="error"><?php echo $error_userName; ?></span>
          <?php } ?></td>
      	</tr>
      	<tr>
        <td><span class="required">*</span> <?php echo $entry_password; ?></td>
        <td><input type="password" name="sbacquiring_password" value="<?php if (isset($sbacquiring_userName)){ echo $sbacquiring_password; }?>" />
          <br />
          <?php if ($error_password) { ?>
          <span class="error"><?php echo $error_password; ?></span>
          <?php } ?></td>
      	</tr>        
        <!-- <tr>
          <td><?php echo $entry_met; ?></td>
          <td><?php if ($sbacquiring_met) { ?>
            <input type="radio" name="sbacquiring_met" value="1" checked="checked" />
            <?php echo $entry_met_preautoriz; ?>
            <input type="radio" name="sbacquiring_met" value="0" />
            <?php echo $entry_met_odnostage; ?>
            <?php } else { ?>
            <input type="radio" name="sbacquiring_met" value="1" />
            <?php echo $entry_met_preautoriz; ?>
            <input type="radio" name="sbacquiring_met" value="0" checked="checked" />
            <?php echo $entry_met_odnostage; ?>
            <?php } ?></td>
        </tr> -->
        <tr>
          <td><span class="required">*</span> callback:</td>
          <td><?php echo $copy_result_url; ?></td>
        </tr>
        <tr>
          <td><?php echo $entry_sbacquiring_name_tab; ?></td>
          <td><?php if ($sbacquiring_name_attach) { ?>
            <input type="radio" name="sbacquiring_name_attach" value="1" checked="checked" />
            <?php echo $text_my; ?>
            <input type="radio" name="sbacquiring_name_attach" value="0" />
            <?php echo $text_default; ?>
            <?php } else { ?>
            <input type="radio" name="sbacquiring_name_attach" value="1" />
            <?php echo $text_my; ?>
            <input type="radio" name="sbacquiring_name_attach" value="0" checked="checked" />
            <?php echo $text_default; ?>
            <?php } ?></td>
        </tr>
        <tr>
          <td><?php echo $entry_sbacquiring_name; ?></td>
          <td><?php foreach ($languages as $language) { ?><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" style="vertical-align:top;"/> <textarea name="sbacquiring_name_<?php echo $language['language_id']; ?>" cols="50" rows="1"><?php echo isset(${'sbacquiring_name_' . $language['language_id']}) ? ${'sbacquiring_name_' . $language['language_id']} : ''; ?></textarea><br />
            <?php } ?></td>
        </tr>
        <tr>
          <td><?php echo $entry_fixen ; ?></td>
          <td><?php if ($sbacquiring_fixen == 'proc') { ?>
            <input type="radio" name="sbacquiring_fixen" value="fix" />
            <?php echo $entry_fixen_fix; ?>
            <input type="radio" name="sbacquiring_fixen" value="proc" checked="checked" />
            <?php echo $entry_fixen_proc; ?>
            <input type="radio" name="sbacquiring_fixen" value="0" />
            <?php echo $entry_fixen_order; ?>
            <?php } else if($sbacquiring_fixen == 'fix'){ ?>
            <input type="radio" name="sbacquiring_fixen" value="fix" checked="checked" />
            <?php echo $entry_fixen_fix; ?>
            <input type="radio" name="sbacquiring_fixen" value="proc" />
            <?php echo $entry_fixen_proc; ?>
            <input type="radio" name="sbacquiring_fixen" value="0" />
            <?php echo $entry_fixen_order; ?>
            <?php } else { ?>
            <input type="radio" name="sbacquiring_fixen" value="fix" />
            <?php echo $entry_fixen_fix; ?>
            <input type="radio" name="sbacquiring_fixen" value="proc" />
            <?php echo $entry_fixen_proc; ?>
            <input type="radio" name="sbacquiring_fixen" value="0" checked="checked" />
            <?php echo $entry_fixen_order; ?>
            <?php } ?></td>
        </tr>
        <tr>
          <td><?php echo $entry_fixen_amount; ?></td>
          <td><input type="text" name="sbacquiring_fixen_amount" value="<?php echo isset($sbacquiring_fixen_amount) ? $sbacquiring_fixen_amount : ''; ?>" ><br />
          <?php if ($error_fixen) { ?>
          <span class="error"><?php echo $error_fixen; ?></span>
          <?php } ?></td>
        </tr>
        <tr>
          <td><?php echo $entry_komis; ?></td>
          <td><input type="text" name="sbacquiring_komis" value="<?php echo isset($sbacquiring_komis) ? $sbacquiring_komis : ''; ?>" >%</td>
        </tr>
        <tr>
          <td><?php echo $entry_minpay; ?></td>
          <td><input type="text" name="sbacquiring_minpay" value="<?php echo isset($sbacquiring_minpay) ? $sbacquiring_minpay : ''; ?>" >руб.</td>
        </tr>
        <tr>
          <td><?php echo $entry_maxpay; ?></td>
          <td><input type="text" name="sbacquiring_maxpay" value="<?php echo isset($sbacquiring_maxpay) ? $sbacquiring_maxpay : ''; ?>" >руб.</td>
        </tr>
        <tr>
          <td><?php echo $entry_style; ?></td>
          <td><?php if ($sbacquiring_style) { ?>
            <input type="radio" name="sbacquiring_style" value="1" checked="checked" />
            <?php echo $text_yes; ?>
            <input type="radio" name="sbacquiring_style" value="0" />
            <?php echo $text_no; ?>
            <?php } else { ?>
            <input type="radio" name="sbacquiring_style" value="1" />
            <?php echo $text_yes; ?>
            <input type="radio" name="sbacquiring_style" value="0" checked="checked" />
            <?php echo $text_no; ?>
            <?php } ?></td>
        </tr>
        <tr>
          <td><?php echo $entry_button_later; ?></td>
          <td><?php if ($sbacquiring_button_later) { ?>
            <input type="radio" name="sbacquiring_button_later" value="1" checked="checked" />
            <?php echo $text_yes; ?>
            <input type="radio" name="sbacquiring_button_later" value="0" />
            <?php echo $text_no; ?>
            <?php } else { ?>
            <input type="radio" name="sbacquiring_button_later" value="1" />
            <?php echo $text_yes; ?>
            <input type="radio" name="sbacquiring_button_later" value="0" checked="checked" />
            <?php echo $text_no; ?>
            <?php } ?></td>
        </tr>
        <tr>
          <td><?php echo $text_createorder_or_notcreate; ?></td>
          <td><?php if ($sbacquiring_createorder_or_notcreate) { ?>
            <input type="radio" name="sbacquiring_createorder_or_notcreate" value="1" checked="checked" />
            <?php echo $text_yes; ?>
            <input type="radio" name="sbacquiring_createorder_or_notcreate" value="0" />
            <?php echo $text_no; ?>
            <?php } else { ?>
            <input type="radio" name="sbacquiring_createorder_or_notcreate" value="1" />
            <?php echo $text_yes; ?>
            <input type="radio" name="sbacquiring_createorder_or_notcreate" value="0" checked="checked" />
            <?php echo $text_no; ?>
            <?php } ?></td>
       </tr>
        <tr>
          <td><?php echo $entry_sbacquiring_success_alert_admin_tab; ?></td>
          <td><?php if ($sbacquiring_success_alert_admin) { ?>
            <input type="radio" name="sbacquiring_success_alert_admin" value="1" checked="checked" />
            <?php echo $text_yes; ?>
            <input type="radio" name="sbacquiring_success_alert_admin" value="0" />
            <?php echo $text_no; ?>
            <?php } else { ?>
            <input type="radio" name="sbacquiring_success_alert_admin" value="1" />
            <?php echo $text_yes; ?>
            <input type="radio" name="sbacquiring_success_alert_admin" value="0" checked="checked" />
            <?php echo $text_no; ?>
            <?php } ?></td>
        </tr>
        <tr>
          <td><?php echo $entry_sbacquiring_success_alert_customer_tab; ?></td>
          <td><?php if ($sbacquiring_success_alert_customer) { ?>
            <input type="radio" name="sbacquiring_success_alert_customer" value="1" checked="checked" />
            <?php echo $text_yes; ?>
            <input type="radio" name="sbacquiring_success_alert_customer" value="0" />
            <?php echo $text_no; ?>
            <?php } else { ?>
            <input type="radio" name="sbacquiring_success_alert_customer" value="1" />
            <?php echo $text_yes; ?>
            <input type="radio" name="sbacquiring_success_alert_customer" value="0" checked="checked" />
            <?php echo $text_no; ?>
            <?php } ?></td>
        </tr>
        <tr>
          <td><?php echo $entry_sbacquiring_instruction_tab; ?></td>
          <td><?php if ($sbacquiring_instruction_attach) { ?>
            <input type="radio" name="sbacquiring_instruction_attach" value="1" checked="checked" />
            <?php echo $text_yes; ?>
            <input type="radio" name="sbacquiring_instruction_attach" value="0" />
            <?php echo $text_no; ?>
            <?php } else { ?>
            <input type="radio" name="sbacquiring_instruction_attach" value="1" />
            <?php echo $text_yes; ?>
            <input type="radio" name="sbacquiring_instruction_attach" value="0" checked="checked" />
            <?php echo $text_no; ?>
            <?php } ?></td>
        </tr>
        <tr>
          <td><?php echo $entry_sbacquiring_instruction; ?></td>
          <td><?php foreach ($languages as $language) { ?><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" style="vertical-align:top;"/> <textarea name="sbacquiring_instruction_<?php echo $language['language_id']; ?>" cols="50" rows="3"><?php echo isset(${'sbacquiring_instruction_' . $language['language_id']}) ? ${'sbacquiring_instruction_' . $language['language_id']} : ''; ?></textarea><br /><?php } ?></td>
        </tr>
        <tr>
          <td><?php echo $entry_sbacquiring_mail_instruction_tab; ?></td>
          <td><?php if ($sbacquiring_mail_instruction_attach) { ?>
            <input type="radio" name="sbacquiring_mail_instruction_attach" value="1" checked="checked" />
            <?php echo $text_yes; ?>
            <input type="radio" name="sbacquiring_mail_instruction_attach" value="0" />
            <?php echo $text_no; ?>
            <?php } else { ?>
            <input type="radio" name="sbacquiring_mail_instruction_attach" value="1" />
            <?php echo $text_yes; ?>
            <input type="radio" name="sbacquiring_mail_instruction_attach" value="0" checked="checked" />
            <?php echo $text_no; ?>
            <?php } ?></td>
        </tr>
        <tr>
          <td><?php echo $entry_sbacquiring_mail_instruction; ?></td>
          <td><?php foreach ($languages as $language) { ?><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" style="vertical-align:top;"/> <textarea name="sbacquiring_mail_instruction_<?php echo $language['language_id']; ?>" cols="50" rows="3"><?php echo isset(${'sbacquiring_mail_instruction_' . $language['language_id']}) ? ${'sbacquiring_mail_instruction_' . $language['language_id']} : ''; ?></textarea><br /><?php } ?></td>
        </tr>
        <tr>
          <td><?php echo $entry_sbacquiring_success_comment_tab; ?></td>
          <td><?php if ($sbacquiring_success_comment_attach) { ?>
            <input type="radio" name="sbacquiring_success_comment_attach" value="1" checked="checked" />
            <?php echo $text_yes; ?>
            <input type="radio" name="sbacquiring_success_comment_attach" value="0" />
            <?php echo $text_no; ?>
            <?php } else { ?>
            <input type="radio" name="sbacquiring_success_comment_attach" value="1" />
            <?php echo $text_yes; ?>
            <input type="radio" name="sbacquiring_success_comment_attach" value="0" checked="checked" />
            <?php echo $text_no; ?>
            <?php } ?></td>
        </tr>
        <tr>
          <td><?php echo $entry_sbacquiring_success_comment; ?></td>
          <td><?php foreach ($languages as $language) { ?><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" style="vertical-align:top;"/> <textarea name="sbacquiring_success_comment_<?php echo $language['language_id']; ?>" cols="50" rows="3"><?php echo isset(${'sbacquiring_success_comment_' . $language['language_id']}) ? ${'sbacquiring_success_comment_' . $language['language_id']} : ''; ?></textarea><br /><?php } ?></td>
        </tr>
        <tr>
          <td><?php echo $entry_sbacquiring_hrefpage_tab; ?></td>
          <td><?php if ($sbacquiring_hrefpage_text_attach) { ?>
            <input type="radio" name="sbacquiring_hrefpage_text_attach" value="1" checked="checked" />
            <?php echo $text_my; ?>
            <input type="radio" name="sbacquiring_hrefpage_text_attach" value="0" />
            <?php echo $text_default; ?>
            <?php } else { ?>
            <input type="radio" name="sbacquiring_hrefpage_text_attach" value="1" />
            <?php echo $text_my; ?>
            <input type="radio" name="sbacquiring_hrefpage_text_attach" value="0" checked="checked" />
            <?php echo $text_default; ?>
            <?php } ?></td>
        </tr>
        <tr>
          <td><?php echo $entry_sbacquiring_hrefpage; ?></td>
          <td><?php foreach ($languages as $language) { ?><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" style="vertical-align:top;"/> <textarea name="sbacquiring_hrefpage_text_<?php echo $language['language_id']; ?>" cols="50" rows="3"><?php echo isset(${'sbacquiring_hrefpage_text_' . $language['language_id']}) ? ${'sbacquiring_hrefpage_text_' . $language['language_id']} : ''; ?></textarea><br /><?php } ?></td>
        </tr>
        <tr>
          <td><?php echo $entry_sbacquiring_success_page_tab; ?></td>
          <td><?php if ($sbacquiring_success_page_text_attach) { ?>
            <input type="radio" name="sbacquiring_success_page_text_attach" value="1" checked="checked" />
            <?php echo $text_my; ?>
            <input type="radio" name="sbacquiring_success_page_text_attach" value="0" />
            <?php echo $text_default; ?>
            <?php } else { ?>
            <input type="radio" name="sbacquiring_success_page_text_attach" value="1" />
            <?php echo $text_my; ?>
            <input type="radio" name="sbacquiring_success_page_text_attach" value="0" checked="checked" />
            <?php echo $text_default; ?>
            <?php } ?></td>
        </tr>
        <tr>
          <td><?php echo $entry_sbacquiring_success_page_text; ?></td>
          <td><?php foreach ($languages as $language) { ?><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" style="vertical-align:top;"/> <textarea name="sbacquiring_success_page_text_<?php echo $language['language_id']; ?>" cols="50" rows="3"><?php echo isset(${'sbacquiring_success_page_text_' . $language['language_id']}) ? ${'sbacquiring_success_page_text_' . $language['language_id']} : ''; ?></textarea><br /><?php } ?></td>
        </tr>
         <?php /* ?><tr>
          <td><?php echo $entry_sbacquiring_waiting_page_tab; ?></td>
          <td><?php if ($sbacquiring_waiting_page_text_attach) { ?>
            <input type="radio" name="sbacquiring_waiting_page_text_attach" value="1" checked="checked" />
            <?php echo $text_my; ?>
            <input type="radio" name="sbacquiring_waiting_page_text_attach" value="0" />
            <?php echo $text_default; ?>
            <?php } else { ?>
            <input type="radio" name="sbacquiring_waiting_page_text_attach" value="1" />
            <?php echo $text_my; ?>
            <input type="radio" name="sbacquiring_waiting_page_text_attach" value="0" checked="checked" />
            <?php echo $text_default; ?>
            <?php } ?></td>
        </tr>
        <tr>
          <td><?php echo $entry_sbacquiring_waiting_page_text; ?></td>
          <td><?php foreach ($languages as $language) { ?><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" style="vertical-align:top;"/> <textarea name="sbacquiring_waiting_page_text_<?php echo $language['language_id']; ?>" cols="50" rows="3"><?php echo isset(${'sbacquiring_waiting_page_text_' . $language['language_id']}) ? ${'sbacquiring_waiting_page_text_' . $language['language_id']} : ''; ?></textarea><br /><?php } ?></td>
        </tr>
        <?php */ ?>
        <tr>
          <td><?php echo $entry_fail_page_tab; ?></td>
          <td><?php if ($sbacquiring_fail_page_text_attach) { ?>
            <input type="radio" name="sbacquiring_fail_page_text_attach" value="1" checked="checked" />
            <?php echo $text_my; ?>
            <input type="radio" name="sbacquiring_fail_page_text_attach" value="0" />
            <?php echo $text_default; ?>
            <?php } else { ?>
            <input type="radio" name="sbacquiring_fail_page_text_attach" value="1" />
            <?php echo $text_my; ?>
            <input type="radio" name="sbacquiring_fail_page_text_attach" value="0" checked="checked" />
            <?php echo $text_default; ?>
            <?php } ?></td>
        </tr>
        <tr>
          <td><?php echo $entry_fail_page_text; ?></td>
          <td><?php foreach ($languages as $language) { ?>
          <img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" style="vertical-align:top;"/> <textarea name="sbacquiring_fail_page_text_<?php echo $language['language_id']; ?>" cols="50" rows="3"><?php echo isset(${'sbacquiring_fail_page_text_' . $language['language_id']}) ? ${'sbacquiring_fail_page_text_' . $language['language_id']} : ''; ?></textarea><br /><?php } ?></td>
        </tr>
        <tr>
        <td><?php echo $entry_on_status; ?></td>
        <td><select name="sbacquiring_on_status_id">
            <?php foreach ($order_statuses as $order_status) { ?>
            <?php if ($order_status['order_status_id'] == $sbacquiring_on_status_id) { ?>
            <option value="<?php echo $order_status['order_status_id']; ?>" selected="selected"><?php echo $order_status['name']; ?></option>
            <?php } else { ?>
            <option value="<?php echo $order_status['order_status_id']; ?>"><?php echo $order_status['name']; ?></option>
            <?php } ?>
            <?php } ?>
          </select></td>
      	</tr>
      	<tr>
        <td><?php echo $entry_order_status; ?></td>
        <td><select name="sbacquiring_order_status_id">
            <?php foreach ($order_statuses as $order_status) { ?>
            <?php if ($order_status['order_status_id'] == $sbacquiring_order_status_id) { ?>
            <option value="<?php echo $order_status['order_status_id']; ?>" selected="selected"><?php echo $order_status['name']; ?></option>
            <?php } else { ?>
            <option value="<?php echo $order_status['order_status_id']; ?>"><?php echo $order_status['name']; ?></option>
            <?php } ?>
            <?php } ?>
          </select></td>
      	</tr>
      	<tr>
        <td><?php echo $entry_geo_zone; ?></td>
        <td><select name="sbacquiring_geo_zone_id">
            <option value="0"><?php echo $text_all_zones; ?></option>
            <?php foreach ($geo_zones as $geo_zone) { ?>
            <?php if ($geo_zone['geo_zone_id'] == $sbacquiring_geo_zone_id) { ?>
            <option value="<?php echo $geo_zone['geo_zone_id']; ?>" selected="selected"><?php echo $geo_zone['name']; ?></option>
            <?php } else { ?>
            <option value="<?php echo $geo_zone['geo_zone_id']; ?>"><?php echo $geo_zone['name']; ?></option>
            <?php } ?>
            <?php } ?>
          </select></td>
      	</tr>
      	<tr>
        <td><?php echo $entry_status; ?></td>
        <td><select name="sbacquiring_status">
            <?php if ($sbacquiring_status) { ?>
            <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
            <option value="0"><?php echo $text_disabled; ?></option>
            <?php } else { ?>
            <option value="1"><?php echo $text_enabled; ?></option>
            <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
            <?php } ?>
          </select></td>
      	</tr>
      	 <tr>
          <td><?php echo $entry_sort_order; ?></td>
          <td><input type="text" name="sbacquiring_sort_order" value="<?php echo $sbacquiring_sort_order; ?>" size="1" /></td>
        </tr>
      </table>

        </div>
        <input type="hidden" name="sbacquiring_methodcode" value="SBA" />
      </form>
    </div>
      <p style="text-align:center;">SBAcquiring <?php echo $version ?></p>
  </div>
</div>
<?php echo $footer; ?> 
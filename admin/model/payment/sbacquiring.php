<?php
class ModelPaymentsbacquiring extends Model {
	private $key;
	private $iv;
	
	public function encrypt($value, $key) {
		$key = hash('sha256', $key, true);
		$iv = mcrypt_create_iv(32, MCRYPT_RAND);
		return strtr(base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, $key, $value, MCRYPT_MODE_ECB, $iv)), '+/=', '-_,');
	}
	
	public function decrypt($value, $key) {
		$key = hash('sha256', $key, true);
		$iv = mcrypt_create_iv(32, MCRYPT_RAND);
		return trim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, $key, base64_decode(strtr($value, '-_,', '+/=')), MCRYPT_MODE_ECB, $iv));
	}

	public function getStatus() {
		$query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "sbacquiring` ORDER BY `yandex_id` DESC");
		return $query->rows;
	}

	public function getPayMethods() {
		
		$sb = array('sbacquiring');
    	$sb_codes = array();  
    	foreach ($sb as $sbcode){if ($this->config->get($sbcode.'_status')){$sb_codes[] = $sbcode;}}

		return $sb_codes;
	}
}
?>
<?php
class ModelExtensionNews extends Model {
	public function addNews($data) {
		$this->db->query("INSERT INTO " . DB_PREFIX . "news SET image = '" . $this->db->escape($data['image']) . "', nc_id = '" . (int)$data['cat_id'] . "', date_added = NOW(), status = '" . (int)$data['status'] . "', alias = '" . $this->db->escape($data['keyword']) . "'");
		
		$news_id = $this->db->getLastId();
		
		foreach ($data['news'] as $key => $value) {
			$this->db->query("INSERT INTO " . DB_PREFIX ."news_description SET news_id = '" . (int)$news_id . "', language_id = '" . (int)$key . "', title = '" . $this->db->escape($value['title']) . "', image_title = '" . $this->db->escape($value['image_title']) . "', period = '" . $this->db->escape($value['period']) . "', description = '" . $this->db->escape($value['description']) . "', meta_description = '" . $this->db->escape($value['meta_description']) . "', short_description = '" . $this->db->escape($value['short_description']) . "'");
		}
		
		if ($data['keyword']) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "url_alias SET query = 'news_id=" . (int)$news_id . "', keyword = '" . $this->db->escape($data['keyword']) . '.html' . "'");
		}

        if (isset($data['product_related'])) {
            foreach ($data['product_related'] as $related_id) {
                $this->db->query("DELETE FROM " . DB_PREFIX . "product_action WHERE product_id = '" . (int)$news_id . "' AND related_id = '" . (int)$related_id . "'");
                $this->db->query("INSERT INTO " . DB_PREFIX . "product_action SET product_id = '" . (int)$news_id . "', related_id = '" . (int)$related_id . "'");
            }
        }
	}

	public function addNewsCat($data) {
		$this->db->query("INSERT INTO " . DB_PREFIX . "news_category SET status = '" . (int)$data['status'] . "', alias = '" . $this->db->escape($data['keyword']) . "', sort_order = '" . (int)$data['sort_order'] . "'");

		$news_id = $this->db->getLastId();

		foreach ($data['news'] as $key => $value) {
			$this->db->query("INSERT INTO " . DB_PREFIX ."news_category_description SET news_cat_id = '" . (int)$news_id . "', language_id = '" . (int)$key . "', title = '" . $this->db->escape($value['title']) . "', meta_description = '" . $this->db->escape($value['meta_description']) . "'");
		}

		if ($data['keyword']) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "url_alias SET query = 'news_cat_id=" . (int)$news_id . "', keyword = '" . $this->db->escape($data['keyword']) . "'");
		}
	}
	
	public function editNews($news_id, $data) {
		$this->db->query("UPDATE " . DB_PREFIX . "news SET image = '" . $this->db->escape($data['image']) . "', nc_id = '" . (int)$data['cat_id'] . "', status = '" . (int)$data['status'] . "', alias = '" . $this->db->escape($data['keyword']) . "' WHERE news_id = '" . (int)$news_id . "'");
		
		$this->db->query("DELETE FROM " . DB_PREFIX . "news_description WHERE news_id = '" . (int)$news_id. "'");
		
		foreach ($data['news'] as $key => $value) {
			$this->db->query("INSERT INTO " . DB_PREFIX ."news_description SET news_id = '" . (int)$news_id . "', language_id = '" . (int)$key . "', title = '" . $this->db->escape($value['title']) . "', image_title = '" . $this->db->escape($value['image_title']) . "', period = '" . $this->db->escape($value['period']) . "', description = '" . $this->db->escape($value['description']) . "', meta_description = '" . $this->db->escape($value['meta_description']) . "', short_description = '" . $this->db->escape($value['short_description']) . "'");
		}
		
		$this->db->query("DELETE FROM " . DB_PREFIX . "url_alias WHERE query = 'news_id=" . (int)$news_id. "'");
		
		if ($data['keyword']) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "url_alias SET query = 'news_id=" . (int)$news_id . "', keyword = '" . $this->db->escape($data['keyword']) . '.html' . "'");
		}

        $this->db->query("DELETE FROM " . DB_PREFIX . "product_related WHERE product_id = '" . (int)$news_id . "'");

        if (isset($data['product_related'])) {
            foreach ($data['product_related'] as $related_id) {
                $this->db->query("DELETE FROM " . DB_PREFIX . "product_action WHERE product_id = '" . (int)$news_id . "' AND related_id = '" . (int)$related_id . "'");
                $this->db->query("INSERT INTO " . DB_PREFIX . "product_action SET product_id = '" . (int)$news_id . "', related_id = '" . (int)$related_id . "'");
            }
        }

		// tags
		$this->db->query("DELETE FROM " . DB_PREFIX . "news_tags_id WHERE news_id = '" . (int)$news_id. "'");
		if (isset($data['tags'])) {

			$gags_ar = explode(',',$data['tags']);
			array_walk($gags_ar, 'trim');
			if (!empty ($gags_ar)) {
				foreach ($gags_ar as $tag) {
					$id_tag = $this->getTagTab(mb_strtolower($tag));
					$this->db->query("INSERT INTO " . DB_PREFIX . "news_tags_id SET news_id = '" . (int)$news_id . "', tags_id = '" . $id_tag . "'");
				}
			}
		}

	}

	public function getTagTab($name) {
		$query = $this->db->query("SELECT tags_id FROM " . DB_PREFIX . "news_tags WHERE name = '" . $this->db->escape($name) . "'");
		if (!empty($query->row['tags_id']) && (int)$query->row['tags_id'] > 0) {
			return (int)$query->row['tags_id'];
		} else {
			$this->db->query("INSERT INTO " . DB_PREFIX . "news_tags SET name = '" . $this->db->escape($name) . "'");
			$tag_id = $this->db->getLastId();
			$alias = $this->url->alias($this->db->escape($name)) . '_' . $tag_id;
			$this->db->query("UPDATE " . DB_PREFIX . "news_tags SET alias = '" . $alias . "' WHERE tags_id = '" . (int)$tag_id . "'");
			$this->db->query("INSERT INTO " . DB_PREFIX . "url_alias SET query = 'tags_id=" . (int)$tag_id . "', keyword = '" . $this->db->escape($alias) . "'");
			return $tag_id;
		}
	}

	public function editNewsCat($news_id, $data) {
		$this->db->query("UPDATE " . DB_PREFIX . "news_category SET status = '" . (int)$data['status'] . "', sort_order = '" . (int)$data['sort_order'] . "', alias = '" . $this->db->escape($data['keyword']) . "' WHERE news_cat_id = '" . (int)$news_id . "'");

		$this->db->query("DELETE FROM " . DB_PREFIX . "news_category_description WHERE news_cat_id = '" . (int)$news_id. "'");

		foreach ($data['news'] as $key => $value) {
			$this->db->query("INSERT INTO " . DB_PREFIX ."news_category_description SET news_cat_id = '" . (int)$news_id . "', language_id = '" . (int)$key . "', title = '" . $this->db->escape($value['title']) . "', meta_description = '" . $this->db->escape($value['meta_description']) . "'");
		}

		$this->db->query("DELETE FROM " . DB_PREFIX . "url_alias WHERE query = 'news_cat_id=" . (int)$news_id. "'");

		if ($data['keyword']) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "url_alias SET query = 'news_cat_id=" . (int)$news_id . "', keyword = '" . $this->db->escape($data['keyword']) . "'");
		}
	}
	
	public function getNews($news_id) {
		$query = $this->db->query("SELECT DISTINCT *, (SELECT keyword FROM " . DB_PREFIX . "url_alias WHERE query = 'news_id=" . (int)$news_id . "') AS keyword FROM " . DB_PREFIX . "news WHERE news_id = '" . (int)$news_id . "'"); 
 
		if ($query->num_rows) {
			return $query->row;
		} else {
			return false;
		}
	}

	public function getNewsCat($news_id) {
		$query = $this->db->query("SELECT DISTINCT *, (SELECT keyword FROM " . DB_PREFIX . "url_alias WHERE query = 'news_cat_id=" . (int)$news_id . "') AS keyword FROM " . DB_PREFIX . "news_category WHERE news_cat_id = '" . (int)$news_id . "'");

		if ($query->num_rows) {
			return $query->row;
		} else {
			return false;
		}
	}
   
	public function getNewsDescription($news_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "news_description WHERE news_id = '" . (int)$news_id . "'"); 
		
		foreach ($query->rows as $result) {
			$news_description[$result['language_id']] = array(
				'title'       			=> $result['title'],
                'image_title'       			=> $result['image_title'],
                'period'       			=> $result['period'],
				'short_description'		=> $result['short_description'],
                'meta_description'		=> $result['meta_description'],
				'description' 			=> $result['description']
			);
		}
		
		return $news_description;
	}

	public function getNewsDescriptionCat($news_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "news_category_description WHERE news_cat_id = '" . (int)$news_id . "'");

		foreach ($query->rows as $result) {
			$news_description[$result['language_id']] = array(
				'title'       			=> $result['title'],
				'image_title'       			=> $result['image_title'],
				'meta_description'		=> $result['meta_description']
			);
		}

		return $news_description;
	}
 
	public function getAllNews($data) {
		$sql = "SELECT * FROM " . DB_PREFIX . "news n LEFT JOIN " . DB_PREFIX . "news_description nd ON n.news_id = nd.news_id WHERE nd.language_id = '" . (int)$this->config->get('config_language_id') . "' AND n.nc_id = '" . (int)$data['cat_id'] . "' ORDER BY date_added DESC";
		
		if (isset($data['start']) && isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}
			
			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}	
		
			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}	
		
		$query = $this->db->query($sql);
 
		return $query->rows;
	}
   
	public function deleteNews($news_id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "news WHERE news_id = '" . (int)$news_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "news_description WHERE news_id = '" . (int)$news_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "url_alias WHERE query = 'news_id=" . (int)$news_id. "'");
	}

	public function deleteNewsCat($news_id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "news_category WHERE news_cat_id = '" . (int)$news_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "news_category_description WHERE news_cat_id = '" . (int)$news_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "url_alias WHERE query = 'news_cat_id=" . (int)$news_id. "'");

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "news WHERE nc_id = '" . (int)$news_id . "'");
		foreach ($query->rows as $result) {
			$this->deleteNews($result['news_id']);
			$this->db->query("DELETE FROM " . DB_PREFIX . "news_tags_id WHERE news_id = '" . (int)$result['news_id'] . "'");
		}
	}
   
	public function getTotalNews() {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "news");
	
		return $query->row['total'];
	}

    public function getUrlAlias($keyword) {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "url_alias WHERE keyword = '" . $this->db->escape($keyword) . "'");

        return $query->row;
    }

	public function getTotalNewsCategory() {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "news_category");

		return $query->row['total'];
	}

	public function getTotalNewsCat($cat_id) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "news_category WHERE news_cat_id = '" . (int)$cat_id );

		return $query->row['total'];
	}

	public function getIsetCat($cat_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "news_category_description WHERE news_cat_id = '" . (int)$cat_id . "' AND language_id = 2");

		if ($query->num_rows) {
			return $query->row;
		} else {
			return false;
		}
	}

	public function getAllNewsCategory($data) {
		$sql = "SELECT * FROM " . DB_PREFIX . "news_category n LEFT JOIN " . DB_PREFIX . "news_category_description nd ON n.news_cat_id = nd.news_cat_id WHERE nd.language_id = '" . (int)$this->config->get('config_language_id') . "' ORDER BY sort_order DESC";

		if (isset($data['start']) && isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}

		$query = $this->db->query($sql);

		return $query->rows;
	}

	public function getCategoriesFilter($data = array()) {
		//$sql = "SELECT cp.news_cat_id AS category_id, GROUP_CONCAT(cd1.title ORDER BY cp.sort_order SEPARATOR '&nbsp;&nbsp;&gt;&nbsp;&nbsp;') AS name, c1.news_cat_id, c1.sort_order FROM " . DB_PREFIX . "news_category cp LEFT JOIN " . DB_PREFIX . "news_category c1 ON (cp.news_cat_id = c1.news_cat_id) LEFT JOIN " . DB_PREFIX . "news_category c2 ON (cp.news_cat_id = c2.news_cat_id) LEFT JOIN " . DB_PREFIX . "news_category_description cd1 ON (cp.news_cat_id = cd1.news_cat_id) LEFT JOIN " . DB_PREFIX . "news_category_description cd2 ON (cp.news_cat_id = cd2.news_cat_id) WHERE cd1.language_id = '" . (int)$this->config->get('config_language_id') . "' AND cd2.language_id = '" . (int)$this->config->get('config_language_id') . "'";

		$sql = "SELECT nc.news_cat_id , c1.title FROM " . DB_PREFIX . "news_category nc LEFT JOIN " . DB_PREFIX . "news_category_description c1 ON (nc.news_cat_id = c1.news_cat_id) WHERE c1.language_id = '" . (int)$this->config->get('config_language_id') . "'";

		if (!empty($data['filter_name'])) {
			$sql .= " AND c1.title LIKE '" . $this->db->escape($data['filter_name']) . "%'";
		}

		$sort_data = array(
			'title',
			'sort_order'
		);

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY sort_order";
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}

		$query = $this->db->query($sql);

		return $query->rows;
	}

	public function getTagsId ($news_id) {
		$query = $this->db->query("SELECT nti.tags_id , nt.name FROM " . DB_PREFIX . "news_tags_id nti LEFT JOIN " . DB_PREFIX . "news_tags nt ON (nt.tags_id = nti.tags_id) WHERE nti.news_id = '" . (int)$news_id . "'");

		$out = array();
		foreach ($query->rows as $result) {
			$out[$result['tags_id']] = $result['name'];
		}

		return $out;
	}
}
<?php
class ModelExtensionAction extends Model {
    public function addAction($data) {

        $prod = isset($data['product_related']) ? 1 : 0;

        $this->db->query("INSERT INTO " . DB_PREFIX . "action SET image = '" . $this->db->escape($data['image']) . "', prod = '" . (int)$prod . "', date_added = NOW(), status = '" . (int)$data['status'] . "', alias = '" . $this->db->escape($data['keyword']) . "', url = '" . $this->db->escape($data['url']) . "', discount = '" . (!empty($data['discount']) ? $this->db->escape($data['discount']) : '') . "'");

        $action_id = $this->db->getLastId();

        foreach ($data['action'] as $key => $value) {
            $this->db->query("INSERT INTO " . DB_PREFIX ."action_description SET action_id = '" . (int)$action_id . "', language_id = '" . (int)$key . "', title = '" . $this->db->escape($value['title']) . "', image_title = '" . $this->db->escape($value['image_title']) . "', period = '" . $this->db->escape($value['period']) . "', description = '" . $this->db->escape($value['description']) . "', meta_description = '" . $this->db->escape($value['meta_description']) . "', short_description = '" . $this->db->escape($value['short_description']) . "'");
        }

        if ($data['keyword']) {
            $this->db->query("INSERT INTO " . DB_PREFIX . "url_alias SET query = 'action_id=" . (int)$action_id . "', keyword = '" . $this->db->escape($data['keyword']) . "'");
        }

        if (isset($data['product_related'])) {
            foreach ($data['product_related'] as $related_id) {
                $this->db->query("DELETE FROM " . DB_PREFIX . "product_action WHERE product_id = '" . (int)$action_id . "' AND related_id = '" . (int)$related_id . "'");
                $this->db->query("INSERT INTO " . DB_PREFIX . "product_action SET product_id = '" . (int)$action_id . "', related_id = '" . (int)$related_id . "'");
            }
        }
    }

    public function editAction($action_id, $data) {

        $prod = isset($data['product_related']) ? 1 : 0;

        $this->db->query("UPDATE " . DB_PREFIX . "action SET image = '" . $this->db->escape($data['image']) . "', prod = '" . (int)$prod . "', status = '" . (int)$data['status'] . "', alias = '" . $this->db->escape($data['keyword']) . "', url = '" . $this->db->escape($data['url']) . "', discount = '" . (!empty($data['discount']) ? $this->db->escape($data['discount']) : '') . "' WHERE action_id = '" . (int)$action_id . "'");

        $this->db->query("DELETE FROM " . DB_PREFIX . "action_description WHERE action_id = '" . (int)$action_id. "'");

        foreach ($data['action'] as $key => $value) {
            $this->db->query("INSERT INTO " . DB_PREFIX ."action_description SET action_id = '" . (int)$action_id . "', language_id = '" . (int)$key . "', title = '" . $this->db->escape($value['title']) . "', image_title = '" . $this->db->escape($value['image_title']) . "', period = '" . $this->db->escape($value['period']) . "', description = '" . $this->db->escape($value['description']) . "', meta_description = '" . $this->db->escape($value['meta_description']) . "', short_description = '" . $this->db->escape($value['short_description']) . "'");
        }

        $this->db->query("DELETE FROM " . DB_PREFIX . "url_alias WHERE query = 'action_id=" . (int)$action_id. "'");

        if ($data['keyword']) {
            $this->db->query("INSERT INTO " . DB_PREFIX . "url_alias SET query = 'action_id=" . (int)$action_id . "', keyword = '" . $this->db->escape($data['keyword']) . "'");
        }

        $this->db->query("DELETE FROM " . DB_PREFIX . "product_action WHERE product_id = '" . (int)$action_id . "'");

        if (isset($data['product_related'])) {
            foreach ($data['product_related'] as $related_id) {
                $this->db->query("DELETE FROM " . DB_PREFIX . "product_action WHERE product_id = '" . (int)$action_id . "' AND related_id = '" . (int)$related_id . "'");
                $this->db->query("INSERT INTO " . DB_PREFIX . "product_action SET product_id = '" . (int)$action_id . "', related_id = '" . (int)$related_id . "'");
            }
        }
    }

    public function getAction($action_id) {
        $query = $this->db->query("SELECT DISTINCT *, (SELECT keyword FROM " . DB_PREFIX . "url_alias WHERE query = 'action_id=" . (int)$action_id . "') AS keyword FROM " . DB_PREFIX . "action WHERE action_id = '" . (int)$action_id . "'");

        if ($query->num_rows) {
            return $query->row;
        } else {
            return false;
        }
    }

    public function getActionDescription($action_id) {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "action_description WHERE action_id = '" . (int)$action_id . "'");

        foreach ($query->rows as $result) {
            $action_description[$result['language_id']] = array(
                'title'       			=> $result['title'],
                'image_title'       			=> $result['image_title'],
                'period'       			=> $result['period'],
                'short_description'		=> $result['short_description'],
                'meta_description'		=> $result['meta_description'],
                'description' 			=> $result['description']
            );
        }

        return $action_description;
    }

    public function getAllAction($data) {
        $sql = "SELECT * FROM " . DB_PREFIX . "action n LEFT JOIN " . DB_PREFIX . "action_description nd ON n.action_id = nd.action_id WHERE nd.language_id = '" . (int)$this->config->get('config_language_id') . "' ORDER BY date_added DESC";

        if (isset($data['start']) && isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }

            $sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
        }

        $query = $this->db->query($sql);

        return $query->rows;
    }

    public function deleteAction($action_id) {
        $this->db->query("DELETE FROM " . DB_PREFIX . "action WHERE action_id = '" . (int)$action_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "action_description WHERE action_id = '" . (int)$action_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "url_alias WHERE query = 'action_id=" . (int)$action_id. "'");
    }

    public function getTotalAction() {
        $query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "action");

        return $query->row['total'];
    }

    public function getUrlAlias($keyword) {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "url_alias WHERE keyword = '" . $this->db->escape($keyword) . "'");

        return $query->row;
    }
}
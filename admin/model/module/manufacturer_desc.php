<?php
/**
 * Created by PhpStorm.
 * User: AtrDevue - manufacturer_desc.php
 * Date: 05.04.15
 * Time: 12:38
 * Project: thai-style
 */
class ModelModuleManufacturerDesc extends Model
{
    public $table_name = 'manufacturer';

    public function checkInstall()
    {
        $qcol = 'SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE COLUMN_NAME = `description` AND TABLE_NAME = `' . DB_PREFIX . $this->table_name . '`';
        if (!$this->db->query($qcol))
        {
            $query = ' ALTER TABLE `' . DB_PREFIX . $this->table_name . '` ADD `description` CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL AFTER AFTER `name`';
            $this->db->query($query);
        }

    }

    public function checkUninstall()
    {

    }
}
<?php
// Heading 
$_['heading_title']     		= 'Faq';
$_['text_list']     		    = 'Все вопросы';


// Text
$_['entry_name']                 = 'Название';
$_['entry_date_added']                 = 'Дата добавления';
$_['button_filter']                 = 'Фильтр Faq';
$_['button_view_all_faq']                 = 'Посмотреть все вопросы';
$_['text_edit']                 = 'Редактировать вопросы';
$_['text_image']				= 'Изображение';
$_['text_title']				= 'Название';
$_['text_answer']		     	= 'Ответ';
$_['text_question']	            = 'Вопрос';
$_['text_date']					= 'Дата добавлнеия';
$_['text_action']				= 'Дкйствие';
$_['text_status']				= 'Статус';
$_['text_keyword']				= 'SEO ключи';
$_['text_no_results']			= 'Нет результатов в списке.';
$_['text_browse']				= 'Просматривать';
$_['text_clear']				= 'Очистить';
$_['text_image_manager']		= 'Менеджер изображений';
$_['text_success']				= 'Вы успешно изменили страницу!';

// Error
$_['error_permission'] 			= 'Внимание: Вы не имеют разрешения на изменения Faq!';
?>
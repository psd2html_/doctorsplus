<?php
// Heading 
$_['heading_title']     		= 'Акции';

// Text
$_['text_list']					= 'Список акциий';
$_['text_image']				= 'Картинка';
$_['text_discount']				= 'Скидка';
$_['text_title']				= 'Название';
$_['text_image_title']			= 'Название на картинке';
$_['text_description']			= 'Описание';
$_['text_period']			    = 'Период';
$_['text_short_description']	= 'Условия проведения';
$_['text_meta_description']	    = 'Мета-тег Description';
$_['text_date']					= 'Дата публикации';
$_['text_action']				= 'Действия';
$_['text_edit']					= 'Редактировать';
$_['text_status']				= 'Статус';
$_['text_keyword']				= 'SEO URL';
$_['text_alias']				= 'SEO URL';
$_['text_no_results']			= 'Пустой список';
$_['text_browse']				= 'Выбрать';
$_['text_clear']				= 'Очитстить';
$_['text_image_manager']		= 'Менеджер изображений';
$_['text_success']				= 'Успешно!';

// Error
$_['error_permission'] 			= 'У вас нет прав!';
?>
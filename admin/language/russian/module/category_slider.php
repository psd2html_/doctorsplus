<?php
// Heading
$_['heading_module']    = 'Thai - Категории в слайдере';

if (!empty($_GET['route']) && strpos($_GET['route'], 'design/layout') !== false) {
    $_['heading_title']             = $_['heading_module'];
} else {
    $_['heading_title']             = '<span style="color: #8abf49; font-weight: bold;">'. $_['heading_module'] .'</span>';
}

// Text
$_['text_module']      = 'Модули';
$_['text_success']     = 'Настройки успешно изменены!';
$_['text_edit']        = 'Настройки модуля';

// Entry
$_['entry_status']     = 'Статус';

// Error
$_['error_permission'] = 'У Вас нет прав для управления данным модулем!';


<?php
// Heading
$_['heading_module']    = 'Thai - Общии настройки';

if (!empty($_GET['route']) && strpos($_GET['route'], 'design/layout') !== false) {
    $_['heading_title']             = $_['heading_module'];
} else {
    $_['heading_title']             = '<span style="color: #8abf49; font-weight: bold;">'. $_['heading_module'] .'</span>';
}

// Text
$_['text_module']      = 'Модули';
$_['text_success']     = 'Настройки успешно изменены!';
$_['text_edit']        = 'Общии настройки модуля';

// Tab
$_['tab_general']      = 'Основное';
$_['tab_social']       = 'Соц сети';

// Entry
$_['entry_name'] = 'Название модуля';
$_['entry_soc_fb']      = 'Facebook';
$_['entry_soc_vk']      = 'Вконтакте';

// Error
$_['error_permission'] = 'У Вас нет прав для управления данным модулем!';
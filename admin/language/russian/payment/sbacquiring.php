<?php
$_['heading_title']      = 'Сбербанк эквайринг';
$_['entry_userName']     = 'Логин API';
$_['entry_password']     = 'Пароль API';
$_['entry_komis'] 		 = 'Комиссия или скидка с покупателя<br/><small>Для скидки установите отрицательное значение</small>';
$_['entry_fixen']        = 'Cумма к оплате';
$_['entry_fixen_order']  = 'Заказ';
$_['entry_fixen_proc']   = 'Процент от заказа';
$_['entry_fixen_fix']    = 'Фиксированная';
$_['entry_fixen_amount'] = 'Сумма или процент к оплате';
$_['entry_minpay'] 		 = 'Минимальная сумма заказа(ели меньше то метод не отображается)';
$_['entry_maxpay'] 		 = 'Максимальная сумма заказа(ели больше то метод не отображается)';
$_['entry_on_status']    = 'Статус заказа после неуспешной или ожидаемой оплаты';
$_['entry_order_status'] = 'Статус после удачной оплаты';
$_['entry_geo_zone']     = 'Регион доступности оплаты';
$_['entry_status']       = 'Статус';
$_['entry_sort_order']   = 'Сортировка';
$_['entry_style']        = 'Вид кнопки под стиль темы';
$_['text_payment']       = 'Оплата';
$_['text_order']         = 'Заказы';
$_['status_title']       = 'Оплаченные заказы';
$_['text_success']       = 'Настройки сохранены';
$_['text_my']       	 = 'Свой';
$_['text_default']       = 'По умолчанию';

$_['entry_met']       			= 'Способ транзакции';
$_['entry_met_odnostage']       = 'Одностадийная';
$_['entry_met_preautoriz']      = 'Двухстадийная (c предавторизацией)';


$_['entry_sbacquiring_instruction'] 				= 'Инструкция при оформлении заказа:<br/><small>Поддерживает переменные:<br/>$href$ - ссылка на оплату<br/>$orderid$ - номер заказа<br/>$itogo$ - сумма заказа<br/>$komis$ - комисcия в процентах<br/>$total-komis$ - посчитанная комиссия от суммы<br/>$plus-komis$ - сумма с комиссией<br/>а также html теги.</small>';
$_['entry_sbacquiring_instruction_tab']      		= 'Использовать инструкцию при оформлении заказа?';
$_['entry_sbacquiring_mail_instruction_tab']      	= 'Использовать инструкцию в письме с заказом?';
$_['entry_sbacquiring_mail_instruction']      		= 'Инструкция в письме с заказом:<br/><small>Поддерживает переменные:<br/>$href$ - ссылка на оплату<br/>$orderid$ - номер заказа<br/>$itogo$ - сумма заказа<br/>$komis$ - комисcия в процентах<br/>$total-komis$ - посчитанная комиссия от суммы<br/>$plus-komis$ - сумма с комиссией<br/> а также html теги.</small>';
$_['entry_sbacquiring_success_comment_tab']      	= 'Использовать комментарий покупателю в письме о успешной оплате?';
$_['entry_sbacquiring_success_comment']      		= 'Комментарий покупателю в письме о успешной оплате:<br/><small>Поддерживает переменные:<br/>$orderid$ - номер заказа<br/>$itogo$ - сумма заказа</small>';
$_['entry_sbacquiring_name_tab']      				= 'Текст в название метода оплаты?';
$_['entry_sbacquiring_name']      					= 'Название метода оплаты';
$_['entry_sbacquiring_success_alert_admin_tab']    = 'Письмо администратору при успешной оплате';
$_['entry_sbacquiring_success_alert_customer_tab'] = 'Письмо покупателю при успешной оплате';
$_['entry_sbacquiring_success_page_tab']      		= 'Свой текст на странице успешной оплаты?';
$_['entry_sbacquiring_success_page_text']     		= 'Текст на странице успешной оплаты:<br/><small>Поддерживает переменные:<br/>$href$ - ссылка на оплату<br/>$orderid$ - номер заказа<br/>$itogo$ - сумма заказа<br/>$komis$ - комисcия в процентах<br/>$total-komis$ - посчитанная комиссия от суммы<br/>$plus-komis$ - сумма с комиссией<br/>а также html теги.</small>';
$_['entry_sbacquiring_waiting_page_tab']      		= 'Свой текст на странице ожидаемой оплаты?';
$_['entry_sbacquiring_waiting_page_text']      	= 'Текст на странице ожидаемой оплаты:<br/><small>Поддерживает переменные:<br/>$href$ - ссылка на оплату<br/>$orderid$ - номер заказа<br/>$itogo$ - сумма заказа<br/>$komis$ - комисcия в процентах<br/>$total-komis$ - посчитанная комиссия от суммы<br/>$plus-komis$ - сумма с комиссией<br/>а также html теги.</small>';
$_['entry_button_later']      					= 'Кнопка оплатить позже в оформлении заказа';

$_['entry_sbacquiring_hrefpage_tab']      			= 'Свой текст на странице после перехода по ссылки из письма или из личного кабинета?';
$_['entry_sbacquiring_hrefpage']      				= 'Текст на странице после перехода по ссылки из письма или из личного кабинета:<br/><small>Поддерживает переменные:<br/>$orderid$ - номер заказа<br/>$itogo$ - сумма заказа<br/>$komis$ - комисcия в процентах<br/>$total-komis$ - посчитанная комиссия от суммы<br/>$plus-komis$ - сумма с комиссией<br/>а также html теги.</small>';

$_['text_createorder_or_notcreate']       = 'Создавать заказ после оплаты<br/><small>Если "Нет" то заказ создатся сразу, независимого от того прошла оплата или нет.</small>';

$_['entry_fail_page_tab']      = 'Свой текст на странице не успешной оплаты?';
$_['entry_fail_page_text']      = 'Текст на странице не успешной оплаты:<br/><small>Поддерживает переменные:<br/>$href$ - ссылка на оплату<br/>$orderid$ - номер заказа<br/>$itogo$ - сумма оплаты<br/>$komis$ - комисcия в процентах<br/>$total-komis$ - посчитанная комиссия от суммы<br/>$plus-komis$ - сумма с комиссией<br/>а также html теги.</small>';


//Ошибки
$_['entry_license']   = 'Лицензионный ключ';
$_['error_license']   = 'Введите Лицензионный ключ!<br/><small>Купить ключ можно на <a href="http://store.pe-art.ru">http://store.pe-art.ru</a></small>';
$_['error_key_er']    = 'Неправильный Лицензионный ключ!<br/><small>Ключ действителен только для одного домена. Купить ключ можно на <a href="http://store.pe-art.ru">http://store.pe-art.ru</a></small>';
$_['error_shopId']    = 'Введите идентификатор контрагента (shopId) полученный от яндекс.денег';
$_['error_scid'] 	  = 'Введите номер витрины контрагента (scid) полученный от яндекс.денег';
$_['error_password']  = 'Введите секретное слово';

$_['pay_text_mail'] 								= 'Перейти на оплату можно по ссылке';
$_['pay_text_admin'] 								= 'Ссылка на оплату:';

//Статус
$_['yandex_id']   = 'ID';
$_['num_order']   = 'Номер заказа';
$_['sum']   = 'Сумма';
$_['user']   = 'Пользователь';
$_['email']   = 'email';
$_['date_created']   = 'Дата создания';
$_['date_enroled']   = 'Дата оплаты';
$_['sender']   = 'Идентификатор операции';
?>
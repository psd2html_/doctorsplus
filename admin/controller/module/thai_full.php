<?php
/**
 * Created by PhpStorm.
 * User: AtrDevue - thai_full.php
 * Date: 02.04.15
 * Time: 14:30
 * Project: thai-style
 */
ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);

class ControllerModuleThaiFull extends Controller
{
    private $error = array();

    public function __construct($registry)
    {
        parent::__construct($registry);

        $this->load->model('module/thai_full');
    }

    public function index()
    {
        $this->load->language('module/thai_full');
        $this->document->setTitle($this->language->get('heading_title'));

        foreach ($this->model_module_thai_full->getListData() as $key => $dat)
        {
            $data[$key] = $dat;
        }

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate())
        {
            $this->model_module_thai_full->editModule($this->request->post);

            $this->session->data['success'] = $this->language->get('text_success');

            $this->response->redirect($this->url->link('module/thai_full', 'token=' . $this->session->data['token'], 'SSL'));
        }

        $data['heading_title'] = $this->language->get('heading_title');
        $data['text_edit'] = $this->language->get('text_edit');

        $data['button_save'] = $this->language->get('button_save');
        $data['button_cancel'] = $this->language->get('button_cancel');

        $data['tab_general'] = $this->language->get('tab_general');
        $data['tab_social'] = $this->language->get('tab_social');

        $data['entry_soc_vk'] = $this->language->get('entry_soc_vk');
        $data['entry_soc_fb'] = $this->language->get('entry_soc_fb');

        if (isset($this->error['warning']))
        {
            $data['error_warning'] = $this->error['warning'];
        }
        else
        {
            $data['error_warning'] = '';
        }

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_module'),
            'href' => $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL')
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('module/thai_full', 'token=' . $this->session->data['token'], 'SSL')
        );

        $data['action'] = $this->url->link('module/thai_full', 'token=' . $this->session->data['token'], 'SSL');

        $data['cancel'] = $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL');

        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');

        $this->response->setOutput($this->load->view('module/thai_full.tpl', $data));

    }

    public function install()
    {
        $this->model_module_thai_full->checkInstall();
    }

    public function uninstall()
    {
        $this->model_module_thai_full->checkUninstall();
    }

    protected function validate()
    {
        if (!$this->user->hasPermission('modify', 'module/banner')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }

        return !$this->error;
    }
}
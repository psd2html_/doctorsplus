<?php 
class ControllerModuleProductSortOrdersManufacturers extends Controller {
	private $error = array(); 
     
  	public function index() {
		$this->load->language('module/product_sort_orders_manufacturers');
    	
		$this->document->setTitle($this->language->get('heading_title')); 
		
		$data = array();
		
		$this->load->model('catalog/category');
		$this->load->model('catalog/manufacturer');

		$data['categories'] = $this->model_catalog_category->getCategories(array( 'sort' => 'name'));
		$data['manufacturers'] = $this->model_catalog_manufacturer->getManufacturers(array( 'sort' => 'name'));

		/*echo "<pre>";
		print_r ($data['manufacturers']);
		die();*/

		if (isset($this->request->get['filter_manufacturer'])) {
			$filter_manufacturer = $this->request->get['filter_manufacturer'];
		} else {
			$filter_manufacturer = NULL;
		}

		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'pd.name';
			$sort = 'p2mo.sort_order,p.sort_order';
		}
		
		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}
		
		$url = '';
						
		if (isset($this->request->get['filter_manufacturer'])) {
			$url .= '&filter_manufacturer=' . $this->request->get['filter_manufacturer'];
		}
						
		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

  		$data['breadcrumbs'] = array();

   		$data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => false
   		);

   		$data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('module/product_sort_orders_manufacturers', 'token=' . $this->session->data['token'] . $url, 'SSL'),
      		'separator' => ' :: '
   		);
		
		$data['save'] = $this->url->link('module/product_sort_orders_manufacturers/save', 'token=' . $this->session->data['token'] . $url, 'SSL');
		
		if (isset($this->request->get['filter_manufacturer'])) {
			$data['products'] = array();

			$filter_data = array(
				'filter_manufacturer' => $filter_manufacturer,
				'sort'                => $sort,
				'order'               => $order
			);
			
			$this->load->model('module/product_sort_orders_manufacturers');
			$this->load->model('tool/image');

			$results = $this->model_module_product_sort_orders_manufacturers->getProducts($filter_data);

			foreach ($results as $result) {
				$category =  $this->model_module_product_sort_orders_manufacturers->getProductCategories($result['product_id']);
				
				if ($result['image'] && file_exists(DIR_IMAGE . $result['image'])) {
					$image = $this->model_tool_image->resize($result['image'], 40, 40);
				} else {
					$image = $this->model_tool_image->resize('no_image.jpg', 40, 40);
				}
		
				$special = false;
				
				$product_specials = $this->model_module_product_sort_orders_manufacturers->getProductSpecials($result['product_id']);
				
				foreach ($product_specials  as $product_special) {
					if (($product_special['date_start'] == '0000-00-00' || $product_special['date_start'] > date('Y-m-d')) && ($product_special['date_end'] == '0000-00-00' || $product_special['date_end'] < date('Y-m-d'))) {
						$special = $product_special['price'];
				
						break;
					}					
				}
		
				$data['products'][] = array(
					'product_id' => $result['product_id'],
					'name'       => $result['name'],
					'model'      => $result['model'],
					'price'      => $result['price'],
					'sort_order' => $result['sort_order'],
					'special'    => $special,
					'category'   => $category,
					'image'      => $image,
					'quantity'   => $result['quantity'],
					'status'     => ($result['status'] ? $this->language->get('text_enabled') : $this->language->get('text_disabled')),
					'selected'   => isset($this->request->post['selected']) && in_array($result['product_id'], $this->request->post['selected']),
					'edit'       => $this->url->link('catalog/product/edit', 'token=' . $this->session->data['token'] . '&product_id=' . $result['product_id'] . $url, 'SSL')
				);
			}
		} else {
			$data['products'] = array();
		}
		
		$data['token'] = $this->session->data['token'];
		
 		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];
		
			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		$url = '';

		if (isset($this->request->get['filter_manufacturer'])) {
			$url .= '&filter_manufacturer=' . $this->request->get['filter_manufacturer'];
		}
								
		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		
        $data['sort_category'] = $this->url->link('module/product_sort_orders_manufacturers', 'token=' . $this->session->data['token'] . '&sort=p2c.category' . $url, 'SSL');
		$data['sort_order'] = $this->url->link('module/product_sort_orders_manufacturers', 'token=' . $this->session->data['token'] . '&sort=p2co.sort_order,p.sort_order' . $url, 'SSL');
		
		$data['sort_name'] = $this->url->link('module/product_sort_orders_manufacturers', 'token=' . $this->session->data['token'] . '&sort=pd.name' . $url, 'SSL');
		$data['sort_model'] = $this->url->link('module/product_sort_orders_manufacturers', 'token=' . $this->session->data['token'] . '&sort=p.model' . $url, 'SSL');
		$data['sort_price'] = $this->url->link('module/product_sort_orders_manufacturers', 'token=' . $this->session->data['token'] . '&sort=p.price' . $url, 'SSL');
		$data['sort_quantity'] = $this->url->link('module/product_sort_orders_manufacturers', 'token=' . $this->session->data['token'] . '&sort=p.quantity' . $url, 'SSL');
		$data['sort_status'] = $this->url->link('module/product_sort_orders_manufacturers', 'token=' . $this->session->data['token'] . '&sort=p.status' . $url, 'SSL');
		
		$url = '';

		// Add
        if (isset($this->request->get['filter_manufacturer'])) {
			$url .= '&filter_manufacturer=' . $this->request->get['filter_manufacturer'];
		}
        // End add
		
		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}
												
		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}
		
		$data['heading_title'] = $this->language->get('heading_title');
		
		$data['column_image'] = $this->language->get('column_image');
		$data['column_sort_order'] = $this->language->get('column_sort_order');
		$data['column_name'] = $this->language->get('column_name');	
		$data['column_category'] = $this->language->get('column_category');
		$data['column_manufacturer'] = $this->language->get('column_manufacturer');
		$data['column_model'] = $this->language->get('column_model');
		$data['column_price'] = $this->language->get('column_price');
		$data['column_quantity'] = $this->language->get('column_quantity');
		$data['column_status'] = $this->language->get('column_status');
		$data['column_action'] = $this->language->get('column_action');
		
		$data['text_no_results'] = $this->language->get('text_no_results');
		
		$data['button_save'] = $this->language->get('button_save');
		$data['button_filter'] = $this->language->get('button_filter');
		$data['button_edit'] = $this->language->get('button_edit');
		
		$data['filter_manufacturer'] = $filter_manufacturer;
		
		$data['sort'] = $sort;
		$data['order'] = $order;
		
		$this->load->model('design/layout');

		$data['layouts'] = $this->model_design_layout->getLayouts();

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');
		
		$this->response->setOutput($this->load->view('module/product_sort_orders_manufacturers.tpl', $data));
  	}
	
	public function save() {
		if ($this->request->server['REQUEST_METHOD'] == 'POST') {
			$this->load->model('module/product_sort_orders_manufacturers');
			$this->model_module_product_sort_orders_manufacturers->saveSortOrdersManufacturers($this->request->post);
			
			$this->load->language('module/product_sort_orders_manufacturers');
			$this->session->data['success'] = $this->language->get('save_success');
		}
		$this->index();
  	}
	
	public function install() {
		$this->load->model('module/product_sort_orders_manufacturers');
		$this->model_module_product_sort_orders_manufacturers->install();
		
		$this->load->language('module/product_sort_orders_manufacturers');
		$this->session->data['success'] = $this->language->get('warning_install');
	}
	
	public function uninstall() {
		$this->load->model('module/product_sort_orders_manufacturers');
		$this->model_module_product_sort_orders_manufacturers->uninstall();
		
		$this->load->language('module/product_sort_orders_manufacturers');
		$this->session->data['success'] = $this->language->get('warning_uninstall');
	}
}
?>
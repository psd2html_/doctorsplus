<?php
class ControllerExtensionNews extends Controller {
	private $error = array();
	
	public function index() {
		$this->language->load('extension/news');
		
		$this->load->model('extension/news');
		
		$this->document->setTitle($this->language->get('heading_title'));
		
		$url = '';
		
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		
		$data['breadcrumbs'] = array();

   		$data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL')
		);

   		$data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_category'),
			'href'      => $this->url->link('extension/news', 'token=' . $this->session->data['token'] . $url, 'SSL')
   		);
		
		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];
		
			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		if (isset($this->error['warning'])) {
			$data['error'] = $this->error['warning'];
		
			unset($this->error['warning']);
		} else {
			$data['error'] = '';
		}
		
		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else { 
			$page = 1;
		}
		
		$url = '';
		
		$filter_data = array(
			'page' => $page,
			'limit' => $this->config->get('config_limit_admin'),
			'start' => $this->config->get('config_limit_admin') * ($page - 1),
		);
		
		$total = $this->model_extension_news->getTotalNewsCategory();

		$pagination = new Pagination();
		$pagination->total = $total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_limit_admin');
		$pagination->url = $this->url->link('extension/news', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');

		$data['pagination'] = $pagination->render();

		$data['results'] = sprintf($this->language->get('text_pagination'), ($total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($total - $this->config->get('config_limit_admin'))) ? $total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $total, ceil($total / $this->config->get('config_limit_admin')));

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_title'] = $this->language->get('text_title');
        $data['text_alias'] = $this->language->get('text_alias');
        $data['text_image_title'] = $this->language->get('text_image_title');
        $data['text_period'] = $this->language->get('text_period');
		$data['text_short_description'] = $this->language->get('text_short_description');
        $data['text_meta_description'] = $this->language->get('text_meta_description');
		$data['text_date'] = $this->language->get('text_date');
		$data['text_action'] = $this->language->get('text_action');
		$data['text_edit'] = $this->language->get('text_edit');
		$data['text_list'] = $this->language->get('text_list_category');
		$data['text_no_results'] = $this->language->get('text_no_results');
		$data['text_confirm'] = $this->language->get('text_confirm');
		
		$data['button_add'] = $this->language->get('button_add');
		$data['button_delete'] = $this->language->get('button_delete');
		
		$url = '';
		
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		
		$data['add'] = $this->url->link('extension/news/insertcat', '&token=' . $this->session->data['token'] . $url, 'SSL');
		$data['delete'] = $this->url->link('extension/news/deletecat', 'token=' . $this->session->data['token'] . $url, 'SSL');
		
		$data['all_news'] = array();

		$all_news = $this->model_extension_news->getAllNewsCategory($filter_data);

		foreach ($all_news as $news) {
			$data['all_news'][] = array (
				'news_id' 			=> $news['news_cat_id'],
				'sort_order' 		=> $news['sort_order'],
				'status' 			=> $news['status'],
				'title' 			=> $news['title'],
				'open' 			    => $this->url->link('extension/news/category', 'category_id=' . $news['news_cat_id'] . '&token=' . $this->session->data['token'] . $url, 'SSL'),
				'edit' 				=> $this->url->link('extension/news/editcat', 'news_id=' . $news['news_cat_id'] . '&token=' . $this->session->data['token'] . $url, 'SSL')
			);
		}
		
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('extension/news_cat_list.tpl', $data));
	}

	public function category() {

		$this->language->load('extension/news');

		$this->load->model('extension/news');

		$this->document->setTitle($this->language->get('heading_title'));

		$cat_info = $this->model_extension_news->getIsetCat($this->request->get['category_id']);

		if (!empty($this->request->get['category_id']) && $this->request->get['category_id'] > 0 && $cat_info) {

			$url = '';

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$data['breadcrumbs'] = array();

			$data['breadcrumbs'][] = array(
				'text'      => $this->language->get('text_home'),
				'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL')
			);

			$data['breadcrumbs'][] = array(
				'text'      => $this->language->get('heading_title'),
				'href'      => $this->url->link('extension/news', 'token=' . $this->session->data['token'] . $url, 'SSL')
			);

			$data['breadcrumbs'][] = array(
				'text'      => $cat_info['title'],
				'href'      => $this->url->link('extension/news/category', 'category_id=' . $cat_info['news_cat_id'] . '&token=' . $this->session->data['token'] . $url, 'SSL')
			);

			if (isset($this->session->data['success'])) {
				$data['success'] = $this->session->data['success'];

				unset($this->session->data['success']);
			} else {
				$data['success'] = '';
			}

			if (isset($this->error['warning'])) {
				$data['error'] = $this->error['warning'];

				unset($this->error['warning']);
			} else {
				$data['error'] = '';
			}

			if (isset($this->request->get['page'])) {
				$page = $this->request->get['page'];
			} else {
				$page = 1;
			}

			$url = '';

			$filter_data = array(
				'cat_id' => $cat_info['news_cat_id'],
				'page' => $page,
				'limit' => $this->config->get('config_limit_admin'),
				'start' => $this->config->get('config_limit_admin') * ($page - 1),
			);

			$total = $this->model_extension_news->getTotalNewsCat($cat_info['news_cat_id']);

			$pagination = new Pagination();
			$pagination->total = $total;
			$pagination->page = $page;
			$pagination->limit = $this->config->get('config_limit_admin');
			$pagination->url = $this->url->link('extension/news/category', 'category_id=' . $cat_info['news_cat_id'] . '&token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');

			$data['pagination'] = $pagination->render();

			$data['results'] = sprintf($this->language->get('text_pagination'), ($total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($total - $this->config->get('config_limit_admin'))) ? $total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $total, ceil($total / $this->config->get('config_limit_admin')));

			$data['heading_title'] = $this->language->get('heading_title');

			$data['text_title'] = $this->language->get('text_title');
			$data['text_alias'] = $this->language->get('text_alias');
			$data['text_image_title'] = $this->language->get('text_image_title');
			$data['text_period'] = $this->language->get('text_period');
			$data['text_short_description'] = $this->language->get('text_short_description');
			$data['text_meta_description'] = $this->language->get('text_meta_description');
			$data['text_date'] = $this->language->get('text_date');
			$data['text_action'] = $this->language->get('text_action');
			$data['text_edit'] = $this->language->get('text_edit');
			$data['text_list'] = $this->language->get('text_list');
			$data['text_no_results'] = $this->language->get('text_no_results');
			$data['text_confirm'] = $this->language->get('text_confirm');

			$data['button_add'] = $this->language->get('button_add');
			$data['button_delete'] = $this->language->get('button_delete');

			$data['return'] = $this->url->link('extension/news', 'token=' . $this->session->data['token'] . $url, 'SSL');

			$url = '';

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$data['add'] = $this->url->link('extension/news/insert', '&token=' . $this->session->data['token'] . $url, 'SSL');
			$data['delete'] = $this->url->link('extension/news/delete', 'token=' . $this->session->data['token'] . $url, 'SSL');

			$data['all_news'] = array();

			$all_news = $this->model_extension_news->getAllNews($filter_data);

			foreach ($all_news as $news) {
				$data['all_news'][] = array (
					'news_id' 			=> $news['news_id'],
					'title' 			=> $news['title'],
					'cat_id' 			=> $cat_info['news_cat_id'],
					'short_description'	=> $news['short_description'],
					'date_added' 		=> date($this->language->get('date_format_short'), strtotime($news['date_added'])),
					'edit' 				=> $this->url->link('extension/news/edit', 'news_id=' . $news['news_id'] . '&cat_id=' . $cat_info['news_cat_id'] . '&token=' . $this->session->data['token'] . $url, 'SSL')
				);
			}

			$data['header'] = $this->load->controller('common/header');
			$data['column_left'] = $this->load->controller('common/column_left');
			$data['footer'] = $this->load->controller('common/footer');

			$this->response->setOutput($this->load->view('extension/news_list.tpl', $data));
		}
	}
	
	public function edit() {
		$this->language->load('extension/news');
		
		$this->load->model('extension/news');
		
		$this->document->setTitle($this->language->get('heading_title'));
		
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			$this->model_extension_news->editNews($this->request->get['news_id'], $this->request->post);		
			
			$this->session->data['success'] = $this->language->get('text_success');
						
			$this->response->redirect($this->url->link('extension/news/category', 'category_id=' . $this->request->post['cat_id'] . '&token=' . $this->session->data['token'], 'SSL'));
		}
		
		$this->form();
	}

	public function editcat() {
		$this->language->load('extension/news');

		$this->load->model('extension/news');

		$this->document->setTitle($this->language->get('heading_title'));

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validatecat()) {
			$this->model_extension_news->editNewsCat($this->request->get['news_id'], $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$this->response->redirect($this->url->link('extension/news', 'token=' . $this->session->data['token'], 'SSL'));
		}

		$this->formCat();
	}
	
	public function insert() {
		$this->language->load('extension/news');
		
		$this->load->model('extension/news');
		
		$this->document->setTitle($this->language->get('heading_title'));
		
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			$this->model_extension_news->addNews($this->request->post);		
			
			$this->session->data['success'] = $this->language->get('text_success');
						
			$this->response->redirect($this->url->link('extension/news', 'token=' . $this->session->data['token'], 'SSL'));
		}

		$this->form();
	}

	public function insertcat() {
		$this->language->load('extension/news');

		$this->load->model('extension/news');

		$this->document->setTitle($this->language->get('heading_title'));

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validatecat()) {
			$this->model_extension_news->addNewsCat($this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$this->response->redirect($this->url->link('extension/news', 'token=' . $this->session->data['token'], 'SSL'));
		}

		$this->formCat();
	}
	
	protected function form() {
		$this->language->load('extension/news');
		
		$this->load->model('extension/news');
        $this->load->model('catalog/product');
		
		$data['breadcrumbs'] = array();

   		$data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => false
   		);

   		$data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('extension/news', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => ' :: '
   		);
		
		if (isset($this->request->get['news_id'])) {
			$data['action'] = $this->url->link('extension/news/edit', '&news_id=' . $this->request->get['news_id'] . '&token=' . $this->session->data['token'], 'SSL');
		} else {
			$data['action'] = $this->url->link('extension/news/insert', '&token=' . $this->session->data['token'], 'SSL');
		}
		
		$data['cancel'] = $this->url->link('extension/news/category', 'category_id=' . $this->request->get['cat_id'] . '&token=' . $this->session->data['token'], 'SSL');
		
		$data['heading_title'] = $this->language->get('heading_title');
		
		$data['text_image'] = $this->language->get('text_image');
		$data['text_title'] = $this->language->get('text_title');
        $data['text_alias'] = $this->language->get('text_alias');
        $data['text_image_title'] = $this->language->get('text_image_title');
        $data['text_period'] = $this->language->get('text_period');
		$data['text_description'] = $this->language->get('text_description');
		$data['text_short_description'] = $this->language->get('text_short_description');
        $data['text_meta_description'] = $this->language->get('text_meta_description');
		$data['text_status'] = $this->language->get('text_status');
		$data['text_keyword'] = $this->language->get('text_keyword');
		$data['text_enabled'] = $this->language->get('text_enabled');
		$data['text_disabled'] = $this->language->get('text_disabled');
		$data['text_browse'] = $this->language->get('text_browse');
		$data['text_clear'] = $this->language->get('text_clear');
		$data['text_image_manager'] = $this->language->get('text_image_manager');
		
		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');
		
		$data['token'] = $this->session->data['token'];
		
		$this->load->model('localisation/language');
		
		$data['languages'] = $this->model_localisation_language->getLanguages();
		
		if (isset($this->error['warning'])) {
			$data['error'] = $this->error['warning'];
		} else {
			$data['error'] = '';
		}

        if (isset($this->error['keyword'])) {
            $data['error_keyword'] = $this->error['keyword'];
        } else {
            $data['error_keyword'] = '';
        }

		if (isset($this->error['cat'])) {
			$data['error_cat'] = $this->error['cat'];
		} else {
			$data['error_cat'] = '';
		}

		$data['cat_id'] = 0;
		$data['cat'] = '';
		
		if (isset($this->request->get['news_id'])) {
			$news = $this->model_extension_news->getNews($this->request->get['news_id']);

			$cat_ar = $this->model_extension_news->getNewsDescriptionCat($news['nc_id']);
			if (!empty($cat_ar)) {
				$data['cat_id'] = $news['nc_id'];
				$data['cat'] = $cat_ar[$this->config->get('config_language_id')]['title'];
			}
		} else {
			$news = array();
		}

		if (isset($this->request->post['news'])) {
			$data['news'] = $this->request->post['news'];
		} elseif (!empty($news)) {
			$data['news'] = $this->model_extension_news->getNewsDescription($this->request->get['news_id']);
		} else {
			$data['news'] = '';
		}
		
		if (isset($this->request->post['image'])) {
			$data['image'] = $this->request->post['image'];
		} elseif (!empty($news)) {
			$data['image'] = $news['image'];
		} else {
			$data['image'] = '';
		}

		if (isset($this->request->get['news_id'])) {
			$news = $this->model_extension_news->getNews($this->request->get['news_id']);
			$data['tags'] = $this->model_extension_news->getTagsId($this->request->get['news_id']);
		} else {
			$news = array();
			$data['tags'] = array();
		}
		
		$this->load->model('tool/image');
		
		if (isset($this->request->post['image'])) {
			$data['thumb'] = $this->model_tool_image->resize($this->request->post['image'], 100, 100);
		} elseif (!empty($news)) {
			$data['thumb'] = $this->model_tool_image->resize($news['image'] ? $news['image'] : 'no_image.png', 100, 100);
		} else {
			$data['thumb'] = $this->model_tool_image->resize('no_image.png', 100, 100);;
		}
		
		$data['no_image'] = $this->model_tool_image->resize('no_image.png', 100, 100);
		
		if (isset($this->request->post['keyword'])) {
			$data['keyword'] = $this->request->post['keyword'];
		} elseif (!empty($news)) {
			$data['keyword'] = $news['keyword'];
		} else {
			$data['keyword'] = '';
		}
		
		if (isset($this->request->post['status'])) {
			$data['status'] = $this->request->post['status'];
		} elseif (!empty($news)) {
			$data['status'] = $news['status'];
		} else {
			$data['status'] = '';
		}

        if (isset($this->request->post['product_related'])) {
            $products = $this->request->post['product_related'];
        } elseif (isset($this->request->get['news_id'])) {
            $products = $this->model_catalog_product->getProductAction($this->request->get['news_id']);
        } else {
            $products = array();
        }

        $data['product_relateds'] = array();

        foreach ($products as $product_id) {
            $related_info = $this->model_catalog_product->getProduct($product_id);

            if ($related_info) {
                $data['product_relateds'][] = array(
                    'product_id' => $related_info['product_id'],
                    'name'       => $related_info['name']
                );
            }
        }

		
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('extension/news_form.tpl', $data));
	}

	public function autocomplete() {
		$json = array();

		if (isset($this->request->get['filter_name'])) {
			$this->load->model('extension/news');

			$filter_data = array(
				'filter_name' => $this->request->get['filter_name'],
				'sort'        => 'title',
				'order'       => 'ASC',
				'start'       => 0,
				'limit'       => 5
			);

			$results = $this->model_extension_news->getCategoriesFilter($filter_data);

			foreach ($results as $result) {
				$json[] = array(
					'category_id' => $result['news_cat_id'],
					'name'        => strip_tags(html_entity_decode($result['title'], ENT_QUOTES, 'UTF-8'))
				);
			}
		}

		$sort_order = array();

		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['name'];
		}

		array_multisort($sort_order, SORT_ASC, $json);

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	protected function formCat() {
		$this->language->load('extension/news');

		$this->load->model('extension/news');
		$this->load->model('catalog/product');

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => false
		);

		$data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_add_category'),
			'href'      => $this->url->link('extension/news', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' :: '
		);

		if (isset($this->request->get['news_id'])) {
			$data['action'] = $this->url->link('extension/news/editcat', '&news_id=' . $this->request->get['news_id'] . '&token=' . $this->session->data['token'], 'SSL');
		} else {
			$data['action'] = $this->url->link('extension/news/insertcat', '&token=' . $this->session->data['token'], 'SSL');
		}

		$data['cancel'] = $this->url->link('extension/news', '&token=' . $this->session->data['token'], 'SSL');

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_image'] = $this->language->get('text_image');
		$data['text_title'] = $this->language->get('text_title');
		$data['text_alias'] = $this->language->get('text_alias');
		$data['text_image_title'] = $this->language->get('text_image_title');
		$data['text_period'] = $this->language->get('text_period');
		$data['text_description'] = $this->language->get('text_description');
		$data['text_short_description'] = $this->language->get('text_short_description');
		$data['text_meta_description'] = $this->language->get('text_meta_description');
		$data['text_status'] = $this->language->get('text_status');
		$data['text_keyword'] = $this->language->get('text_keyword');
		$data['text_enabled'] = $this->language->get('text_enabled');
		$data['text_disabled'] = $this->language->get('text_disabled');
		$data['text_browse'] = $this->language->get('text_browse');
		$data['text_clear'] = $this->language->get('text_clear');
		$data['text_image_manager'] = $this->language->get('text_image_manager');

		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');

		$data['token'] = $this->session->data['token'];

		$this->load->model('localisation/language');

		$data['languages'] = $this->model_localisation_language->getLanguages();

		if (isset($this->error['warning'])) {
			$data['error'] = $this->error['warning'];
		} else {
			$data['error'] = '';
		}

		if (isset($this->error['keyword'])) {
			$data['error_keyword'] = $this->error['keyword'];
		} else {
			$data['error_keyword'] = '';
		}

		if (isset($this->error['title'])) {
			$data['error_title'] = $this->error['title'];
		} else {
			$data['error_title'] = '';
		}

		if (isset($this->request->get['news_id'])) {
			$news = $this->model_extension_news->getNewsCat($this->request->get['news_id']);
		} else {
			$news = array();
		}

		if (isset($this->request->post['news'])) {
			$data['news'] = $this->request->post['news'];
		} elseif (!empty($news)) {
			$data['news'] = $this->model_extension_news->getNewsDescriptionCat($this->request->get['news_id']);
		} else {
			$data['news'] = '';
		}

		if (isset($this->request->post['keyword'])) {
			$data['keyword'] = $this->request->post['keyword'];
		} elseif (!empty($news)) {
			$data['keyword'] = $news['keyword'];
		} else {
			$data['keyword'] = '';
		}

		if (isset($this->request->post['sort_order'])) {
			$data['sort_order'] = $this->request->post['sort_order'];
		} elseif (!empty($news)) {
			$data['sort_order'] = $news['sort_order'];
		} else {
			$data['sort_order'] = '';
		}

		if (isset($this->request->post['status'])) {
			$data['status'] = $this->request->post['status'];
		} elseif (!empty($news)) {
			$data['status'] = $news['status'];
		} else {
			$data['status'] = '';
		}

		$this->load->model('tool/image');

		if (isset($this->request->post['image'])) {
			$data['thumb'] = $this->model_tool_image->resize($this->request->post['image'], 100, 100);
		} elseif (!empty($news)) {
			$data['thumb'] = $this->model_tool_image->resize($news['image'] ? $news['image'] : 'no_image.png', 100, 100);
		} else {
			$data['thumb'] = $this->model_tool_image->resize('no_image.png', 100, 100);;
		}

		$data['no_image'] = $this->model_tool_image->resize('no_image.png', 100, 100);

		if (isset($this->request->post['keyword'])) {
			$data['keyword'] = $this->request->post['keyword'];
		} elseif (!empty($news)) {
			$data['keyword'] = $news['keyword'];
		} else {
			$data['keyword'] = '';
		}

		if (isset($this->request->post['status'])) {
			$data['status'] = $this->request->post['status'];
		} elseif (!empty($news)) {
			$data['status'] = $news['status'];
		} else {
			$data['status'] = '';
		}

		if (isset($this->request->post['product_related'])) {
			$products = $this->request->post['product_related'];
		} elseif (isset($this->request->get['news_id'])) {
			$products = $this->model_catalog_product->getProductAction($this->request->get['news_id']);
		} else {
			$products = array();
		}

		$data['product_relateds'] = array();

		foreach ($products as $product_id) {
			$related_info = $this->model_catalog_product->getProduct($product_id);

			if ($related_info) {
				$data['product_relateds'][] = array(
					'product_id' => $related_info['product_id'],
					'name'       => $related_info['name']
				);
			}
		}


		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('extension/news_cat_form.tpl', $data));
	}
	
	public function delete() {
		$this->language->load('extension/news');
		
		$this->load->model('extension/news');

		$this->document->setTitle($this->language->get('heading_title'));
		
		if (isset($this->request->post['selected']) && $this->validateDelete()) {
			foreach ($this->request->post['selected'] as $news_id) {
				$this->model_extension_news->deleteNews($news_id);
			}

			$this->session->data['success'] = $this->language->get('text_success');
		}
		
		$this->response->redirect($this->url->link('extension/news', 'token=' . $this->session->data['token'], 'SSL'));
	}

	public function deletecat() {
		$this->language->load('extension/news');

		$this->load->model('extension/news');

		$this->document->setTitle($this->language->get('heading_title'));

		if (isset($this->request->post['selected']) && $this->validateDelete()) {
			foreach ($this->request->post['selected'] as $news_id) {
				$this->model_extension_news->deleteNewsCat($news_id);
			}

			$this->session->data['success'] = $this->language->get('text_success');
		}

		$this->response->redirect($this->url->link('extension/news', 'token=' . $this->session->data['token'], 'SSL'));
	}
	
	protected function validateDelete() {
		if (!$this->user->hasPermission('modify', 'extension/news')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
 
		if (!$this->error) {
			return true; 
		} else {
			return false;
		}
	}
	
	protected function validate() {
		if (!$this->user->hasPermission('modify', 'extension/news')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

        $title = '';
		foreach ($this->request->post['news'] as $language_id => $news) {
			if (!empty($news['title'])) {
				if ((utf8_strlen($news['title']) < 3) || (utf8_strlen($news['title']) > 255)) {
					$this->error['title'][$language_id] = 'Название должно быть больше 3, но меньше 255 символов.';
				} else {
					$title = $news['title'];
				}
			} else {
				$this->error['title'][$language_id] = 'Название не должно быть пустое';
			}
		}

        if(empty($this->request->post['keyword']) && !empty($title)) {
            $this->request->post['keyword'] = $this->url->alias($title);
        }

        $this->load->model('extension/news');

        $url_alias_info = $this->model_extension_news->getUrlAlias($this->request->post['keyword']);

		if (!empty($this->request->post['keyword'])) {
			if (!empty($url_alias_info)) {
				if (!isset($this->request->get['news_id']) || (!empty($this->request->get['news_id']) && ('news_id=' . $this->request->get['news_id']) != $url_alias_info['query'])) {
					$this->error['keyword'] = 'Такой URL уже существует, измените пожалуйста.';
				}
			}
		} else {
			$this->error['keyword'] = 'URL должен быть заполненный.';
		}

		if ((int)$this->request->post['cat_id'] < 1 ) {
			$this->error['cat'] = 'Вы забыли выбрать категорию.';
		}
		
		if (!$this->error) {
			return true;
		} else {
			return false;
		}	
	}

	protected function validatecat() {
		if (!$this->user->hasPermission('modify', 'extension/news')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		$title = '';
		foreach ($this->request->post['news'] as $language_id => $news) {
			if (!empty($news['title'])) {
				if ((utf8_strlen($news['title']) < 3) || (utf8_strlen($news['title']) > 255)) {
					$this->error['title'][$language_id] = 'Название должно быть больше 3, но меньше 255 символов.';
				} else {
					$title = $news['title'];
				}
			} else {
				$this->error['title'][$language_id] = 'Название не должно быть пустое';
			}
		}

		if(empty($this->request->post['keyword']) && !empty($title)) {
			$this->request->post['keyword'] = $this->url->alias($title);
		}

		$this->load->model('extension/news');

		$url_alias_info = $this->model_extension_news->getUrlAlias($this->request->post['keyword']);

		if (!empty($this->request->post['keyword']))
		{
			if (!empty($url_alias_info)) {
				if (!isset($this->request->get['news_id']) || (!empty($this->request->get['news_id']) && ('news_cat_id=' . $this->request->get['news_id']) != $url_alias_info['query'])) {
					$this->error['keyword'] = 'Такой URL уже существует, измените пожалуйста.';
				}
			}
		} else {
			$this->error['keyword'] = 'URL должен быть заполненный.';
		}


		if (!$this->error) {
			return true;
		} else {
			return false;
		}
	}
}
<?php
class ControllerExtensionAction extends Controller {
    private $error = array();

    public function index() {
        $this->language->load('extension/action');

        $this->load->model('extension/action');

        $this->document->setTitle($this->language->get('heading_title'));

        $url = '';

        if (isset($this->request->get['page'])) {
            $url .= '&page=' . $this->request->get['page'];
        }

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text'      => $this->language->get('text_home'),
            'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL')
        );

        $data['breadcrumbs'][] = array(
            'text'      => $this->language->get('heading_title'),
            'href'      => $this->url->link('extension/action', 'token=' . $this->session->data['token'] . $url, 'SSL')
        );

        if (isset($this->session->data['success'])) {
            $data['success'] = $this->session->data['success'];

            unset($this->session->data['success']);
        } else {
            $data['success'] = '';
        }

        if (isset($this->error['warning'])) {
            $data['error'] = $this->error['warning'];

            unset($this->error['warning']);
        } else {
            $data['error'] = '';
        }

        if (isset($this->request->get['page'])) {
            $page = $this->request->get['page'];
        } else {
            $page = 1;
        }

        $url = '';

        $filter_data = array(
            'page' => $page,
            'limit' => $this->config->get('config_limit_admin'),
            'start' => $this->config->get('config_limit_admin') * ($page - 1),
        );

        $total = $this->model_extension_action->getTotalAction();

        $pagination = new Pagination();
        $pagination->total = $total;
        $pagination->page = $page;
        $pagination->limit = $this->config->get('config_limit_admin');
        $pagination->url = $this->url->link('catalog/product', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');

        $data['pagination'] = $pagination->render();

        $data['results'] = sprintf($this->language->get('text_pagination'), ($total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($total - $this->config->get('config_limit_admin'))) ? $total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $total, ceil($total / $this->config->get('config_limit_admin')));

        $data['heading_title'] = $this->language->get('heading_title');

        $data['text_title'] = $this->language->get('text_title');
        $data['text_alias'] = $this->language->get('text_alias');
        $data['text_image_title'] = $this->language->get('text_image_title');
        $data['text_period'] = $this->language->get('text_period');
        $data['text_short_description'] = $this->language->get('text_short_description');
        $data['text_meta_description'] = $this->language->get('text_meta_description');
        $data['text_date'] = $this->language->get('text_date');
        $data['text_action'] = $this->language->get('text_action');
        $data['text_edit'] = $this->language->get('text_edit');
        $data['text_list'] = $this->language->get('text_list');
        $data['text_no_results'] = $this->language->get('text_no_results');
        $data['text_confirm'] = $this->language->get('text_confirm');

        $data['button_add'] = $this->language->get('button_add');
        $data['button_delete'] = $this->language->get('button_delete');

        $url = '';

        if (isset($this->request->get['page'])) {
            $url .= '&page=' . $this->request->get['page'];
        }

        $data['add'] = $this->url->link('extension/action/insert', '&token=' . $this->session->data['token'] . $url, 'SSL');
        $data['delete'] = $this->url->link('extension/action/delete', 'token=' . $this->session->data['token'] . $url, 'SSL');

        $data['all_action'] = array();

        $all_action = $this->model_extension_action->getAllAction($filter_data);

        foreach ($all_action as $action) {
            $data['all_action'][] = array (
                'action_id' 			=> $action['action_id'],
                'title' 			=> $action['title'],
                'short_description'	=> $action['short_description'],
                'date_added' 		=> date($this->language->get('date_format_short'), strtotime($action['date_added'])),
                'edit' 				=> $this->url->link('extension/action/edit', 'action_id=' . $action['action_id'] . '&token=' . $this->session->data['token'] . $url, 'SSL')
            );
        }

        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');

        $this->response->setOutput($this->load->view('extension/action_list.tpl', $data));
    }

    public function edit() {
        $this->language->load('extension/action');

        $this->load->model('extension/action');

        $this->document->setTitle($this->language->get('heading_title'));

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
            $this->model_extension_action->editAction($this->request->get['action_id'], $this->request->post);

            $this->session->data['success'] = $this->language->get('text_success');

            $this->response->redirect($this->url->link('extension/action', 'token=' . $this->session->data['token'], 'SSL'));
        }

        $this->form();
    }

    public function insert() {
        $this->language->load('extension/action');

        $this->load->model('extension/action');

        $this->document->setTitle($this->language->get('heading_title'));

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
            $this->model_extension_action->addAction($this->request->post);

            $this->session->data['success'] = $this->language->get('text_success');

            $this->response->redirect($this->url->link('extension/action', 'token=' . $this->session->data['token'], 'SSL'));
        }

        $this->form();
    }

    protected function form() {
        $this->language->load('extension/action');

        $this->load->model('extension/action');
        $this->load->model('catalog/product');

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text'      => $this->language->get('text_home'),
            'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
            'separator' => false
        );

        $data['breadcrumbs'][] = array(
            'text'      => $this->language->get('heading_title'),
            'href'      => $this->url->link('extension/action', 'token=' . $this->session->data['token'], 'SSL'),
            'separator' => ' :: '
        );

        $data['actions'] = $this->url->link('extension/action/insert', '&token=' . $this->session->data['token'], 'SSL');

        if (isset($this->request->get['action_id'])) {
            $data['actions'] = $this->url->link('extension/action/edit', '&action_id=' . $this->request->get['action_id'] . '&token=' . $this->session->data['token'], 'SSL');
        }

        $data['cancel'] = $this->url->link('extension/action', '&token=' . $this->session->data['token'], 'SSL');

        $data['heading_title'] = $this->language->get('heading_title');

        $data['text_image'] = $this->language->get('text_image');
        $data['text_title'] = $this->language->get('text_title');
        $data['text_alias'] = $this->language->get('text_alias');
        $data['text_discount'] = $this->language->get('text_discount');
        $data['text_image_title'] = $this->language->get('text_image_title');
        $data['text_period'] = $this->language->get('text_period');
        $data['text_description'] = $this->language->get('text_description');
        $data['text_short_description'] = $this->language->get('text_short_description');
        $data['text_meta_description'] = $this->language->get('text_meta_description');
        $data['text_status'] = $this->language->get('text_status');
        $data['text_keyword'] = $this->language->get('text_keyword');
        $data['text_enabled'] = $this->language->get('text_enabled');
        $data['text_disabled'] = $this->language->get('text_disabled');
        $data['text_browse'] = $this->language->get('text_browse');
        $data['text_clear'] = $this->language->get('text_clear');
        $data['text_image_manager'] = $this->language->get('text_image_manager');

        $data['button_save'] = $this->language->get('button_save');
        $data['button_cancel'] = $this->language->get('button_cancel');

        $data['token'] = $this->session->data['token'];

        $this->load->model('localisation/language');

        $data['languages'] = $this->model_localisation_language->getLanguages();

        if (isset($this->error['warning'])) {
            $data['error'] = $this->error['warning'];
        } else {
            $data['error'] = '';
        }

        if (isset($this->error['keyword'])) {
            $data['error_keyword'] = $this->error['keyword'];
        } else {
            $data['error_keyword'] = '';
        }

        if (isset($this->error['url'])) {
            $data['error_url'] = $this->error['url'];
        } else {
            $data['error_url'] = '';
        }

        if (isset($this->request->get['action_id'])) {
            $action = $this->model_extension_action->getAction($this->request->get['action_id']);
        } else {
            $action = array();
        }

        if (isset($this->request->post['action'])) {
            $data['action'] = $this->request->post['action'];
        } elseif (!empty($action)) {
            $data['action'] = $this->model_extension_action->getActionDescription($this->request->get['action_id']);
        } else {
            $data['action'] = '';
        }

        if (isset($this->request->post['image'])) {
            $data['image'] = $this->request->post['image'];
        } elseif (!empty($action)) {
            $data['image'] = $action['image'];
        } else {
            $data['image'] = '';
        }

        if (isset($this->request->post['discount'])) {
            $data['discount'] = $this->request->post['discount'];
        } elseif (!empty($action)) {
            $data['discount'] = $action['discount'];
        } else {
            $data['discount'] = '';
        }

        if (isset($this->request->post['url'])) {
            $data['url'] = $this->request->post['url'];
        } elseif (!empty($action)) {
            $data['url'] = $action['url'];
        } else {
            $data['url'] = '';
        }

        $this->load->model('tool/image');

        if (isset($this->request->post['image'])) {
            $data['thumb'] = $this->model_tool_image->resize($this->request->post['image'], 100, 100);
        } elseif (!empty($action)) {
            $data['thumb'] = $this->model_tool_image->resize($action['image'] ? $action['image'] : 'no_image.png', 100, 100);
        } else {
            $data['thumb'] = $this->model_tool_image->resize('no_image.png', 100, 100);;
        }

        $data['no_image'] = $this->model_tool_image->resize('no_image.png', 100, 100);

        if (isset($this->request->post['keyword'])) {
            $data['keyword'] = $this->request->post['keyword'];
        } elseif (!empty($action)) {
            $data['keyword'] = $action['keyword'];
        } else {
            $data['keyword'] = '';
        }

        if (isset($this->request->post['status'])) {
            $data['status'] = $this->request->post['status'];
        } elseif (!empty($action)) {
            $data['status'] = $action['status'];
        } else {
            $data['status'] = '';
        }

        if (isset($this->request->post['product_related'])) {
            $products = $this->request->post['product_related'];
        } elseif (isset($this->request->get['action_id'])) {
            $products = $this->model_catalog_product->getProductAction($this->request->get['action_id']);
        } else {
            $products = array();
        }

        $data['product_relateds'] = array();

        foreach ($products as $product_id) {
            $related_info = $this->model_catalog_product->getProduct($product_id);

            if ($related_info) {
                $data['product_relateds'][] = array(
                    'product_id' => $related_info['product_id'],
                    'name'       => $related_info['name']
                );
            }
        }


        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');

        $this->response->setOutput($this->load->view('extension/action_form.tpl', $data));
    }

    public function delete() {
        $this->language->load('extension/action');

        $this->load->model('extension/action');

        $this->document->setTitle($this->language->get('heading_title'));

        if (isset($this->request->post['selected']) && $this->validateDelete()) {
            foreach ($this->request->post['selected'] as $action_id) {
                $this->model_extension_action->deleteAction($action_id);
            }

            $this->session->data['success'] = $this->language->get('text_success');
        }

        $this->response->redirect($this->url->link('extension/action', 'token=' . $this->session->data['token'], 'SSL'));
    }

    protected function validateDelete() {
        if (!$this->user->hasPermission('modify', 'extension/action')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }

        if (!$this->error) {
            return true;
        } else {
            return false;
        }
    }

    protected function validate() {
        if (!$this->user->hasPermission('modify', 'extension/action')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }

        $title = '';
        foreach ($this->request->post['action'] as $action) {
            if (!empty($action['title'])) {
                $title = $action['title'];
            }
        }

        if(empty($this->request->post['keyword']) && !empty($title)) {
            $this->request->post['keyword'] = $this->url->alias($title);
        }

        $this->load->model('extension/action');

        $url_alias_info = $this->model_extension_action->getUrlAlias( $this->request->post['keyword']);

        if (!empty($url_alias_info)) {
            if (!isset($this->request->get['action_id']) || (!empty($this->request->get['action_id']) && ('action_id=' . $this->request->get['action_id']) != $url_alias_info['query'])) {
                $this->error['keyword'] = 'Такой URL уже существует, измените пожалуйста.';
            }
        }

        if (utf8_strlen($this->request->post['url']) > 255) {
            $this->error['url'] = 'URL должен содержать не более 255 символов';
        }

        if (!$this->error) {
            return true;
        } else {
            return false;
        }
    }
}